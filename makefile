# makefile for my1codelib - my reusable code collection

# find all c source files
SOURCES = $(subst .c,,$(subst src/,,$(sort $(wildcard src/*.c))))
# modules are sources with custom makefiles in recipe/
MODULES = $(subst recipe/makefile_,,$(sort $(wildcard recipe/makefile_*)))
# singles are independent single-file sources (without a recipe)
SINGLES = $(filter-out $(MODULES),$(SOURCES))

# special needs
NEED_MATH = complex1 num trig vector vector_ft vect0r vect0r_ft
NEED_PTHR = thread
NEED_WSCK = sockbase
NEED_RTCK = tick

# extra compile flags for singles
ifneq ($(DOFLAG),)
	CFLAGS += $(DOFLAG)
endif
LFLAGS += $(LDFLAGS)
ifneq ($(DOLINK),)
	LFLAGS += $(DOLINK)
endif

MODLIST = $(foreach a, $(SINGLES), $(subst my1,,$a))
MODLIST += | $(foreach a, $(MODULES), $(subst my1,,$a))

.PHONY: dummy tests clean

dummy:
	@echo "Run 'make <module>' OR 'make tests'"
	@echo "  <module> = { $(MODLIST) }"

$(NEED_MATH): LFLAGS += -lm
$(NEED_PTHR): LFLAGS += -lpthread
$(NEED_RTCK): LFLAGS += -lrt
ifeq ($(OS),Windows_NT)
$(NEED_WSCK): LFLAGS += -lws2_32
endif

# build rule for modules
%: recipe/makefile_my1%
	@make --no-print-directory -f recipe/makefile_my1$@

# build rule for singles
%: CFLAGS +=  -Wall -DTEST_MODULE -DTEST_$(shell echo $@|tr a-z A-Z)
%: src/my1%.c src/my1%.h
	gcc $(CFLAGS) -o test_my1$@ $< $(LFLAGS) $(OFLAGS)

tests: clean $(MODLIST)

clean:
	-rm -rf test_my1* *.o
