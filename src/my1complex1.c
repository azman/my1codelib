/*----------------------------------------------------------------------------*/
#include "my1complex1.h"
/*----------------------------------------------------------------------------*/
#include <math.h>
/*----------------------------------------------------------------------------*/
void complex_zero(my1complex_t* data) {
	data->real = 0.0;
	data->imag = 0.0;
}
/*----------------------------------------------------------------------------*/
void complex_real(my1complex_t* data, double real) {
	data->real = real;
	data->imag = 0.0;
}
/*----------------------------------------------------------------------------*/
void complex_imag(my1complex_t* data, double imag) {
	data->real = 0.0;
	data->imag = imag;
}
/*----------------------------------------------------------------------------*/
void complex_full(my1complex_t* data, double real, double imag) {
	data->real = real;
	data->imag = imag;
}
/*----------------------------------------------------------------------------*/
void complex_zero_threshold(my1complex_t* data, double threshold) {
	zero_threshold(data->real,threshold);
	zero_threshold(data->imag,threshold);
}
/*----------------------------------------------------------------------------*/
void complex_zero_check(my1complex_t* data) {
	complex_zero_threshold(data,DO_PRECISION);
}
/*----------------------------------------------------------------------------*/
void complex_conjugate(my1complex_t* data) {
	data->imag = -data->imag;
}
/*----------------------------------------------------------------------------*/
double complex_magnitude(my1complex_t* data) {
	return sqrt(data->real*data->real+data->imag*data->imag);
}
/*----------------------------------------------------------------------------*/
double complex_phase(my1complex_t* data) {
	return atan2(data->imag,data->real);
}
/*----------------------------------------------------------------------------*/
void complex_from_exponent(my1complex_t* data,double eval) {
	data->real = cos(eval);
	data->imag = sin(eval);
}
/*----------------------------------------------------------------------------*/
void complex_make_polar(my1complex_t* data) {
	double temp;
	temp = data->real;
	data->real = sqrt(temp*temp+data->imag*data->imag);
	data->imag = atan2(data->imag,temp);
}
/*----------------------------------------------------------------------------*/
void complex_make_cartesian(my1complex_t* data) {
	double temp;
	temp = data->real;
	data->real = temp*cos(data->imag);
	data->imag = temp*sin(data->imag);
}
/*----------------------------------------------------------------------------*/
void complex_math_add(my1complex_t* data,
		my1complex_t* num1, my1complex_t* num2) {
	data->real = num1->real + num2->real;
	data->imag = num1->imag + num2->imag;
}
/*----------------------------------------------------------------------------*/
void complex_math_sub(my1complex_t* data,
		my1complex_t* num1, my1complex_t* num2) {
	data->real = num1->real - num2->real;
	data->imag = num1->imag - num2->imag;
}
/*----------------------------------------------------------------------------*/
void complex_math_mul(my1complex_t* data,
		my1complex_t* num1, my1complex_t* num2) {
	data->real = num1->real * num2->real - num1->imag * num2->imag;
	data->imag = num1->imag * num2->real + num1->real * num2->imag;
}
/*----------------------------------------------------------------------------*/
void complex_math_div(my1complex_t* data,
		my1complex_t* num1, my1complex_t* num2) {
	double temp = num2->real * num2->real + num2->imag * num2->imag;
	data->real = num1->real * num2->real + num1->imag * num2->imag;
	data->imag = num1->imag * num2->real - num1->real * num2->imag;
	data->real /= temp;
	data->imag /= temp;
}
/*----------------------------------------------------------------------------*/
#if defined(TEST_MODULE) && defined(TEST_COMPLEX1)
/*----------------------------------------------------------------------------*/
/**
 * Simple test code for complex1 module
 * - run basic math op and check against results in standard C library
 * - requires DOLINK=-lm
**/
/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <complex.h>
/*----------------------------------------------------------------------------*/
typedef double _Complex complex_t;
/*----------------------------------------------------------------------------*/
void print_complex(my1complex_t* num, char* msg) {
	if (msg) printf("%s",msg);
	if (!num->imag) printf("%g",num->real);
	else printf("(%g,%gi)[%g]",num->real,num->imag,complex_magnitude(num));
}
/*----------------------------------------------------------------------------*/
int complex_from_str(my1complex_t* num, char* str) {
	double real, imag;
	char* tmp = 0x0;
	int ecnt = 0, test = 0;
	/* str should be comma-separated complex notation: a,b <= a+bi  */
	while (str[test]) {
		if (str[test]==',')
			break;
		test++;
	}
	do {
		if (!str[test]) {
			/* only real */
			real = strtod(str,&tmp);
			if (!tmp) {
				fprintf(stderr,"** Invalid conversion (%s)!\n",str);
				ecnt++;
				break;
			}
			complex_full(num,real,0.0);
		}
		else {
			str[test] = 0x0;
			real = strtod(str,&tmp);
			if (!tmp) {
				fprintf(stderr,"** Invalid conversion (%s)!\n",str);
				ecnt++;
				break;
			}
			str = &str[test+1];
			imag = strtod(str,&tmp);
			if (!tmp) {
				fprintf(stderr,"** Invalid conversion (%s)!\n",str);
				ecnt++;
				break;
			}
			complex_full(num,real,imag);
		}
	} while (0);
	return ecnt;
}
/*----------------------------------------------------------------------------*/
int complex_test_exp(my1complex_t* num, char* str) {
	complex_t test;
	double what;
	char* tmp = 0x0;
	int ecnt = 0;
	do {
		what = strtod(str,&tmp);
		if (!tmp) {
			fprintf(stderr,"** Invalid conversion (%s)!\n",str);
			ecnt++;
			break;
		}
		complex_from_exponent(num,what);
		test = cexp(what*I);
		what = fabs(creal(test)-num->real);
		if (what>DO_PRECISION) {
			fprintf(stderr,"** Error real diff (%lf|%lf)!\n",
				creal(test),num->real);
			ecnt++;
		}
		what = fabs(cimag(test)-num->imag);
		if (what>DO_PRECISION) {
			fprintf(stderr,"** Error imag diff (%lf|%lf)!\n",
				cimag(test),num->imag);
			ecnt++;
		}
	} while (0);
	return ecnt;
}
/*----------------------------------------------------------------------------*/
void complex_make_self(my1complex_t* num, complex_t* chk) {
	*chk = num->real + num->imag * I;
}
/*----------------------------------------------------------------------------*/
void complex_from_self(my1complex_t* num, complex_t* chk) {
	num->real = creal(*chk);
	num->imag = cimag(*chk);
}
/*----------------------------------------------------------------------------*/
int complex_compare(my1complex_t* my1, complex_t* std, char* txt) {
	int ecnt = 0;
	if (my1->real!=creal(*std)) {
		fprintf(stderr,"** Error real %s (%e|%e)[diff:%e]!\n",txt,
			creal(*std),my1->real,fabs(creal(*std)-my1->real));
		ecnt++;
	}
	if (my1->imag!=cimag(*std)) {
		fprintf(stderr,"** Error imag %s (%e|%e)[diff:%e]!\n",txt,
			cimag(*std),my1->imag,fabs(cimag(*std)-my1->imag));
		ecnt++;
	}
	return ecnt;
}
/*----------------------------------------------------------------------------*/
int complex_test_add(my1complex_t* num1, my1complex_t* num2,
		my1complex_t* num3) {
	complex_t chk1,chk2,chk3;
	complex_make_self(num1,&chk1);
	complex_make_self(num2,&chk2);
	chk3 = chk1 + chk2;
	complex_math_add(num3,num1,num2);
	return complex_compare(num3,&chk3,"add");
}
/*----------------------------------------------------------------------------*/
int complex_test_sub(my1complex_t* num1, my1complex_t* num2,
		my1complex_t* num3) {
	complex_t chk1,chk2,chk3;
	complex_make_self(num1,&chk1);
	complex_make_self(num2,&chk2);
	chk3 = chk1 - chk2;
	complex_math_sub(num3,num1,num2);
	return complex_compare(num3,&chk3,"sub");
}
/*----------------------------------------------------------------------------*/
int complex_test_mul(my1complex_t* num1, my1complex_t* num2,
		my1complex_t* num3) {
	complex_t chk1,chk2,chk3;
	complex_make_self(num1,&chk1);
	complex_make_self(num2,&chk2);
	chk3 = chk1 * chk2;
	complex_math_mul(num3,num1,num2);
	return complex_compare(num3,&chk3,"mul");
}
/*----------------------------------------------------------------------------*/
int complex_test_div(my1complex_t* num1, my1complex_t* num2,
		my1complex_t* num3) {
	complex_t chk1,chk2,chk3;
	complex_make_self(num1,&chk1);
	complex_make_self(num2,&chk2);
	chk3 = chk1 / chk2;
	complex_math_div(num3,num1,num2);
	return complex_compare(num3,&chk3,"div");
}
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	int loop, ecnt, test;
	double step, val1, val2;
	my1complex_t chk1, chk2, chk3;
	/* print tool info */
	printf("\nTest program for my1complex_t data\n");
	printf("  => by azman@my1matrix.org\n\n");
	/* check args */
	if (argc<2) {
		printf("-- Running default test\n");
		val1 = -2.0; val2 = 2.0; step = 0.0000001; loop = 0;
		while (val1<=2.0f) {
			loop++;
			complex_full(&chk1,val1,val2);
			complex_full(&chk2,-val2,-val1);
			ecnt += complex_test_add(&chk1,&chk2,&chk3);
			ecnt += complex_test_sub(&chk1,&chk2,&chk3);
			ecnt += complex_test_mul(&chk1,&chk2,&chk3);
			ecnt += complex_test_div(&chk1,&chk2,&chk3);
			if (ecnt>100)
				break;
			val1 += step;
			val2 -= step;
		}
		printf("@@ Iterations(s): %d {step:%e}\n",loop,step);
	}
	else {
		for (loop=1,ecnt=0;loop<argc;loop++) {
			if (!strncmp(argv[loop],"--zero1",8))
				complex_zero(&chk1);
			else if (!strncmp(argv[loop],"--zero2",8))
				complex_zero(&chk2);
			else if (!strncmp(argv[loop],"--init1=",8)) {
				test = complex_from_str(&chk1,&argv[loop][8]);
				if (!test) {
					complex_zero_check(&chk1);
					print_complex(&chk1,"-- VAL1 = ");
					printf("\n");
				}
				ecnt += test;
			}
			else if (!strncmp(argv[loop],"--init2=",8)) {
				test = complex_from_str(&chk2,&argv[loop][8]);
				if (!test) {
					complex_zero_check(&chk2);
					print_complex(&chk2,"-- VAL2 = ");
					printf("\n");
				}
				ecnt += test;
			}
			else if (!strncmp(argv[loop],"--exp1=",7)) {
				test = complex_test_exp(&chk1,&argv[loop][7]);
				if (!test) {
					complex_zero_check(&chk1);
					print_complex(&chk1,"-- VAL1 = e^(ix) = ");
					printf("\n");
				}
				ecnt += test;
			}
			else if (!strncmp(argv[loop],"--exp2=",7)) {
				test = complex_test_exp(&chk2,&argv[loop][7]);
				if (!test) {
					complex_zero_check(&chk2);
					print_complex(&chk2,"-- VAL2 = e^(ix) = ");
					printf("\n");
				}
				ecnt += test;
			}
			else if (!strncmp(argv[loop],"--add",6)) {
				complex_math_add(&chk3,&chk1,&chk2);
				complex_zero_check(&chk3);
				print_complex(&chk3,"-- VAL1 + VAL2 = ");
				printf("\n");
			}
			else if (!strncmp(argv[loop],"--sub",6)) {
				complex_math_sub(&chk3,&chk1,&chk2);
				complex_zero_check(&chk3);
				print_complex(&chk3,"-- VAL1 - VAL2 = ");
				printf("\n");
			}
			else if (!strncmp(argv[loop],"--mul",6)) {
				test = complex_test_mul(&chk1,&chk2,&chk3);
				if (!test) {
					complex_zero_check(&chk3);
					print_complex(&chk3,"-- VAL1 * VAL2 = ");
					printf("\n");
				}
				ecnt += test;
			}
			else if (!strncmp(argv[loop],"--div",6)) {
				test = complex_test_div(&chk1,&chk2,&chk3);
				if (!test) {
					complex_zero_check(&chk3);
					print_complex(&chk3,"-- VAL1 / VAL2 = ");
					printf("\n");
				}
				ecnt += test;
			}
			else {
				fprintf(stderr,"** Unknown option '%s'!\n",argv[loop]);
				ecnt++;
			}
		}
	}
	printf("## Error(s): %d\n\n",ecnt);
	return 0;
}
/*----------------------------------------------------------------------------*/
#endif
/*----------------------------------------------------------------------------*/
