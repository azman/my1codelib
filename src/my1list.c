/*----------------------------------------------------------------------------*/
#include "my1list.h"
/*----------------------------------------------------------------------------*/
#include <stdlib.h>
/*----------------------------------------------------------------------------*/
void list_init(my1list_t* list, ffree_item done) {
	list->init = 0x0;
	list->last = 0x0;
	list->curr = 0x0;
	list->done = done;
	list->size = 0;
	/*list->step = 0; only valid during list_scan_item */
}
/*----------------------------------------------------------------------------*/
void list_free(my1list_t* list) {
	while (list->init) {
		list->curr = list->init->next;
		if (list->done) list->done((void*)list->init);
		list->init = list->curr;
		list->size--;
	}
}
/*----------------------------------------------------------------------------*/
void list_free_item(void* that) {
	/* use this if item->data is NOT dynamically created */
	free(that); /* assuming item is dynamic object, NOT done in list_free */
}
/*----------------------------------------------------------------------------*/
void list_free_item_data(void* that) {
	my1item_t* item = (my1item_t*)that;
	free(item->data);
	free(that);
}
/*----------------------------------------------------------------------------*/
void list_redo(my1list_t* list) {
	list_free(list);
	list->last = 0; /* init/curr/size should already be 0! */
}
/*----------------------------------------------------------------------------*/
my1item_t* list_push_item(my1list_t* list, my1item_t* item) {
	item->next = 0x0;
	item->prev = list->last;
	if (!list->last) list->init = item;
	else list->last->next = item;
	list->last = item;
	list->size++;
	return item;
}
/*----------------------------------------------------------------------------*/
my1item_t* list_pull_item(my1list_t* list) {
	my1item_t* item = list->init;
	if (item) {
		list->init = item->next;
		if (!list->init) list->last = 0x0;
		else list->init->prev = 0x0;
		if (list->done) list->done(item);
		list->size--;
	}
	return item;
}
/*----------------------------------------------------------------------------*/
my1item_t* list_scan_item(my1list_t* list) {
	if (!list->curr) {
		list->curr = list->init;
		list->step = 0;
	}
	else {
		list->curr = list->curr->next;
		list->step++;
	}
	return list->curr;
}
/*----------------------------------------------------------------------------*/
my1item_t* list_find_item(my1list_t* list, void* that) {
	my1item_t *find, *item;
	find = 0x0;
	item = list->init;
	while (item) {
		if (item->data==that) {
			find = item;
			break;
		}
		item = item->next;
	}
	return find;
}
/*----------------------------------------------------------------------------*/
my1item_t* list_hack_item(my1list_t* list, my1item_t* item) {
	/* assumed valid item! */
	if (item->prev) item->prev->next = item->next;
	else list->init = item->next;
	if (item->next) item->next->prev = item->prev;
	else list->last = item->prev;
	if (list->curr==item)
		list->curr = item->next;
	if (list->done) list->done(item);
	list->size--;
	return item;
}
/*----------------------------------------------------------------------------*/
my1item_t* list_next_item(my1list_t* list, my1item_t* here, my1item_t* item) {
	/* assumed here is valid item! */
	if (here) { /* take here's place */
		item->next = here;
		item->prev = here->prev;
		here->prev = item;
	}
	else { /* as last item */
		item->next = 0x0;
		item->prev = list->last;
		list->last = item;
	}
	if (!item->prev)
		list->init = item;
	else
		item->prev->next = item;
	list->size++;
	return item;
}
/*----------------------------------------------------------------------------*/
my1item_t* list_push_item_data(my1list_t* list, void* data) {
	my1item_t* item;
	item = make_item();
	if (item) {
		item->data = data;
		list_push_item(list,item);
	}
	return item;
}
/*----------------------------------------------------------------------------*/
my1item_t* list_next_item_data(my1list_t* list, my1item_t* here, void* data) {
	my1item_t* item;
	item = make_item();
	if (item) {
		item->data = data;
		list_next_item(list,here,item);
	}
	return item;
}
/*----------------------------------------------------------------------------*/
my1item_t* list_pop_item(my1list_t* list) {
	my1item_t* item = list->last;
	if (item) {
		list->last = item->prev;
		if (!list->last) list->init = 0x0;
		else list->last->next = 0x0;
		if (list->done) list->done(item);
		list->size--;
	}
	return item;
}
/*----------------------------------------------------------------------------*/
#if defined(TEST_MODULE) && defined(TEST_LIST)
/*----------------------------------------------------------------------------*/
#define TASK_EXIT 0
#define TASK_LIST 1
#define TASK_INSERT 2
#define TASK_REMOVE 3
#define TASK_REMOVE2 4
#define TASK_EMPTY 5
#define TASK_SORT 6
#define TASK_INSERT2 7
/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
#ifndef TEST_LIST_SORT
#include "my1list_sort.c"
#endif
/*----------------------------------------------------------------------------*/
#define NAME_CHARSIZE 80
/*----------------------------------------------------------------------------*/
typedef struct _data_t {
	char name[NAME_CHARSIZE];
	int id, tpad;
} data_t;
/*----------------------------------------------------------------------------*/
void print_item(my1item_t* item) {
	data_t* data = (data_t*) item->data;
	printf("ID: %010d , Label: %s\n",data->id,data->name);
}
/*----------------------------------------------------------------------------*/
void* find_item_id(my1list_t* list, int itemid) {
	void* find = 0x0;
	list->curr = 0x0;
	while (list_scan_item(list)) {
		data_t *test = (data_t*) list->curr->data;
		if (test->id==itemid) {
			find = (void*)test;
			break;
		}
	}
	return find;
}
/*----------------------------------------------------------------------------*/
int find_free_id(my1list_t* list) {
	int max_id = 0;
	list->curr = 0x0;
	while (list_scan_item(list)) {
		data_t *test = (data_t*) list->curr->data;
		if (test->id>max_id)
			max_id = test->id;
	}
	return ++max_id;
}
/*----------------------------------------------------------------------------*/
int data_compare(void* pchk, void* qchk) {
	data_t *pdat = (data_t*) pchk;
	data_t *qdat = (data_t*) qchk;
	return strncmp(pdat->name,qdat->name,NAME_CHARSIZE);
}
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	int task, test;
	char buff[16];
	my1list_t list;
	list_init(&list,list_free_item_data);
	do {
		printf("\n");
		printf("------------------------\n");
		printf("Test Program for my1list\n");
		printf("------------------------\n");
		printf(" {Init:%p",list.init);
		printf(",Last:%p}\n",list.last);
		printf("List Size: %d\n\n",list.size);
		printf("%d - List Items\n",TASK_LIST);
		printf("%d - Insert Item (push)\n",TASK_INSERT);
		printf("%d - Remove Item (pull)\n",TASK_REMOVE);
		printf("%d - Remove Item (select label)\n",TASK_REMOVE2);
		printf("%d - Clear List\n",TASK_EMPTY);
		printf("%d - Sort List\n",TASK_SORT);
		printf("%d - Insert item (auto-sort)\n",TASK_INSERT2);
		printf("%d - Exit\n\n",TASK_EXIT);
		printf("Select: ");
		test = scanf("%d",&task);
		printf("\n");
		if (test!=1) {
			fprintf(stderr,"** Cannot get integer?\n");
			fgets(buff,16,stdin);
			continue;
		}
		if (test==EOF) {
			fprintf(stderr,"** Invalid input!\n");
			fgets(buff,16,stdin);
			continue;
		}
		if (task==TASK_EXIT) break;
		else if (task==TASK_LIST) {
			if (list.size>0) {
				printf("----------\n");
				printf("Items List\n");
				printf("----------\n");
				list.curr = 0x0;
				while (list_scan_item(&list)) {
					printf("[%d/%d] ",list.step,list.size);
					print_item(list.curr);
				}
			}
			else printf("No items in list!\n");
		}
		else if (task==TASK_INSERT) {
			data_t *data = (data_t*)malloc(sizeof(data_t));
			data->id = find_free_id(&list);
			printf("Enter item label: ");
			scanf(" ");
			fgets(data->name,NAME_CHARSIZE,stdin);
			task = 0; /* reuse task */
			while (data->name[task]) {
				if (data->name[task]=='\n')
					data->name[task] = 0x0;
				task++;
			}
			if (strcmp(data->name,"")) {
				list_push_item_data(&list,data);
				printf("Item '%s' inserted!\n",data->name);
			}
			else {
				printf("Empty label! Cancelled!\n");
				free((void*)data);
			}
		}
		else if (task==TASK_REMOVE) {
			if (list.size>0) {
				data_t *data = (data_t*) list.init->data;
				printf("Item '%s' removed!\n",data->name);
				list_pull_item(&list);
			}
			else printf("No item in list!\n");
		}
		else if (task==TASK_REMOVE2) {
			if (list.size>0) {
				data_t *data = (data_t*)malloc(sizeof(data_t));
				printf("Enter label for item to be removed: ");
				scanf(" ");
				fgets(data->name,NAME_CHARSIZE,stdin);
				task = 0;
				while (data->name[task]) {
					if (data->name[task]=='\n')
						data->name[task] = 0x0;
					task++;
				}
				if (!strcmp(data->name,""))
					printf("Empty label! Not searching!\n");
				else {
					data_t *find = 0x0;
					list.curr = 0x0;
					while (list_scan_item(&list)) {
						data_t *temp = (data_t*) list.curr->data;
						if (!strcmp(temp->name,data->name)) {
							find = temp;
							break;
						}
					}
					if (find) {
						printf("Item '%s' removed!\n",find->name);
						list_hack_item(&list,list.curr);
					}
					else printf("Item '%s' not found!\n",data->name);
				}
				free((void*)data);
			}
			else printf("No item in list!\n");
		}
		else if (task==TASK_EMPTY) {
			if (list.size>0) {
				printf("Current list (will be cleared):\n");
				list.curr = 0x0;
				while (list_scan_item(&list)) {
					data_t* data = (data_t*) list.curr->data;
					printf("[%03d] ID: %010d , Label: %s\n",
						list.step,data->id,data->name);
				}
				printf("\nEmptying list... ");
				list_free(&list);
				printf("done!\n");
			}
			else printf("No item in list!\n");
		}
		else if (task==TASK_SORT) {
			if (list.size>2) {
				printf("Sorting list... ");
				list_sort(&list,data_compare);
				printf("done!\n");
			}
			else printf("Nothing to sort!\n");
		}
		else if (task==TASK_INSERT2) {
			data_t *data = (data_t*)malloc(sizeof(data_t));
			data->id = find_free_id(&list);
			printf("Enter item label: ");
			scanf(" ");
			fgets(data->name,NAME_CHARSIZE,stdin);
			task = 0;
			while (data->name[task]) {
				if (data->name[task]=='\n')
					data->name[task] = 0x0;
				task++;
			}
			if (strcmp(data->name,"")) {
				list.curr = 0x0;
				while (list_scan_item(&list)) {
					if (data_compare((void*)data,list.curr->data)<0)
						break;
				}
				printf("@@ Insert point:%p\n",list.curr);
				list_next_item_data(&list,list.curr,(void*)data);
				printf("Item '%s' inserted!\n",data->name);
			}
			else {
				printf("Empty label! Cancelled!\n");
				free((void*)data);
			}
		}
		else printf("Invalid selection!\n");
	} while (1);
	list_free(&list);
	return 0;
}
/*----------------------------------------------------------------------------*/
#endif
/*----------------------------------------------------------------------------*/
