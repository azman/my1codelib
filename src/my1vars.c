/*----------------------------------------------------------------------------*/
#include "my1vars.h"
/*----------------------------------------------------------------------------*/
#include <stdlib.h>
#include <string.h>
/*----------------------------------------------------------------------------*/
#ifndef VAR_NAMELEN
#define VAR_NAMELEN 40
#endif
/*----------------------------------------------------------------------------*/
/* using c naming rule? */
#define is_uppercase(ch) (ch>=0x41&&ch<=0x5a)
#define is_lowercase(ch) (ch>=0x61&&ch<=0x7a)
#define is_validchar(ch) (is_uppercase(ch)||is_lowercase(ch))
#define is_validinit(ch) (is_validchar(ch)||(ch=='_'))
#define is_validdgit(ch) (ch>=0x30&&ch<=0x39)
#define is_validname(ch) (is_validinit(ch)||is_validdgit(ch))
/*----------------------------------------------------------------------------*/
void var_init(my1var_t* pvar, char* name, pdata_t data, pfunc_t done) {
	pvar->data = data;
	pvar->done = done;
	cstr_init(&pvar->name);
	if (name) cstr_setstr(&pvar->name,name);
	else cstr_resize(&pvar->name,VAR_NAMELEN);
}
/*----------------------------------------------------------------------------*/
void var_free(my1var_t* pvar) {
	cstr_free(&pvar->name);
	if (pvar->done) {
		if (pvar->data)
			pvar->done(pvar->data);
	}
}
/*----------------------------------------------------------------------------*/
int var_name_valid8(char *name) {
	if (!is_validinit(name[0]))
		return 0;
	name++;
	while (*name) {
		if (!is_validname(name[0]))
			return 0;
		name++;
	}
	return 1;
}
/*----------------------------------------------------------------------------*/
void list_free_var(void* that) {
	my1item_t* item;
	item = (my1item_t*)that;
	var_free((my1var_t*)item->data);
	free(item->data); /* my1var_t* */
	free(that); /* my1item_t* */
}
/*----------------------------------------------------------------------------*/
void vars_init(my1vars_t* vars) {
	list_init(&vars->list,list_free_var);
	vars->pick = 0x0; /* not really needed? */
}
/*----------------------------------------------------------------------------*/
void vars_free(my1vars_t* vars) {
	list_free(&vars->list);
}
/*----------------------------------------------------------------------------*/
my1var_t* vars_make(my1vars_t* vars, char* name, pdata_t data, pfunc_t done) {
	my1var_t *pvar, *ptmp;
	vars->pick = 0x0;
	pvar = (my1var_t*)malloc(sizeof(my1var_t));
	if (pvar) {
		var_init(pvar,name,data,done);
		list_scan_prep(&vars->list);
		while (list_scan_item(&vars->list)) {
			ptmp = (my1var_t*) vars->list.curr->data;
			if (strncmp(name,ptmp->name.buff,ptmp->name.fill)<0)
				break;
		}
		list_next_item_data(&vars->list,vars->list.curr,(void*)pvar);
		vars->pick = pvar;
		vars->loop = vars->list.step;
	}
	return pvar;
}
/*----------------------------------------------------------------------------*/
my1var_t* vars_find(my1vars_t* vars, char* name) {
	my1var_t *ptmp;
	vars->pick = 0x0;
	list_scan_prep(&vars->list);
	while (list_scan_item(&vars->list)) {
		ptmp = (my1var_t*) vars->list.curr->data;
		if (!strncmp(name,ptmp->name.buff,ptmp->name.fill)) {
			vars->pick = ptmp;
			vars->loop = vars->list.step;
			break;
		}
	}
	return vars->pick;
}
/*----------------------------------------------------------------------------*/
my1var_t* vars_hack(my1vars_t* vars, char* name) {
	my1var_t *ptmp;
	vars->pick = 0x0;
	list_scan_prep(&vars->list);
	while (list_scan_item(&vars->list)) {
		ptmp = (my1var_t*) vars->list.curr->data;
		if (!strncmp(name,ptmp->name.buff,ptmp->name.fill)) {
			vars->pick = ptmp;
			vars->loop = vars->list.step;
			list_hack_item(&vars->list,vars->list.curr);
			break;
		}
	}
	return vars->pick;
}
/*----------------------------------------------------------------------------*/
#if defined(TEST_MODULE) && defined(TEST_VARS)
/*----------------------------------------------------------------------------*/
#include "my1cstr.c"
#include "my1cstr_line.c"
#include "my1cstr_util.c"
#include "my1list.c"
/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
/*----------------------------------------------------------------------------*/
void* make_data_4var(double dval) {
	void* pdat;
	pdat = malloc(sizeof(double));
	if (pdat)
		*((double*)pdat) = dval;
	return pdat;
}
/*----------------------------------------------------------------------------*/
void free_data_4var(void* that) {
	free(that);
}
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	my1vars_t vars;
	my1cstr_t buff, arg0, arg1;
	my1var_t *pvar;
	pdata_t pdat;
	long int idat;
	double rdat;
	vars_init(&vars);
	cstr_init(&buff);
	cstr_init(&arg0);
	cstr_init(&arg1);
	do {
		printf("my1vars> ");
		cstr_read_line(&buff,stdin);
		if (!buff.fill) break;
		cstr_shword(&buff,&arg0,STRING_DELIM);
		if (!strncmp(arg0.buff,"quit",5)) {
			printf("@@ Exiting...\n");
			break;
		}
		if (!strncmp(arg0.buff,"list",5)) {
			if (!vars.list.size) {
				printf("-- List empty!\n");
				continue;
			}
			printf("## List:\n");
			list_scan_prep(&vars.list);
			while (list_scan_item(&vars.list)) {
				pvar = (my1var_t*) vars.list.curr->data;
				printf("   {Var:%s,Val:%lf}\n",pvar->name.buff,
					*(double*)pvar->data);
			}
		}
		else if (!strncmp(arg0.buff,"make",5)) {
			cstr_shword(&buff,&arg0,STRING_DELIM);
			if (!arg0.fill) {
				printf("** No name given for var\n");
				continue;
			}
			if (!var_name_valid8(arg0.buff)) {
				printf("** Invalid var name '%s'!\n",arg0.buff);
				continue;
			}
			if (vars_find(&vars,arg0.buff)) {
				printf("** Var name '%s' exists!\n",arg0.buff);
				continue;
			}
			cstr_shword(&buff,&arg1,STRING_DELIM);
			if (!arg1.fill) {
				printf("** No value given for '%s'\n",arg0.buff);
				continue;
			}
			idat = atol(arg1.buff);
			rdat = atof(arg1.buff);
			if (idat!=(long int)rdat) {
				printf("** Not a value? (%ld|%lf)!\n",idat,rdat);
				continue;
			}
			pdat = make_data_4var(rdat);
			if (!pdat) {
				printf("** MakeContainer failed! (%ld|%lf)!\n",idat,rdat);
				continue;
			}
			pvar = vars_make(&vars,arg0.buff,pdat,free_data_4var);
			if (!pvar) {
				printf("** MakeVar '%s' failed! (%ld|%lf)!\n",
					arg0.buff,idat,rdat);
				continue;
			}
			printf("-- Var {%s:%lf}\n",pvar->name.buff,*(double*)pvar->data);
		}
		else {
			pvar = vars_find(&vars,arg0.buff);
			if (!pvar) {
				printf("** Var %s not found!\n",arg0.buff);
				continue;
			}
			if (buff.fill>0)
				printf("** Args {%s} ignored!\n\n",buff.buff);
			printf("## Var:%s Val:%lf\n",pvar->name.buff,*(double*)pvar->data);
		}
	} while(1);
	cstr_free(&arg1);
	cstr_free(&arg0);
	cstr_free(&buff);
	vars_free(&vars);
	return 0;
}
/*----------------------------------------------------------------------------*/
#endif
/*----------------------------------------------------------------------------*/
