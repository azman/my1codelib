/*----------------------------------------------------------------------------*/
#include "my1sockcli.h"
/*----------------------------------------------------------------------------*/
//#include <string.h>
/*----------------------------------------------------------------------------*/
void sockcli_init(my1sockcli_t* pcli) {
	sock_init(&pcli->sock);
	host_init(&pcli->host);
	pcli->stat = 0;
	pcli->flag = 0;
	pcli->opt1 = 0;
	pcli->opt2 = 0;
}
/*----------------------------------------------------------------------------*/
void sockcli_free(my1sockcli_t* pcli) {
	sock_free(&pcli->sock);
}
/*----------------------------------------------------------------------------*/
int host_is_ipv4(char* host) {
	int step = 0, loop = 0;
	int chk1 = 0; /* count '.' */
	while (host[loop]) {
		if (host[loop]=='.') {
			/* no digits - NOT ip! */
			if (!step) return 0;
			chk1++;
			/* max 3 dots - NOT ip! */
			if (chk1>3) return 0;
			/* should we check max255? */
			step = 0;
		}
		/* not a number - NOT ip! */
		else if (host[loop]<0x30||host[loop]>0x39) return 0;
		else {
			step++;
			/* more than 3 digits - NOT ip! */
			if (step>3) return 0;
		}
		loop++;
	}
	if (loop>12) return 0;
	return 1;
}
/*----------------------------------------------------------------------------*/
int sockcli_host(my1sockcli_t* pcli, char* host, int port) {
	if (host_is_ipv4(host)) host_from_ip(&pcli->host,host,port);
	else host_from_name(&pcli->host,host,port);
	return pcli->host.flag&HOST_FLAG_NAME_VALID;
}
/*----------------------------------------------------------------------------*/
int sockcli_open(my1sockcli_t* pcli) {
	if (pcli->flag&CLIENT_SOCKET_OPENED)
		return pcli->stat;
	pcli->stat = CLIENT_READY;
	if (!(pcli->host.flag&HOST_FLAG_NAME_VALID)) {
		pcli->stat |= CLIENT_ERROR_HOST;
		return pcli->stat;
	}
	sock_create(&pcli->sock);
	if (pcli->sock.sock==INVALID_SOCKET) {
		pcli->stat |= CLIENT_ERROR_SOCK;
		return pcli->stat;
	}
	/* set remote host info */
	pcli->sock.addr.sin_family = AF_INET;
	pcli->sock.addr.sin_port = htons(pcli->host.port);
	pcli->sock.addr.sin_addr = pcli->host.iadd;
	sock_client(&pcli->sock);
	if (pcli->sock.flag&SOCKET_ERROR)
		pcli->stat |= CLIENT_ERROR_OPEN;
	else pcli->flag |= CLIENT_SOCKET_OPENED;
	return pcli->stat;
}
/*----------------------------------------------------------------------------*/
int sockcli_done(my1sockcli_t* pcli) {
	if (pcli->flag&CLIENT_SOCKET_OPENED) {
		if (sock_finish(&pcli->sock,1))
			pcli->stat |= CLIENT_ERROR_SOCK;
		pcli->flag &= ~CLIENT_SOCKET_OPENED; /* close anyways? */
	}
	return pcli->stat;
}
/*----------------------------------------------------------------------------*/
int sockcli_send(my1sockcli_t* pcli, void* buff, int size) {
	int test = 0;
	if (pcli->flag&CLIENT_SOCKET_OPENED) {
		test = sock_send(pcli->sock.sock,size,buff);
		if (test<0)
			pcli->stat |= CLIENT_ERROR_SEND;
	}
	return test;
}
/*----------------------------------------------------------------------------*/
int sockcli_read(my1sockcli_t* pcli, void* buff, int size) {
	int test = 0;
	if (pcli->flag&CLIENT_SOCKET_OPENED) {
		test = sock_read(pcli->sock.sock,size,buff);
		if (test<0)
			pcli->stat |= CLIENT_ERROR_READ;
	}
	return test;
}
/*----------------------------------------------------------------------------*/
int sockcli_peek(my1sockcli_t* pcli, void* buff, int size) {
	int test = 0;
	if (pcli->flag&CLIENT_SOCKET_OPENED) {
		test = sock_peek(pcli->sock.sock,size,buff);
		if (test<0)
			pcli->stat |= CLIENT_ERROR_READ;
	}
	return test;
}
/*----------------------------------------------------------------------------*/
#if defined(TEST_MODULE) && defined(TEST_SOCKCLI)
/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
/*----------------------------------------------------------------------------*/
#include "my1keys.c"
#include "my1sockbase.c"
/*----------------------------------------------------------------------------*/
#define CLIC_BUFFSIZE 1024
/*----------------------------------------------------------------------------*/
#define DEFAULT_PORT 1337
/*----------------------------------------------------------------------------*/
#define STAT_ERROR 0x80000000
#define STAT_ERROR_ARGS (STAT_ERROR|0x0001)
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	my1sockcli_t client;
	my1key_t ikey;
	unsigned char buff[CLIC_BUFFSIZE];
	unsigned int stat;
	char *host, *pchk, hdef[] = "localhost";
	int loop, port, temp, fill;
	/* process parameters */
	host = hdef; port = DEFAULT_PORT; fill = 0;
	for (loop=1,stat=0;loop<argc;loop++) {
		if (argv[loop][0]=='-'&&argv[loop][1]=='-') {
			if (!strcmp(&argv[loop][2],"port")) {
				loop++;
				port = atoi(argv[loop]);
			}
			else if (!strcmp(&argv[loop][2],"host")) {
				loop++;
				host = argv[loop];
			}
			else if (!strcmp(&argv[loop][2],"data")) {
				loop++;
				pchk = argv[loop];
				while (*pchk) {
					if (fill<CLIC_BUFFSIZE)
						buff[fill++] = (unsigned char)pchk[0];
					else {
						printf("** Buffer overflow!\n");
						break;
					}
					pchk++;
				}
			}
			else {
				printf("** Unknown option '%s'! Aborting!\n",argv[loop]);
				stat |= STAT_ERROR_ARGS;
			}
		}
		else {
			/* not an option */
			if (argv[loop][0]>='0'&&argv[loop][0]<='9') { /* data! */
				char format[] = "%x";
				if (argv[loop][0]!='0'||argv[loop][1]!='x')
					format[1] = 'd';
				sscanf(argv[loop],format,&temp);
				if ((temp&0xff)!=temp)
					printf("** Invalid byte (0x%x)@(%d)? [%s]\n",
						temp,temp,argv[loop]);
				else {
					if (fill<CLIC_BUFFSIZE)
						buff[fill++] = (unsigned char)temp;
					else
						printf("** Buffer overflow!\n");
				}
			}
			else {
				printf("** Unknown parameter '%s'! Aborting!\n",argv[loop]);
				stat |= STAT_ERROR_ARGS;
			}
		}
	}
	sockcli_init(&client);
	do {
		printf("@@ Test: %s(%d)\n",host,host_is_ipv4(host));
		if (stat&STAT_ERROR) break;
		if (!sockcli_host(&client,host,port)) {
			printf("** Invalid host '%s'!\n",host);
			stat |= STAT_ERROR_ARGS;
			break;
		}
		printf("@@ Host: %s@%s(%d)[%x]\n",client.host.addr,
			client.host.name,client.host.port,client.host.flag);
		if (sockcli_open(&client)) {
			printf("** Error connecting to host! (%x)\n",client.stat);
			break;
		}
		if (fill>0) {
			printf("-- Sending data:");
			for (loop=0;loop<fill;loop++)
				printf(" 0x%02x",buff[loop]);
			printf("\n");
			sockcli_send(&client,(void*)buff,fill);
		}
		printf("-- Reading... press <ESC> to exit.\n");
		fill = CLIC_BUFFSIZE;
		while (1) {
			ikey = get_keyhit();
			if (ikey==KEY_ESCAPE||ikey=='q'||ikey=='Q')
				break;
			temp = sockcli_peek(&client,(void*)buff,fill);
			if (temp<0) {
				printf("\n** Socket error? (%x)\n",client.stat);
				break;
			}
			if (temp>0) {
				printf("-- Reading data:");
				for (loop=0;loop<temp;loop++)
					printf(" 0x%02x",buff[loop]);
				printf("\n");
			}
		}
	} while(0); /* end of dummy loop */
	sockcli_done(&client);
	/* free up resources when done */
	sockcli_free(&client);
	return 0;
}
/*----------------------------------------------------------------------------*/
#endif
/*----------------------------------------------------------------------------*/
