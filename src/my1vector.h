/*----------------------------------------------------------------------------*/
#ifndef __MY1VECTOR_H__
#define __MY1VECTOR_H__
/*----------------------------------------------------------------------------*/
/**
 *  my1vector is an array of double complex (c99 standard type)
 *  - written by azman@my1matrix.org
 *  - double _Complex has an alias 'double complex' in complex.h
**/
/*----------------------------------------------------------------------------*/
#include <complex.h>
#include "my1math_constants.h"
/*----------------------------------------------------------------------------*/
typedef double _Complex complex_t;
/*----------------------------------------------------------------------------*/
typedef struct  _my1vector_t {
	complex_t *data;
	int size, fill;
} my1vector_t;
/*----------------------------------------------------------------------------*/
void vector_init(my1vector_t* pvec);
void vector_free(my1vector_t* pvec);
void vector_make(my1vector_t* pvec, int size);
void vector_copy(my1vector_t* vdst, my1vector_t* vsrc);
void vector_fill(my1vector_t* pvec, double real, double imag);
void vector_append(my1vector_t* pvec, double real, double imag);
void vector_conjugate(my1vector_t* pvec);
void vector_zero_pad(my1vector_t* pvec);
void vector_zero_pad_fill(my1vector_t* pvec, int from, int stop);
void vector_norm(my1vector_t* pvec, int offs);
/*----------------------------------------------------------------------------*/
void vector_copy_some(my1vector_t* pvec,my1vector_t*,int,int);
void vector_copy_band(my1vector_t* pvec,my1vector_t*,int);
void vector_copy_append(my1vector_t* pvec,my1vector_t*);
void vector_scale(my1vector_t*,double);
void vector_summation(my1vector_t*,my1vector_t*);
void vector_difference(my1vector_t*,my1vector_t*);
void vector_dotproduct(my1vector_t*,my1vector_t*);
void vector_average(my1vector_t*);
void vector_magnitude(my1vector_t*);
void vector_make_polar(my1vector_t*);
void vector_make_cartesian(my1vector_t*);
void vector_zero_threshold(my1vector_t*,double);
/*----------------------------------------------------------------------------*/
#endif /** __MY1VECTOR_H__ */
/*----------------------------------------------------------------------------*/
