/*----------------------------------------------------------------------------*/
#ifndef __MY1SOCKCLI_H__
#define __MY1SOCKCLI_H__
/*----------------------------------------------------------------------------*/
/**
 *  my1sockcli client socket interface library
 *  - written by azman@my1matrix.org
 */
/*----------------------------------------------------------------------------*/
#include "my1sockbase.h"
/*----------------------------------------------------------------------------*/
#define CLIENT_DEFAULT_PORT 1337
/*----------------------------------------------------------------------------*/
/* client status bits */
/*----------------------------------------------------------------------------*/
#define CLIENT_READY SOCKET_OK
#define CLIENT_ERROR SOCKET_ERROR
#define CLIENT_ERROR_SOCK (CLIENT_ERROR|0x01)
#define CLIENT_ERROR_SEND (CLIENT_ERROR|0x02)
#define CLIENT_ERROR_READ (CLIENT_ERROR|0x04)
#define CLIENT_ERROR_PEEK (CLIENT_ERROR|0x08)
#define CLIENT_ERROR_HOST (CLIENT_ERROR|0x10)
#define CLIENT_ERROR_OPEN (CLIENT_ERROR|0x20)
/*----------------------------------------------------------------------------*/
/* client flag bits */
/*----------------------------------------------------------------------------*/
#define CLIENT_SOCKET_OPENED 0x01
/*----------------------------------------------------------------------------*/
typedef struct _my1sockcli_t {
	my1sock_t sock;
	my1host_t host;
	unsigned int stat, flag;
	unsigned int opt1, opt2; /* application level flags... 4u to use! */
} my1sockcli_t;
/*----------------------------------------------------------------------------*/
void sockcli_init(my1sockcli_t* pcli);
void sockcli_free(my1sockcli_t* pcli);
int sockcli_host(my1sockcli_t* pcli, char* host, int port);
int sockcli_open(my1sockcli_t* pcli);
int sockcli_done(my1sockcli_t* pcli);
int sockcli_send(my1sockcli_t* pcli, void* buff, int size);
int sockcli_read(my1sockcli_t* pcli, void* buff, int size);
/*----------------------------------------------------------------------------*/
#endif /** __MY1SOCKCLI_H__ */
/*----------------------------------------------------------------------------*/
