/*----------------------------------------------------------------------------*/
#ifndef __MY1VECTOR_FT_H__
#define __MY1VECTOR_FT_H__
/*----------------------------------------------------------------------------*/
/**
 *  my1vector extension for fourier transforms
 *  - written by azman@my1matrix.org
**/
/*----------------------------------------------------------------------------*/
#include "my1vector.h"
/*----------------------------------------------------------------------------*/
/* 5n precision for dft - can be user-defined */
#ifndef DFT_PRECISION
#define DFT_PRECISION 0.000000005
#endif
#ifndef FFT_PRECISION
#define FFT_PRECISION 0.0000000001
#endif
/*----------------------------------------------------------------------------*/
void vector_freq_shift(my1vector_t* vec);
void vector_freq_window(my1vector_t* vec, int size);
void vector_dft(my1vector_t* vec, my1vector_t* out);
void vector_dft_inv(my1vector_t* vec, my1vector_t* out);
void vector_fft(my1vector_t* vec, my1vector_t* out);
void vector_fft_inv(my1vector_t* vec, my1vector_t* out);
/*----------------------------------------------------------------------------*/
#endif /** __MY1VECTOR_FT_H__ */
/*----------------------------------------------------------------------------*/
