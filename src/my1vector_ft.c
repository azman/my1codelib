/*----------------------------------------------------------------------------*/
#include "my1vector_ft.h"
/*----------------------------------------------------------------------------*/
#include <math.h>
/*----------------------------------------------------------------------------*/
void vector_freq_shift(my1vector_t* vec) {
	complex_t swap;
	int loop, offs, size;
	offs = size = vec->fill;
	if (offs%2) offs++;
	offs >>= 1; size >>= 1;
	for (loop=0;loop<size;loop++) {
		swap = vec->data[loop];
		vec->data[loop] = vec->data[loop+offs];
		vec->data[loop+offs] = swap;
	}
}
/*----------------------------------------------------------------------------*/
void vector_freq_window(my1vector_t* vec, int size) {
	int loop, step, save;
	save = vec->fill;
	vec->fill = size; /* size>0 */
	for (loop=0,step=save-size;loop<size;loop++,step++)
		vec->data[vec->fill++] = vec->data[step];
}
/*----------------------------------------------------------------------------*/
void vector_dft(my1vector_t* vec, my1vector_t* out) {
	int loop, next, size, temp;
	double real, imag, that, step, term;
	/* prepare output */
	temp = vec->fill;
	size = vec->fill; /* in case we want to allow different output size! */
	vector_make(out,size);
	out->fill = out->size;
	/* for each output term */
	that = 2*CONSTANT_PI_/size;
	for (loop=0;loop<size;loop++) {
		real = 0.0; imag = 0.0;
		step = that * loop; term = 0.0;
		for (next=0;next<temp;next++) {
			/* cosine term */
			real += creal(vec->data[next])*cos(term);
			imag += cimag(vec->data[next])*cos(term);
			/* sine term */
			real += cimag(vec->data[next])*sin(term);
			imag -= creal(vec->data[next])*sin(term);
			/* next phase */
			term += step;
		}
		if (fabs(real)<DFT_PRECISION) real = 0.0;
		if (fabs(imag)<DFT_PRECISION) imag = 0.0;
		out->data[loop] = real + imag * I;
	}
}
/*----------------------------------------------------------------------------*/
void vector_dft_inv(my1vector_t* vec, my1vector_t* out) {
	int loop, next, size, temp;
	double real, imag, that, step, term;
	/* prepare output */
	temp = vec->fill;
	size = vec->fill;
	vector_make(out,size);
	out->fill = out->size;
	/* for each output term */
	that = 2*CONSTANT_PI_/size;
	for (loop=0;loop<size;loop++) {
		real = 0.0; imag = 0.0;
		step = that * loop; term = 0.0;
		for (next=0;next<temp;next++) {
			/* cosine term */
			real += creal(vec->data[next])*cos(term);
			imag += cimag(vec->data[next])*cos(term);
			/* sine term */
			real -= cimag(vec->data[next])*sin(term);
			imag += creal(vec->data[next])*sin(term);
			/* next phase */
			term += step;
		}
		real /= temp;
		imag /= temp;
		if (fabs(real)<DFT_PRECISION) real = 0.0;
		if (fabs(imag)<DFT_PRECISION) imag = 0.0;
		out->data[loop] = real + imag * I;
	}
}
/*----------------------------------------------------------------------------*/
int bit_reverse(int that, int step) {
	int test = 0, mask = step;
	while (mask>0) {
		test >>= 1;
		if (that&mask)
			test |= step;
		mask >>= 1;
	}
	return test;
}
/*----------------------------------------------------------------------------*/
void vector_fft(my1vector_t* vec, my1vector_t* out) {
	int loop, test = 0, step = 1, size = vec->fill, half = size >> 1;
	int temp, wide, next, iter, itop, ibot;
	complex_t term, that;
	/* find step size */
	while (step<size) { step <<= 1; test++; }
	step >>= 1; /* needed for rearrangement */
	/* prepare output */
	vector_make(out,size);
	out->fill = size; /* will fill to size! */
	/* rearrange for butterfly - decimation in time! */
	for (loop=0;loop<size;loop++) {
		temp = bit_reverse(loop,step);
		out->data[temp] = vec->data[loop];
	}
	/* loop through instead of recursive */
	for (loop=0,step=1,temp=size;loop<test;loop++) {
		wide = step; /* butterfly pair width */
		step <<= 1; /* butterfly step */
		temp >>= 1; /* number of butterly pair(s) */
		/* do the butterfly */
		for (next=0,iter=0;next<wide;next++,iter+=temp) {
			term = cexp(-I*CONSTANT_PI_*iter/half); /** W<N>^iter */
			for (itop=next;itop<size;itop+=step) {
				ibot = itop + wide;
				that = out->data[ibot] * term;
				out->data[ibot] = out->data[itop] - that;
				out->data[itop] = out->data[itop] + that;
			}
		}
	}
}
/*----------------------------------------------------------------------------*/
void vector_fft_inv(my1vector_t* vec, my1vector_t* out) {
	int loop, test = 0, step = 1, size = vec->fill, half = size >> 1;
	int temp, wide, next, iter, itop, ibot;
	complex_t term, that;
	double real,imag;
	while (step<size) { step <<= 1; test++; }
	step >>= 1;
	vector_make(out,size);
	out->fill = size; /* will fill to size! */
	for (loop=0;loop<size;loop++) {
		temp = bit_reverse(loop,step);
		out->data[temp] = vec->data[loop];
	}
	for (loop=0,step=1,temp=size;loop<test;loop++) {
		wide = step;
		step <<= 1;
		temp >>= 1;
		for (next=0,iter=0;next<wide;next++,iter+=temp) {
			/* exactly like fft... but, step 1: make this +ve */
			term = cexp(I*CONSTANT_PI_*iter/half); /** W<N>^iter */
			for (itop=next;itop<size;itop+=step) {
				ibot = itop + wide;
				that = out->data[ibot] * term;
				out->data[ibot] = out->data[itop] - that;
				out->data[itop] = out->data[itop] + that;
			}
		}
	}
	/* exactly like fft... but, step 2: divide by N */
	for (loop=0;loop<size;loop++) {
		real = creal(out->data[loop])/size;
		if (fabs(real)<FFT_PRECISION) real = 0.0;
		imag = cimag(out->data[loop])/size;
		if (fabs(imag)<FFT_PRECISION) imag = 0.0;
		/* filter small values? */
		out->data[loop] = real + imag * I;
	}
}
/*----------------------------------------------------------------------------*/
#if defined(TEST_MODULE) && defined(TEST_VECTOR_FT)
/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include <sys/time.h>
/*----------------------------------------------------------------------------*/
#include "my1vector.c"
/*----------------------------------------------------------------------------*/
void print_complex(complex_t* num) {
	double buff = creal(*num);
	double temp = cimag(*num);
	if (!temp) printf("%g",buff);
	else printf("(%g,%gi)[%g]",buff,temp,cabs(*num));
}
/*----------------------------------------------------------------------------*/
void vector_show(my1vector_t* vec, char* str) {
	int loop, size = vec->fill;
	printf("%s{%p}[%d/%d]: ",str,vec,size,vec->size);
	for (loop=0;loop<size;loop++) {
		if (loop) printf(", ");
		print_complex(&vec->data[loop]);
	}
	printf("\n");
}
/*----------------------------------------------------------------------------*/
void vector_text(my1vector_t* vec, char* str) {
	int loop, size = vec->fill;
	printf("%s{%p}[%d/%d]:\n",str,vec,size,vec->size);
	for (loop=0;loop<size;loop++) {
		printf("REAL[%d]=%10lf\t",loop,creal(vec->data[loop]));
		printf("IMAG[%d]=%10lf\t",loop,cimag(vec->data[loop]));
		printf("ABS[%d]=%10lf\t",loop,cabs(vec->data[loop]));
		printf("ARG[%d]=%10lf\n",loop,carg(vec->data[loop]));
	}
}
/*----------------------------------------------------------------------------*/
#define DATASIZE 8
/*----------------------------------------------------------------------------*/
void vector_real_sequence(my1vector_t* vec, int fill, int offs, int step) {
	int loop, next, stop;
	vec->fill = 0; stop = fill*step;
	for (loop=0,next=offs;loop<vec->size;loop++,next+=step) {
		if (next>stop) next -= stop;
		vector_append(vec,loop<fill?(double)next:(double)0,(double)0);
	}
}
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	int loop;
	my1vector_t test, buff, next;
	struct timeval init, stop, diff;
	/* print tool info */
	printf("\nTest program for my1vector_ft extension module\n");
	printf("  => by azman@my1matrix.org\n\n");
	/* initialize */
	vector_init(&test);
	vector_init(&buff);
	vector_init(&next);
	vector_make(&test,DATASIZE);
	/* prepare input test values */
	vector_real_sequence(&test,4,1,1);
	/* show it! */
	vector_show(&test,"DAT");
	printf("\n");
	/* run dft */
	vector_dft(&test,&buff);
	vector_show(&buff,"DFT");
	/* run fft */
	vector_fft(&test,&buff);
	vector_show(&buff,"FFT");
	printf("\n");
	/* run dft_inv */
	vector_dft_inv(&buff,&next);
	vector_show(&next,"INV");
	/* run fft_inv */
	vector_fft_inv(&buff,&next);
	vector_show(&next,"INV");
	printf("\n");
	/* tests */
	vector_fill(&test,0.0,0.0);
	test.data[0] = (double)1;
	vector_fft(&test,&buff);
	vector_text(&test,"TEST1");
	vector_text(&buff,"BUFF1");
	test.data[0] = (double)0;
	test.data[1] = (double)1;
	vector_fft(&test,&buff);
	vector_text(&test,"TEST2");
	vector_text(&buff,"BUFF1");
	printf("\n");
	/* special test */
	for (loop=0;loop<8;loop++)
	{
		vector_real_sequence(&test,8,(loop+1),2);
		vector_show(&test,"DAT");
		vector_dft(&test,&buff);
		vector_norm(&buff,1);
		vector_show(&buff,"DFT-NORM");
	}
	printf("\n");
	vector_freq_window(&buff,2);
	vector_show(&buff,"WIN");
	printf("\n");
	/* check exec time */
	gettimeofday(&init,0x0);
	for (loop=0;loop<1024;loop++)
		vector_dft(&test,&buff);
	gettimeofday(&stop,0x0);
	timersub(&stop,&init,&diff);
	printf("-- Diff(DFT):<%ld.%06ld>\n",
		(long int)(diff.tv_sec),(long int)(diff.tv_usec));
	gettimeofday(&init,0x0);
	for (loop=0;loop<1024;loop++)
		vector_fft(&test,&buff);
	gettimeofday(&stop,0x0);
	timersub(&stop,&init,&diff);
	printf("-- Diff(FFT):<%ld.%06ld>\n",
		(long int)(diff.tv_sec),(long int)(diff.tv_usec));
	/* release resource */
	vector_free(&next);
	vector_free(&test);
	vector_free(&buff);
	/* done! */
	printf("\n");
	return 0;
}
/*----------------------------------------------------------------------------*/
#endif
/*----------------------------------------------------------------------------*/
