/*----------------------------------------------------------------------------*/
#include "my1json_file.h"
/*----------------------------------------------------------------------------*/
#include <stdlib.h>
//#include <string.h>
/*----------------------------------------------------------------------------*/
int json_read(my1json_t* json, FILE* file) {
	json_read_data_t data;
	json_read_data_init(&data,json);
	while ((data.test=fgetc(file))!=EOF) {
		if (!json_read_data_char(&data,(char)data.test))
			break;
	}
	if (data.temp) {
#if defined(MY1DEBUG) || defined(MY1DEBUG_READ)
		fprintf(stderr,"[JSON] Temp NOT free!\n");
#endif
		json_free(data.temp);
		free((void*)data.temp);
	}
	if (data.stat) json_free(json);
	return data.stat;
}
/*----------------------------------------------------------------------------*/
int json_only_data(my1json_t* json) {
	int test = 1;
	void* temp = json->data.curr;
	list_scan_prep(&json->data);
	while (list_scan_item(&json->data)) {
		my1json_t* curr = (my1json_t*) json->data.curr->data;
		if (curr->data.size>0) {
			test = 0;
			break;
		}
	}
	json->data.curr = temp;
	return test;
}
/*----------------------------------------------------------------------------*/
int json_view_once(my1json_t* json, FILE* file) {
	if (!file) file = stdout;
	if (json->pkey) fprintf(file,"\"%s\":",json->pkey);
	if (json->pval) {
		if (json->type==JSON_VALUE_STRING)
			fprintf(file,"\"%s\"",(char*)json->pval);
		else
			fprintf(file,"%s",(char*)json->pval);
	}
	else fprintf(file,"%s",json->type==JSON_ASSOC?"{}":"[]");
	return 0;
}
/*----------------------------------------------------------------------------*/
int json_view(my1json_t* json, FILE* file, int eol) {
	int step = json_find_root(json,0x0)<<1;
	if (!file) file = stdout;
	if (eol) fprintf(file,"%*s",step,"");
	if (json->pkey) fprintf(file,"\"%s\":",json->pkey);
	if (json->pval) {
		if (json->type==JSON_VALUE_STRING)
			fprintf(file,"\"%s\"",(char*)json->pval);
		else
			fprintf(file,"%s",(char*)json->pval);
	}
	if (json->data.size) {
		void* temp = json->data.curr;
		int test = json_only_data(json);
		if (json->type&JSON_ARRAY_MASK) {
			if (json->type==JSON_ASSOC) fputc((int)'{',file);
			else fputc((int)'[',file);
			if (eol&&!test) fputc((int)'\n',file);
		}
		list_scan_prep(&json->data);
		while (list_scan_item(&json->data)) {
			my1json_t* that = (my1json_t*) json->data.curr->data;
			json_view(that,file,eol&&!test);
			if (json->data.curr->next) {
				fputc((int)',',file);
				if (eol&&!test) fputc((int)'\n',file);
			}
		}
		json->data.curr = temp;
		if (json->type&JSON_ARRAY_MASK) {
			if (eol&&!test) {
				fputc((int)'\n',file);
				fprintf(file,"%*s",step,"");
			}
			if (json->type==JSON_ASSOC) fputc((int)'}',file);
			else fputc((int)']',file);
		}
	}
	else if (!json->pval) /* still indicate empty array */ {
		if (json->type==JSON_ASSOC) fprintf(file,"{}");
		else fprintf(file,"[]");
	}
	if (eol&&!step) fputc((int)'\n',file);
	return 0;
}
/*----------------------------------------------------------------------------*/
int json_load_string(my1json_t* json,char* string, int size) {
	int test;
	FILE *fstr = fmemopen(string,size,"r");
	if (!fstr) {
		fprintf(stderr,"[JSON] Cannot read from given string!\n");
		return -1;
	}
	test = json_read(json,fstr);
	fclose(fstr);
	return test;
}
/*----------------------------------------------------------------------------*/
int json_load(my1json_t* json,char* filename) {
	int test;
	FILE *file = fopen(filename,"r");
	if (!file) {
		fprintf(stderr,"[JSON] Cannot read '%s'\n",filename);
		return -1;
	}
	test = json_read(json,file);
	fclose(file);
	return test;
}
/*----------------------------------------------------------------------------*/
int json_save(my1json_t* json,char* filename,int eol) {
	int test;
	FILE *file = fopen(filename,"w");
	if (!file) {
		fprintf(stderr,"[JSON] Cannot write to '%s'\n",filename);
		return -1;
	}
	test = json_view(json,file,eol);
	fclose(file);
	return test;
}
/*----------------------------------------------------------------------------*/
#if defined(TEST_MODULE) && defined(TEST_JSON_FILE)
/*----------------------------------------------------------------------------*/
#include "my1list.c"
#include "my1list_data.c"
#include "my1json.c"
#include "my1cstr.c"
#include "my1cstr_util.c"
/*----------------------------------------------------------------------------*/
#define FLAG_DOVIEW 0x01
#define FLAG_DOLIST 0x02
/*----------------------------------------------------------------------------*/
#define arg_check(x,y) !strncmp(x,y,strlen(y))
/*----------------------------------------------------------------------------*/
int main (int argc, char* argv[]) {
	my1json_t data;
	my1cstr_t buff;
	int loop, test;
	int temp, flag = 0;
	char *name = 0x0, *pstr = 0x0;
	for (loop=1;loop<argc;loop++) {
		if (arg_check(argv[loop],"--file")) {
			if (++loop<argc) name = argv[loop];
			else printf("** No arg for %s!\n",argv[loop-1]);
		}
		else if (arg_check(argv[loop],"--json")) {
			if (++loop==argc)
				printf("** No arg for %s!\n",argv[loop-1]);
			do {
				cstr_addcol(&buff,argv[loop],MY1CSTR_PREFIX_SPACING);
			} while (++loop<argc);
			if (buff.fill) pstr = buff.buff;
			break;
		}
		else if (arg_check(argv[loop],"--view"))
			flag |= FLAG_DOVIEW;
		else if (!strncmp(argv[loop],"--list",6))
			flag |= FLAG_DOLIST;
	}
	cstr_init(&buff);
	json_init(&data);
	if (name) {
		temp = json_load(&data,name);
		if (temp) {
			loop = temp/ERROR_LINE_OFFSET;
			test = temp%ERROR_LINE_OFFSET;
			printf("[LOAD] (Line:%d, Error:%d) => ",loop,test);
			switch (test) {
				case ERROR_JSON_FILE:
					printf("** Cannot open file '%s'!",name);
					break;
				default:
					printf("** Unknown temp (%d/%d/%d)!",temp,loop,test);
			}
			printf("\n");
		}
		else {
			if (!json_is_valid(&data))
				printf("[WARN] Invalid json!\n");
			if (flag&FLAG_DOVIEW) {
				printf("\n[JSON] Viewing file...\n");
				json_view(&data,stdout,1);
				printf("\n[JSON] Done viewing file.\n");
			}
			else if (flag&FLAG_DOLIST) {
				my1json_t *item = &data;
				json_item_init(item);
				printf("\n[ITEM] Listing item(s)...\n");
				while ((item=json_item_next(item))) {
					printf("[%p] ",item);
					json_view_once(item,stdout);
					fputc((int)'\n',stdout);
				}
				printf("[ITEM] End listing item(s).\n");
			}
		}
	}
	if (pstr) {
		if (!json_from_str(&data,pstr))
			printf("** Cannot read JSON string:'%s'!\n",pstr);
		else {
			if (!json_is_valid(&data))
				printf("[WARN] Invalid json!\n");
			pstr = json_make_str(&data);
			if (pstr) {
				printf("[JSON]Begins...\n%s",pstr);
				printf("\n[JSON]Ends...\n");
				free((void*)pstr);
			}
		}
	}
	json_free(&data);
	cstr_free(&buff);
	return 0;
}
/*----------------------------------------------------------------------------*/
#endif
/*----------------------------------------------------------------------------*/
