/*----------------------------------------------------------------------------*/
#ifndef __MY1CRC16_H__
#define __MY1CRC16_H__
/*----------------------------------------------------------------------------*/
/**
 *  my1crc16_t - calculates crc16 and generates table for faster use
 *  - written by azman@my1matrix.org
 */
/*----------------------------------------------------------------------------*/
#include "my1type.h"
/*----------------------------------------------------------------------------*/
#define CRC16_FLAG_IHEX 0x01
/* flag to reflect output (refout=true) */
#define CRC16_FLAG_OREF 0x02
/* flag to reflect input (refin=true) */
#define CRC16_FLAG_IREF 0x04
/*----------------------------------------------------------------------------*/
/** the default settings used */
/* CRC-16/CCITT (KERMIT) poly: x^16 + x^12 + x^5 + 1 => 0x1021 */
#define CRC16_CCITT_INIT 0x0000
#define CRC16_CCITT_POLY 0x1021
#define CRC16_CCITT_FLAG (CRC16_FLAG_IREF|CRC16_FLAG_OREF)
#define CRC16_CCITT_FXOR 0x0000
/*----------------------------------------------------------------------------*/
typedef struct _my1crc16_t {
	word16_t ccrc, poly;
	word16_t fxor, flag;
	word16_t* ptab;
} my1crc16_t;
/*----------------------------------------------------------------------------*/
void crc16_init(my1crc16_t* pcrc);
void crc16_free(my1crc16_t* pcrc);
void crc16_data(my1crc16_t* pcrc, byte08_t data);
void crc16_make_ptab(my1crc16_t* pcrc);
void crc16_data_ptab(my1crc16_t* pcrc, byte08_t data);
void crc16_calc(my1crc16_t* pcrc, byte08_t* pdat, int size);
/*----------------------------------------------------------------------------*/
#endif /** __MY1CRC16_H__ */
/*----------------------------------------------------------------------------*/
