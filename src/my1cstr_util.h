/*----------------------------------------------------------------------------*/
#ifndef __MY1CSTR_UTIL_H__
#define __MY1CSTR_UTIL_H__
/*----------------------------------------------------------------------------*/
/**
 * - extension for my1cstr_t to provide useful string processing functions
 *  - written by azman@my1matrix.org
**/
/*----------------------------------------------------------------------------*/
#include "my1cstr.h"
/*----------------------------------------------------------------------------*/
#define COMMON_DELIM " \n\r\t"
#define STRING_DELIM " \t"
/*----------------------------------------------------------------------------*/
#define MY1CSTR_TRIM_NOREPEAT 0x01
#define MY1CSTR_TRIM_DOREMOVE 0x02
/*----------------------------------------------------------------------------*/
#define MY1CSTR_SEP_NEWLINE 0x01
#define MY1CSTR_SEP_SPACING 0x02
#define MY1CSTR_SEP_TABCHAR 0x03
#define MY1CSTR_SEP_SEMICOL 0x04
#define MY1CSTR_SEP_COLON 0x05
#define MY1CSTR_SEP_COMMA 0x06
#define MY1CSTR_SEP_MASK 0x0FF
#define MY1CSTR_SEP_PREFIX 0x0100
#define MY1CSTR_PREFIX_SPACING (MY1CSTR_SEP_PREFIX|MY1CSTR_SEP_SPACING)
#define MY1CSTR_PREFIX_SEMICOL (MY1CSTR_SEP_PREFIX|MY1CSTR_SEP_SEMICOL)
#define MY1CSTR_PREFIX_COLON (MY1CSTR_SEP_PREFIX|MY1CSTR_SEP_COLON)
#define MY1CSTR_PREFIX_COMMA (MY1CSTR_SEP_PREFIX|MY1CSTR_SEP_COMMA)
/*----------------------------------------------------------------------------*/
#define my1cstr_xtra_char(xtra) (xtra==MY1CSTR_SEP_NEWLINE?'\n':\
	(xtra==MY1CSTR_SEP_SPACING?' ':\
	(xtra==MY1CSTR_SEP_TABCHAR?'\t':\
	(xtra==MY1CSTR_SEP_SEMICOL?';':\
	(xtra==MY1CSTR_SEP_COLON?':':\
	(xtra==MY1CSTR_SEP_COMMA?',':'\0'))))))
/*----------------------------------------------------------------------------*/
void cstr_uppercase(my1cstr_t* pstr);
void cstr_lowercase(my1cstr_t* pstr);
int cstr_trimws(my1cstr_t* pstr, int trim_opt);
int cstr_findwd(my1cstr_t* pstr, char* word);
int cstr_rmword(my1cstr_t* pstr, char* word, char* dlim);
int cstr_shword(my1cstr_t* pstr, my1cstr_t* word, char* dlim);
char* cstr_addcol(my1cstr_t* pstr, char* more, int what);
/*----------------------------------------------------------------------------*/
/* helper function(s) */
char cstr_delim(char* del,char chk);
/*----------------------------------------------------------------------------*/
#endif /** __MY1CSTR_UTIL_H__ */
/*----------------------------------------------------------------------------*/
