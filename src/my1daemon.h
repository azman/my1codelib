/*----------------------------------------------------------------------------*/
#ifndef __MY1DAEMONH__
#define __MY1DAEMONH__
/*----------------------------------------------------------------------------*/
/**
 *  my1daemon kit
 *  - written by azman@my1matrix.org
 *  - creates a daemon out of any program
 *  - uses signals for IPC
 *  - maybe include socket in the future?
 */
/*----------------------------------------------------------------------------*/
#include <unistd.h>
/*----------------------------------------------------------------------------*/
#define NAMEBUFFSIZE 32
/*----------------------------------------------------------------------------*/
typedef struct _my1daemon_t
{
	pid_t pid, sid, did;
	int lock;
	char buff[NAMEBUFFSIZE], name[NAMEBUFFSIZE];
	char filelogs[2*NAMEBUFFSIZE];
	char filelock[2*NAMEBUFFSIZE];
}
my1daemon_t;
/*----------------------------------------------------------------------------*/
void daemon_log(my1daemon_t* dstuff, char* message);
void daemon_error(my1daemon_t* dstuff, char* message);
void exec_daemon(my1daemon_t* dstuff);
void stop_daemon(my1daemon_t* dstuff);
/*----------------------------------------------------------------------------*/
#endif
/*----------------------------------------------------------------------------*/
