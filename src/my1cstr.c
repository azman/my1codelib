/*----------------------------------------------------------------------------*/
#include "my1cstr.h"
/*----------------------------------------------------------------------------*/
#include <stdlib.h>
#include <string.h>
/*----------------------------------------------------------------------------*/
void cstr_init(my1cstr_t* pstr) {
	pstr->buff = 0x0;
	pstr->size = 0;
	pstr->fill = 0;
}
/*----------------------------------------------------------------------------*/
void cstr_free(my1cstr_t* pstr) {
	if (pstr->buff) {
		free((void*)pstr->buff);
		cstr_init(pstr);
	}
}
/*----------------------------------------------------------------------------*/
void cstr_null(my1cstr_t* pstr) {
	/* empty existing string, but retain buffer */
	if (pstr->buff) {
		pstr->buff[0] = 0x0;
		pstr->fill = 0;
	}
}
/*----------------------------------------------------------------------------*/
char* cstr_substr(my1cstr_t* pstr, my1cstr_t* from, int offs, int size) {
	int temp;
	temp = from->fill - offs;
	if (size<=0||size>temp) size = temp;
	size++; /*extra for null */
	if (size>pstr->size) {
		if (!cstr_resize(pstr,size))
			return 0x0;
	}
	strncpy(pstr->buff,&from->buff[offs],size);
	pstr->fill = size;
	pstr->buff[size-1] = 0x0;
	return pstr->buff;
}
/*----------------------------------------------------------------------------*/
char* cstr_resize(my1cstr_t* pstr, int size) {
	char* pnew = 0x0;
	if (size>1) { /* at least space for null! */
		pnew = (char*)realloc((void*)pstr->buff,size);
		if (pnew) {
			/* put null if was empty OR shrinking */
			if (pstr->size==0) pnew[0] = 0x0;
			else if (pstr->size>size) pnew[size-1] = 0x0;
			pstr->buff = pnew;
			pstr->size = size;
		}
	}
	return pnew;
}
/*----------------------------------------------------------------------------*/
char* cstr_assign(my1cstr_t* pstr, char* that) {
	char *pnew;
	int step, size;
	step = strlen(that);
	size = step + 1; /* plus null */
	pnew = cstr_resize(pstr, size);
	if (pnew) {
		strncpy(pnew,that,size); /* null is always copied! */
		pstr->fill = step;
	}
	return pnew;
}
/*----------------------------------------------------------------------------*/
char* cstr_setstr(my1cstr_t* pstr, char* that) {
	char *pnew;
	int step, size;
	step = strlen(that);
	size = step + 1;
	if (size>pstr->size) /* need to grow! */
		pnew = cstr_resize(pstr, size);
	else /* use existing buffer space */
		pnew = pstr->buff;
	if (pnew) {
		strncpy(pnew,that,size);
		pstr->fill = step;
	}
	return pnew;
}
/*----------------------------------------------------------------------------*/
char* cstr_append(my1cstr_t* pstr, char* more) {
	char *pnew;
	int step, size;
	step = strlen(more);
	size = pstr->fill + step + 1;
	if (size>pstr->size)
		pnew = cstr_resize(pstr, size);
	else pnew = pstr->buff;
	if (pnew) {
		strncat(pnew,more,step); /* append - null guaranteed! */
		pstr->fill += step;
	}
	return pnew;
}
/*----------------------------------------------------------------------------*/
char* cstr_prefix(my1cstr_t* pstr, char* more) {
	char *pnew;
	int step, size;
	int loop, test;
	step = strlen(more);
	size = pstr->fill + step + 1;
	if (size>pstr->size)
		pnew = cstr_resize(pstr, size);
	else pnew = pstr->buff;
	if (pnew)
	{
		/* shift the string back - copy null as well! */
		for (loop=size-1,test=pstr->fill;test>=0;loop--,test--)
			pnew[loop] = pnew[test];
		strncpy(pnew,more,step);
		pstr->fill += step;
	}
	return pnew;
}
/*----------------------------------------------------------------------------*/
char* cstr_single(my1cstr_t* pstr, char slip) {
	char *pnew;
	int size;
	size = pstr->fill + 2; /* plus 1 char + null! */
	if (size>pstr->size) /* need to grow! */
		pnew = cstr_resize(pstr, size);
	else /* use existing buffer space */
		pnew = pstr->buff;
	/* append single char */
	if (pnew) {
		pnew[pstr->fill++] = slip;
		pnew[pstr->fill] = 0x0;
	}
	return pnew;
}
/*----------------------------------------------------------------------------*/
char cstr_rmtail(my1cstr_t* pstr) {
	char temp;
	if (pstr->fill>0) {
		pstr->fill--;
		temp = pstr->buff[pstr->fill];
		pstr->buff[pstr->fill] = 0x0;
		return temp;
	}
	return 0;
}
/*----------------------------------------------------------------------------*/
#if defined(TEST_MODULE) && defined(TEST_CSTR)
/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include "my1cmds.c"
/*----------------------------------------------------------------------------*/
#define MAX_LINES_CHAR 80
#define SHPROMPT "my1cstr"
/*----------------------------------------------------------------------------*/
typedef struct _my1dat_t {
	char *ptmp, rbuf[MAX_LINES_CHAR];
	char *init, *args;
	my1cstr_t buff, temp;
} my1dat_t;
/*----------------------------------------------------------------------------*/
void data_init(my1dat_t* that) {
	cstr_init(&that->buff);
	cstr_init(&that->temp);
	strcpy(that->rbuf,"ABCDEFGHIJKLMNOPQRSTUVWXYZ");
	cstr_assign(&that->buff,that->rbuf);
	cstr_null(&that->buff);
}
/*----------------------------------------------------------------------------*/
void data_free(my1dat_t* that) {
	cstr_free(&that->temp);
	cstr_free(&that->buff);
}
/*----------------------------------------------------------------------------*/
char is_whitespace(char that) {
	switch(that) {
		case ' ': case '\t': break;
		default: that = 0x0; break;
	}
	return that;
}
/*----------------------------------------------------------------------------*/
int data_read(my1dat_t* that) {
	printf("\n"SHPROMPT"> ");
	/* try to read a line, limited buffer */
	if (!fgets((char*)&that->rbuf, MAX_LINES_CHAR, stdin))
	{
		fprintf(stderr,"\n** Cannot read from console!\n");
		return -1;
	}
	/* strip newline */
	that->ptmp = strchr(that->rbuf,'\r');
	if(that->ptmp) that->ptmp[0] = 0x0;
	that->ptmp = strchr(that->rbuf,'\n');
	if(that->ptmp) that->ptmp[0] = 0x0;
	/* skip leading whitespace */
	that->ptmp = that->rbuf;
	while (is_whitespace(*that->ptmp))
		that->ptmp++;
	return 0;
}
/*----------------------------------------------------------------------------*/
int data_prep(my1dat_t* that) {
	that->init = strtok(that->ptmp," \t");
	if (!that->init) {
		fprintf(stderr,"\n** Cannot get first word!\n");
		return -1;
	}
	that->args = strtok(0x0,"\0");
	return 0;
}
/*----------------------------------------------------------------------------*/
int show_help(my1cmds_t* that) {
	my1cmd_t* pcmd;
	pcmd = that->list;
	that->loop = 0;
	printf("\n-- Command list:\n\n");
	while (that->loop<that->fill) {
		printf("   %s - %s\n",pcmd->name,pcmd->info);
		that->loop++;
		pcmd++;
	}
	return 0;
}
/*----------------------------------------------------------------------------*/
void show_buff(my1cmds_t* this) {
	my1dat_t* that = (my1dat_t*)this->data;
	printf("-- Size:%d (%d)\n",that->buff.fill,that->buff.size);
	printf("-- Buff:{%s}\n",that->buff.buff);
}
/*----------------------------------------------------------------------------*/
void assign_buff(my1cmds_t* this) {
	my1dat_t* that = (my1dat_t*)this->data;
	if (!that->args) {
		fprintf(stderr,"\n** Nothing to set!\n");
		return;
	}
	cstr_setstr(&that->buff,that->args);
	printf("-- Text copied:'%s'\n",that->args);
}
/*----------------------------------------------------------------------------*/
void clear_buff(my1cmds_t* this) {
	my1dat_t* that = (my1dat_t*)this->data;
	cstr_null(&that->buff);
	printf("-- Buff emptied\n");
}
/*----------------------------------------------------------------------------*/
void append_buff(my1cmds_t* this) {
	my1dat_t* that = (my1dat_t*)this->data;
	if (!that->args) {
		fprintf(stderr,"\n** Nothing to append!\n");
		return;
	}
	cstr_single(&that->buff,' ');
	cstr_append(&that->buff,that->args);
	printf("-- Text appended:'%s'\n",that->args);
}
/*----------------------------------------------------------------------------*/
void prefix_buff(my1cmds_t* this) {
	my1dat_t* that = (my1dat_t*)this->data;
	if (!that->args) {
		fprintf(stderr,"\n** Nothing to insert!\n");
		return;
	}
	cstr_prefix(&that->buff," ");
	cstr_prefix(&that->buff,that->args);
	printf("-- Text inserted:'%s'\n",that->args);
}
/*----------------------------------------------------------------------------*/
void substr_buff(my1cmds_t* this) {
	my1dat_t* that = (my1dat_t*)this->data;
	my1cstr_t temp;
	int offs, size;
	if (!that->args) {
		fprintf(stderr,"\n** No offs/size pair given!\n");
		return;
	}
	if (sscanf(that->args,"%d %d",&offs,&size)!=2) {
		fprintf(stderr,"\n** Invalid offs/size pair! (%s)\n",that->args);
		return;
	}
	cstr_init(&temp);
	cstr_copy(&temp,&that->buff);
	cstr_substr(&that->buff,&temp,offs,size);
	cstr_free(&temp);
	printf("-- Text extracted:'%s'\n",that->buff.buff);
}
/*----------------------------------------------------------------------------*/
void rmtail_buff(my1cmds_t* this) {
	my1dat_t* that = (my1dat_t*)this->data;
	char what;
	if ((what=cstr_rmtail(&that->buff))) {
		printf("-- Last char '%c' removed\n",what);
	}
	printf("-- Current text:'%s'\n",that->buff.buff);
}
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	my1cmds_t cmds;
	my1dat_t data;
	cmds_init(&cmds);
	cmds_make(&cmds,"help","Show this help",CMDFLAG_TASK,(ptask_t)show_help);
	cmds_make(&cmds,"show","Show current buffer content",
		CMDFLAG_TASK,(ptask_t)show_buff);
	cmds_make(&cmds,"set","Set current buffer",
		CMDFLAG_TASK,(ptask_t)assign_buff);
	cmds_make(&cmds,"clear","Clear current buffer",
		CMDFLAG_TASK,(ptask_t)clear_buff);
	cmds_make(&cmds,"append","Append current buffer",
		CMDFLAG_TASK,(ptask_t)append_buff);
	cmds_make(&cmds,"prefix","Prepend current buffer",
		CMDFLAG_TASK,(ptask_t)prefix_buff);
	cmds_make(&cmds,"substr","Create sub-string of current buffer",
		CMDFLAG_TASK,(ptask_t)substr_buff);
	cmds_make(&cmds,"notail","remove last character in current buffer",
		CMDFLAG_TASK,(ptask_t)rmtail_buff);
	cmds_make(&cmds,"exit","Exit Program",CMDFLAG_EXIT,0x0);
	cmds_make(&cmds,"quit","",CMDFLAG_EXIT,0x0);
	cmds_data(&cmds,&data);
	data_init(&data);
	while(1) {
		if (data_read(&data)) break;
		if (!strlen(data.ptmp)) continue;
		/**fprintf(stderr,"@@ Line:%s\n",ptmp);*/
		if (data_prep(&data)) continue;
		if (cmds_find(&cmds,data.init)) {
			if (cmds_done(&cmds))
				break;
			if (!cmds_exec(&cmds))
				fprintf(stderr,"\n** No exec function for '%s'!\n",
					cmds.pick->name);
		}
		else {
			fprintf(stderr,"\n** Unknown command '%s'! (%s)\n",
				data.init,data.args?data.args:"[None]");
		}
	}
	printf("\n");
	data_free(&data);
	cmds_free(&cmds);
	return 0;
}
/*----------------------------------------------------------------------------*/
#endif
/*----------------------------------------------------------------------------*/
