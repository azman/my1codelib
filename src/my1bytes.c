/*----------------------------------------------------------------------------*/
#include "my1bytes.h"
/*----------------------------------------------------------------------------*/
#include <stdlib.h>
/*----------------------------------------------------------------------------*/
void bytes_init(my1bytes_t* that) {
	that->size = 0;
	that->fill = 0;
	that->data = 0x0;
}
/*----------------------------------------------------------------------------*/
void bytes_free(my1bytes_t* that) {
	if (that->data) {
		free((void*)that->data);
		that->data = 0x0;
		that->size = 0;
		that->fill = 0;
	}
}
/*----------------------------------------------------------------------------*/
void bytes_make(my1bytes_t* that, int size) {
	byte08_t *temp;
	if (size>0&&size!=that->size) {
		temp = (byte08_t*) realloc(that->data,sizeof(byte08_t)*size);
		if (temp) {
			if (that->fill>size)
				that->fill = size; /* data truncated! */
			that->size = size;
			that->data = temp;
		}
	}
}
/*----------------------------------------------------------------------------*/
void bytes_fill(my1bytes_t* that, byte08_t fill) {
	int loop, size;
	size = that->size;
	for (loop=that->fill;loop<size;loop++)
		that->data[loop] = fill;
	that->fill = size;
}
/*----------------------------------------------------------------------------*/
void bytes_slip(my1bytes_t* that, byte08_t byte) {
	/* fill <= size! */
	if (that->fill==that->size)
		bytes_make(that,that->size+1); /* increase size by 1 */
	that->data[that->fill++] = byte;
}
/*----------------------------------------------------------------------------*/
void bytes_more(my1bytes_t* that, byte08_t* psrc, int size) {
	int loop, temp;
	temp = that->fill + size;
	if (temp>that->size)
		bytes_make(that,temp);
	for (loop=0;loop<size;loop++)
		that->data[that->fill++] = psrc[loop];
}
/*----------------------------------------------------------------------------*/
void bytes_fill_size(my1bytes_t* that, byte08_t fill, int size) {
	int loop;
	loop = that->fill + size;
	if (loop>that->size)
		bytes_make(that,loop);
	for (loop=0;loop<size;loop++)
		that->data[that->fill++] = fill;
}
/*----------------------------------------------------------------------------*/
#if defined(TEST_MODULE) && defined(TEST_BYTES)
/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
#include "my1bytes.h"
/*----------------------------------------------------------------------------*/
void bytes_show(my1bytes_t* data, char* name, int deca) {
	int loop, fill = data->fill, size = data->size;
	char format[] = "[0x%02x]";
	if (deca) strcpy(format,"[%d]");
	printf("%s(%d/%d): ",name,fill,size);
	for (loop=0;loop<fill;loop++)
		printf(format,data->data[loop]);
	printf("\n");
}
/*----------------------------------------------------------------------------*/
#define INITIAL_SIZE 4
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	int loop, buff;
	my1bytes_t test;
	/* print tool info */
	printf("\nTest program for my1bytes module\n");
	printf("  => by azman@my1matrix.org\n\n");
	/* initialize */
	bytes_init(&test);
	/* prepare input */
	bytes_make(&test,INITIAL_SIZE);
	/* initial state */
	bytes_show(&test,"INIT",0);
	/* get from command line */
	for (loop=1;loop<argc;loop++) {
		if (argv[loop][0]>='0'&&argv[loop][0]<='9') { /* data! */
			char format[] = "%x";
			if (argv[loop][0]!='0'||argv[loop][1]!='x')
				format[1] = 'd';
			sscanf(argv[loop],format,&buff);
			if ((buff&0xff)!=buff)
				printf("** Invalid byte (0x%x)@(%d)? [%s]\n",
					buff,buff,argv[loop]);
			bytes_slip(&test,(byte08_t)buff);
			bytes_show(&test,"CURR",0);
		}
		else printf("** Not a byte? [%s]\n",argv[loop]);
	}
	/* initial state */
	bytes_show(&test,"DONE",1);
	/* release resource */
	bytes_free(&test);
	/* done! */
	printf("\n");
	return 0;
}
/*----------------------------------------------------------------------------*/
#endif
/*----------------------------------------------------------------------------*/
