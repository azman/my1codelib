/*----------------------------------------------------------------------------*/
#include "my1trig.h"
/*----------------------------------------------------------------------------*/
double my1cos_core(double radian, int terms) {
	double next, full, rval;
	int loop, temp;
	next = 1.0, full = 0.0;
	rval = radian*radian; rval = -rval;
	loop = 0;
	while (loop<terms) {
		full += next;
		temp = (++loop)<<1;
		temp *= (temp-1);
		next *= rval/(double)temp;
	}
	return full;
}
/*----------------------------------------------------------------------------*/
double my1sin_core(double radian, int terms) {
	double next, full, rval;
	int loop, temp;
	next = radian, full = 0.0;
	rval = radian*radian; rval = -rval;
	loop = 0;
	while (loop<terms) {
		full += next;
		temp = (++loop)<<1;
		temp *= (temp+1);
		next *= rval/(double)temp;
	}
	return full;
}
/*----------------------------------------------------------------------------*/
double limit_radian(double radian) {
	while (radian<0)
		radian += PERIOD_;
	while (radian>PERIOD_)
		radian -= PERIOD_;
	return radian;
}
/*----------------------------------------------------------------------------*/
#define HALF_PI (MATH_PI/2)
#define QUAD_PI (MATH_PI/4)
#define CYCLEQ1 0
#define CYCLEQ2 1
#define CYCLEQ3 2
#define CYCLEQ4 3
/*----------------------------------------------------------------------------*/
double my1cos_full(double radian, int costs, int sints) {
	int pick;
	double result;
	/* make sure within 0 -> 2pi */
	radian = limit_radian(radian);
	/* find 0 -> pi/2 quadrant */
	if (radian<HALF_PI) pick = CYCLEQ1;
	else if (radian<MATH_PI)
	{
		radian = MATH_PI-radian;
		pick = CYCLEQ2;
	}
	else if (radian<(MATH_PI+HALF_PI))
	{
		radian = radian-MATH_PI;
		pick = CYCLEQ3;
	}
	else
	{
		radian = PERIOD_-radian;
		pick = CYCLEQ4;
	}
	/* get result for 0 -> pi/4 , use sin if > pi/4 */
	if (radian<QUAD_PI) result = my1cos_core(radian,costs);
	else result = my1sin_core(HALF_PI-radian,sints);
	/* assign sign based on quadrant */
	if ((pick==CYCLEQ2)||(pick==CYCLEQ3))
		result = -result;
	return result;
}
/*----------------------------------------------------------------------------*/
double my1sin_full(double radian, int costs, int sints) {
	int pick;
	double result;
	/* make sure within 0 -> 2pi */
	radian = limit_radian(radian);
	/* find 0 -> pi/2 quadrant */
	if (radian<HALF_PI) pick = CYCLEQ1;
	else if (radian<MATH_PI)
	{
		radian = MATH_PI-radian;
		pick = CYCLEQ2;
	}
	else if (radian<(MATH_PI+HALF_PI))
	{
		radian = radian-MATH_PI;
		pick = CYCLEQ3;
	}
	else
	{
		radian = PERIOD_-radian;
		pick = CYCLEQ4;
	}
	/* get result for 0 -> pi/4 , use cos if > pi/4 */
	if (radian<QUAD_PI) result = my1sin_core(radian,sints);
	else result = my1cos_core(HALF_PI-radian,costs);
	/* assign sign based on quadrant */
	if ((pick==CYCLEQ3)||(pick==CYCLEQ4))
		result = -result;
	return result;
}
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#if defined(TEST_MODULE) && defined(TEST_TRIG)
/*----------------------------------------------------------------------------*/
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
/*----------------------------------------------------------------------------*/
#define ERROR_THRESHOLD 0.000001
#define DEFAULT_INCREMENT 0.01
#define DEFAULT_CYCLE_LIMIT 1
#define FULL_CYCLE 360.0
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[])
{
	int fail = 0, loop, lim = DEFAULT_CYCLE_LIMIT, cnt, test, cts, sts;
	double tmp, ans, chk, err, end = lim * (FULL_CYCLE/2), beg = -end, sum;
	double inc = DEFAULT_INCREMENT, thresh = ERROR_THRESHOLD;
	double max;

	cts = COS_TERMS; sts = SIN_TERMS;
	for (loop=1;loop<argc;loop++)
	{
		if (!strcmp(argv[loop],"-e"))
		{
			if ((++loop)==argc) break;
			thresh = atof(argv[loop]);
		}
		else if (!strcmp(argv[loop],"-i"))
		{
			if ((++loop)==argc) break;
			inc = atof(argv[loop]);
		}
		else if (!strcmp(argv[loop],"--cterms"))
		{
			if ((++loop)==argc) break;
			test = atoi(argv[loop]);
			if (test>1) cts = test;
		}
		else if (!strcmp(argv[loop],"--sterms"))
		{
			if ((++loop)==argc) break;
			test = atoi(argv[loop]);
			if (test>1) sts = test;
		}
		else if (!strcmp(argv[loop],"-l"))
		{
			if ((++loop)==argc) break;
			lim = atoi(argv[loop]);
			if (lim>0)
			{
				end = lim * (FULL_CYCLE/2);
				beg = -end;
			}
		}
	}

	printf("\nTest my1cos function (%d|%d)\n",cts,sts);
	printf("  *Error threshold: %e\n",thresh);
	printf("  *Degree Increment: %lf\n",inc);
	printf("  *Cycle limit: %d (%lf-%lf)\n\n",lim,beg,end);
	cnt = 0; sum = 0.0;
	tmp = beg; max = 0.0;
	while (tmp<=end)
	{
		ans = my1cos_full(deg2rad(tmp),cts,sts);
		chk = cos(deg2rad(tmp));
		err = fabs(ans-chk);
		if (err>thresh)
		{
			fail++;
			printf("{input:%lf} my1cos:%lf , cos:%lf , err:%e\n",
				tmp,ans,chk,err);
		}
		if (err>max) max = err;
		tmp += inc;
		sum += err;
		cnt++;
	}
	if (!fail)
		printf("All good from %lf-%lf degrees\n",beg,end);
	printf("Avg error:%e\n",sum/cnt);
	printf("Max error:%e\n",max);
	putchar('\n');

	printf("\nTest my1sin function (%d|%d)\n",cts,sts);
	printf("  *Error threshold: %e\n",thresh);
	printf("  *Degree Increment: %lf\n",inc);
	printf("  *Cycle limit: %d (%lf,%lf)\n\n",lim,beg,end);
	cnt = 0; sum = 0.0;
	tmp = beg; max = 0.0;
	while (tmp<=end)
	{
		ans = my1sin_full(deg2rad(tmp),cts,sts);
		chk = sin(deg2rad(tmp));
		err = fabs(ans-chk);
		if (err>thresh)
		{
			fail++;
			printf("{input:%lf} my1sin:%lf , sin:%lf , err:%e\n",
				tmp,ans,chk,err);
		}
		if (err>max) max = err;
		tmp += inc;
		sum += err;
		cnt++;
	}
	if (!fail)
		printf("All good from %lf-%lf degrees\n",beg,end);
	printf("Avg error:%e\n",sum/cnt);
	printf("Max error:%e\n",max);
	putchar('\n');

	return 0;
}
/*----------------------------------------------------------------------------*/
/**
RUN202301221315 on x86_64 Intel(R) Core(TM) i3-4170 CPU @ 3.70GHz

./test_my1trig -i 0.00001 -e 0.001 --cterms 5 --sterms 5

Test my1cos function (5|5)
Avg error:1.187261e-09
Max error:2.449675e-08

Test my1sin function (5|5)
Avg error:1.187307e-09
Max error:2.449682e-08

./test_my1trig -i 0.00001 -e 0.001 --cterms 4 --sterms 4

Test my1cos function (4|4)
Avg error:2.139731e-07
Max error:3.566364e-06

Test my1sin function (4|4)
Avg error:2.139730e-07
Max error:3.566364e-06

./test_my1trig -i 0.00001 -e 0.001 --cterms 3 --sterms 3

Test my1cos function (3|3)
Avg error:2.535717e-05
Max error:3.224255e-04

Test my1sin function (3|3)
Avg error:2.535717e-05
Max error:3.224255e-04

RUN201811190836 on x86_64 Intel(R) Core(TM) i5-4570 CPU @ 3.20GHz

Test my1cos function
  *Error threshold: 3.116300e-07
  *Degree Increment: 0.000010
  *Cycle limit: 1 (360.000000)

All good from 0-360.000000 degrees

Test my1sin function
  *Error threshold: 3.116300e-07
  *Degree Increment: 0.000010
  *Cycle limit: 1 (360.000000)

All good from 0-360.000000 degrees
**/
/**
RUN201901090547 on x86_64 Intel(R) Core(TM) i3-6006U CPU @ 2.00GHz

Test my1cos function
  *Error threshold: 3.000000e-07
  *Degree Increment: 0.000010
  *Cycle limit: 1 (-180.000000-180.000000)

All good from -180.000000-180.000000 degrees


Test my1sin function
  *Error threshold: 3.000000e-07
  *Degree Increment: 0.000010
  *Cycle limit: 1 (-180.000000-180.000000)

All good from -180.000000-180.000000 degrees
**/
/*----------------------------------------------------------------------------*/
#endif
/*----------------------------------------------------------------------------*/
