/*----------------------------------------------------------------------------*/
#include "my1vect0r.h"
/*----------------------------------------------------------------------------*/
#include <stdlib.h>
#include <math.h>
/*----------------------------------------------------------------------------*/
void vector_init(my1vector_t* pvec) {
	pvec->size = 0;
	pvec->fill = 0;
	pvec->data = 0x0;
}
/*----------------------------------------------------------------------------*/
void vector_free(my1vector_t* pvec) {
	if (pvec->data) {
		free((void*)pvec->data);
		pvec->size = 0;
		pvec->fill = 0;
	}
}
/*----------------------------------------------------------------------------*/
void vector_make(my1vector_t* pvec, int size) {
	complex_t *temp;
	if (size>0&&size!=pvec->size) {
		temp = (complex_t*) realloc(pvec->data,sizeof(complex_t)*size);
		if (temp) {
			if (pvec->fill>size)
				pvec->fill = size; /* truncated! */
			pvec->size = size;
			pvec->data = temp;
		}
	}
}
/*----------------------------------------------------------------------------*/
void vector_copy(my1vector_t* dst, my1vector_t* src) {
	int loop, size = src->fill;
	vector_make(dst,size);
	for (loop=0;loop<size;loop++)
		dst->data[loop] = src->data[loop];
	dst->fill = size;
}
/*----------------------------------------------------------------------------*/
void vector_fill(my1vector_t* pvec, double real, double imag) {
	while (pvec->fill<pvec->size) {
		pvec->data[pvec->fill].real = real;
		pvec->data[pvec->fill].imag = imag;
		pvec->fill++;
	}
}
/*----------------------------------------------------------------------------*/
void vector_append(my1vector_t* pvec, double real, double imag) {
	if (pvec->fill==pvec->size)
		vector_make(pvec,pvec->size+1); /* increase size by 1 */
	pvec->data[pvec->fill].real = real;
	pvec->data[pvec->fill].imag = imag;
	pvec->fill++;
}
/*----------------------------------------------------------------------------*/
void vector_conjugate(my1vector_t* pvec) {
	int loop, size = pvec->fill;
	for (loop=0;loop<size;loop++)
		complex_conjugate(&pvec->data[loop]);
}
/*----------------------------------------------------------------------------*/
void vector_zero_pad(my1vector_t* pvec) {
	while (pvec->fill<pvec->size)
		complex_zero(&pvec->data[pvec->fill++]);
}
/*----------------------------------------------------------------------------*/
void vector_zero_pad_fill(my1vector_t* pvec, int from, int stop) {
	int loop, size = pvec->size;
	for (loop=from;loop<size&&loop<stop;loop++)
		complex_zero(&pvec->data[loop]);
}
/*----------------------------------------------------------------------------*/
void vector_norm(my1vector_t* pvec, int offs) {
	double temp;
	int loop, size;
	/* zero out all before offset e.g. dc @0? */
	for (loop=0;loop<offs;loop++)
		complex_zero(&pvec->data[loop]);
	temp = complex_magnitude(&pvec->data[loop]);
	if (fabs(temp)<DO_PRECISION) return; /* do we need this? */
	size = pvec->fill;
	for (;loop<size;loop++) {
		pvec->data[loop].real /= temp;
		pvec->data[loop].imag /= temp;
	}
}
/*----------------------------------------------------------------------------*/
void vector_copy_some(my1vector_t* dst, my1vector_t* src, int skip, int size) {
	int loop, step;
	complex_t zero;
	zero.real = 0.0; zero.imag = 0.0;
	vector_make(dst,size);
	for (loop=0,step=skip;loop<size;loop++,step++) {
		if (step<src->fill) dst->data[loop] = src->data[step];
		else dst->data[loop] = zero;
	}
	dst->fill = size;
}
/*----------------------------------------------------------------------------*/
void vector_copy_band(my1vector_t* dst, my1vector_t* src, int size) {
	int loop, step, half;
	vector_make(dst,size); /* size>0 && size<=src->fill */
	dst->fill = size;
	half = size >> 1;
	for (loop=0;loop<half;loop++)
		dst->data[loop] = src->data[loop];
	step = src->fill-half;
	if (size%2) step--;
	while (loop<size)
		dst->data[loop++] = src->data[step++];
}
/*----------------------------------------------------------------------------*/
void vector_copy_append(my1vector_t* dst, my1vector_t* src) {
	int loop, size;
	size = src->fill;
	for (loop=0;loop<size;loop++) {
		if (dst->fill<dst->size)
			dst->data[dst->fill++] = src->data[loop];
		else break;
	}
}
/*----------------------------------------------------------------------------*/
void vector_scale(my1vector_t* pvec, double vscale) {
	int loop;
	for (loop=0;loop<pvec->fill;loop++) {
		pvec->data[loop].real *= vscale;
		pvec->data[loop].imag *= vscale;
	}
}
/*----------------------------------------------------------------------------*/
void vector_summation(my1vector_t* sums, my1vector_t* more) {
	int loop, size;
	size = more->fill;
	if (size>sums->fill) size = sums->fill;
	for (loop=0;loop<size;loop++) {
		sums->data[loop].real += more->data[loop].real;
		sums->data[loop].imag += more->data[loop].imag;
	}
}
/*----------------------------------------------------------------------------*/
void vector_difference(my1vector_t* diff, my1vector_t* less) {
	int loop, size;
	size = less->fill;
	if (size>diff->fill) size = diff->fill;
	for (loop=0;loop<size;loop++) {
		diff->data[loop].real -= less->data[loop].real;
		diff->data[loop].imag -= less->data[loop].imag;
	}
}
/*----------------------------------------------------------------------------*/
void vector_dotproduct(my1vector_t* pvec, my1vector_t* vec1) {
	int loop, size;
	double real,imag;
	size = vec1->fill;
	if (size>pvec->fill) size = pvec->fill;
	for (loop=0;loop<size;loop++) {
		real = pvec->data[loop].real;
		imag = pvec->data[loop].imag;
		pvec->data[loop].real = (real*vec1->data[loop].real) -
			(imag*vec1->data[loop].imag);
		pvec->data[loop].imag = (imag*vec1->data[loop].real) +
			(real*vec1->data[loop].imag);
	}
	/* zero the rest (assume multiply by zero?) */
	while (loop<pvec->size)
		complex_zero(&pvec->data[loop++]);
}
/*----------------------------------------------------------------------------*/
void vector_average(my1vector_t* pvec) {
	int loop, size = pvec->fill;
	for (loop=0;loop<size;loop++) {
		pvec->data[loop].real /= size;
		pvec->data[loop].imag /= size;
	}
}
/*----------------------------------------------------------------------------*/
void vector_magnitude(my1vector_t* pvec) {
	int loop, size = pvec->fill;
	for (loop=0;loop<size;loop++) {
		pvec->data[loop].real = complex_magnitude(&pvec->data[loop]);
		pvec->data[loop].imag = 0.0;
	}
}
/*----------------------------------------------------------------------------*/
void vector_make_polar(my1vector_t* pvec) {
	int loop, size = pvec->fill;
	for (loop=0;loop<size;loop++)
		complex_make_polar(&pvec->data[loop]);
}
/*----------------------------------------------------------------------------*/
void vector_make_cartesian(my1vector_t* pvec) {
	int loop, size = pvec->fill;
	for (loop=0;loop<size;loop++)
		complex_make_cartesian(&pvec->data[loop]);
}
/*----------------------------------------------------------------------------*/
void vector_zero_threshold(my1vector_t* pvec, double threshold) {
	int loop, size = pvec->fill;
	for (loop=0;loop<size;loop++)
		complex_zero_threshold(&pvec->data[loop],threshold);
}
/*----------------------------------------------------------------------------*/
#if defined(TEST_MODULE) && defined(TEST_VECT0R)
/*----------------------------------------------------------------------------*/
#include <stdio.h>
/*----------------------------------------------------------------------------*/
#include "my1complex1.c"
/*----------------------------------------------------------------------------*/
void print_complex(complex_t* num) {
	if (!num->imag) printf("%g",num->real);
	else printf("(%g,%gi)[%g]",num->real,num->imag,
		complex_magnitude(num));
}
/*----------------------------------------------------------------------------*/
void vector_show(my1vector_t* pvec, char* str) {
	int loop, size = pvec->fill;
	printf("%s{%p}[%d/%d]: ",str,pvec,size,pvec->size);
	for (loop=0;loop<size;loop++) {
		if (loop) printf(", ");
		print_complex(&pvec->data[loop]);
	}
	printf("\n");
}
/*----------------------------------------------------------------------------*/
void vector_text(my1vector_t* pvec, char* str) {
	int loop, size = pvec->fill;
	printf("%s{%p}[%d/%d]:\n",str,pvec,size,pvec->size);
	for (loop=0;loop<size;loop++) {
		printf("REAL[%d]=%10lf\t",loop,pvec->data[loop].real);
		printf("IMAG[%d]=%10lf\t",loop,pvec->data[loop].imag);
		printf("ABS[%d]=%10lf\t",loop,complex_magnitude(&pvec->data[loop]));
		printf("ARG[%d]=%10lf\n",loop,complex_phase(&pvec->data[loop]));
	}
}
/*----------------------------------------------------------------------------*/
#define DATASIZE 8
/*----------------------------------------------------------------------------*/
void vector_real_sequence(my1vector_t* pvec, int fill, int offs, int step) {
	int loop, next, stop;
	pvec->fill = 0; stop = fill*step;
	for (loop=0,next=offs;loop<pvec->size;loop++,next+=step) {
		if (next>stop) next -= stop;
		vector_append(pvec,loop<fill?(double)next:(double)0,(double)0);
	}
}
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	my1vector_t test, buff, next;
	/* print tool info */
	printf("\nTest program for my1vect0r module\n");
	printf("  => by azman@my1matrix.org\n\n");
	/* initialize */
	vector_init(&test);
	vector_init(&buff);
	vector_init(&next);
	vector_make(&test,DATASIZE);
	/* prepare input test values */
	vector_real_sequence(&test,4,1,1);
	/* show it! */
	vector_show(&test,"DAT");
	printf("\n");
	/* tests */
	vector_fill(&test,0.0,0.0);
	test.data[0].real = (double)1;
	vector_text(&test,"TEST1");
	vector_text(&buff,"BUFF1");
	test.data[0].real = (double)0;
	test.data[1].real = (double)1;
	vector_text(&test,"TEST2");
	vector_text(&buff,"BUFF1");
	printf("\n");
	/* release resource */
	vector_free(&next);
	vector_free(&test);
	vector_free(&buff);
	/* done! */
	return 0;
}
/*----------------------------------------------------------------------------*/
#endif
/*----------------------------------------------------------------------------*/
