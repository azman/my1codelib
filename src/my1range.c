/*----------------------------------------------------------------------------*/
#include "my1range.h"
/*----------------------------------------------------------------------------*/
#include <stdlib.h>
/*----------------------------------------------------------------------------*/
void range_init(my1range_t* that) {
	that->size = 0;
	that->fill = 0;
	that->data = 0x0;
}
/*----------------------------------------------------------------------------*/
void range_free(my1range_t* that) {
	if (that->data) {
		free((void*)that->data);
		that->size = 0;
		that->fill = 0;
	}
}
/*----------------------------------------------------------------------------*/
void range_make(my1range_t* that, int size) {
	double *temp;
	if (size>0&&size!=that->size) {
		temp = (double*) realloc(that->data,sizeof(double)*size);
		if (temp) {
			if (that->fill>size)
				that->fill = size; /* data truncated! */
			that->size = size;
			that->data = temp;
		}
	}
}
/*----------------------------------------------------------------------------*/
void range_fill(my1range_t* that, double fill) {
	int loop, size = that->size;
	for (loop=0;loop<size;loop++)
		that->data[loop] = fill;
	that->fill = size;
}
/*----------------------------------------------------------------------------*/
void range_slip(my1range_t* that, double data) {
	/* fill <= size! */
	if (that->fill==that->size)
		range_make(that,that->size+1); /* increase size by 1 */
	that->data[that->fill++] = data;
}
/*----------------------------------------------------------------------------*/
void range_more(my1range_t* that, double* psrc, int size) {
	int loop;
	int temp = that->fill + size;
	if (temp>=that->size)
		range_make(that,temp);
	for (loop=0;loop<size;loop++)
		that->data[that->fill++] = psrc[loop];
}
/*----------------------------------------------------------------------------*/
void range_copy(my1range_t* that, my1range_t* from) {
	int loop, size;
	size = from->fill;
	if (that->size<size)
		range_make(that,size);
	for (loop=0;loop<size;loop++)
		that->data[loop] = from->data[loop];
	that->fill = size;
}
/*----------------------------------------------------------------------------*/
void range_offset(my1range_t* that, double offs) {
	int loop, size;
	size = that->fill;
	for (loop=0;loop<size;loop++)
		that->data[loop] += offs;
}
/*----------------------------------------------------------------------------*/
void range_scaled(my1range_t* that, double sval) {
	int loop, size;
	size = that->fill;
	for (loop=0;loop<size;loop++)
		that->data[loop] *= sval;
}
/*----------------------------------------------------------------------------*/
void range_add(my1range_t* that, my1range_t* from) {
	int loop, size;
	size = that->fill;
	for (loop=0;loop<size;loop++) {
		if (loop>=from->fill) break;
		that->data[loop] += from->data[loop];
	}
}
/*----------------------------------------------------------------------------*/
void range_mul(my1range_t* that, my1range_t* from) {
	int loop, size;
	size = that->fill;
	for (loop=0;loop<size;loop++) {
		if (loop>=from->fill) break;
		that->data[loop] *= from->data[loop];
	}
}
/*----------------------------------------------------------------------------*/
#if defined(TEST_MODULE) && defined(TEST_RANGE)
/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
#include "my1cstr.c"
#include "my1cstr_line.c"
#include "my1cstr_util.c"
#include "my1list.c"
#include "my1cmds.c"
#include "my1vars.c"
#include "my1shell.c"
/*----------------------------------------------------------------------------*/
#define INITIAL_SIZE 4
#define INIT_VARNAME "buff"
/*----------------------------------------------------------------------------*/
void range_show(my1range_t* data, char* name, int dp) {
	int loop, fill = data->fill, size = data->size;
	char format[32];
	if (dp<0) sprintf(format,"%%lf");
	else sprintf(format,"%%.%dlf",dp);
	printf("## %s(%d/%d):",name,fill,size);
	for (loop=0;loop<fill;loop++) {
		printf("\n   >%d: ",loop);
		printf(format,data->data[loop]);
	}
	printf("\n");
}
/*----------------------------------------------------------------------------*/
void app_free_range(my1range_t *pdat) {
	range_free(pdat);
	free((void*)pdat);
}
/*----------------------------------------------------------------------------*/
void show_data(my1var_t* pvar) {
	my1range_t *pdat = (my1range_t*) pvar->data;
	range_show(pdat,pvar->name.buff,-1);
}
/*----------------------------------------------------------------------------*/
void post_exec(my1shell_t* that) {
	printf("** Unknown command '%s' (%s)\n",that->arg0.buff,that->buff.buff);
}
/*----------------------------------------------------------------------------*/
int shell_make_data(my1shell_t* that) {
	my1var_t *pvar;
	char *pstr;
	my1cstr_t arg1, name;
	my1range_t *pchk;
	double buff;
	cstr_init(&arg1);
	cstr_init(&name);
	do {
		if (!that->args.fill) {
			printf("** No name for making var!\n");
			break;
		}
		cstr_shword(&that->args,&name,STRING_DELIM);
		pstr = name.buff;
		if (!var_name_valid8(pstr)) {
			printf("** Invalid var name '%s'!\n",pstr);
			break;
		}
		if (shell_find_data(that,pstr)) {
			printf("** Var {%s} exists!\n",pstr);
			break;
		}
		pchk = (my1range_t*) malloc(sizeof(my1range_t));
		if (!pchk) {
			printf("** Failed to make data for '%s'!\n",pstr);
			break;
		}
		range_init(pchk);
		range_make(pchk,INITIAL_SIZE);
		while (cstr_shword(&that->args,&arg1,STRING_DELIM)) {
			buff = atof(arg1.buff);
			if (!buff&&arg1.buff[0]!='0')
				printf("** Not a value? (%s)\n",arg1.buff);
			else range_slip(pchk,buff);
		}
		pvar = shell_data(that,pstr,pchk,app_free_range);
		if (!pvar) {
			app_free_range(pchk);
			printf("** Failed to make var '%s'!\n",pstr);
			break;
		}
		printf("-- Made {%s}\n",pvar->name.buff);
		if (pchk->fill>0)
			show_data(pvar);
		printf("\n");
	} while (0);
	cstr_free(&name);
	cstr_free(&arg1);
	return 0;
}
/*----------------------------------------------------------------------------*/
int shell_copy_data(my1shell_t* that) {
	my1var_t *pvar, *pvr2;
	char *pstr;
	my1cstr_t arg1;
	cstr_init(&arg1);
	do {
		if (!that->args.fill) {
			printf("** No copy destination!\n");
			break;
		}
		cstr_shword(&that->args,&arg1,STRING_DELIM);
		pstr = arg1.buff;
		if (!var_name_valid8(pstr)) {
			printf("** Invalid var name '%s'!\n",pstr);
			break;
		}
		if (!(pvar=shell_find_data(that,pstr))) {
			printf("** Var {%s} not found!\n",pstr);
			break;
		}
		if (!cstr_shword(&that->args,&arg1,STRING_DELIM)) {
			printf("** No copy source for '%s'!\n",pvar->name.buff);
			break;
		}
		pstr = arg1.buff;
		if (!var_name_valid8(pstr)) {
			printf("** Invalid var2 name '%s'!\n",pstr);
			break;
		}
		if (!(pvr2=shell_find_data(that,pstr))) {
			printf("** Var {%s} not found!\n",pstr);
			break;
		}
		range_copy(pvar->data,pvr2->data);
		printf("-- {%s} <= {%s}\n",pvar->name.buff,pvr2->name.buff);
		show_data(pvar);
	} while (0);
	cstr_free(&arg1);
	return 0;
}
/*----------------------------------------------------------------------------*/
int shell_voff_data(my1shell_t* that) {
	my1var_t *pvar;
	char *pstr;
	my1range_t *pchk;
	double buff;
	my1cstr_t arg1;
	cstr_init(&arg1);
	do {
		if (!that->args.fill) {
			printf("** No var to set!\n");
			break;
		}
		cstr_shword(&that->args,&arg1,STRING_DELIM);
		pstr = arg1.buff;
		if (!var_name_valid8(pstr)) {
			printf("** Invalid var name '%s'!\n",pstr);
			break;
		}
		if (!(pvar=shell_find_data(that,pstr))) {
			printf("** Var {%s} not found!\n",pstr);
			break;
		}
		if (!that->args.fill) {
			printf("** No value to set to {%s}!\n",pstr);
			break;
		}
		pchk = (my1range_t*) pvar->data;
		if (!cstr_shword(&that->args,&arg1,STRING_DELIM)) {
			printf("** No offset value for {%s}!\n",pstr);
			break;
		}
		buff = atof(arg1.buff);
		if (!buff&&arg1.buff[0]!='0') {
			printf("** Invalid offset value (%s) for {%s}\n",arg1.buff,pstr);
			break;
		}
		range_offset(pchk,buff);
		printf("-- {%s} + %s\n",pvar->name.buff,arg1.buff);
		show_data(pvar);
	} while (0);
	cstr_free(&arg1);
	return 0;
}
/*----------------------------------------------------------------------------*/
int shell_vscl_data(my1shell_t* that) {
	my1var_t *pvar;
	char *pstr;
	my1range_t *pchk;
	double buff;
	my1cstr_t arg1;
	cstr_init(&arg1);
	do {
		if (!that->args.fill) {
			printf("** No var to set!\n");
			break;
		}
		cstr_shword(&that->args,&arg1,STRING_DELIM);
		pstr = arg1.buff;
		if (!var_name_valid8(pstr)) {
			printf("** Invalid var name '%s'!\n",pstr);
			break;
		}
		if (!(pvar=shell_find_data(that,pstr))) {
			printf("** Var {%s} not found!\n",pstr);
			break;
		}
		if (!that->args.fill) {
			printf("** No value to set to {%s}!\n",pstr);
			break;
		}
		pchk = (my1range_t*) pvar->data;
		if (!cstr_shword(&that->args,&arg1,STRING_DELIM)) {
			printf("** No scale value for {%s}!\n",pstr);
			break;
		}
		buff = atof(arg1.buff);
		if (!buff&&arg1.buff[0]!='0') {
			printf("** Invalid scale value (%s) for {%s}\n",arg1.buff,pstr);
			break;
		}
		range_scaled(pchk,buff);
		printf("-- {%s} * %s\n",pvar->name.buff,arg1.buff);
		show_data(pvar);
	} while (0);
	cstr_free(&arg1);
	return 0;
}
/*----------------------------------------------------------------------------*/
int shell_vadd_data(my1shell_t* that) {
	my1var_t *pvar, *pvr2;
	char *pstr;
	my1cstr_t arg1;
	cstr_init(&arg1);
	do {
		if (!that->args.fill) {
			printf("** No copy destination!\n");
			break;
		}
		cstr_shword(&that->args,&arg1,STRING_DELIM);
		pstr = arg1.buff;
		if (!var_name_valid8(pstr)) {
			printf("** Invalid var name '%s'!\n",pstr);
			break;
		}
		if (!(pvar=shell_find_data(that,pstr))) {
			printf("** Var {%s} not found!\n",pstr);
			break;
		}
		if (!cstr_shword(&that->args,&arg1,STRING_DELIM)) {
			printf("** No copy source for '%s'!\n",pvar->name.buff);
			break;
		}
		pstr = arg1.buff;
		if (!var_name_valid8(pstr)) {
			printf("** Invalid var2 name '%s'!\n",pstr);
			break;
		}
		if (!(pvr2=shell_find_data(that,pstr))) {
			printf("** Var {%s} not found!\n",pstr);
			break;
		}
		range_add(pvar->data,pvr2->data);
		printf("-- {%s} <= {%s}\n",pvar->name.buff,pvr2->name.buff);
		show_data(pvar);
	} while (0);
	cstr_free(&arg1);
	return 0;
}
/*----------------------------------------------------------------------------*/
int shell_vmul_data(my1shell_t* that) {
	my1var_t *pvar, *pvr2;
	char *pstr;
	my1cstr_t arg1;
	cstr_init(&arg1);
	do {
		if (!that->args.fill) {
			printf("** No copy destination!\n");
			break;
		}
		cstr_shword(&that->args,&arg1,STRING_DELIM);
		pstr = arg1.buff;
		if (!var_name_valid8(pstr)) {
			printf("** Invalid var name '%s'!\n",pstr);
			break;
		}
		if (!(pvar=shell_find_data(that,pstr))) {
			printf("** Var {%s} not found!\n",pstr);
			break;
		}
		if (!cstr_shword(&that->args,&arg1,STRING_DELIM)) {
			printf("** No copy source for '%s'!\n",pvar->name.buff);
			break;
		}
		pstr = arg1.buff;
		if (!var_name_valid8(pstr)) {
			printf("** Invalid var2 name '%s'!\n",pstr);
			break;
		}
		if (!(pvr2=shell_find_data(that,pstr))) {
			printf("** Var {%s} not found!\n",pstr);
			break;
		}
		range_mul(pvar->data,pvr2->data);
		printf("-- {%s} <= {%s}\n",pvar->name.buff,pvr2->name.buff);
		show_data(pvar);
	} while (0);
	cstr_free(&arg1);
	return 0;
}
/*----------------------------------------------------------------------------*/
int shell_sets_data(my1shell_t* that) {
	my1var_t *pvar;
	char *pstr;
	my1range_t *pchk;
	double buff;
	my1cstr_t arg1;
	cstr_init(&arg1);
	do {
		if (!that->args.fill) {
			printf("** No var to set!\n");
			break;
		}
		cstr_shword(&that->args,&arg1,STRING_DELIM);
		pstr = arg1.buff;
		if (!var_name_valid8(pstr)) {
			printf("** Invalid var name '%s'!\n",pstr);
			break;
		}
		if (!(pvar=shell_find_data(that,pstr))) {
			printf("** Var {%s} not found!\n",pstr);
			break;
		}
		if (!that->args.fill) {
			printf("** No value to set to {%s}!\n",pstr);
			break;
		}
		pchk = (my1range_t*) pvar->data;
		pchk->fill = 0;
		while (cstr_shword(&that->args,&arg1,STRING_DELIM)) {
			buff = atof(arg1.buff);
			if (!buff&&arg1.buff[0]!='0')
				printf("** Not a value? (%s)\n",arg1.buff);
			else range_slip(pchk,buff);
		}
		printf("-- {%s} <= %s\n",pvar->name.buff,that->args.buff);
		show_data(pvar);
	} while (0);
	cstr_free(&arg1);
	return 0;
}
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	my1shell_t that;
	int loop;
	double buff;
	my1range_t test;
	/* print tool info */
	printf("\nTest program for my1range module\n");
	printf("  => by azman@my1matrix.org\n\n");
	/* initialize */
	range_init(&test);
	/* prepare input */
	range_make(&test,INITIAL_SIZE);
	/* initial state */
	range_show(&test,"INIT",0);
	/* get from command line */
	for (loop=1;loop<argc;loop++) {
		buff = atof(argv[loop]);
		if (!buff&&argv[loop][0]!='0')
			printf("## Not a value? (%s)\n",argv[loop]);
		else {
			range_slip(&test,buff);
			range_show(&test,"CURR",5);
		}
	}
	/* initial state */
	range_show(&test,"DONE",0);
	/* start a shell */
	shell_init(&that,"my1range");
	shell_task(&that,"list","List Variables",CMDFLAG_TASK,shell_list_vars);
	shell_task(&that,"help","List Commands",CMDFLAG_TASK,shell_list_cmds);
	shell_task(&that,"make","Make Range (e.g. make <name> [val1 ...])",
		CMDFLAG_TASK,shell_make_data);
	shell_task(&that,"copy","Copy Range (e.g. copy <vdst> <vsrc>)",
		CMDFLAG_TASK,shell_copy_data);
	shell_task(&that,"sets","Sets range (e.g. sets <var> [val1 ...])",
		CMDFLAG_TASK,shell_sets_data);
	shell_task(&that,"voff","Offset Range (e.g. voff <var> <offs>)",
		CMDFLAG_TASK,shell_voff_data);
	shell_task(&that,"vscl","Scale Range (e.g. vscl <vdst> <vsrc>)",
		CMDFLAG_TASK,shell_vscl_data);
	shell_task(&that,"vadd","Sum Range (e.g. vadd <vdst> <vsrc>)",
		CMDFLAG_TASK,shell_vadd_data);
	shell_task(&that,"vmul","Multiply Range (e.g. vmul <vdst> <vsrc>)",
		CMDFLAG_TASK,shell_vmul_data);
	that.show = show_data;
	that.post = post_exec;
	if (test.fill>0) {
		if (!shell_data(&that,INIT_VARNAME,&test,0x0))
			printf("** Failed to make init var '%s'!\n",INIT_VARNAME);
	}
	while (1) {
		shell_wait(&that);
		if (that.flag&SHELL_FLAG_SKIP) continue;
		shell_exec(&that);
		if (that.flag&SHELL_FLAG_DONE) break;
		if (that.flag&SHELL_FLAG_SKIP) continue;
		shell_post(&that);
	}
	shell_free(&that);
	printf("\nBye!\n\n");
	/* release resource */
	range_free(&test);
	/* done! */
	printf("\n");
	return 0;
}
/*----------------------------------------------------------------------------*/
#endif
/*----------------------------------------------------------------------------*/
