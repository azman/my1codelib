/*----------------------------------------------------------------------------*/
#ifndef __MY1JSON_H__
#define __MY1JSON_H__
/*----------------------------------------------------------------------------*/
/**
 *  my1json read/write JSON data format
 *  - written by azman@my1matrix.org
 *  - requires my1list (with data extension)
 */
/*----------------------------------------------------------------------------*/
#include "my1list_data.h"
/*----------------------------------------------------------------------------*/
#define JSON_VALUE 0x00
#define JSON_ARRAY 0x10
#define JSON_ASSOC 0x20
#define JSON_ARRAY_MASK 0xF0
#define JSON_VALUE_MASK 0x0F
#define JSON_VALUE_UNKNOWN 0x00
#define JSON_VALUE_STRING  0x01
#define JSON_VALUE_NUMBER  0x02
#define JSON_VALUE_BOOLEAN 0x04
/*----------------------------------------------------------------------------*/
#define ERROR_LINE_OFFSET 100
/*----------------------------------------------------------------------------*/
#define ERROR_JSON_FILE 1
#define ERROR_JSON_INIT 2
#define ERROR_JSON_FIND_KEY 3
#define ERROR_JSON_BUFF 4
#define ERROR_JSON_OKEY 5
#define ERROR_JSON_FIND_COLON 6
#define ERROR_JSON_OCOM 7
#define ERROR_JSON_SYNTAX 8
#define ERROR_JSON_TASK 9
#define ERROR_JSON_TEMP 10
#define ERROR_JSON_ITEM 11
/*----------------------------------------------------------------------------*/
typedef struct _my1json_t {
	unsigned int type;
	char* pkey; /* key string */
	char* pval; /* value string */
	struct _my1json_t* root; /* parent object (for object in my1list) */
	my1list_t data; /* list of my1json_t object */
} my1json_t;
/*----------------------------------------------------------------------------*/
void json_init(my1json_t* json);
void json_free(my1json_t* json);
void json_set_keyval(my1json_t* json, char* pkey, char* pval);
void json_add_object(my1json_t* json, my1json_t* data);
my1json_t* json_add_keyval(my1json_t* json, char* pkey, char* pval);
my1json_t* json_add_array(my1json_t* json, int type);
int json_is_valid(my1json_t* json);
int json_find_root(my1json_t* json, my1json_t* root);
void json_item_init(my1json_t* json);
my1json_t* json_item_next(my1json_t* json);
my1json_t* json_item_item(my1json_t* json, my1json_t* prev);
my1json_t* json_find_item(my1json_t* json, char* pkey); /* for JSON_ASSOC? */
char* json_make_str(my1json_t* json);
char* json_from_str(my1json_t* json, char* conv);
/*----------------------------------------------------------------------------*/
#define BUFF_SIZE 128
/*----------------------------------------------------------------------------*/
#define TASK_FIND_INIT 0
#define TASK_FIND_KEY 1
#define TASK_FIND_VALUE 2
#define TASK_FIND_COLON 3
#define TASK_FIND_COMMA 4
#define TASK_FIND_DONE 5
/*----------------------------------------------------------------------------*/
typedef struct _json_read_data_t {
	int task, line, skip, size;
	int text, test, flag, stat;
	my1json_t *curr,  *temp;
	char *pbuf, buff[BUFF_SIZE];
} json_read_data_t;
/*----------------------------------------------------------------------------*/
void json_read_data_init(json_read_data_t* data, my1json_t* json);
int json_read_data_char(json_read_data_t* data, char what);
/*----------------------------------------------------------------------------*/
#endif /** __MY1JSON_H__ */
/*----------------------------------------------------------------------------*/
