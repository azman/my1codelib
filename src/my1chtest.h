/*----------------------------------------------------------------------------*/
#ifndef __MY1CHTEST_H__
#define __MY1CHTEST_H__
/*----------------------------------------------------------------------------*/
/**
 *  my1chtest : character detection utilities
 *  - written by azman@my1matrix.org
 *  - useful function and macros for text parsing
 */
/*----------------------------------------------------------------------------*/
/* text parsing */
#define is_spacing(ch) ((ch==' ')||(ch=='\t'))
#define is_whitespace(ch) (is_spaces(ch)||(ch=='\n')||(ch=='\r'))
/* c naming rules */
#define is_number(ch) (ch>=0x30&&ch<=0x39)
#define is_letter(ch) ((ch>=0x41&&ch<=0x5a)||(ch>=0x61&&ch<=0x7a))
#define is_name_init(ch) (is_letter(ch)||ch=='_')
#define is_name(ch) (is_name_init(ch)||is_number(ch))
/*----------------------------------------------------------------------------*/
/* supported operators (the rest can be implemented as functions) */
#define OPERATOR_ADD '+'
#define OPERATOR_SUB '-'
#define OPERATOR_MUL '*'
#define OPERATOR_DIV '/'
#define OPERATOR_MOD '%'
#define OPERATOR_AND '&'
#define OPERATOR_ORR '|'
#define OPERATOR_XOR '^'
/* binary operators only! */
#define CHTEST_OPRS "+-*/%&|^"
/*----------------------------------------------------------------------------*/
#ifndef CHAR_COMPLEX
#define CHAR_COMPLEX 'i'
#endif
/*----------------------------------------------------------------------------*/
int chtest_delim(char achar, char* filter);
int chtest_cname(char *pstr);
int chtest_number(char *pstr);
char* chtest_tokenize(char *pstr, int last);
/*----------------------------------------------------------------------------*/
#endif
/*----------------------------------------------------------------------------*/
