/*----------------------------------------------------------------------------*/
#ifndef __MY1HTTP_H__
#define __MY1HTTP_H__
/*----------------------------------------------------------------------------*/
#define HTTP_HEAD_OK_SIZE 15
/*----------------------------------------------------------------------------*/
#define HTTP_HEAD_OK "HTTP/1.1 200 OK"
#define HTTP_HEAD_KO "HTTP/1.1 404 Not Found"
#define HTTP_TYPE_HTML "\r\nContent-Type:text/html"
#define HTTP_TYPE_JSON "\r\nContent-Type:application/json"
#define HTTP_HEAD_ENDS "\r\n\r\n"
/** HTTP header templates */
#define HTTP_TPLT_HEAD "%s %s HTTP/1.1"
#define HTTP_TPLT_HOST "\r\nHost: %s"
#define HTTP_TPLT_UAGT "\r\nUser-Agent: %s"
#define HTTP_TPLT_CLEN "\r\nContent-Length: %d"
/*----------------------------------------------------------------------------*/
#define HTTP_TPLT_SIZE 64
/*----------------------------------------------------------------------------*/
/* size for static buffers */
#define HTTP_BUFFSIZE 4096
#define HTTP_TEMPSIZE 256
/* for http header info */
#define HTTP_USRAGENT "MY1HTTP"
/*----------------------------------------------------------------------------*/
/* http methods - the one i use */
#define HTTP_GET 0
#define HTTP_POST 1
/*----------------------------------------------------------------------------*/
/* http content type */
#define HTTP_BODY_HTML 0
#define HTTP_BODY_JSON 1
/*----------------------------------------------------------------------------*/
#include "my1sockcli.h"
/*----------------------------------------------------------------------------*/
#define HTTP_STAT_ERROR CLIENT_ERROR
#define HTTP_STAT_SEND_ERROR (HTTP_STAT_ERROR|0x100)
#define HTTP_STAT_SEND_LESS (HTTP_STAT_ERROR|0x200)
#define HTTP_STAT_WAIT_ERROR (HTTP_STAT_ERROR|0x400)
#define HTTP_STAT_REPLY_ERROR (HTTP_STAT_ERROR|0x800)
/*----------------------------------------------------------------------------*/
#define HTTP_FLAG_DOPOST 0x01
/*----------------------------------------------------------------------------*/
typedef struct _my1http_t {
	char tmpbuf[HTTP_TEMPSIZE];
	char *txbuff, *rxbuff;
	char *host, *repl;
	int type, port;
	int txsize, rxsize;
	int txfill, rxfill;
	int flag, stat;
	my1sockcli_t send;
} my1http_t;
/*----------------------------------------------------------------------------*/
void http_init(my1http_t* http, int txsize, int rxsize);
void http_free(my1http_t* http);
void http_host(my1http_t* http, char* host, int port);
void http_prep(my1http_t* http, char* host, int port);
int http_send(my1http_t* http);
int http_wait(my1http_t* http);
void http_query_init(my1http_t* http, char* page, int type);
void http_query_done(my1http_t* http, char* mesg, int type);
void http_reply_mark(my1http_t* http);
/*----------------------------------------------------------------------------*/
#endif
/*----------------------------------------------------------------------------*/
