/*----------------------------------------------------------------------------*/
#ifndef __MY1CMDS_H__
#define __MY1CMDS_H__
/*----------------------------------------------------------------------------*/
/**
 *  my1cmd_t is a structure for a command
 *  - i.e. name, info and a function pointer for command execution
 *  my1cmds_t is a structure to manage my1cmd_t
**/
/*----------------------------------------------------------------------------*/
#define MY1CMD_NAME_SIZE 16
#define MY1CMD_INFO_SIZE 80
/*----------------------------------------------------------------------------*/
#define CMDFLAG_NONE 0x0000
#define CMDFLAG_EXIT 0x0001
#define CMDFLAG_TASK 0x0010
#define CMDFLAG_DONE CMDFLAG_EXIT
/*----------------------------------------------------------------------------*/
#define CMD_NONE CMDFLAG_NONE
#define CMD_EXIT CMDFLAG_EXIT
#define CMD_TASK CMDFLAG_TASK
#define CMD_DONE CMDFLAG_DONE
/*----------------------------------------------------------------------------*/
#include "my1type.h"
/*----------------------------------------------------------------------------*/
typedef struct _my1cmd_t {
	word32_t flag;
	int scmp; /* compare size for name: strlen+1 (include null) */
	char name[MY1CMD_NAME_SIZE];
	char info[MY1CMD_INFO_SIZE];
	ptask_t exec; /* returns int, 0...n params */
} my1cmd_t;
/*----------------------------------------------------------------------------*/
#define ACMD(pc) ((my1cmd_t*)pc)
/*----------------------------------------------------------------------------*/
#define cmd_info(pc,nf) strncpy(ACMD(pc)->info,nf,MY1CMD_INFO_SIZE); \
	ACMD(pc)->info[MY1CMD_INFO_SIZE-1] = 0x0
#define cmd_flag(pc,fl) ACMD(pc)->flag = fl
#define cmd_exit(pc) ACMD(pc)->flag |= CMDFLAG_EXIT
/*----------------------------------------------------------------------------*/
typedef struct _my1cmds_t {
	my1cmd_t *list;
	unsigned int size, fill;
	void *data;
	char *prom; /* malloc'ed prompt */
	/* temporary stuff */
	my1cmd_t *pick;
	int loop, temp; 
} my1cmds_t;
/*----------------------------------------------------------------------------*/
#define CMDS(pc) ((my1cmds_t*)pc)
#define CMDX(pc) CMDS(pc)->pick
/*----------------------------------------------------------------------------*/
#define cmds_done(pc) (CMDX(pc)&&(CMDX(pc)->flag&CMDFLAG_EXIT))
#define cmds_data(pc,pd) CMDS(pc)->data = (void*)pd
/*----------------------------------------------------------------------------*/
void cmds_init(my1cmds_t*);
void cmds_free(my1cmds_t*);
void cmds_prompt(my1cmds_t*,char*);
my1cmd_t* cmds_size(my1cmds_t*,int);
my1cmd_t* cmds_find(my1cmds_t*,char*);
my1cmd_t* cmds_make(my1cmds_t*,char*,char*,word32_t,ptask_t);
/*----------------------------------------------------------------------------*/
#endif /** __MY1CMDS_H__ */
/*----------------------------------------------------------------------------*/
