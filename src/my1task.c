/*----------------------------------------------------------------------------*/
#include "my1task.h"
/*----------------------------------------------------------------------------*/
void task_init(my1task_t* task, word32_t type) {
	task->type = type;
	task->flag = 0;
	task->task = 0x0;
	task->data = 0x0;
	task->temp = 0x0;
}
/*----------------------------------------------------------------------------*/
void task_make(my1task_t* task, ptask_t func, pdata_t data) {
	task->task = func;
	task->data = data;
}
/*----------------------------------------------------------------------------*/
int task_call(my1task_t* task, pdata_t that, pdata_t xtra) {
	if (!task->task) return 0;
	task->flag = task->task((void*)task,that,xtra);
	return 1;
}
/*----------------------------------------------------------------------------*/
#if defined(TEST_MODULE) && defined(TEST_TASK)
/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libgen.h>
/*----------------------------------------------------------------------------*/
int print_message(my1task_t* task, pdata_t that, pdata_t xtra) {
	char* message = (char*) that;
	printf("-- Message: %s\n",message);
	return 0;
}
/*----------------------------------------------------------------------------*/
int print_hello_user(my1task_t* task, pdata_t that, pdata_t xtra) {
	char* user = (char*) task->data;
	printf("** Hello, %s\n",user);
	return 1;
}
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	char *buff;
	int loop;
	my1task_t test;
	/* print tool info */
	printf("\nTest program for my1task module\n");
	printf("  => by azman@my1matrix.org\n\n");
	/* initialize */
	task_init(&test,0);
	/* get from command line */
	for (loop=1;loop<argc;loop++) {
		if (!strncmp(argv[loop],"--hello",7)) {
			task_make(&test,print_hello_user,argv[++loop]);
			task_call(&test,0x0,0x0);
			printf("## Task return value: %d\n",test.flag);
		} else if (!strncmp(argv[loop],"--message",9)) {
			task_make(&test,print_message,0x0);
			task_call(&test,argv[++loop],0x0);
			printf("## Task return value: %d\n",test.flag);
		} else
			printf("Unknown request? [%s]\n",argv[loop]);
	}
	if (argc<2) {
		buff = strdup(argv[0]);
		printf("Usage: %s <request>\n\n",basename(buff));
		free((void*)buff);
		printf("Requests:\n");
		printf("  --message [message-to-show]\n");
		printf("  --hello [person-to-say-hello-to]\n");
	}
	/* done! */
	printf("\n");
	return 0;
}
/*----------------------------------------------------------------------------*/
#endif
/*----------------------------------------------------------------------------*/
