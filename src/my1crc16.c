/*----------------------------------------------------------------------------*/
#include "my1crc16.h"
/*----------------------------------------------------------------------------*/
/* free/malloc */
#include <stdlib.h>
/*----------------------------------------------------------------------------*/
void crc16_init(my1crc16_t* pcrc) {
	pcrc->ccrc = CRC16_CCITT_INIT;
	pcrc->poly = CRC16_CCITT_POLY;
	pcrc->fxor = CRC16_CCITT_FXOR;
	pcrc->flag = CRC16_CCITT_FLAG;
	pcrc->ptab = 0x0;
}
/*----------------------------------------------------------------------------*/
void crc16_free(my1crc16_t* pcrc) {
	if (pcrc->ptab) {
		free((void*)pcrc->ptab);
		pcrc->ptab = 0x0;
	}
}
/*----------------------------------------------------------------------------*/
byte08_t reverse_byte08(byte08_t data) {
	byte08_t brev;
	int loop;
	for (loop=0,brev=0;loop<8;loop++) {
		brev <<= 1;
		brev |= data & 0x1;
		data >>= 1;
	}
	return brev;
}
/*----------------------------------------------------------------------------*/
word16_t reverse_word16(word16_t data) {
	word16_t brev;
	int loop;
	for (loop=0,brev=0;loop<16;loop++) {
		brev <<= 1;
		brev |= data & 0x1;
		data >>= 1;
	}
	return brev;
}
/*----------------------------------------------------------------------------*/
void crc16_data(my1crc16_t* pcrc, byte08_t data) {
	int loop;
	word16_t temp;
	if (pcrc->flag&CRC16_FLAG_IREF)
		data = reverse_byte08(data);
	temp = (word16_t)data<<8;
	/* process big endian */
	pcrc->ccrc ^= temp;
	for (loop=0;loop<8;loop++) {
		if (pcrc->ccrc&0x8000)
			pcrc->ccrc = (pcrc->ccrc<<1)^pcrc->poly;
		else pcrc->ccrc <<= 1;
	}
}
/*----------------------------------------------------------------------------*/
void crc16_make_ptab(my1crc16_t* pcrc) {
/**
 * - validated against:
 *    http://www.sunshine2k.de/coding/javascript/crc/crc_js.html
**/
	int loop, iter;
	word16_t test;
	if (!pcrc->ptab) pcrc->ptab = (word16_t*) malloc(256*sizeof(word16_t));
	/* assume pcrc->ptab size is always 256 */
	for (loop=0;loop<256;loop++) {
		test = (word16_t)(loop&0xff)<<8;
		for (iter=0;iter<8;iter++) {
			if (test&0x8000) test = (test<<1)^pcrc->poly;
			else test <<= 1;
		}
		pcrc->ptab[loop] = test;
	}
}
/*----------------------------------------------------------------------------*/
void crc16_data_ptab(my1crc16_t* pcrc, byte08_t data) {
	if (!pcrc->ptab) crc16_make_ptab(pcrc);
	if (pcrc->flag&CRC16_FLAG_IREF)
		data = reverse_byte08(data);
	pcrc->ccrc = (pcrc->ccrc<<8)^pcrc->ptab[((pcrc->ccrc>>8)^data)];
}
/*----------------------------------------------------------------------------*/
typedef void (*crc16_data_pick_t)(my1crc16_t* pcrc, byte08_t data);
/*----------------------------------------------------------------------------*/
void crc16_calc(my1crc16_t* pcrc, byte08_t* pdat, int size) {
	crc16_data_pick_t pick = crc16_data;
	if (pcrc->ptab) pick = crc16_data_ptab;
	while (size>0) {
		pick(pcrc,*pdat);
		pdat++; size--;
	}
	if (pcrc->flag&CRC16_FLAG_OREF)
		pcrc->ccrc = reverse_word16(pcrc->ccrc);
	if (pcrc->fxor)
		pcrc->ccrc ^= pcrc->fxor;
}
/*----------------------------------------------------------------------------*/
#if defined(TEST_MODULE) && defined(TEST_CRC16)
/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
/*----------------------------------------------------------------------------*/
#include "my1bytes.c"
/*----------------------------------------------------------------------------*/
#define FLAG_SHOW_TABLE 0x01
#define FLAG_EXEC_TABLE 0x02
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	int loop, test;
	unsigned int temp, flag;
	my1crc16_t mcrc, scrc;
	my1bytes_t buff;
	crc16_init(&mcrc);
	crc16_init(&scrc);
	bytes_init(&buff);
	test = 0; flag = 0;
	putchar('\n');
	for (loop=1;loop<argc;loop++) {
		if (!strncmp(argv[loop],"--init",7)) {
			if (++loop<argc) {
				sscanf(argv[loop],"%x",&temp);
				mcrc.ccrc = (word16_t)temp;
			}
		}
		else if (!strncmp(argv[loop],"--poly",7)) {
			if (++loop<argc) {
				sscanf(argv[loop],"%x",&temp);
				mcrc.poly = (word16_t)temp;
			}
		}
		else if (!strncmp(argv[loop],"--fxor",7)) {
			if (++loop<argc) {
				sscanf(argv[loop],"%x",&temp);
				mcrc.fxor = (word16_t)temp;
			}
		}
		else if (!strncmp(argv[loop],"--no-refout",12))
			mcrc.flag &= ~CRC16_FLAG_OREF;
		else if (!strncmp(argv[loop],"--no-refin",11))
			mcrc.flag &= ~CRC16_FLAG_IREF;
		else if (!strncmp(argv[loop],"--dhex",7)) {
			mcrc.flag |= CRC16_FLAG_IHEX;
			test = loop + 1;
			break;
		}
		else if (!strncmp(argv[loop],"--data",7)) {
			test = loop + 1;
			break;
		}
		else if (!strncmp(argv[loop],"--show-table",13))
			flag |= FLAG_SHOW_TABLE;
		else if (!strncmp(argv[loop],"--exec-table",13))
			flag |= FLAG_EXEC_TABLE;
	}
	/* show these anyways */
	printf("-- CRC16 Poly: 0x%04x!\n",mcrc.poly);
	printf("-- CRC16 Init: 0x%04x!\n",mcrc.ccrc);
	printf("-- CRC16 fXOR: 0x%04x!\n",mcrc.fxor);
	printf("-- CRC16 iREF: %s!\n",
		(mcrc.flag&CRC16_FLAG_IREF)?"true":"false");
	printf("-- CRC16 oREF: %s!\n",
		(mcrc.flag&CRC16_FLAG_OREF)?"true":"false");
	/* make a copy */
	scrc.poly = mcrc.poly;
	scrc.ccrc = mcrc.ccrc;
	scrc.fxor = mcrc.fxor;
	if (flag&FLAG_SHOW_TABLE) {
		crc16_make_ptab(&mcrc);
		printf("\n## Table:\n");
		for (loop=0;loop<256;loop++) {
			if (!(loop%16)) printf("\n%04x0 -",loop/16);
			printf(" %04x",mcrc.ptab[loop]);
		}
		printf("\n");
	}
	if (test) {
		printf("\nData: ");
		loop = test;
		while (loop<argc) {
			if (argv[loop][0]=='0'&&(argv[loop][1]=='x'||argv[loop][1]=='X')) {
				if(sscanf(argv[loop],"%x",&test)!=1)
					printf("\n** Error reading value '%s'!\n",argv[loop]);
			}
			else if (mcrc.flag&CRC16_FLAG_IHEX) {
				if(sscanf(argv[loop],"%x",&test)!=1)
					printf("\n** Error reading value '%s'!\n",argv[loop]);
			}
			else {
				if(sscanf(argv[loop],"%d",&test)!=1)
					printf("\n** Error reading value '%s'!\n",argv[loop]);
			}
			if(test<0||test>0xff)
				printf("\n** Invalid byte '%s'!\n",argv[loop]);
			printf("%d (0x%02x) ",test,test);
			bytes_slip(&buff,test&0xff);
			if (flag&FLAG_EXEC_TABLE)
				crc16_data_ptab(&mcrc,(byte08_t)test&0xff);
			else
				crc16_data(&mcrc,(byte08_t)test&0xff);
			loop++;
		}
		printf("\n\n");
		if (mcrc.flag&CRC16_FLAG_OREF) {
			printf("## Reversing: 0x%04x\n",mcrc.ccrc);
			mcrc.ccrc = reverse_word16(mcrc.ccrc);
		}
		if (mcrc.fxor) {
			printf("## Final XOR: 0x%04x\n",mcrc.fxor);
			mcrc.ccrc ^= mcrc.fxor;
		}
		printf("-- CRC16 Data: 0x%04x\n",mcrc.ccrc);
		/* restore initial value */
		mcrc.ccrc = scrc.ccrc;
		crc16_calc(&mcrc,buff.data,buff.fill);
		printf("-- CRC16 Calc: 0x%04x\n",mcrc.ccrc);
	}
	else printf("\n## No data to process!\n");
	putchar('\n');
	bytes_free(&buff);
	crc16_free(&mcrc);
	return 0;
}
/*----------------------------------------------------------------------------*/
/**
 *
Personal notes on crc16: got this from somewhere...

KERMIT
width=16 poly=0x1021 init=0x0000 refin=true refout=true xorout=0x0000
check=0x2189 name="KERMIT"

XMODEM
width=16 poly=0x1021 init=0x0000 refin=false refout=false xorout=0x0000
check=0x31c3 name="XMODEM"

CRC-16/CCITT-FALSE
width=16 poly=0x1021 init=0xffff refin=false refout=false xorout=0x0000
check=0x29b1 name="CRC-16/CCITT-FALSE"

*note:*
refin:reflected input
refout:reflected output
check:crc for input string "123456789"
 *
**/
/*----------------------------------------------------------------------------*/
#endif /** defined(TEST_MODULE) && defined(TEST_CRC16) */
/*----------------------------------------------------------------------------*/
