/*----------------------------------------------------------------------------*/
#ifndef __MY1PATH_LIST_H__
#define __MY1PATH_LIST_H__
/*----------------------------------------------------------------------------*/
/**
 *  extension for my1path_t to enable listing
 *  - requires my1list_t
**/
/*----------------------------------------------------------------------------*/
#include "my1path.h"
#include "my1list.h"
/*----------------------------------------------------------------------------*/
#define PATH_TRAP_ERROR_ODIR (PATH_TRAP_ERROR|0x08)
#define PATH_TRAP_ERROR_LMEM (PATH_TRAP_ERROR|0x10)
#define PATH_TRAP_ERROR_LIST (PATH_TRAP_ERROR|0x20)
#define PATH_TRAP_ERROR_SLST (PATH_TRAP_ERROR|0x40)
/*----------------------------------------------------------------------------*/
#define PATH_LIST_PATH PATH_FLAG_PATH
#define PATH_LIST_FILE PATH_FLAG_FILE
#define PATH_LIST_BOTH (PATH_LIST_PATH|PATH_LIST_FILE)
/*----------------------------------------------------------------------------*/
void path_dolist(my1path_t* path, my1list_t* list, int list_flag);
/*----------------------------------------------------------------------------*/
/* useful helper functions */
char* path_getenv(my1list_t* list);
/*----------------------------------------------------------------------------*/
#endif /** __MY1PATH_LIST_H__ */
/*----------------------------------------------------------------------------*/
