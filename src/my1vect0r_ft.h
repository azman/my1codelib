/*----------------------------------------------------------------------------*/
#ifndef __MY1VECTOR_FT_H__
#define __MY1VECTOR_FT_H__
/*----------------------------------------------------------------------------*/
/**
 *  my1vect0r extension for fourier transforms
 *  - written by azman@my1matrix.org
**/
/*----------------------------------------------------------------------------*/
#include "my1vect0r.h"
/*----------------------------------------------------------------------------*/
/* 5n precision for dft - can be user-defined */
#ifndef DFT_PRECISION
#define DFT_PRECISION (5*DO_PRECISION_01N)
#endif
#ifndef FFT_PRECISION
#define FFT_PRECISION (5*DO_PRECISION_01N)
#endif
/*----------------------------------------------------------------------------*/
/* opts for vector_dft_base */
#define DFT_EXP_NEGATIVE 0x01
#define DFT_EXEC_AVERAGE 0x02
/* opt to calc windowed dft */
#define DFT_CALC_FRQBAND 0x04
/*----------------------------------------------------------------------------*/
/** comment this to force using exponent instead of sine/cos term */
#define USE_TRIGFUNCTION
/*----------------------------------------------------------------------------*/
/*
 * dft & idft only need to be:
 *  - at opposite exponent signs (e.g. dft can use +ve, then idft use -ve)
 *  - product of normalization factors should be 1/N (e.g. {1,1/N}{1/N,1})
*/
#define vector_dft(vec,out) vector_dft_base(vec,out,DFT_EXP_NEGATIVE)
#define vector_dft_inv(vec,out) vector_dft_base(vec,out,DFT_EXEC_AVERAGE)
/*----------------------------------------------------------------------------*/
void vector_freq_shift(my1vector_t* vec);
void vector_freq_window(my1vector_t* vec, int size);
void vector_freq_insert(my1vector_t* vec, int size);
void vector_dft_base(my1vector_t* vec, my1vector_t* out, int opts);
void vector_fft(my1vector_t* vec, my1vector_t* out);
void vector_fft_inv(my1vector_t* vec, my1vector_t* out);
/*----------------------------------------------------------------------------*/
#endif /** __MY1VECTOR_FT_H__ */
/*----------------------------------------------------------------------------*/
