/*----------------------------------------------------------------------------*/
#ifndef __MY1MAT_H__
#define __MY1MAT_H__
/*----------------------------------------------------------------------------*/
/**
 *  my1mat_t for matrix representation & operations
 *  - written by azman@my1matrix.org
 *  - requires my1num
 */
/*----------------------------------------------------------------------------*/
#include "my1num.h"
/*----------------------------------------------------------------------------*/
#define MAT_OK 0
#define MAT_ERROR (~(~0U>>1))
#define MAT_ERROR_STR2 (MAT_ERROR|0x0001)
#define MAT_ERROR_SIZE (MAT_ERROR|0x0002)
/*----------------------------------------------------------------------------*/
typedef struct _my1mat_t {
	my1num_t *data; /* 1-d vector for 2/3-d data */
	int size; /* memory is cheap - precalculate this! */
	unsigned int stat, tpad;
	int cols,rows,lays;
} my1mat_t;
/*----------------------------------------------------------------------------*/
void mat_init(my1mat_t* mat);
void mat_free(my1mat_t* mat);
void mat_str2(my1mat_t* mat,char* str);
my1mat_t* mat_clone(my1mat_t* mat);
my1num_t* mat_resize(my1mat_t* mat, int width, int height, int depth);
/** 2-d functions */
my1num_t* mat_getrow(my1mat_t* mat, int row);
void mat_copy(my1mat_t* dst, my1mat_t* src);
void mat_move(my1mat_t* dst, my1mat_t* src);
void mat_zero(my1mat_t* mat);
void mat_identity(my1mat_t* mat); /* must be square! */
void mat_transpose(my1mat_t* mat);
void mat_add(my1mat_t* dst, my1mat_t* mat1, my1mat_t* mat2);
void mat_subtract(my1mat_t* dst, my1mat_t* mat1, my1mat_t* mat2);
void mat_multiply(my1mat_t* dst, my1mat_t* mat1, my1mat_t* mat2); /* cross */
void mat_multinum(my1mat_t* dst, my1mat_t* mat1, my1mat_t* mat2); /* dot */
void mat_divide(my1mat_t* dst, my1mat_t* mat1, my1mat_t* mat2);
void mat_divrem(my1mat_t* dst, my1mat_t* mat1, my1mat_t* mat2); /* remainder */
void mat_power(my1mat_t* dst, my1mat_t* mat1, my1mat_t* mat2); /* imag = 0! */
/* math functions */
void mat_log(my1mat_t* dst, my1mat_t* src);
void mat_log10(my1mat_t* dst, my1mat_t* src);
void mat_sin(my1mat_t* dst, my1mat_t* src);
void mat_cos(my1mat_t* dst, my1mat_t* src);
void mat_tan(my1mat_t* dst, my1mat_t* src);
void mat_rad(my1mat_t* dst, my1mat_t* src);
void mat_deg(my1mat_t* dst, my1mat_t* src);
/*----------------------------------------------------------------------------*/
#endif
/*----------------------------------------------------------------------------*/
