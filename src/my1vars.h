/*----------------------------------------------------------------------------*/
#ifndef __MY1VARS_H__
#define __MY1VARS_H__
/*----------------------------------------------------------------------------*/
/**
 *  my1var - named variable
 *  - written by azman@my1matrix.org
 *  - requires my1cstr
 *  my1vars collection of my1var
 *  - written by azman@my1matrix.org
 *  - requires my1list
 */
/*----------------------------------------------------------------------------*/
#include "my1type.h"
#include "my1cstr.h"
#include "my1list.h"
/*----------------------------------------------------------------------------*/
typedef struct _my1var_t {
	pdata_t data;
	pfunc_t done; /* frees data */
	my1cstr_t name;
} my1var_t;
/*----------------------------------------------------------------------------*/
#define AVAR(pv) ((my1var_t*)pv)
/*----------------------------------------------------------------------------*/
#define var_name(pv,nn) cstr_setstr(&AVAR(pv)->buff,nn)
#define var_data(pv) ACMD(pc)->data
/*----------------------------------------------------------------------------*/
void var_init(my1var_t* pvar, char* name, pdata_t data, pfunc_t done);
void var_free(my1var_t* pvar);
int var_name_valid8(char *name);
/*----------------------------------------------------------------------------*/
typedef struct _my1vars_t {
	my1list_t list;
	/* temp stuff */
	my1var_t *pick;
	int loop, test;
} my1vars_t;
/*----------------------------------------------------------------------------*/
void vars_init(my1vars_t* vars);
void vars_free(my1vars_t* vars);
my1var_t* vars_make(my1vars_t* vars, char* name, pdata_t data, pfunc_t done);
my1var_t* vars_find(my1vars_t* vars, char* name);
my1var_t* vars_hack(my1vars_t* vars, char* name);
/*----------------------------------------------------------------------------*/
#endif /** __MY1VARS_H__ */
/*----------------------------------------------------------------------------*/
