/*----------------------------------------------------------------------------*/
#ifndef __MY1EXPR_H__
#define __MY1EXPR_H__
/*----------------------------------------------------------------------------*/
/**
 *  my1expr : math expression evaluator
 *  - written by azman@my1matrix.org
 *  - creates a tree structure of an equation (math expression) on heap
 *  - requires my1cstr, my1list
 */
/*----------------------------------------------------------------------------*/
#include "my1type.h"
#include "my1cstr.h"
/*----------------------------------------------------------------------------*/
/** set MSB of unsigned int to 1 */
#define EXPR_TYPE_ERR (~(~0U>>1))
#define EXPR_TYPE_CHK 0x0000
#define EXPR_TYPE_VAL 0x0001
#define EXPR_TYPE_VAR 0x0002
#define EXPR_TYPE_FUN 0x0004
#define EXPR_TYPE_TMP 0x0008
#define EXPR_TYPE_OPR 0x0010
#define EXPR_TYPE_BRK (EXPR_TYPE_FUN|EXPR_TYPE_TMP)
/*----------------------------------------------------------------------------*/
#define EXPR_ERROR_INVAL (EXPR_TYPE_ERR|0x000100)
#define EXPR_ERROR_EXVAL (EXPR_TYPE_ERR|0x000200)
#define EXPR_ERROR_ALLOC (EXPR_TYPE_ERR|0x000400)
#define EXPR_ERROR_NOVAR (EXPR_TYPE_ERR|0x000800)
#define EXPR_ERROR_NOOP1 (EXPR_TYPE_ERR|0x001000)
#define EXPR_ERROR_NOOP2 (EXPR_TYPE_ERR|0x002000)
#define EXPR_ERROR_NOFUN (EXPR_TYPE_ERR|0x004000)
#define EXPR_ERROR_INVEX (EXPR_TYPE_ERR|0x008000)
/*----------------------------------------------------------------------------*/
#define FLAG_OK 0
#define FLAG_MAKEERROR (FLAG_ERROR|0x800000)
#define FLAG_EMPTY_STR (FLAG_ERROR|0x000001)
#define FLAG_DOUBLE_EQ (FLAG_ERROR|0x000002)
#define FLAG_EQ_NOLEFT (FLAG_ERROR|0x000004)
#define FLAG_READ_NONE (FLAG_ERROR|0x000008)
#define FLAG_LABEL_ERR (FLAG_ERROR|0x000010)
#define FLAG_UNKNOWN_1 (FLAG_ERROR|0x000020)
#define FLAG_MISSING_B (FLAG_ERROR|0x000040)
#define FLAG_MISSING_F (FLAG_ERROR|0x000080)
#define FLAG_SYM_ERROR (FLAG_ERROR|0x000100)
#define FLAG_NOFUN_BRK (FLAG_ERROR|0x000200)
#define FLAG_EXTRA_BRK (FLAG_ERROR|0x000400)
#define FLAG_MISSING_P (FLAG_ERROR|0x000800)
#define FLAG_OPR_PARAM (FLAG_ERROR|0x001000)
#define FLAG_MISSING_O (FLAG_ERROR|0x002000)
#define FLAG_UNKNOWN_2 (FLAG_ERROR|0x004000)
#define FLAG_EVAL_STEP (FLAG_ERROR|0x008000)
#define FLAG_LEFTOVER1 (FLAG_ERROR|0x010000)
#define FLAG_LEFTOVER2 (FLAG_ERROR|0x020000)
/*----------------------------------------------------------------------------*/
typedef struct _my1expr_t {
	word32_t type;
	int temp; /* temp to align-64? holds exec return value */
	my1cstr_t tval;
	ptask_t exec; /* for actual evaluation */
	pdata_t data; /* for evaluation (exec) */
	struct _my1expr_t *opr1, *opr2, *read;
} my1expr_t;
/*----------------------------------------------------------------------------*/
void expr_init(my1expr_t* expr);
void expr_free(my1expr_t* expr);
void expr_exec(my1expr_t* expr, pdata_t chk1, pdata_t chk2);
int  expr_read(my1expr_t* expr, char* pbuf);
/*----------------------------------------------------------------------------*/
#endif
/*----------------------------------------------------------------------------*/
