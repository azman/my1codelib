/*----------------------------------------------------------------------------*/
#ifndef __MY1BYTES_H__
#define __MY1BYTES_H__
/*----------------------------------------------------------------------------*/
/**
 *  my1bytes is an array of byte (unsigned char) data
 *  - written by azman@my1matrix.org
 */
/*----------------------------------------------------------------------------*/
#include "my1type.h"
/*----------------------------------------------------------------------------*/
typedef struct  _my1bytes_t {
	byte08_t* data;
	int size, fill; /* size is storage size, fill is data count */
} my1bytes_t;
/*----------------------------------------------------------------------------*/
#define BYTES(dat) ((my1bytes_t*)dat)
#define bytes_null(that) BYTES(that)->fill = 0
/*----------------------------------------------------------------------------*/
void bytes_init(my1bytes_t* that);
void bytes_free(my1bytes_t* that);
void bytes_make(my1bytes_t* that, int size);
void bytes_fill(my1bytes_t* that, byte08_t fill);
void bytes_slip(my1bytes_t* that, byte08_t byte); /* adds 1 byte to array */
void bytes_more(my1bytes_t* that, byte08_t* psrc, int size); /* adds >1 byte */
void bytes_fill_size(my1bytes_t* that, byte08_t fill, int size);
/*----------------------------------------------------------------------------*/
#endif /** __MY1BYTES_H__ */
/*----------------------------------------------------------------------------*/
