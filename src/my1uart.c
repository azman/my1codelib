/*----------------------------------------------------------------------------*/
#include "my1uart.h"
#include <stdio.h> /* sprintf, sscanf */
#ifndef DO_MINGW
#include <string.h> /* memset */
#include <unistd.h> /* close */
#include <fcntl.h> /* OPEN_DFLAG */
#include <sys/ioctl.h> /* FIONREAD */
#include <linux/serial.h> /* struct serial_struct */
#include <sys/time.h> /* gettimeofday */
#else
/* mingw-w64 got this! */
#include <sys/time.h>
#endif
/*----------------------------------------------------------------------------*/
#ifdef DO_MINGW
#define COM_PORT_NAME "COM"
#else
#define COM_PORT_NAME "/dev/ttyUSB"
#define USLEEP_FOR_FLUSH 200000
#define OPEN_DFLAG (O_RDWR|O_NOCTTY|O_NONBLOCK)
#define INVALID_HANDLE_VALUE -1
#endif
/*----------------------------------------------------------------------------*/
void uart_init(my1uart_t* port) {
	/* reset handler/flag */
	port->hand = INVALID_HANDLE_VALUE;
	port->term = INVALID_PORT_INDEX;
	port->stat = MY1PORT_STATUS_OK;
	uart_name(port,COM_PORT_NAME);
#ifdef DO_MINGW
	/* configure 9600,N81 for COM */
	port->curr.DCBlength = sizeof(DCB);
	port->curr.BaudRate  = CBR_9600;
	port->curr.ByteSize  = 8;
	port->curr.Parity    = 0;
	port->curr.StopBits  = 0;
#else
	/* define raw settings? */
	memset((void*)&port->curr, 0x00, sizeof(port->curr));
	/* port->curr.c_iflag = 0; / * raw input? */
	/* port->curr.c_oflag = 0; / * raw output? */
	/* port->curr.c_lflag = 0; / * raw local? */
	/* port->curr.c_cflag = 0; / * clear everything first */
	/* configure 9600,N81 for ttyS */
	cfsetspeed(&port->curr,(speed_t)B9600); /* default baudrate */
	/** cfsetispeed(&port->curr,(speed_t)B9600); */
	/** cfsetospeed(&port->curr,(speed_t)B9600); */
	/* port->curr.c_cflag |= PARENB; / * enable parity */
	/* port->curr.c_cflag |= PARODD; / * use odd-parity, else even */
	/* port->curr.c_cflag |= CSTOPB; / * 2 stop bit, else 1 */
	/* port->curr.c_cflag &= ~CSIZE; / * clear size */
	port->curr.c_cflag |= CS8; /* set 8-bit char */
	port->curr.c_cflag |= CLOCAL; /* ignore modem control */
	port->curr.c_cflag |= CREAD; /* enable receiver */
	/** testing these... do we need it? maybe if using blocking mode only? */
	port->curr.c_cc[VTIME] = 1; /* timeout in 10th of a second */
	port->curr.c_cc[VMIN]  = 0; /* blocking read until 0 chars received */
#endif
}
/*----------------------------------------------------------------------------*/
void make_portname(char* buff, char* name, int term) {
#ifdef DO_MINGW
	/* needed for COM10 and above */
	*buff = '\\'; buff++;
	*buff = '\\'; buff++;
	*buff = '.'; buff++;
	*buff = '\\'; buff++;
#endif
	sprintf(buff,"%s%d",name,COM_PORT(term));
}
/*----------------------------------------------------------------------------*/
int uart_valid(my1uart_t* port, int term) {
	int done = 1;
#ifdef DO_MINGW
	HANDLE hand;
#else
	long hand;
#endif
	if (COM_PORT(term)<COM_PORT(1)||COM_PORT(term)>COM_PORT(MAX_COM_PORT))
		return 0;
	make_portname(port->temp,port->name,term);
#ifdef DO_MINGW
	hand = CreateFile(port->temp,GENERIC_READ|GENERIC_WRITE,
		0,NULL,OPEN_EXISTING,0,NULL);
#else
	hand = open(port->temp,OPEN_DFLAG);
#endif
	if (hand==INVALID_HANDLE_VALUE)
		done = 0;
	else
#ifdef DO_MINGW
		CloseHandle(hand);
#else
		close(hand);
#endif
	return done;
}
/*----------------------------------------------------------------------------*/
int uart_find(my1uart_t* port, int* count) {
	int test, step = 0, init = INVALID_PORT_INDEX;
	if (port->hand!=INVALID_HANDLE_VALUE)
		return INVALID_PORT_INDEX; /* port opened! */
	for (test=1;test<=MAX_COM_PORT;test++) {
		if (uart_valid(port,test)) {
			if (init==INVALID_PORT_INDEX)
				init = test;
			if (!count) break;
			step++;
		}
	}
	if (count) *count = step;
	return init;
}
/*----------------------------------------------------------------------------*/
int uart_prep(my1uart_t* port, int term) {
	int prep = 0;
	if (uart_valid(port,term)) {
		port->term = term;
		prep = 1;
	}
	return prep;
}
/*----------------------------------------------------------------------------*/
int uart_open(my1uart_t* port) {
	if (port->hand!=INVALID_HANDLE_VALUE||port->term==INVALID_PORT_INDEX)
		return 0;
	/* only if not opened and ready! */
	make_portname(port->temp,port->name,port->term);
#ifdef DO_MINGW
	port->hand = CreateFile(port->temp,GENERIC_READ|GENERIC_WRITE,
		0,0x0,OPEN_EXISTING,0,0x0); /* NON-OVERLAPPED */
#else
	port->hand = open(port->temp,OPEN_DFLAG);
#endif
	if (port->hand==INVALID_HANDLE_VALUE)
		return 0;
#ifdef DO_MINGW
	/* save current attributes */
	if (!GetCommState(port->hand,&port->save)) {
		CloseHandle(port->hand);
		port->hand = INVALID_HANDLE_VALUE;
		return 0;
	}
	{ /* need this to declare local variable here - classic c! */
		/* transfer current to our DCB, needs a swapbuff */
		DCB temp = port->curr;
		port->curr = port->save; /* save will be restored on close */
		/* re-assign configurable params */
		port->curr.BaudRate = temp.BaudRate;
		port->curr.ByteSize = temp.ByteSize;
		port->curr.Parity   = temp.Parity;
		port->curr.StopBits = temp.StopBits;
	}
	/* set desired attributes */
	if (!SetCommState(port->hand,&port->curr)) {
		CloseHandle(port->hand);
		port->hand = INVALID_HANDLE_VALUE;
		return 0;
	}
#else
	uart_flush(port);
	uart_purge(port);
	/** back to blocking mode! */
	fcntl(port->hand,F_SETFL,fcntl(port->hand,F_GETFL)&~O_NONBLOCK);
	/* save current attributes */
	if (tcgetattr(port->hand,&port->save)<0) {
		close(port->hand);
		port->hand = INVALID_HANDLE_VALUE;
		return 0;
	}
	/* set desired attributes */
	if (tcsetattr(port->hand,TCSANOW,&port->curr)<0) {
		close(port->hand);
		port->hand = INVALID_HANDLE_VALUE;
		return 0;
	}
#endif
	return 1; /* should be valid if we get here! */
}
/*----------------------------------------------------------------------------*/
int uart_done(my1uart_t* port) {
	int done = 1;
	if (port->hand==INVALID_HANDLE_VALUE)
		return done;
#ifdef DO_MINGW
	/* set saved original attributes */
	SetCommState(port->hand,&port->save);
	done = CloseHandle(port->hand); /* returns !0:success, 0:error! */
#else
	/* set saved original attributes */
	tcsetattr(port->hand, TCSANOW, &port->save);
	done = close(port->hand); /* returns 0:success, -1:error! */
	if (done<0) done = 0; else done = 1;
#endif
	/* no matter what, we 'release' that handle! */
	port->hand = INVALID_HANDLE_VALUE;
	return done;
}
/*----------------------------------------------------------------------------*/
int uart_isopen(my1uart_t* port) {
	return port->hand==INVALID_HANDLE_VALUE?0:1;
}
/*----------------------------------------------------------------------------*/
int uart_okstat(my1uart_t* port) {
	return port->stat!=MY1PORT_STATUS_OK?0:1;
}
/*----------------------------------------------------------------------------*/
void uart_name(my1uart_t* port,char* name) {
	/* prepare 2 spaces for portnum! */
	strncpy(port->name,name,MAX_COM_CHAR-2);
}
/*----------------------------------------------------------------------------*/
void uart_purge(my1uart_t* port) {
	if (port->hand==INVALID_HANDLE_VALUE)
		return;
#ifdef DO_MINGW
	PurgeComm(port->hand,PURGE_RXCLEAR); /* clear the input buffer */
#else
	usleep(USLEEP_FOR_FLUSH);
	tcflush(port->hand,TCIFLUSH);
	/* ioctl(port->hand,TCFLSH,TCIFLUSH); */
#endif
}
/*----------------------------------------------------------------------------*/
void uart_flush(my1uart_t* port) {
	if (port->hand==INVALID_HANDLE_VALUE)
		return;
#ifdef DO_MINGW
	FlushFileBuffers(port->hand); /* flush the transmit buffer */
#else
	usleep(USLEEP_FOR_FLUSH);
	tcflush(port->hand,TCOFLUSH);
	/* ioctl(port->hand,TCFLSH,TCOFLUSH); */
#endif
}
/*----------------------------------------------------------------------------*/
void uart_get_config(my1uart_t* port, my1uart_conf_t* conf) {
#ifdef DO_MINGW
	switch (port->curr.BaudRate) {
		case CBR_19200: conf->baud = MY1BAUD19200; break;
		case CBR_38400: conf->baud = MY1BAUD38400; break;
		case CBR_57600: conf->baud = MY1BAUD57600; break;
		case CBR_115200: conf->baud = MY1BAUD115200; break;
		case CBR_1200: conf->baud = MY1BAUD1200; break;
		case CBR_2400: conf->baud = MY1BAUD2400; break;
		case CBR_4800: conf->baud = MY1BAUD4800; break;
		default: case CBR_9600: conf->baud = MY1BAUD9600; break;
	}
	conf->size = port->curr.ByteSize;
	conf->bpar = port->curr.Parity;
	conf->stop = port->curr.StopBits;
#else
	switch (cfgetispeed(&port->curr)) {
		case B19200: conf->baud = MY1BAUD19200; break;
		case B38400: conf->baud = MY1BAUD38400; break;
		case B57600: conf->baud = MY1BAUD57600; break;
		case B115200: conf->baud = MY1BAUD115200; break;
		case B1200: conf->baud = MY1BAUD1200; break;
		case B2400: conf->baud = MY1BAUD2400; break;
		case B4800: conf->baud = MY1BAUD4800; break;
		case B9600: default: conf->baud = MY1BAUD9600; break;
	}
	switch (port->curr.c_cflag&CSIZE) {
		case CS5: conf->size = 5; break;
		case CS6: conf->size = 6; break;
		case CS7: conf->size = 7; break;
		case CS8: default: conf->size = 8; break;
	}
	switch (port->curr.c_cflag&(PARENB|PARODD)) {
		case (PARENB|PARODD): conf->bpar = MY1PARITY_ODD; break;
		case PARENB: conf->bpar = MY1PARITY_EVEN; break;
		default: conf->bpar = MY1PARITY_NONE;
	}
	switch (port->curr.c_cflag&CSTOPB) {
		case CSTOPB: conf->stop = 2; break;
		default: conf->stop = 1; break;
	}
#endif
}
/*----------------------------------------------------------------------------*/
void uart_set_config_byte(my1uart_t* port,
	byte08_t baud, byte08_t size, byte08_t bpar, byte08_t stop) {
#ifdef DO_MINGW
	switch (baud) {
		case MY1BAUD19200: port->curr.BaudRate  = CBR_19200; break;
		case MY1BAUD38400: port->curr.BaudRate  = CBR_38400; break;
		case MY1BAUD57600: port->curr.BaudRate  = CBR_57600; break;
		case MY1BAUD115200: port->curr.BaudRate  = CBR_115200; break;
		case MY1BAUD1200: port->curr.BaudRate  = CBR_1200; break;
		case MY1BAUD2400: port->curr.BaudRate  = CBR_2400; break;
		case MY1BAUD4800: port->curr.BaudRate  = CBR_4800; break;
		case MY1BAUD9600: default: port->curr.BaudRate  = CBR_9600; break;
	}
	if (size<0x9&&size>0x3) port->curr.ByteSize = size;
	if (bpar<0x05) port->curr.Parity = bpar;
	if (stop<0x03) port->curr.StopBits = stop;
#else
	switch (baud) {
		case MY1BAUD19200: cfsetspeed(&port->curr,(speed_t)B19200); break;
		case MY1BAUD38400: cfsetspeed(&port->curr,(speed_t)B38400); break;
		case MY1BAUD57600: cfsetspeed(&port->curr,(speed_t)B57600); break;
		case MY1BAUD115200: cfsetspeed(&port->curr,(speed_t)B115200); break;
		case MY1BAUD1200: cfsetspeed(&port->curr,(speed_t)B1200); break;
		case MY1BAUD2400: cfsetspeed(&port->curr,(speed_t)B2400); break;
		case MY1BAUD4800: cfsetspeed(&port->curr,(speed_t)B4800); break;
		case MY1BAUD9600: default: cfsetspeed(&port->curr,(speed_t)B9600);
	}
	port->curr.c_cflag &= ~CSIZE; /* clear size bits */
	switch (size) {
		case 5: port->curr.c_cflag |= CS5; break;
		case 6: port->curr.c_cflag |= CS6; break;
		case 7: port->curr.c_cflag |= CS7; break;
		case 8: default: port->curr.c_cflag |= CS8;
	}
	port->curr.c_cflag &= (~PARENB&~PARODD); /* clear parity flags */
	switch (bpar) {
		case MY1PARITY_ODD: port->curr.c_cflag |= (PARENB|PARODD); break;
		case MY1PARITY_EVEN: port->curr.c_cflag |= PARENB; break;
		/* not implemented? */
		case MY1PARITY_MARK: /* mark - parity always logical 1 (-12v) */
		case MY1PARITY_SPACE: /* space - parity always logical 0 (+12v) */
		/* nothing to do - cleared! */
		case MY1PARITY_NONE: break;
	}
	switch (stop) {
		case 2: /* 2 stop bit */ port->curr.c_cflag |= CSTOPB; break;
		case 1: /* 1.5 not implemented */
		case 0: /* 1 stop bit */ default: port->curr.c_cflag &= ~CSTOPB;
	}
#endif
}
/*----------------------------------------------------------------------------*/
void uart_set_config(my1uart_t* port, my1uart_conf_t* conf) {
	uart_set_config_byte(port,conf->baud,conf->size,conf->bpar,conf->stop);
}
/*----------------------------------------------------------------------------*/
int uart_open_args(my1uart_t* port ,int term,
	byte08_t baud, byte08_t size, byte08_t bpar, byte08_t stop) {
	uart_set_config_byte(port,baud,size,bpar,stop);
	return uart_open(port);
}
/*----------------------------------------------------------------------------*/
void uart_send_byte(my1uart_t* port, byte08_t data) {
#ifdef DO_MINGW
	DWORD temp;
#endif
	if (port->hand==INVALID_HANDLE_VALUE)
		return;
#ifdef DO_MINGW
	WriteFile(port->hand,&data,1,&temp,0x0);
#else
	write(port->hand,&data,1);
#endif
#ifdef MY1DEBUG
	if ((data < ' ')||(data >'~'))
		fprintf(stderr,">%02x ",data);
	else
		fprintf(stderr,">%02x@'%c' ",data,data);
#endif
}
/*----------------------------------------------------------------------------*/
byte08_t uart_read_byte(my1uart_t* port) {
	byte08_t data;
#ifdef DO_MINGW
	DWORD temp;
#endif
	if (port->hand==INVALID_HANDLE_VALUE)
		return 0;
#ifdef DO_MINGW
	if (!ReadFile(port->hand,&data,1,&temp,0x0))
#else
	if (read(port->hand,&data,1)<=0)
#endif
		data = 0;
#ifdef MY1DEBUG
	if ((data < ' ')||(data >'~'))
		fprintf(stderr,"<%02x ",data);
	else
		fprintf(stderr,"<%02x@'%c' ",data,data);
#endif
	return data;
}
/*----------------------------------------------------------------------------*/
void uart_outgoing(my1uart_t* port) {
#ifdef DO_MINGW
	return;
#else
	tcdrain(port->hand);
#endif
}
/*----------------------------------------------------------------------------*/
int uart_incoming(my1uart_t* port) {
#ifdef DO_MINGW
	COMSTAT stat;
	DWORD temp; /* ignore errors! */
#else
	int size;
#endif
	if (port->hand==INVALID_HANDLE_VALUE)
		return 0;
#ifdef DO_MINGW
	port->stat = ClearCommError(port->hand,&temp,&stat);
	if (!port->stat) port->stat = MY1PORT_STATUS_EIO;
	else port->stat = MY1PORT_STATUS_OK;
	return stat.cbInQue; /* just check input queue buffer */
#else
	/* use TIOCOUTQ instead of FIONREAD for output */
	port->stat = ioctl(port->hand,FIONREAD,&size);
	if (port->stat) port->stat = MY1PORT_STATUS_EIO;
	return size;
#endif
}
/*----------------------------------------------------------------------------*/
int uart_read_byte_timed(my1uart_t* port,int timeout_us) {
	struct timeval init, curr;
	int diff;
	gettimeofday(&init,0x0);
	while (!uart_incoming(port)) {
		gettimeofday(&curr,0x0);
		diff = curr.tv_sec-init.tv_sec;
		if (diff>0) diff *= 1000000; /** in us */
		diff += curr.tv_usec;
		diff -= init.tv_usec;
		if (diff>timeout_us)
			return UART_TIMEOUT;
	}
	return (int) uart_read_byte(port);
}
/*----------------------------------------------------------------------------*/
int uart_send_data(my1uart_t* port, byte08_t* data, int size) {
	int test;
#ifdef DO_MINGW
	DWORD temp;
#endif
	if (port->hand==INVALID_HANDLE_VALUE)
		return -1;
#ifdef DO_MINGW
	test = WriteFile(port->hand,data,size,&temp,0x0);
	if (!test) test = -1;
	else test = temp;
#else
	test = write(port->hand,data,size);
#endif
#ifdef MY1DEBUG
{
	int loop;
	for (loop=0;loop<test;loop++) {
		if ((data[loop]<' ')||(data[loop]>'~'))
			fprintf(stderr,">0x%02x\n",data[loop]);
		else
			fprintf(stderr,">0x%02x@'%c'\n",data[loop],data[loop]);
	}
}
#endif
	return test;
}
/*----------------------------------------------------------------------------*/
int uart_read_data(my1uart_t* port, byte08_t* data, int size) {
	int test;
#ifdef DO_MINGW
	DWORD temp;
#endif
	if (port->hand==INVALID_HANDLE_VALUE)
		return -1;
#ifdef DO_MINGW
	test = ReadFile(port->hand,data,size,&temp,0x0);
	if (!test) test = -1;
	else test = temp;
#else
	test = read(port->hand,data,size);
#endif
#ifdef MY1DEBUG
{
	int loop;
	for (loop=0;loop<test;loop++) {
		if ((data[loop]<' ')||(data[loop]>'~'))
			fprintf(stderr,">0x%02x\n",data[loop]);
		else
			fprintf(stderr,">0x%02x@'%c'\n",data[loop],data[loop]);
	}
}
#endif
	return test;
}
/*----------------------------------------------------------------------------*/
int uart_pin_assert(my1uart_t* port, int pin, int assert) {
	int status = 0;
#ifdef DO_MINGW
	switch (pin) {
		case MY1PIN_DTR: status = assert ? SETDTR : CLRDTR ; break;
		case MY1PIN_RTS: status = assert ? SETRTS : CLRRTS ; break;
	}
	if (status)
		status = EscapeCommFunction(port->hand,status) ? 0 : 1;
#else
	switch (pin) {
		case MY1PIN_DTR: status = TIOCM_DTR; break;
		case MY1PIN_RTS: status = TIOCM_RTS; break;
	}
	if (status) {
		assert = assert ? TIOCMBIS : TIOCMBIC;
		status = ioctl(port->hand, assert, &status);
	}
#endif
	return status;
}
/*----------------------------------------------------------------------------*/
int uart_pin_detect(my1uart_t* port, int pin, int *detect) {
	int status = 0, result = 1; /* non-zero result is failure! */
	*detect = 0; /* detection value default to 0! */
#ifdef DO_MINGW
	switch (pin) {
		case MY1PIN_DTR: status = pin; break;
		case MY1PIN_RTS: status = pin; break;
		case MY1PIN_DSR: status = MS_DSR_ON; break;
		case MY1PIN_CTS: status = MS_CTS_ON; break;
	}
	if (status) {
		if (status==MS_DSR_ON||status==MS_CTS_ON) {
			DWORD check;
			result = GetCommModemStatus(port->hand,&check) ? 0 : 1;
			if (!result) *detect = check & status;
		} else {
			result = GetCommState(port->hand,&port->curr) ? 0 : 1;
			if (!result) {
				switch (status) {
					case MY1PIN_DTR:
						if (port->curr.fDtrControl==DTR_CONTROL_ENABLE)
							*detect = 1;
						else if (port->curr.fDtrControl!=DTR_CONTROL_DISABLE)
							result = 1;
						break;
					case MY1PIN_RTS:
						if (port->curr.fRtsControl==RTS_CONTROL_ENABLE)
							*detect = 1;
						else if (port->curr.fRtsControl!=RTS_CONTROL_DISABLE)
							result = 1;
						break;
				}
			}
		}
	}
#else
	switch (pin) {
		case MY1PIN_DTR: status = TIOCM_DTR; break; /* pin 4/9 */
		case MY1PIN_RTS: status = TIOCM_RTS; break; /* pin 7/9 */
		case MY1PIN_DSR: status = TIOCM_DSR; break; /* pin 6/9 */
		case MY1PIN_CTS: status = TIOCM_CTS; break; /* pin 8/9 */
	}
	if (status) {
		result = ioctl(port->hand, TIOCMGET, detect);
		*detect = *detect & status;
	}
#endif
	return result;
}
/*----------------------------------------------------------------------------*/
int uart_actual_baudrate(int encoded) {
	int baudrate;
	switch (encoded) {
		case MY1BAUD1200: baudrate = 1200; break;
		case MY1BAUD2400: baudrate = 2400; break;
		case MY1BAUD4800: baudrate = 4800; break;
		case MY1BAUD9600: baudrate = 9600; break;
		case MY1BAUD19200: baudrate = 19200; break;
		case MY1BAUD38400: baudrate = 38400; break;
		case MY1BAUD57600: baudrate = 57600; break;
		case MY1BAUD115200: baudrate = 115200; break;
		default: baudrate = 0;
	}
	return baudrate;
}
/*----------------------------------------------------------------------------*/
int uart_encoded_baudrate(int baudrate) {
	int baudcode;
	switch (baudrate) {
		case 1200: baudcode = MY1BAUD1200; break;
		case 2400: baudcode = MY1BAUD2400; break;
		case 4800: baudcode = MY1BAUD4800; break;
		case 9600: baudcode = MY1BAUD9600; break;
		case 19200: baudcode = MY1BAUD19200; break;
		case 38400: baudcode = MY1BAUD38400; break;
		case 57600: baudcode = MY1BAUD57600; break;
		case 115200: baudcode = MY1BAUD115200; break;
		default: baudcode = -1;
	}
	return baudcode;
}
/*----------------------------------------------------------------------------*/
int uart_get_baudrate(my1uart_t* port) {
	my1uart_conf_t conf;
	uart_get_config(port,&conf);
	return uart_actual_baudrate(conf.baud);
}
/*----------------------------------------------------------------------------*/
int uart_set_baudrate(my1uart_t* port, int baud) {
	int temp;
	my1uart_conf_t conf;
	uart_get_config(port,&conf);
	temp = uart_encoded_baudrate(baud);
	if (temp<0) return 0;
	else if (temp==conf.baud) return -baud;
	conf.baud = temp;
	uart_set_config(port,&conf);
	return uart_get_baudrate(port);
}
/*----------------------------------------------------------------------------*/
int uart_set_parity(my1uart_t* port, int psel) {
	my1uart_conf_t conf;
	switch (psel) {
		case MY1PARITY_NONE: case MY1PARITY_ODD: case MY1PARITY_EVEN:
		case MY1PARITY_MARK: case MY1PARITY_SPACE:
			break;
		default: psel = MY1PARITY_NONE;
	}
	uart_get_config(port,&conf);
	conf.bpar = (byte08_t)psel;
	uart_set_config(port,&conf);
	return psel;
}
/*----------------------------------------------------------------------------*/
#if defined(TEST_MODULE) && defined(TEST_UART)
/*----------------------------------------------------------------------------*/
#include "my1keys.c"
/*----------------------------------------------------------------------------*/
#define PROGNAME "my1uart"
#define FILENAME_LEN 80
/*----------------------------------------------------------------------------*/
#define ERROR_GENERAL -1
#define ERROR_PARAM_PORT -2
#define ERROR_PARAM_BAUD -3
#define ERROR_PARAM_DEVN -4
#define ERROR_PARAM_WHAT -5
/*----------------------------------------------------------------------------*/
#define OPT_LOCALECHO 0x00000001
#define OPT_SHOWHEX 0x00000002
#define OPT_CR4LF 0x00000004
/*----------------------------------------------------------------------------*/
#define SEND_MODE_NORMAL 0
#define SEND_MODE_RETURN 1
#define SEND_QUEUE_SIZE 80
/*----------------------------------------------------------------------------*/
typedef unsigned int termopts_t;
/*----------------------------------------------------------------------------*/
void about(void) {
	printf("Use: %s [options]\n",PROGNAME);
	printf("Options are:\n");
	printf("  --port <number> : port number between 1-%d.\n",MAX_COM_PORT);
	printf("  --baud <number> : baudrate e.g. 9600(default),38400,115200.\n");
	printf("  --tty <device>  : alternate device name (useful in Linux).\n");
	printf("  --help  : show this message. overrides other options.\n");
	printf("  --scan  : scan and list ports. overrides other options.\n");
	printf("  --qsend : use send queue.\n");
	printf("  --echo  : generate local echo.\n");
	printf("  --num   : display number (hex) instead of text.\n");
	printf("  --dos   : msdos terminal - send cr before lf.\n\n");
}
/*----------------------------------------------------------------------------*/
void print_portscan(my1uart_t* port) {
	int test, size = 0;
	printf("--------------------\n");
	printf("COM Port Scan Result\n");
	printf("--------------------\n");
	for (test=1;test<=MAX_COM_PORT;test++) {
		if (uart_prep(port,test)) {
			printf("%s: Ready\n",port->temp);
			size++;
		}
	}
	printf("\nDetected Port(s): %d\n\n",size);
}
/*----------------------------------------------------------------------------*/
void change_baudrate(my1uart_t* port, my1uart_conf_t* conf, int next) {
	int baudrate;
	uart_get_config(port,conf);
	baudrate = uart_actual_baudrate(conf->baud);
	printf("-- Current baudrate: %d\n",baudrate);
	if (next>0) {
		switch (conf->baud) {
			case MY1BAUD1200:
			case MY1BAUD2400:
			case MY1BAUD9600:
			case MY1BAUD19200:
			case MY1BAUD38400:
			case MY1BAUD57600:
				conf->baud++;
				break;
			case MY1BAUD4800:
				conf->baud = MY1BAUD9600;
				break;
			case MY1BAUD115200:
				printf("@@ Already at maximum supported baudrate!\n");
				baudrate = 0;
				break;
		}
	} else if (next<0) {
		switch (conf->baud) {
			case MY1BAUD2400:
			case MY1BAUD4800:
			case MY1BAUD19200:
			case MY1BAUD38400:
			case MY1BAUD57600:
			case MY1BAUD115200:
				conf->baud--;
				break;
			case MY1BAUD9600:
				conf->baud = MY1BAUD4800;
				break;
			case MY1BAUD1200:
				printf("@@ Already at minimum supported baudrate!\n");
				baudrate = 0;
				break;
		}
	} else baudrate = 0;
	/* only if changing... */
	if (baudrate) {
		baudrate = uart_actual_baudrate(conf->baud);
		printf("-- Setting baudrate: %d\n",baudrate);
		uart_set_config(port,conf);
		/* try to reopen */
		uart_done(port);
		if (uart_open(port))
			uart_purge(port); /* clear input buffer */
		else
			printf("** Cannot re-open port '%s'!\n",port->temp);
	}
	printf("\n");
}
/*----------------------------------------------------------------------------*/
#define KEY_LF 0x0A
#define KEY_CR 0x0D
/*----------------------------------------------------------------------------*/
void sendprint(my1uart_t* port, int data, termopts_t opts) {
	if ((opts&OPT_CR4LF)&&data==KEY_LF) { /* newline '\n' */
		uart_send_byte(port,KEY_CR); /* carriage-return '\r' */
		if (opts&OPT_LOCALECHO) {
			if (opts&OPT_SHOWHEX) printf("[%02X]",KEY_CR);
			else putchar(KEY_CR);
		}
	}
	uart_send_byte(port,data); /* send */
	if  (opts&OPT_LOCALECHO) {
		if (opts&OPT_SHOWHEX) printf("[%02X]",data);
		else putchar(data);
	}
}
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	my1uart_t port;
	my1uart_conf_t conf;
	my1key_t key, esc;
	termopts_t opts = 0x0;
	int term = 1, baud = 0, scan = 0, send = SEND_MODE_NORMAL;
	int test, loop, done = 0;
	int pin_dtr = 1, pin_rts = 1;
	char *ptty = 0x0;
	char send_queue[SEND_QUEUE_SIZE];
	int send_qsize = 0;
	/* using unbuffered output */
	setbuf(stdout,0);
	/* print tool info */
	printf("\n%s - Test for UART Interface Module ("
		"by azman@my1matrix.org)\n\n",PROGNAME);
	/* check program arguments */
	for (loop=1;loop<argc;loop++) {
		if (argv[loop][0]=='-') { /* options! */
			if (!strcmp(argv[loop],"--port")) {
				if (get_param_int(argc,argv,&loop,&test)<0) {
					printf("Cannot get port number!\n\n");
					return ERROR_PARAM_PORT;
				} else if (test>MAX_COM_PORT) {
					printf("Invalid port number! (%d)\n\n", test);
					return ERROR_PARAM_PORT;
				}
				term = test;
			} else if (!strcmp(argv[loop],"--baud")) {
				if (get_param_int(argc,argv,&loop,&test)<0) {
					printf("Cannot get baud rate!\n\n");
					return ERROR_PARAM_BAUD;
				}
				baud = test;
			} else if (!strcmp(argv[loop],"--tty")) {
				if (!(ptty=get_param_str(argc,argv,&loop))) {
					printf("Error getting tty name!\n\n");
					return ERROR_PARAM_DEVN;
				}
			} else if (!strcmp(argv[loop],"--help")) {
				about();
				return 0;
			} else if (!strcmp(argv[loop],"--scan"))
				scan = 1;
			else if (!strcmp(argv[loop],"--qsend"))
				send = SEND_MODE_RETURN;
			else if (!strcmp(argv[loop],"--echo"))
				opts |= OPT_LOCALECHO; /* local echo */
			else if (!strcmp(argv[loop],"--num"))
				opts |= OPT_SHOWHEX; /* numbers not text */
			else if (!strcmp(argv[loop],"--dos"))
				opts |= OPT_CR4LF; /* ms-dos lf cr */
			else {
				printf("Unknown option '%s'!\n\n",argv[loop]);
				return ERROR_PARAM_WHAT;
			}
		} else { /* not an option? */
			printf("Unknown parameter %s!\n\n",argv[loop]);
			return ERROR_PARAM_WHAT;
		}
	}
	/* initialize port */
	uart_init(&port);
	/* check user requested name change */
	if (ptty) uart_name(&port,ptty);
	/* check if user requested a port scan */
	if (scan) {
		print_portscan(&port);
		return 0;
	}
	/* try to prepare port with requested terminal */
	if (!term) term = uart_find(&port,0x0);
	if (!uart_prep(&port,term)) {
		about();
		print_portscan(&port);
		printf("Cannot prepare port '%s%d'!\n\n",port.name,COM_PORT(term));
		return ERROR_GENERAL;
	}
	/* apply custom baudrate */
	if (baud) {
		if (!uart_set_baudrate(&port,baud))
			printf("-- Invalid baudrate (%d)! ",baud);
		printf("-- Baudrate: %d\n",uart_get_baudrate(&port));
	}
	/* try opening port */
	if (!uart_open(&port)) {
		printf("Cannot open port '%s'!\n\n",port.temp);
		return ERROR_GENERAL;
	}
	/* check control pin DTR/RTS status */
	if (uart_pin_detect(&port,MY1PIN_DTR,&pin_dtr))
		printf("-- Cannot read DTR pin status!\n");
	if (uart_pin_detect(&port,MY1PIN_RTS,&pin_rts))
		printf("-- Cannot read RTS pin status!\n");
	/* clear input buffer */
	uart_purge(&port);
	/* start main loop */
	while (1) {
		key = get_keyhit_extended(&esc);
		if (key!=KEY_NONE) {
			/* process control keys? */
			switch (esc) {
				case KEY_F1: /* show commands */
					printf("\n\n------------\n");
					printf("Command Keys\n");
					printf("------------\n");
					printf("<F1>     - Show this help\n");
					printf("<F4>     - Show current settings\n");
					printf("<F5>     - Toggle local echo\n");
					printf("<F6>     - Toggle CR4LF option (Win32)\n");
					printf("<F7>     - Toggle Hex display mode\n");
					printf("<F8>     - Decrease baudrate\n");
					printf("<F9>     - Increase baudrate\n");
					printf("<F10>    - Exit this program\n");
					printf("<F11>    - Clear input/output buffer\n");
					printf("<ALT+p> - Purge Send Queue\n");
					printf("<ALT+s> - Toggle Send Mode\n");
					printf("<ALT+r> - Insert newline into Send Queue\n");
					printf("<CTRL+d> - Toggle DTR Line\n");
					printf("<CTRL+r> - Toggle RTS Line\n");
					printf("<CTRL+t> - Check DSR/CTS Status\n\n");
					break;
				case KEY_F4: /* show current settings */
					printf("\n\nProgram '%s' - Current Settings:\n",PROGNAME);
					if (uart_isopen(&port))
						printf("\nConnected to '%s'!",port.temp);
					else printf("\nNOT connected?!");
					if (opts&OPT_LOCALECHO) printf("\nLocal echo ON!");
					else printf("\nLocal echo OFF!");
					if (opts&OPT_CR4LF) printf("\nCR4LF option ON!");
					else printf("\nCR4LF option OFF!");
					if (opts&OPT_SHOWHEX) printf("\nHex Display Mode ON!");
					else printf("\nHex Display Mode OFF!");
					baud = uart_get_baudrate(&port);
					printf("\nEffective baudrate: %d",baud);
					printf("\nSend Mode: %s",
						send==SEND_MODE_RETURN?"ON-RETURN":"NORMAL");
					send_queue[send_qsize] = 0x0;
					printf("\nSend Queue: '%s'\n\n",send_queue);
					break;
				case KEY_F5: /* toggle local echo */
					opts ^= OPT_LOCALECHO;
					if (opts&OPT_LOCALECHO) printf("\n\nLocal echo ON!\n\n");
					else printf("\n\nLocal echo OFF!\n\n");
					break;
				case KEY_F6: /* toggle win32 newline option */
					opts ^= OPT_CR4LF;
					if (opts&OPT_CR4LF) printf("\n\nCR4LF option ON!\n\n");
					else printf("\n\nCR4LF option OFF!\n\n");
					break;
				case KEY_F7: /* toggle hex display */
					opts ^= OPT_SHOWHEX;
					if (opts&OPT_SHOWHEX)
						printf("\n\nHex Display Mode ON!\n\n");
					else printf("\n\nHex Display Mode OFF!\n\n");
					break;
				case KEY_F8: /* decrease baudrate */
					change_baudrate(&port,&conf,-1);
					break;
				case KEY_F9: /* increase baudrate */
					change_baudrate(&port,&conf,1);
					break;
				case KEY_F10:
					printf("\n\nUser Exit Request! Program '%s' Ends.\n\n",
						PROGNAME);
					done = 1;
					break;
				case KEY_F11: /* clear input/output buffer */
					printf("\nPurging input buffer!\n");
					uart_purge(&port);
					printf("Flushing output buffer!\n\n");
					uart_flush(&port);
					break;
				case KEY_NONE:
					/* normal keystroke? */
					if (key==KEY_CTRL_d) {
						pin_dtr = !pin_dtr;
						if (uart_pin_assert(&port,MY1PIN_DTR,pin_dtr))
							printf("\nCannot set DTR pin to %d!\n",pin_dtr);
						else
							printf("\nDTR pin set to %d!\n",pin_dtr);
					} else if(key==KEY_CTRL_r) {
						pin_rts = !pin_rts;
						if (uart_pin_assert(&port,MY1PIN_RTS,pin_rts))
							printf("\nCannot set RTS pin to %d!\n",pin_rts);
						else
							printf("\nRTS pin set to %d!\n",pin_rts);
					} else if (key==KEY_CTRL_t) {
						if (uart_pin_detect(&port,MY1PIN_DSR,&test))
							printf("\nCannot read DSR pin status!");
						else
							printf("\nDSR pin status: %08X!",test);
						if (uart_pin_detect(&port,MY1PIN_CTS,&test))
							printf("\nCannot read CTS pin status!");
						else
							printf("\nCTS pin status: %08X!",test);
						if (uart_pin_detect(&port,MY1PIN_DTR,&test))
							printf("\nCannot read DTR pin status!");
						else
							printf("\nDTR pin status: %08X!",test);
						if (uart_pin_detect(&port,MY1PIN_RTS,&test))
							printf("\nCannot read RTS pin status!");
						else
							printf("\nRTS pin status: %08X!",test);
						printf("\n");
					} else {
						if (send==SEND_MODE_RETURN) {
							if (key==KEY_ENTER) {
								if (send_qsize>0) {
									send_queue[send_qsize] = 0x0;
									printf("\nSending text queue: %s (%d)\n",
										send_queue,send_qsize);
									for (loop=0;loop<send_qsize;loop++)
										sendprint(&port,send_queue[loop],
											opts);
									/**sendprint(&port,KEY_ENTER,opts);*/
									send_qsize = 0;
								} else putchar(key);
							} else {
								send_queue[send_qsize++] = key;
								if (send_qsize>=SEND_QUEUE_SIZE-1) {
									send_queue[send_qsize] = 0x0;
									printf("\nPurging text queue: %s (%d/%d)\n",
										send_queue,send_qsize,SEND_QUEUE_SIZE);
									send_qsize = 0;
								} else putchar(key);
							}
						} else sendprint(&port,key,opts);
					}
					break;
				default:
					if (key==KEY_ESCAPE) {
						if (esc==(my1key_t)'p'||esc==(my1key_t)'P') {
							/* alt+p => purge send queue */
							if (send_qsize>0) {
								send_queue[send_qsize] = 0x0;
								printf("\nPurge text queue: %s\n",send_queue);
								send_qsize = 0;
							}
						} else if (esc==(my1key_t)'s'||esc==(my1key_t)'S') {
							 /* alt+s => toggle send mode */
							if (send==SEND_MODE_RETURN) {
								send = SEND_MODE_NORMAL;
								printf("\nSend Mode: NORMAL\n");
							} else {
								send = SEND_MODE_RETURN;
								printf("\nSend Mode: ON-RETURN");
							}
						} else if (esc==(my1key_t)'r'||esc==(my1key_t)'R') {
							 /* alt+r => insert newline in queue */
							if (send==SEND_MODE_RETURN&&
									send_qsize<SEND_QUEUE_SIZE-1) {
								printf("\nNewline inserted into send queue\n");
								send_queue[send_qsize++] = KEY_ENTER;
							}
						} else if ((esc>=0x41&&esc<0x5B)||
								(esc>=0x61&&esc<0x7B)||(esc>=0x30&&esc<0x39)) {
							/* ignore other alt+<key> ??? */
						} else {
							/* ignore other extended keys ??? */
						}
					}
					break;
			}
			/* break in switch :( */
			if (done) break;
		}
		/* check serial port for incoming data */
		if (uart_incoming(&port)) {
			test = uart_read_byte(&port);
			if (opts&OPT_SHOWHEX) printf("[%02X]",test);
			else putchar(test);
		}
		/* check port status */
		if (!uart_okstat(&port)) {
			printf("Port Error (%d)! Aborting!\n",port.stat);
			break;
		}
	}
	/* close port */
	uart_done(&port);
	return 0;
}
/*----------------------------------------------------------------------------*/
#endif
/*----------------------------------------------------------------------------*/
