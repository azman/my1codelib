/*----------------------------------------------------------------------------*/
#include "my1cstr_util.h"
/*----------------------------------------------------------------------------*/
#include <string.h>
/*----------------------------------------------------------------------------*/
void cstr_uppercase(my1cstr_t* pstr) {
	char* temp = pstr->buff;
	while (*temp) {
		if (*temp>=0x61&&*temp<=0x7a)
			*temp -= 0x20; /* make uppercase */
		temp++;
	}
}
/*----------------------------------------------------------------------------*/
void cstr_lowercase(my1cstr_t* pstr) {
	char* temp = pstr->buff;
	while (*temp) {
		if (*temp>=0x41&&*temp<=0x5a)
			*temp += 0x20; /* make lowercase */
		temp++;
	}
}
/*----------------------------------------------------------------------------*/
int cstr_trimws(my1cstr_t* pstr, int trim_opt) {
	/* from my1expr module */
	int size, loop, step;
	char prev, curr;
	/* trim right - size is last index */
	size = pstr->fill - 1;
	while (cstr_delim(COMMON_DELIM,pstr->buff[size])&&size>0)
		size--;
	/* get actual size */
	size++;
	/* trim left */
	loop = 0;
	while (cstr_delim(COMMON_DELIM,pstr->buff[loop])) loop++;
	/* 'rewrite' & trim multiple sequence if requested */
	for (prev=' ',step=0;loop<size;loop++) {
		curr = cstr_delim(COMMON_DELIM,pstr->buff[loop]);
		if (curr) { /* if a delimiter */
			if (trim_opt&MY1CSTR_TRIM_DOREMOVE) /* fully remove */
				continue;
			if ((trim_opt&MY1CSTR_TRIM_NOREPEAT)&&prev)
				continue;
		}
		prev = curr;
		if (curr) curr = ' '; /** only ' ' ws passed on to final string! */
		else curr = pstr->buff[loop];
		pstr->buff[step++] = curr;
	}
	/* terminate string */
	pstr->buff[step] = 0x0;
	pstr->fill = step;
	/* return length of newly trimmed string */
	return step;
}
/*----------------------------------------------------------------------------*/
int cstr_findwd(my1cstr_t* pstr, char* word) {
	char *pchk;
	int loop, find, size, step;
	find = -1;
	size = pstr->fill;
	step = strlen(word);
	pchk = pstr->buff;
	for (loop=0;loop<size;loop++) {
		if (word[0]==pchk[loop]) {
			if (!strncmp(word,&pchk[loop],step)) {
				find = loop;
				break;
			}
		}
	}
	return find;
}
/*----------------------------------------------------------------------------*/
int cstr_rmword(my1cstr_t* pstr, char* word, char* dlim) {
	char *pchk;
	int loop, init, size, step;
	init = 0;
	size = pstr->fill;
	step = strlen(word);
	pchk = pstr->buff;
	for (loop=0;loop<size;loop++) {
		if (word[0]==pchk[loop]) {
			if (!strncmp(word,&pchk[loop],step)) {
				init = loop;
				loop += step;
				for (;loop<size;loop++) {
					if (cstr_delim(dlim,pchk[loop]))
						break;
				}
				if (loop<size) {
					/* found delimiter */
					step = loop+1;
					for (loop=init;step<size;loop++,step++)
						pchk[loop] = pchk[step];
					pchk[loop] = 0x0;
					init = pstr->fill - loop;
					pstr->fill = loop;
				}
				else {
					/* simply remove all */
					step = init;
					if (step>0&&cstr_delim(dlim,pchk[step-1]))
						step--; /* remove prefix delim! */
					pchk[step] = 0x0;
					init = pstr->fill - step;
					pstr->fill = step;
				}
				break;
			}
		}
	}
	return init; /* number of removed char */
}
/*----------------------------------------------------------------------------*/
int cstr_shword(my1cstr_t* pstr, my1cstr_t* word, char* dlim) {
	int size, ends, loop, init;
	cstr_null(word);
	size = pstr->fill; ends = size - 1;
	for (loop=0;loop<size;loop++) {
		if (!cstr_delim(dlim,pstr->buff[loop]))
			break;
	}
	for (init=loop;loop<size;loop++) {
		if (cstr_delim(dlim,pstr->buff[loop])||loop==ends) {
			if (loop<ends) pstr->buff[loop] = 0x0;
			cstr_setstr(word,&pstr->buff[init]);
			init = loop+1;
			for (loop=0;init<size;loop++,init++)
				pstr->buff[loop] = pstr->buff[init];
			pstr->buff[loop] = 0x0;
			pstr->fill = loop;
			break;
		}
	}
	return word->fill;
}
/*----------------------------------------------------------------------------*/
char* cstr_addcol(my1cstr_t* pstr, char* more, int what) {
	int step, prev, size, pfix, xtra;
	char *pnew;
	/* setup */
	step = strlen(more);
	prev = pstr->fill;
	pfix = what&MY1CSTR_SEP_PREFIX;
	what = what&MY1CSTR_SEP_MASK;
	xtra = (int)my1cstr_xtra_char(what);
	size = prev + step + 1; /* +null */
	if (pfix&&!prev) xtra = 0;
	if (xtra) size++;
	/* grow OR use existing */
	if (size>pstr->size) pnew = cstr_resize(pstr, size);
	else  pnew = pstr->buff;
	/* append the string! */
	if (pnew) {
		if (xtra&&pfix) {
			pnew[pstr->fill++] = (char)xtra;
			pnew[pstr->fill] = 0x0;
		}
		strncat(pnew,more,step);
		pstr->fill += step;
		if (xtra&&!pfix) {
			pnew[pstr->fill++] = (char)xtra;
			pnew[pstr->fill] = 0x0;
		}
	}
	return pnew;
}
/*----------------------------------------------------------------------------*/
char cstr_delim(char* del,char chk) {
	while (*del) {
		if (chk==(*del))
			return chk;
		del++;
	}
	return (char)0;
}
/*----------------------------------------------------------------------------*/
#if defined(TEST_MODULE) && defined(TEST_CSTR_UTIL)
/*----------------------------------------------------------------------------*/
#include "my1cstr.c"
#include "my1cstr_line.c"
#include <stdio.h>
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	int stat, test;
	my1cstr_t buff, cmd0, args, temp, word;
	cstr_init(&buff);
	cstr_init(&cmd0);
	cstr_init(&args);
	cstr_init(&temp);
	cstr_init(&word);
	stat = 0;
	while (1) {
		printf("\nEnter text (empty string to exit): ");
		test = cstr_read_line(&buff,stdin);
		if (!test) break;
		cstr_trimws(&buff,1);
		if (!buff.fill) break;
		cstr_null(&cmd0); cstr_null(&args);
		printf("\nInput: '%s' (%d:%d/%d)",buff.buff,test,buff.fill,buff.size);
		cstr_copy(&temp,&buff);
		printf("\nToken: ");
		test = 0;
		while (temp.fill) {
			if (!cstr_shword(&temp,&word,COMMON_DELIM))
				break;
			if (!test) cstr_setstr(&cmd0,word.buff);
			else cstr_addcol(&args,word.buff,MY1CSTR_PREFIX_SPACING);
			printf("[%d:%s]",++test,word.buff);
		}
		putchar('\n');
		/* first word should be always available */
		if (!strncmp(cmd0.buff,"/find",6)) {
			/* find substr (arg1) in string (args) */
			if (!cstr_shword(&args,&word,STRING_DELIM)||!args.fill)
				fprintf(stderr,"** Not enough arguments for find!\n");
			else {
				test = cstr_findwd(&args,word.buff);
				printf("\n-- FIND: '%s' %sfound in {%s}! [%d]\n",
					word.buff,(test<0?"not ":""),args.buff,test);
			}
		}
		else if (!strncmp(cmd0.buff,"/delw",6)) {
			/* remove substr (arg1) in string (args) */
			if (!cstr_shword(&args,&word,STRING_DELIM)||!args.fill)
				fprintf(stderr,"** Not enough arguments for rmword!\n");
			else {
				test = cstr_rmword(&args,word.buff,STRING_DELIM);
				printf("\n-- DELW: '%s' %s {%s}! [%d]\n",
					word.buff,(!test?"not found":"deleted"),args.buff,test);
			}
		}
		/* ignore unknown commands */
	};
	putchar('\n');
	cstr_free(&word);
	cstr_free(&temp);
	cstr_free(&args);
	cstr_free(&cmd0);
	cstr_free(&buff);
	return stat;
}
/*----------------------------------------------------------------------------*/
#endif
/*----------------------------------------------------------------------------*/
