/*----------------------------------------------------------------------------*/
#ifndef __MY1TYPE_H__
#define __MY1TYPE_H__
/*----------------------------------------------------------------------------*/
typedef unsigned char byte08_t;
typedef unsigned short word16_t;
typedef unsigned int word32_t;
/* long changes with pointer size... */
/* on 64-bit systems, long IS 64-bits! */
/* on 32-bit systems, long IS 32-bits! */
typedef unsigned long long word64_t;
/*----------------------------------------------------------------------------*/
/* generic object pointer type */
typedef void* pdata_t;
/* generic function pointer type - can have 0 or more params! */
typedef void (*pfunc_t)();
/* pfunc_t with return value! */
typedef int (*ptask_t)();
/* pfunc_t with pointer-type return value - e.g. return malloced data */
typedef void* (*pmake_t)();
/*----------------------------------------------------------------------------*/
/* some useful stuffs */
/*----------------------------------------------------------------------------*/
#define UINT32_MSB1 (~(~0U>>1))
#define WORD32_MSB1 UINT32_MSB1
/*----------------------------------------------------------------------------*/
#define STAT_OK 0
#define STAT_ERROR WORD32_MSB1
/* will remove this in the future */
#define FLAG_ERROR WORD32_MSB1
/*----------------------------------------------------------------------------*/
#endif /** __MY1TYPE_H__ */
/*----------------------------------------------------------------------------*/
