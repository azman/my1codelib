/*----------------------------------------------------------------------------*/
#include "my1cstr_conv.h"
/*----------------------------------------------------------------------------*/
void cstr_append_uint(my1cstr_t* pstr, unsigned int that) {
	unsigned int test;
	unsigned char what, temp;
	what = 0; test = 1000000000; /* max is 4billion++ */
	while (test>=10) {
		temp = (unsigned char)(that/test);
		if (temp) {
			that %= test;
			cstr_single(pstr,(temp+0x30));
			what++;
		}
		else if (what)
			cstr_single(pstr,0x30);
		test /= 10;
	}
	cstr_single(pstr,(that+0x30));
}
/*----------------------------------------------------------------------------*/
void cstr_append_int(my1cstr_t* pstr, int that) {
	if (that<0) {
		cstr_single(pstr,'-');
		that = -that;
	}
	cstr_append_uint(pstr,(unsigned int)that);
}
/*----------------------------------------------------------------------------*/
void cstr_append_real(my1cstr_t* pstr, float that, int dcnt) {
	unsigned int test;
	/** check negative value */
	if (that<0) {
		cstr_single(pstr,'-');
		that = -that;
	}
	test = (unsigned int) that;
	that -= (float) test;
	cstr_append_uint(pstr,(unsigned int)test);
	if (dcnt>0) {
		/** show decimal part */
		cstr_single(pstr,'.');
		while (dcnt>0) {
			that *= 10;
			test = (unsigned int) that;
			cstr_single(pstr,test+0x30);
			that -= (float) test;
			dcnt--;
		}
	}
}
/*----------------------------------------------------------------------------*/
void cstr_make_uint(my1cstr_t* pstr, unsigned int* pval, int offs) {
	*pval = 0;
	while (pstr->buff[offs]) {
		if (pstr->buff[offs]<0x30||pstr->buff[offs]>0x39)
			break;
		*pval = (*pval*10) + (pstr->buff[offs]-0x30);
		offs++;
	}
}
/*----------------------------------------------------------------------------*/
void cstr_make_int(my1cstr_t* pstr, int* pval, int offs) {
	int temp;
	if (pstr->buff[offs]=='-') {
		offs++;
		temp = 1;
	}
	else temp = 0;
	cstr_make_uint(pstr,(unsigned int*)pval,offs);
	if (temp) {
		offs--;
		*pval = -(*pval);
	}
}
/*----------------------------------------------------------------------------*/
void cstr_make_real(my1cstr_t* pstr, float* pval, int offs) {
	int idot, ilen, ineg;
	float divf;
	unsigned int itmp;
	ineg = 0;
	if (pstr->buff[offs]=='-') {
		offs++;
		ineg = 1;
	}
	ilen = offs; idot = -1;
	while (pstr->buff[ilen]) {
		if (pstr->buff[ilen]=='.') idot = ilen;
		ilen++;
	}
	if (idot<0) idot = ilen;
	else pstr->buff[idot] = 0x0;
	cstr_make_uint(pstr,&itmp,offs);
	if (idot<ilen) pstr->buff[idot] = '.';
	/** loop decimal points */
	*pval = 0.0; divf = 10.0;
	while (++idot<ilen) {
		*pval += (float)(pstr->buff[idot]-0x30)/divf;
		divf *= 10.0;
	}
	/** add up and apply sign flag */
	*pval += (float) itmp;
	if (ineg) *pval = -(*pval);
}
/*----------------------------------------------------------------------------*/
#if defined(TEST_MODULE) && defined(TEST_CSTR_CONV)
/*----------------------------------------------------------------------------*/
#include "my1cstr.c"
#include "my1cstr_line.c"
#include "my1cstr_util.c"
#include <stdio.h>
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	int stat, test;
	my1cstr_t buff, show;
	float that;
	cstr_init(&buff);
	cstr_init(&show);
	stat = 0;
	while (1) {
		printf("\nEnter integer (empty string to exit): ");
		test = cstr_read_line(&buff,stdin);
		if (!test) break;
		cstr_trimws(&buff,1);
		if (!buff.fill) break;
		cstr_setstr(&show,"Input: '");
		cstr_append(&show,buff.buff);
		cstr_append(&show,"' (");
		cstr_append_int(&show,test);
		cstr_append(&show,":");
		cstr_append_int(&show,buff.fill);
		cstr_append(&show,"/");
		cstr_append_int(&show,buff.size);
		cstr_append(&show,")");
		printf("\n>Input: '%s' (%d:%d/%d)",buff.buff,test,buff.fill,buff.size);
		printf("\n@%s",show.buff);
		cstr_make_int(&buff,&test,0);
		cstr_setstr(&show,"Conv:[");
		cstr_append_int(&show,test);
		cstr_append(&show,"]");
		printf("\n#%s\n",show.buff);
		printf("\nEnter real number (empty string to exit): ");
		test = cstr_read_line(&buff,stdin);
		if (!test) break;
		cstr_trimws(&buff,1);
		if (!buff.fill) break;
		printf("\n>Input: '%s' (%d:%d/%d)",buff.buff,test,buff.fill,buff.size);
		cstr_make_real(&buff,&that,0);
		cstr_setstr(&show,"Conv:[");
		cstr_append_real(&show,that,4);
		cstr_append(&show,"]");
		printf("\n#%s\n",show.buff);
	};
	putchar('\n');
	cstr_free(&show);
	cstr_free(&buff);
	return stat;
}
/*----------------------------------------------------------------------------*/
#endif
/*----------------------------------------------------------------------------*/
