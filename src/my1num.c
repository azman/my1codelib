/*----------------------------------------------------------------------------*/
#include "my1num.h"
/*----------------------------------------------------------------------------*/
#include <stdlib.h>
#include <string.h>
#include <math.h>
/*----------------------------------------------------------------------------*/
void num_init(my1num_t* num, int type) {
	switch (type) {
		case NUMBER_FLOATING:
			num->type = NUMBER_FLOATING;
			num->real.numF = 0.0;
			num->imag.numF = 0.0;
			break;
		default:
		case NUMBER_INTEGER:
			num->type = NUMBER_INTEGER;
			num->real.numI = 0;
			num->imag.numI = 0;
			break;
	}
}
/*----------------------------------------------------------------------------*/
void num_make(my1num_t* num, int type) {
	switch (type) {
		case NUMBER_FLOATING:
			if (num->type==NUMBER_INTEGER) {
				num->type = NUMBER_FLOATING;
				num->real.numF = (double) num->real.numI;
				num->imag.numF = (double) num->imag.numI;
			}
			break;
		case NUMBER_INTEGER:
			if (num->type==NUMBER_FLOATING) {
				num->type = NUMBER_INTEGER;
				num->real.numI = (long long) num->real.numF;
				num->imag.numI = (long long) num->imag.numF;
			}
			break;
	}
}
/*----------------------------------------------------------------------------*/
void num_str2(my1num_t* num, char* str) {
	char *pbuf;
	int ichk = -1, idot = -1, size = 0;
	/* get size, check ws */
	while (str[size]) {
		switch (str[size]) {
			case ' ': case '\t':
				num->type = NUMBER_ERROR_STR;
				break;
			case '.':
				if (idot<0) idot = size;
				else num->type = NUMBER_ERROR_STR;
				break;
			case CHAR_COMPLEX:
				if (ichk<0) ichk = size;
				else num->type = NUMBER_ERROR_STR;
				break;
			default:
				/* check if not number */
				if (str[size]<0x30||str[size]>0x39)
					num->type = NUMBER_ERROR_STR;
				break;
		}
		if (num->type<0) return;
		size++;
	}
	if (ichk>=0) {
		size--;
		if (ichk!=size) {
			num->type = NUMBER_ERROR_STR;
			return;
		}
	}
	size ++; /* include null */
	pbuf = malloc(size*sizeof(char));
	strncpy(pbuf,str,size);
	pbuf[size-1] = 0x0;
	if (idot<0) {
		num->type = NUMBER_INTEGER;
		if (ichk<0) num->real.numI = atoi(pbuf);
		else num->imag.numI = atoi(pbuf);
	}
	else {
		num->type = NUMBER_FLOATING;
		if (ichk<0) num->real.numF = atof(pbuf);
		else num->imag.numF = atof(pbuf);
	}
	free((void*)pbuf);
}
/*----------------------------------------------------------------------------*/
my1num_t* num_clone(my1num_t* num) {
	my1num_t* tmp = (my1num_t*) malloc(sizeof(my1num_t));
	if (tmp) {
		if (num) {
			tmp->type = num->type;
			/* no matter what, 8-bytes are copied! */
			tmp->real.numI = num->real.numI;
			tmp->imag.numI = num->imag.numI;
			tmp->type = num->type;
			/* num_copy(tmp,num);*/
		}
		else num_init(num,NUMBER_INTEGER);
	}
	return tmp;
}
/*----------------------------------------------------------------------------*/
void num_calc(my1num_t* num, my1pol_t* pol) {
	my1num_t* tmp = num_clone(num);
	num_make(tmp,NUMBER_FLOATING);
	pol->value = sqrt((tmp->real.numF*tmp->real.numF)+
		(tmp->imag.numF*tmp->imag.numF));
	if (tmp->real.numF==0.0)
		pol->phase = 0.0;
	else
		pol->phase = atan(tmp->imag.numF/tmp->real.numF);
	free((void*)tmp);
}
/*----------------------------------------------------------------------------*/
void num_pol2(my1num_t* num, my1pol_t* pol) {
	num->type = NUMBER_FLOATING;
	num->real.numF = pol->value*cos(pol->phase);
	num->imag.numF = pol->value*sin(pol->phase);
}
/*----------------------------------------------------------------------------*/
void num_zero(my1num_t* num) {
	/* because of union, double value is 0! */
	num->real.numI = 0;
	num->imag.numI = 0;
}
/*----------------------------------------------------------------------------*/
void num_real(my1num_t* num) {
	/* because of union, double value is 0! */
	num->imag.numI = 0;
}
/*----------------------------------------------------------------------------*/
void num_negate(my1num_t* num) {
	switch (num->type) {
		case NUMBER_FLOATING:
			num->real.numF = -num->real.numF;
			num->imag.numF = -num->imag.numF;
			break;
		default:
			/* correct unknown type to default */
			num->type = NUMBER_INTEGER;
			/* intentionally no break here */
		case NUMBER_INTEGER:
			num->real.numI = -num->real.numI;
			num->imag.numI = -num->imag.numI;
			break;
	}
}
/*----------------------------------------------------------------------------*/
void num_set_i(my1num_t* num, long long real, long long imag) {
	num->type = NUMBER_INTEGER;
	num->real.numI = real;
	num->imag.numI = imag;
}
/*----------------------------------------------------------------------------*/
void num_set_f(my1num_t* num, double real, double imag) {
	num->type = NUMBER_FLOATING;
	num->real.numF = real;
	num->imag.numF = imag;
}
/*----------------------------------------------------------------------------*/
void num_copy(my1num_t* dst, my1num_t* src) {
	switch (dst->type) {
		case NUMBER_FLOATING:
			if (src->type==NUMBER_FLOATING) {
				dst->real.numF = src->real.numF;
				dst->imag.numF = src->imag.numF;
			}
			else {
				dst->real.numF = (double) src->real.numI;
				dst->imag.numF = (double) src->imag.numI;
			}
			break;
		case NUMBER_INTEGER:
			if (src->type==NUMBER_FLOATING) {
				dst->real.numI = (long) src->real.numF;
				dst->imag.numI = (long) src->imag.numF;
			}
			else {
				dst->real.numI = src->real.numI;
				dst->imag.numI = src->imag.numI;
			}
			break;
	}
}
/*----------------------------------------------------------------------------*/
void num_move(my1num_t* dst, my1num_t* src) {
	dst->type = src->type;
	/* no matter what, 8-bytes are copied! */
	dst->real.numI = src->real.numI;
	dst->imag.numI = src->imag.numI;
}
/*----------------------------------------------------------------------------*/
void num_add(my1num_t* dst, my1num_t* src1, my1num_t* src2) {
	if (src1->type==NUMBER_FLOATING||src2->type==NUMBER_FLOATING) {
		/** result is float whenever any operand is float */
		dst->type = NUMBER_FLOATING;
		src1 = num_clone(src1);
		src2 = num_clone(src2);
		if (src1->type!=NUMBER_FLOATING) num_make(src1,NUMBER_FLOATING);
		if (src2->type!=NUMBER_FLOATING) num_make(src2,NUMBER_FLOATING);
		dst->real.numF = src1->real.numF+src2->real.numF;
		dst->imag.numF = src1->imag.numF+src2->imag.numF;
		free((void*)src1); free((void*)src2);
	}
	else {
		/** both int value, result is also int! */
		dst->type = NUMBER_INTEGER;
		dst->real.numI = src1->real.numI+src2->real.numI;
		dst->imag.numI = src1->imag.numI+src2->imag.numI;
	}
}
/*----------------------------------------------------------------------------*/
void num_subtract(my1num_t* dst, my1num_t* src1, my1num_t* src2) {
	if (src1->type==NUMBER_FLOATING||src2->type==NUMBER_FLOATING) {
		dst->type = NUMBER_FLOATING;
		src1 = num_clone(src1);
		src2 = num_clone(src2);
		if (src1->type!=NUMBER_FLOATING) num_make(src1,NUMBER_FLOATING);
		if (src2->type!=NUMBER_FLOATING) num_make(src2,NUMBER_FLOATING);
		dst->real.numF = src1->real.numF-src2->real.numF;
		dst->imag.numF = src1->imag.numF-src2->imag.numF;
		free((void*)src1); free((void*)src2);
	}
	else {
		dst->type = NUMBER_INTEGER;
		dst->real.numI = src1->real.numI-src2->real.numI;
		dst->imag.numI = src1->imag.numI-src2->imag.numI;
	}
}
/*----------------------------------------------------------------------------*/
void num_multiply(my1num_t* dst, my1num_t* src1, my1num_t* src2) {
	if (src1->type==NUMBER_FLOATING||src2->type==NUMBER_FLOATING) {
		dst->type = NUMBER_FLOATING;
		src1 = num_clone(src1);
		src2 = num_clone(src2);
		if (src1->type!=NUMBER_FLOATING) num_make(src1,NUMBER_FLOATING);
		if (src2->type!=NUMBER_FLOATING) num_make(src2,NUMBER_FLOATING);
		dst->real.numF = (src1->real.numF*src2->real.numF) -
			(src1->imag.numF*src2->imag.numF);
		dst->imag.numF = (src1->real.numF*src2->imag.numF) +
			(src1->imag.numF*src2->real.numF);
		free((void*)src1); free((void*)src2);
	}
	else {
		dst->type = NUMBER_INTEGER;
		dst->real.numI = (src1->real.numI*src2->real.numI) -
			(src1->imag.numI*src2->imag.numI);
		dst->imag.numI = (src1->real.numI*src2->imag.numI) +
			(src1->imag.numI*src2->real.numI);
	}
}
/*----------------------------------------------------------------------------*/
void num_divide(my1num_t* dst, my1num_t* src1, my1num_t* src2) {
	if (src1->type==NUMBER_FLOATING||src2->type==NUMBER_FLOATING) {
		double temp;
		src1 = num_clone(src1);
		src2 = num_clone(src2);
		if (src1->type!=NUMBER_FLOATING) num_make(src1,NUMBER_FLOATING);
		if (src2->type!=NUMBER_FLOATING) num_make(src2,NUMBER_FLOATING);
		/* get denominator */
		temp = (src2->real.numF*src2->real.numF) +
			(src2->imag.numF*src2->imag.numF);
		/* temp should not be zero! */
		if (temp!=0.0) {
			dst->real.numF = ((src1->real.numF*src2->real.numF) +
				(src1->imag.numF*src2->imag.numF))/temp;
			dst->imag.numF = ((src1->imag.numF*src2->real.numF) +
				(src1->real.numF*src2->imag.numF))/temp;
		}
		else dst->type = NUMBER_ERROR_DBZ;
		free((void*)src1); free((void*)src2);
		dst->type = NUMBER_FLOATING;
	}
	else {
		long temp = (src2->real.numI*src2->real.numI) +
			(src2->imag.numI*src2->imag.numI);
		if (temp!=0) {
			dst->real.numI = ((src1->real.numI*src2->real.numI) +
				(src1->imag.numI*src2->imag.numI))/temp;
			dst->imag.numI = ((src1->imag.numI*src2->real.numI) +
				(src1->real.numI*src2->imag.numI))/temp;
		}
		else dst->type = NUMBER_ERROR_DBZ;
		dst->type = NUMBER_INTEGER;
	}
}
/*----------------------------------------------------------------------------*/
void num_divrem(my1num_t* dst, my1num_t* src1, my1num_t* src2) {
	/* clone and force number to integers */
	src1 = num_clone(src1);
	src2 = num_clone(src2);
	if(src1->type==NUMBER_FLOATING)
		num_make(src1,NUMBER_INTEGER);
	if(src2->type==NUMBER_FLOATING)
		num_make(src2,NUMBER_INTEGER);
	dst->type = NUMBER_INTEGER;
	/* use operator when number is not complex (Gaussian integers?) */
	if (src1->imag.numI==0&&src2->imag.numI==0) {
		dst->real.numI = src1->real.numI % src2->real.numI;
		dst->imag.numI = 0;
	}
	else {
		/* do standard complex division */
		my1num_t buff;
		num_init(&buff,NUMBER_INTEGER);
		long long temp = (src2->real.numI*src2->real.numI) +
			(src2->imag.numI*src2->imag.numI);
		if (temp!=0) {
			dst->real.numI = ((src1->real.numI*src2->real.numI) +
				(src1->imag.numI*src2->imag.numI))/temp;
			dst->imag.numI = ((src1->imag.numI*src2->real.numI) +
				(src1->real.numI*src2->imag.numI))/temp;
			/* get remainder! result may be negative? */
			num_multiply(&buff,src2,dst);
			num_subtract(dst,src1,&buff);
		}
		else dst->type = NUMBER_ERROR_DBZ;
	}
	free((void*)src1);
	free((void*)src2);
}
/*----------------------------------------------------------------------------*/
void num_power(my1num_t* dst, my1num_t* src1, my1num_t* src2) {
	/* clone and force number to real value */
	src1 = num_clone(src1);
	src2 = num_clone(src2);
	if (src1->type==NUMBER_INTEGER)
		num_make(src1,NUMBER_FLOATING);
	if (src2->type==NUMBER_INTEGER)
		num_make(src2,NUMBER_FLOATING);
	dst->type = NUMBER_FLOATING;
	/* use only real value, suppress imaginary */
	dst->real.numF = pow((double)src1->real.numF,(double)src2->real.numF);
	dst->imag.numF = 0.0;
	free((void*)src1);
	free((void*)src2);
}
/*----------------------------------------------------------------------------*/
void num_log(my1num_t* dst, my1num_t* src) {
	/**
	 * for z = a+ib = r(cos t + i sin t) = re^it
	 * thus, log (z) = log re^it
	 * for natural log, ln z = ln r + ln e^it
	 * thus w = ln z = ln r + it
	***/
	my1pol_t pol;
	/* clone and force number to real value */
	src = num_clone(src);
	if (src->type==NUMBER_INTEGER)
		num_make(src,NUMBER_FLOATING);
	dst->type = NUMBER_FLOATING;
	/* the operation */
	num_calc(src,&pol);
	dst->real.numF = log(pol.value);
	dst->imag.numF = pol.phase;
	free((void*)src);
}
/*----------------------------------------------------------------------------*/
void num_log10(my1num_t* dst, my1num_t* src) {
	num_log(dst,src);
	dst->real.numF = dst->real.numF/CONSTANT_LOGE_10_;
	dst->imag.numF = dst->imag.numF/CONSTANT_LOGE_10_;
}
/*----------------------------------------------------------------------------*/
void num_sin(my1num_t* dst, my1num_t* src) {
	src = num_clone(src);
	if (src->type==NUMBER_INTEGER)
		num_make(src,NUMBER_FLOATING);
	dst->type = NUMBER_FLOATING;
	dst->real.numF = sin(src->real.numF)*cosh(src->imag.numF);
	dst->imag.numF = cos(src->real.numF)*sinh(src->imag.numF);
	free((void*)src);
}
/*----------------------------------------------------------------------------*/
void num_cos(my1num_t* dst, my1num_t* src) {
	src = num_clone(src);
	if (src->type==NUMBER_INTEGER)
		num_make(src,NUMBER_FLOATING);
	dst->type = NUMBER_FLOATING;
	dst->real.numF = cos(src->real.numF)*cosh(src->imag.numF);
	dst->imag.numF = sin(src->real.numF)*sinh(src->imag.numF)*(-1);
	free((void*)src);
}
/*----------------------------------------------------------------------------*/
void num_tan(my1num_t* dst, my1num_t* src) {
	my1num_t ccos,ssin;
	src = num_clone(src);
	if (src->type==NUMBER_INTEGER)
		num_make(src,NUMBER_FLOATING);
	dst->type = NUMBER_FLOATING;
	num_init(&ssin,NUMBER_FLOATING);
	num_sin(&ssin,src);
	num_init(&ccos,NUMBER_FLOATING);
	num_cos(&ccos,src);
	num_divide(dst,&ssin,&ccos);
	free((void*)src);
}
/*----------------------------------------------------------------------------*/
void num_rad(my1num_t* dst, my1num_t* src) {
	src = num_clone(src);
	if (src->type==NUMBER_INTEGER)
		num_make(src,NUMBER_FLOATING);
	dst->type = NUMBER_FLOATING;
	if (src->imag.numF==0.0) {
		/* real number - convert real only */
		dst->real.numF = (src->real.numF*CONSTANT_PI_)/180.0;
		dst->imag.numF = 0.0;
	}
	else {
		/* complex number - convert imaginary, copy real */
		dst->real.numF = src->real.numF;
		dst->imag.numF = (src->imag.numF*CONSTANT_PI_)/180.0;
	}
	free((void*)src);
}
/*----------------------------------------------------------------------------*/
void num_deg(my1num_t* dst, my1num_t* src) {
	src = num_clone(src);
	if(src->type==NUMBER_INTEGER)
		num_make(src,NUMBER_FLOATING);
	dst->type = NUMBER_FLOATING;
	if (src->imag.numF==0.0) {
		/* real number - convert real only */
		dst->real.numF = (src->real.numF*180.0)/CONSTANT_PI_;
		dst->imag.numF = 0.0;
	}
	else {
		/* complex number - convert imaginary, copy real */
		dst->real.numF = src->real.numF;
		dst->imag.numF = (src->imag.numF*180.0)/CONSTANT_PI_;
	}
	free((void*)src);
}
/*----------------------------------------------------------------------------*/
#if defined(TEST_MODULE)
/*----------------------------------------------------------------------------*/
#include <stdio.h>
/*----------------------------------------------------------------------------*/
void num_print(my1num_t* num) {
	int test = 0;
	if (num->type<0) {
		switch (num->type) {
			case NUMBER_ERROR_DBZ: printf("<Divide-By-Zero>"); break;
			case NUMBER_ERROR_STR: printf("<Parse-Error>"); break;
			default: printf("<Unknown-Error>"); break;
		}
	}
	else {
		if (num->type==NUMBER_FLOATING) {
			double temp;
			if (num->real.numF!=0.0) {
				printf("%lf",num->real.numF);
				test = 1;
			}
			if (num->imag.numF!=0.0) {
				temp = num->imag.numF;
				if (temp<0.0) {
					putchar('-');
					temp = -temp;
				}
				else {
					if (test) putchar('+');
				}
				printf("%lf%c",temp,CHAR_COMPLEX);
			}
			else {
				if (!test)
					printf("%lf",num->real.numF);
			}
		}
		else {
			long long temp;
			if (num->real.numI!=0) {
				printf("%lld",num->real.numI);
				test = 1;
			}
			if (num->imag.numI!=0) {
				temp = num->imag.numI;
				if (temp<0.0) {
					putchar('-');
					temp = -temp;
				}
				else {
					if (test) putchar('+');
				}
				printf("%lld%c",temp,CHAR_COMPLEX);
			}
			else {
				if (!test)
					printf("%lf",num->imag.numF);
			}
		}
		{
			my1pol_t pol;
			num_calc(num,&pol);
			printf(" {M:%.4lf,P:%.4lf}",pol.value,pol.phase);
		}
	}
}
/*----------------------------------------------------------------------------*/
#endif
/*----------------------------------------------------------------------------*/
#if defined(TEST_MODULE) && defined(TEST_NUM)
/*----------------------------------------------------------------------------*/
#define RESULT_NAME "result"
/*----------------------------------------------------------------------------*/
#include "my1vars.c"
#include "my1cmds.c"
#include "my1list.c"
#include "my1list_data.c"
#include "my1expr.c"
#include "my1cstr.c"
#include "my1cstr_util.c"
#include "my1cstr_line.c"
#include "my1shell.c"
#include "my1chtest.c"
/*----------------------------------------------------------------------------*/
void print_expression(my1expr_t *expr);
/*----------------------------------------------------------------------------*/
my1var_t* make_shell_data(my1shell_t* that, char* name, char* dstr) {
	my1var_t* pvar;
	my1num_t *pnum;
	pvar = 0x0;
	pnum = (my1num_t*) malloc(sizeof(my1num_t));
	if (pnum) {
		num_init(pnum,NUMBER_INTEGER);
		if (dstr) num_str2(pnum,dstr);
		pvar = shell_data(that,name,(void*)pnum,list_free_item);
		if (!pvar) free((void*)pnum);
	}
	return pvar;
}
/*----------------------------------------------------------------------------*/
void show_data(my1var_t* pvar) {
	printf("%s = ",pvar->name.buff);
	num_print((my1num_t*)pvar->data);
	printf("\n");
}
/*----------------------------------------------------------------------------*/
int exec_expr(my1expr_t* expr, void* data, void* more) {
	char *pbuf;
	my1var_t *pvar;
	my1num_t *pchk, *pnxt, *pnum;
	my1shell_t *that = (my1shell_t*)data;
	if (expr->type==EXPR_TYPE_VAL) {
		if (expr->data) {
			expr->type |= EXPR_ERROR_EXVAL;
			return EXPR_ERROR_EXVAL;
		}
		pnum = (my1num_t*) malloc(sizeof(my1num_t));
		if (!pnum) {
			expr->type |= EXPR_ERROR_ALLOC;
			return EXPR_ERROR_ALLOC;
		}
		num_init(pnum,0);
		num_str2(pnum,expr->tval.buff);
		if (pnum->type<0) {
			expr->type |= EXPR_ERROR_INVAL;
			return EXPR_ERROR_INVAL;
		}
		expr->data = (void*) pnum;
	}
	else if (expr->type==EXPR_TYPE_VAR) {
		pvar = shell_find_data(that,expr->tval.buff);
		if (!pvar) {
			printf("** Missing var:{%s}!\n",expr->tval.buff);
			expr->type |= EXPR_ERROR_NOVAR;
			return EXPR_ERROR_NOVAR;
		}
		/* assume pvar ALWAYS have value! */
		pchk = (my1num_t*) pvar->data;
		pnum = num_clone(pchk);
		if (!pnum) {
			expr->type |= EXPR_ERROR_ALLOC;
			return EXPR_ERROR_ALLOC;
		}
		expr->data = (void*) pnum;
	}
	else if(expr->type==EXPR_TYPE_FUN) {
		if(!expr->opr1||!expr->opr1->data||
				(expr->opr1->type&EXPR_TYPE_ERR)) {
			expr->type |= EXPR_ERROR_NOOP1;
			return EXPR_ERROR_NOOP1;
		}
		pchk = (my1num_t*) expr->opr1->data;
		pnum = num_clone(pchk);
		if (!pnum) {
			expr->type |= EXPR_ERROR_ALLOC;
			return EXPR_ERROR_ALLOC;
		}
		/* check function name */
		pbuf = expr->tval.buff;
		if (!strncmp(pbuf,"ln",3))
			num_log(pnum,pchk);
		else if (!strncmp(pbuf,"log",4))
			num_log10(pnum,pchk);
		else if (!strncmp(pbuf,"sin",4))
			num_sin(pnum,pchk);
		else if (!strncmp(pbuf,"cos",4))
			num_cos(pnum,pchk);
		else if (!strncmp(pbuf,"tan",4))
			num_tan(pnum,pchk);
		else if (!strncmp(pbuf,"rad",4))
			num_rad(pnum,pchk);
		else if (!strncmp(pbuf,"deg",4))
			num_deg(pnum,pchk);
		else {
			expr->type |= EXPR_ERROR_NOFUN;
			return EXPR_ERROR_NOFUN;
		}
		expr->data = (void*) pnum;
	}
	else if(expr->type==EXPR_TYPE_OPR) {
		if(!expr->opr1||!expr->opr1->data||
				(expr->opr1->type&EXPR_TYPE_ERR)) {
			expr->type |= EXPR_ERROR_NOOP1;
			return EXPR_ERROR_NOOP1;
		}
		if(!expr->opr2||!expr->opr2->data||
				(expr->opr2->type&EXPR_TYPE_ERR)) {
			expr->type |= EXPR_ERROR_NOOP2;
			return EXPR_ERROR_NOOP2;
		}
		pchk = (my1num_t*) expr->opr1->data;
		pnum = num_clone(pchk);
		if (!pnum) {
			expr->type |= EXPR_ERROR_ALLOC;
			return EXPR_ERROR_ALLOC;
		}
		pnxt = (my1num_t*) expr->opr2->data;
		/* assume valid op? */
		pbuf = expr->tval.buff;
		switch (pbuf[0]) {
			case OPERATOR_ADD: num_add(pnum,pchk,pnxt); break;
			case OPERATOR_SUB: num_subtract(pnum,pchk,pnxt); break;
			case OPERATOR_MUL: num_multiply(pnum,pchk,pnxt); break;
			case OPERATOR_DIV: num_divide(pnum,pchk,pnxt); break;
			case OPERATOR_MOD: num_divrem(pnum,pchk,pnxt); break;
		}
		expr->data = (void*) pnum;
		printf("@@ Ops:{%c}\n",pbuf[0]);
	}
	else {
		expr->type |= EXPR_ERROR_INVEX;
		return EXPR_ERROR_INVEX;
	}
	return 0;
}
/*----------------------------------------------------------------------------*/
int show_intro(void) {
	printf("\nMY1 Number (my1num) Test Program\n\n");
	printf("- based on my1expr module in my1codelib\n");
	printf("- arithmetic expressions () / %% * + -\n");
	printf("  - unary operator not supported (e.g. 0-2 instead of -2)\n");
	printf("- trigonometry functions (sin,cos,tan)\n");
	printf("  - rad()/deg() function to convert degree to/from radian\n");
	printf("- logarithmic functions (log,ln)\n");
	printf("- complex number supported\n");
	printf("  - complex char '%c' specified AFTER number (e.g. 5%c)\n",
		CHAR_COMPLEX,CHAR_COMPLEX);
	printf("- variables are automatically created on assignment\n");
	printf("- type 'help' to list available commands\n");
	printf("- type 'intro' to show this introductory message\n");
	printf("- type 'exit' or 'quit' to quit this program\n");
	return 0;
}
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	my1var_t *pvar, *pres;
	my1expr_t expr;
	my1shell_t that;
	int flag;
	expr_init(&expr);
	expr.exec = exec_expr;
	shell_init(&that,"my1num");
	shell_task(&that,"quit","Quit Shell",CMDFLAG_EXIT,0x0);
	shell_task(&that,"list","List Variables",CMDFLAG_TASK,shell_list_vars);
	shell_task(&that,"help","Show Commands",CMDFLAG_TASK,shell_list_cmds);
	shell_task(&that,"intro","Show Intro Message",CMDFLAG_TASK,show_intro);
	/* create default result space */
	pres = make_shell_data(&that,RESULT_NAME,"0");
	if (!pres) printf("** Cannot create storage for default result!\n");
	that.show = show_data;
	show_intro();
	/* start main loop */
	while (1) {
		shell_wait(&that);
		if (that.flag&SHELL_FLAG_SKIP) continue;
		shell_exec(&that);
		if (that.flag&SHELL_FLAG_DONE) break;
		if (that.flag&SHELL_FLAG_SKIP) continue;
		/* application code */
		flag = expr_read(&expr,that.buff.buff);
		if (flag&FLAG_ERROR||!expr.read) {
			printf("** Error: (%08x) Input: '%s'\n",flag,that.buff.buff);
			continue;
		}
		expr_exec(&expr,&that,0x0);
		if (expr.read->temp) {
			printf("** Error?(%08x)\n",expr.read->temp);
			continue;
		}
		pvar = pres;
		if (expr.tval.fill) {
			pvar = shell_find_data(&that,expr.tval.buff);
			if (!pvar)
				pvar = make_shell_data(&that,expr.tval.buff,0x0);
		}
		if (!expr.read->data) {
			printf("** Missing data?!\n");
			fflush(stdout);
			if (pvar!=pres)
				show_data(pvar);
		}
		else if (!pvar) {
			printf("** Cannot assign value: ");
			num_print((my1num_t*)expr.read->data);
			printf("\n");
		}
		else {
			num_move((my1num_t*)pvar->data,(my1num_t*)expr.read->data);
			show_data(pvar);
		}
	}
	shell_free(&that);
	expr_free(&expr);
	printf("\nBye!\n\n");
	return 0;
}
/*----------------------------------------------------------------------------*/
#endif
/*----------------------------------------------------------------------------*/
