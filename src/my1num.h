/*----------------------------------------------------------------------------*/
#ifndef __MY1NUM_H__
#define __MY1NUM_H__
/*----------------------------------------------------------------------------*/
/**
 *  my1num for number representation & operations
 *  - written by azman@my1matrix.org
 *  - covers complex numbers
 */
/*----------------------------------------------------------------------------*/
#define NUMBER_ERROR_DBZ -2
#define NUMBER_ERROR_STR -1
#define NUMBER_STRING  -1
#define NUMBER_INTEGER  0
#define NUMBER_FLOATING 1
#define NUMBER_FLOATING_MASK 0x01
#define NUMBER_COMPLEX 2
#define NUMBER_COMPLEX_MASK 0x02
#ifndef CHAR_COMPLEX
#define CHAR_COMPLEX 'i'
#endif
/*----------------------------------------------------------------------------*/
/** 3.14159265358979 */
#define CONSTANT_PI_ 3.14159265
#define CONSTANT_E__ 2.718281828
#define CONSTANT_LOG10_E_ 0.434294482
#define CONSTANT_LOGE_10_ 2.30258509
/*----------------------------------------------------------------------------*/
typedef union _my1real_t {
	long long numI; /* 64-bit number! */
	double numF;
} my1real_t;
/*----------------------------------------------------------------------------*/
typedef struct _my1num_t {
	my1real_t real, imag;
	int type;
	unsigned int stat; /* padding */
} my1num_t;
/*----------------------------------------------------------------------------*/
typedef struct _my1pol_t {
	double value, phase; /* polar values */
} my1pol_t;
/*----------------------------------------------------------------------------*/
void num_init(my1num_t* num, int type);
void num_make(my1num_t* num, int type);
void num_str2(my1num_t* num, char* str);
my1num_t* num_clone(my1num_t* num);
void num_calc(my1num_t* num, my1pol_t* pol);
void num_pol2(my1num_t* num, my1pol_t* pol);
void num_zero(my1num_t* num);
void num_real(my1num_t* num);
void num_negate(my1num_t* num); /* change sign! */
void num_set_i(my1num_t* num, long long real, long long imag);
void num_set_f(my1num_t* num, double real, double imag);
void num_copy(my1num_t* dst, my1num_t* src);
void num_move(my1num_t* dst, my1num_t* src);
void num_add(my1num_t* dst, my1num_t* src1, my1num_t* src2);
void num_subtract(my1num_t* dst, my1num_t* src1, my1num_t* src2);
void num_multiply(my1num_t* dst, my1num_t* src1, my1num_t* src2);
void num_divide(my1num_t* dst, my1num_t* src1, my1num_t* src2);
void num_divrem(my1num_t* dst, my1num_t* src1, my1num_t* src2);
void num_power(my1num_t* dst, my1num_t* src1, my1num_t* src2);
/* math functions */
void num_log(my1num_t* dst, my1num_t* src);
void num_log10(my1num_t* dst, my1num_t* src);
void num_sin(my1num_t* dst, my1num_t* src);
void num_cos(my1num_t* dst, my1num_t* src);
void num_tan(my1num_t* dst, my1num_t* src);
void num_rad(my1num_t* dst, my1num_t* src);
void num_deg(my1num_t* dst, my1num_t* src);
/*----------------------------------------------------------------------------*/
#endif /** __MY1NUM_H__ */
/*----------------------------------------------------------------------------*/
