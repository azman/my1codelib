/*----------------------------------------------------------------------------*/
#include "my1keys.h"
/*----------------------------------------------------------------------------*/
#include <stdlib.h>
/*----------------------------------------------------------------------------*/
#ifndef DO_MINGW
/*----------------------------------------------------------------------------*/
#include <stdio.h> /* getchar? */
#include <unistd.h> /* STDIN_FILENO */
#include <termios.h> /* struct termios */
#include <fcntl.h> /* fcntl() */
/*----------------------------------------------------------------------------*/
#define MASK_LFLAG (ICANON|ECHO|ECHOE|ISIG)
/*----------------------------------------------------------------------------*/
int getch(void) {
	struct termios oldt, newt;
	int ch;
	tcgetattr(STDIN_FILENO, &oldt);
	newt = oldt;
	newt.c_lflag &= ~MASK_LFLAG;
	tcsetattr(STDIN_FILENO, TCSANOW, &newt);
	ch = getchar();
	tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
	return ch;
}
/*----------------------------------------------------------------------------*/
int kbhit(void) {
	struct termios oldt, newt;
	int ch;
	int oldf;
	tcgetattr(STDIN_FILENO, &oldt);
	newt = oldt;
	newt.c_lflag &= ~MASK_LFLAG;
	tcsetattr(STDIN_FILENO, TCSANOW, &newt);
	oldf = fcntl(STDIN_FILENO, F_GETFL, 0);
	fcntl(STDIN_FILENO, F_SETFL, oldf | O_NONBLOCK);
	ch = getchar();
	tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
	fcntl(STDIN_FILENO, F_SETFL, oldf);
	if(ch != EOF) {
		ungetc(ch, stdin);
		return 1;
	}
	return 0;
}
/*----------------------------------------------------------------------------*/
void makeraw_iterminal(void) {
	struct termios oldt, newt;
	tcgetattr(0,&oldt);
	newt = oldt;
	newt.c_lflag &= ~MASK_LFLAG;
	tcsetattr(0,TCSANOW, &newt);
}
/*----------------------------------------------------------------------------*/
void restore_iterminal(void) {
	struct termios oldt;
	tcgetattr(STDIN_FILENO, &oldt);
	oldt.c_lflag |= MASK_LFLAG; /* this is default?! */
	tcsetattr(0,TCSANOW, &oldt);
}
/*----------------------------------------------------------------------------*/
#endif
/*----------------------------------------------------------------------------*/
my1key_t get_keypress(my1key_t *extended) {
	my1key_t init, full, temp;
	init = getch();
	full = KEY_NONE;
	while (kbhit()) {
#ifdef DO_MINGW
		if (full==KEY_NONE) full = KEY_ESCAPE; /** init; */
#endif
		temp = getch();
		full <<= 8;
		full |= temp;
	}
	if (extended) *extended = full;
	return init;
}
/*----------------------------------------------------------------------------*/
my1key_t get_key(void) {
	my1key_t cKey, cEsc;
	cKey = get_keypress(&cEsc);
	if (cEsc!=KEY_NONE) cKey = cEsc;
	return cKey;
}
/*----------------------------------------------------------------------------*/
my1key_t get_keyhit(void) {
	if (!kbhit()) return KEY_NONE;
	return get_key();
}
/*----------------------------------------------------------------------------*/
my1key_t get_keyhit_extended(my1key_t *extended) {
	if (!kbhit()) return KEY_NONE;
	return get_keypress(extended);
}
/*----------------------------------------------------------------------------*/
int get_param_int(int argc, char* argv[], int *index, int *value) {
	(*index)++;
	if (*index>=argc) return -1;
	*value = atoi(argv[*index]);
	return 0;
}
/*----------------------------------------------------------------------------*/
char* get_param_str(int argc, char* argv[], int *index) {
	char* pparam = 0x0;
	(*index)++;
	if (*index<argc) pparam = argv[*index];
	return pparam;
}
/*----------------------------------------------------------------------------*/
#if defined(TEST_MODULE) && defined(TEST_KEYS)
/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
/*----------------------------------------------------------------------------*/
void about(char* name) {
	printf("\nA console key test interface.\n\n");
	printf("Use: %s [options]\n",name);
	printf("Options are:\n");
	printf("  --getint <int-param>: Test getting int param.\n");
	printf("  --getstr <str-param>: Test getting str param.\n");
	printf("\n");
}
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	my1key_t cKey, cEsc;
	int loop, test;
	char *ptest;

	if (argc>1) {
		for (loop=1;loop<argc;loop++) {
			putchar('\n');
			if (!strcmp(argv[loop],"--getint")) {
				if (get_param_int(argc,argv,&loop,&test)<0)
					printf("** Error getting int param!\n");
				else
					printf("Got Int Param: {%d}\n",test);
			}
			else if (!strcmp(argv[loop],"--getstr")) {
				if (!(ptest=get_param_str(argc,argv,&loop)))
					printf("** Error getting str param!\n");
				else
					printf("Got Str Param: \"%s\"!\n",ptest);
			}
			else printf("** Unknown Param: %s!\n",argv[loop]);
		}
		putchar('\n');
	}
	else about(argv[0]);
	printf("Press <ESC> to continue... ");
	while (1) {
		if (get_keyhit()==KEY_ESCAPE) {
			printf("OK.\n\n");
			break;
		}
	}
	/* start main loop */
	printf("Testing keypress... press <ESC> to exit.\n\n");
	while (1) {
		cKey = get_keypress(&cEsc);
		printf("You pressed the ");
		/* check for non-extended char */
		if (cEsc==KEY_NONE) {
			switch (cKey) {
				case KEY_ESCAPE: printf("[ESC]"); break;
				case KEY_ENTER: printf("[ENTER]"); break;
				case KEY_SPACE: printf("[SPACEBAR]"); break;
				case KEY_BSPACE: printf("[BACKSPACE]"); break;
				case KEY_TAB: printf("[TAB]"); break;
				default:
					/* check printable ASCII characters? */
					if(cKey>=0x20&&cKey<0x80) printf("'%c'",cKey);
					else printf("\b\b\b\ba");
			}
		}
		else {
			switch (cEsc) {
				case KEY_F1: printf("[F1]"); break;
				case KEY_F2: printf("[F2]"); break;
				case KEY_F3: printf("[F3]"); break;
				case KEY_F4: printf("[F4]"); break;
				case KEY_F5: printf("[F5]"); break;
				case KEY_F6: printf("[F6]"); break;
				case KEY_F7: printf("[F7]"); break;
				case KEY_F8: printf("[F8]"); break;
				case KEY_F9: printf("[F9]"); break;
				case KEY_F10: printf("[F10]"); break;
				case KEY_F11: printf("[F11]"); break;
				case KEY_F12: printf("[F12]"); break;
				case KEY_UP: printf("[UP]"); break;
				case KEY_DOWN: printf("[DOWN]"); break;
				case KEY_LEFT: printf("[LEFT]"); break;
				case KEY_RIGHT: printf("[RIGHT]"); break;
				case KEY_HOME: printf("[HOME]"); break;
				case KEY_END: printf("[END]"); break;
				case KEY_PAGEUP: printf("[PAGEUP]"); break;
				case KEY_PAGEDOWN: printf("[PAGEDOWN]"); break;
				case KEY_INSERT: printf("[INSERT]"); break;
				case KEY_DELETE: printf("[DELETE]"); break;
				case KEY_ALTF1: printf("[ALT+F1]"); break;
				case KEY_ALTF2: printf("[ALT+F2]"); break;
				case KEY_ALTF3: printf("[ALT+F3]"); break;
				case KEY_ALTF4: printf("[ALT+F4]"); break;
				case KEY_ALTF5: printf("[ALT+F5]"); break;
				case KEY_ALTF6: printf("[ALT+F6]"); break;
				case KEY_ALTF7: printf("[ALT+F7]"); break;
				case KEY_ALTF8: printf("[ALT+F8]"); break;
				case KEY_ALTF9: printf("[ALT+F9]"); break;
				case KEY_ALTF10: printf("[ALT+F10]"); break;
				case KEY_ALTF11: printf("[ALT+F11]"); break;
				case KEY_ALTF12: printf("[ALT+F12]"); break;
				default:
					if (cKey==KEY_ESCAPE&&((cEsc>=0x41&&cEsc<0x5B)||
						(cEsc>=0x61&&cEsc<0x7B)||(cEsc>=0x30&&cEsc<0x39)))
						printf("[ALT+'%c']",cEsc);
					else printf("\b\b\b\ban extended");
			}
		}
		printf(" key. (Raw info: Key=0x%08X cEsc=0x%08X)\n",cKey,cEsc);
		if (cEsc==KEY_NONE&&cKey==KEY_ESCAPE) {
			printf("\nUser Abort! Terminated!\n\n");
			break;
		}
	}
	return 0;
}
/*----------------------------------------------------------------------------*/
#endif
/*----------------------------------------------------------------------------*/
