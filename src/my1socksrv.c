/*----------------------------------------------------------------------------*/
#include "my1socksrv.h"
/*----------------------------------------------------------------------------*/
#include <string.h>
/*----------------------------------------------------------------------------*/
void socksrv_init(my1socksrv_t* psrv, int port) {
	psrv->code = 0x0;
	psrv->done = 0x0;
	psrv->tidy = 0x0;
	psrv->data = 0x0;
	psrv->port = port;
	psrv->ccnt = 1; /** listening connection count */
	psrv->flag = SERVER_READY;
	psrv->exit = 0;
	psrv->opt1 = 0;
	psrv->opt2 = 0;
	sock_init(&psrv->sock);
	psrv->conn.data = (void*)psrv;
}
/*----------------------------------------------------------------------------*/
void socksrv_free(my1socksrv_t* psrv) {
	if (psrv->tidy&&psrv->data)
		psrv->tidy(psrv->data);
	sock_free(&psrv->sock);
}
/*----------------------------------------------------------------------------*/
int socksrv_prep(my1socksrv_t* psrv) {
	sock_create(&psrv->sock);
	if (psrv->sock.sock==INVALID_SOCKET) {
		psrv->flag |= SERVER_ERROR_SOCK;
	}
	else {
		/* set self host info */
		psrv->sock.addr.sin_family = AF_INET;
		psrv->sock.addr.sin_port = htons(psrv->port);
		psrv->sock.addr.sin_addr.s_addr = htonl(INADDR_ANY);
		psrv->flag = SERVER_READY;
	}
	return psrv->flag;
}
/*----------------------------------------------------------------------------*/
int socksrv_exec(my1socksrv_t* psrv) {
	my1conn_t *tcon;
	int test;
	sock_server(&psrv->sock,psrv->ccnt);
	if (psrv->sock.flag&SOCKET_ERROR) {
		psrv->flag |= SERVER_ERROR_EXEC;
		return psrv->flag;
	}
	tcon = &psrv->conn;
	tcon->alen = sizeof(struct sockaddr);
	/* main server loop wait for connections */
	psrv->exit = 0;
	while (!psrv->exit) {
		test = sock_accept(&psrv->sock,tcon);
		/* allow external check to break this loop */
		if (psrv->done) {
			if ((*psrv->done)((void*)tcon))
				break;
		}
		if (!test) continue; /** EAGAIN / EWOULDBLOCK */
		else if (test<0) {
			/* socket NOT created - safe to return? */
			psrv->flag |= SERVER_ERROR_ACCEPT;
			break;
		}
		/* should create new thread for this??? */
		if (psrv->code)
			psrv->exit = (*psrv->code)((void*)tcon);
		/* clean up connection */
		sock_finish((my1sock_t*)tcon,0);
	}
	sock_finish(&psrv->sock,1);
	return psrv->flag;
}
/*----------------------------------------------------------------------------*/
#if defined(TEST_MODULE) && defined(TEST_SOCKSRV)
/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
/*----------------------------------------------------------------------------*/
#include "my1keys.c"
#include "my1sockbase.c"
/*----------------------------------------------------------------------------*/
#define SRVC_BUFFSIZE 1024
/*----------------------------------------------------------------------------*/
int socksrv_code(void* stuffs) {
	my1conn_t *pcon = (my1conn_t*) stuffs;
	char buff[SRVC_BUFFSIZE];
	int test, exit, loop;
	/* notify? */
	printf("Request from {%s}!\n",pcon->addr);
	/* loop to get all request data */
	exit = 0;
	while (1) {
		test = sock_peek(pcon->sock,SRVC_BUFFSIZE,(void*)buff);
		if (test<0) {
			printf("\nSocket Error? (%d)\n",test);
			break;
		}
		else if (test>0) {
			for (loop = 0;loop<test;loop++) {
				if (isascii(buff[loop])) putchar(buff[loop]);
				else printf("[0x%02X]",buff[loop]);
			}
			if (!strncmp(buff,"exit",5)) {
				exit = 1;
				break;
			}
		}
		else break; /* got nothing! */
	}
	return exit;
}
/*----------------------------------------------------------------------------*/
int socksrv_done(void* stuffs) {
	my1conn_t *pcon = (my1conn_t*) stuffs;
	my1socksrv_t *psrv = (my1socksrv_t*)pcon->data;
	if (kbhit()) {
		switch (getch()) {
			case (int)0x1b: /* escape key */
				printf("\nUser Abort Request!\n");
				psrv->exit = 1;
				return 1;
		}
	}
	return 0;
}
/*----------------------------------------------------------------------------*/
int socksrv_tidy(void* stuffs) {
	if (stuffs) free((void*)stuffs);
	return 0;
}
/*----------------------------------------------------------------------------*/
#define DEFAULT_PORT 1337
/*----------------------------------------------------------------------------*/
#define STAT_ERROR 0x80000000
#define STAT_ERROR_ARGS (STAT_ERROR|0x0001)
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	my1socksrv_t server;
	unsigned int stat;
	int loop, port;
	/* process parameters */
	port = DEFAULT_PORT;
	for (loop=1,stat=0;loop<argc;loop++) {
		if (argv[loop][0]=='-'&&argv[loop][1]=='-') {
			if (!strcmp(&argv[loop][2],"port")) {
				loop++;
				port = atoi(argv[loop]);
			}
			else {
				printf("** Unknown option '%s'! Aborting!\n",argv[loop]);
				stat |= STAT_ERROR_ARGS;
			}
		}
		else {
			printf("** Unknown parameter '%s'! Aborting!\n",argv[loop]);
			stat |= STAT_ERROR_ARGS;
		}
	}
	socksrv_init(&server,port);
	/** dummy loop */
	do
	{
		if (stat&STAT_ERROR) break;
		server.code = &socksrv_code;
		server.done = &socksrv_done;
		server.tidy = &socksrv_tidy;
		if (socksrv_prep(&server)) {
			printf("** Socket create error! (%d)\n",server.sock.flag);
			break;
		}
		printf("-- Running server @port %d!\n",port);
		printf("Press <ESC> to exit.\n");
		socksrv_exec(&server);
		/* finishing up... */
		printf("Server code done with code %d!\n",server.flag);
	} while(0); /* end of dummy loop */
	/* free up resources when done */
	socksrv_free(&server);
	return 0;
}
/*----------------------------------------------------------------------------*/
#endif
/*----------------------------------------------------------------------------*/
