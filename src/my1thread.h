/*----------------------------------------------------------------------------*/
#ifndef __MY1THREADH__
#define __MY1THREADH__
/*----------------------------------------------------------------------------*/
/**
 *  my1thread cross-platform thread library wrapper
 *  - written by azman@my1matrix.org
 *  - build: make thread DOLINK=-lpthread
 */
/*----------------------------------------------------------------------------*/
#define MY1THREAD_ERROR_CREATE_FAILED 1
/*----------------------------------------------------------------------------*/
#ifdef DO_MINGW
#include <windows.h>
typedef unsigned int pthread_t;
typedef unsigned __stdcall (*thread_code_t)(void*);
#else
#include <pthread.h>
typedef unsigned int HANDLE;
typedef void* (*thread_code_t)(void*);
#endif
/*----------------------------------------------------------------------------*/
typedef thread_code_t thread_code;
/*----------------------------------------------------------------------------*/
typedef struct __my1thread_t {
	int running, stopped;
	thread_code runcode;
	pthread_t id;
	HANDLE created;
} my1thread_t;
/*----------------------------------------------------------------------------*/
typedef my1thread_t my1thread;
/*----------------------------------------------------------------------------*/
void thread_init(my1thread_t* thread, thread_code_t pcode);
void thread_free(my1thread_t* thread);
int thread_exec(my1thread_t* thread,void* arg); /* returns non-zero on error*/
int thread_wait(my1thread_t* thread); /* wait for thread to end */
int thread_done(my1thread_t* thread);
int thread_created(my1thread_t* thread); /* returns non-zero if created */
/*----------------------------------------------------------------------------*/
#endif
/*----------------------------------------------------------------------------*/
