/*----------------------------------------------------------------------------*/
#include "my1list_sort.h"
/*----------------------------------------------------------------------------*/
#include <stdlib.h>
/*----------------------------------------------------------------------------*/
void list_sort(my1list_t* list, fcomp_item pcmp) {
	/* merge sort */
	my1item_t *head, *tail, *qcur, *temp;
	int size, loop, pchk, qchk;
	/* cannot do this without comparator function! */
	if (!pcmp) return;
	size = 1;
	head = list->init;
	while (head) {
		list->curr = head;
		list->step = 0;
		head = 0x0;
		tail = 0x0;
		while (list->curr) {
			list->step++;
			qcur = list->curr;
			/* count item in this set */
			for (loop=0;loop<size&&qcur!=0x0;loop++)
				qcur = qcur->next;
			pchk = loop; qchk = size;
			while (pchk>0||(qchk>0&&qcur)) {
				if (!pchk||!qchk||!qcur) {
					if (!pchk) {
						temp = qcur;
						qcur = qcur->next;
						qchk--;
					}
					else {
						temp = list->curr;
						list->curr = list->curr->next;
						pchk--;
					}
				}
				else {
					if ((*pcmp)(list->curr->data,qcur->data)>0) {
						temp = qcur;
						qcur = qcur->next;
						qchk--;
					}
					else {
						temp = list->curr;
						list->curr = list->curr->next;
						pchk--;
					}
				}
				temp->next = 0x0;
				if (tail) tail->next = temp;
				else head = temp;
				tail = temp;
			}
			list->curr = qcur;
		}
		/* we are done when only at most 1 merge occurs */
		if (list->step<=1) {
			/* repair prev links */
			list->curr = 0x0;
			qcur = head;
			while (qcur) {
				qcur->prev = list->curr;
				list->curr = qcur;
				qcur = qcur->next;
			}
			/** list->curr should be tail */
			/* update list */
			list->init = head;
			list->last = tail;
			break;
		}
		size <<= 1; /* double size */
	}
}
/*----------------------------------------------------------------------------*/
#if defined(TEST_MODULE) && defined(TEST_LIST_SORT)
/*----------------------------------------------------------------------------*/
#define TEST_LIST
#include "my1list.c"
/*----------------------------------------------------------------------------*/
#endif
/*----------------------------------------------------------------------------*/
