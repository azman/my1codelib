/*----------------------------------------------------------------------------*/
#ifndef __MY1LIST_DATA_H__
#define __MY1LIST_DATA_H__
/*----------------------------------------------------------------------------*/
/**
 *  my1list_data_t is an extension for my1list_t
 *  - written by azman@my1matrix.org
 *  - for lists that does not need to deal with item container
 *  - it DOES NOT handle data cleanup (feature in the old my1list)
**/
/*----------------------------------------------------------------------------*/
#include "my1list.h"
/*----------------------------------------------------------------------------*/
/* to be used in list_init */
#define LIST_OF_DATA 0x0
/*----------------------------------------------------------------------------*/
void* list_push_data(my1list_t*,void*);
void* list_pull_data(my1list_t*);
void* list_pop_data(my1list_t*); /* stack support */
void* list_scan_data(my1list_t*);
/*----------------------------------------------------------------------------*/
#endif /** __MY1LIST_DATA_H__ */
/*----------------------------------------------------------------------------*/
