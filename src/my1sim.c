/*----------------------------------------------------------------------------*/
#include "my1sim.h"
/*----------------------------------------------------------------------------*/
#include <stdlib.h>
#include <string.h>
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void simthing_init(my1simthing_t *pthing, char *name, void *data) {
	strncpy(pthing->name,name,SIM_NAMESIZE);
	pthing->name[SIM_NAMESIZE-1] = 0x0; /* just in case */
	pthing->flag = 0; /* used by app? */
	pthing->data = data;
	pthing->pcode = 0x0; /** assign this manually */
}
/*----------------------------------------------------------------------------*/
int simthing_chkname(my1simthing_t *pthing, char *name) {
	return strncmp(pthing->name,name,SIM_NAMESIZE);
}
/*----------------------------------------------------------------------------*/
void simthing_doevent(my1simthing_t *pthing, word32_t time, int flag) {
	if (pthing->pcode)
		pthing->pcode(pthing->data,time,
			SIMTHING_CMD_EVENT|(flag&SIMEVENT_MASK));
}
/*----------------------------------------------------------------------------*/
void simthing_doreset(my1simthing_t *pthing, word32_t time) {
	if (pthing->pcode)
		pthing->pcode(pthing->data,time,SIMTHING_CMD_RESET);
}
/*----------------------------------------------------------------------------*/
void simthing_docheck(my1simthing_t *pthing, word32_t time) {
	if (pthing->pcode)
		pthing->pcode(pthing->data,time,SIMTHING_CMD_CHECK);
}
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void simevent_init(my1simevent_t *pevent, word32_t type, word32_t time) {
	pevent->type = type;
	pevent->time_pick = time;
	pevent->time_next = 0;
	pevent->time_swap = 0;
	pevent->pthing = 0x0; /* assign this manually! */
}
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void timesim_init(my1timesim_t *psim) {
	psim->time_unit = 0;
	psim->time_complete = 0;
	psim->data = 0x0;
	/** assign these 'manually' */
	psim->pcode_check = 0x0;
	psim->pcode_mtime = 0x0;
	psim->pcode_ntime = 0x0;
	/** setup simulation object/event lists */
	list_init(&psim->things,list_free_item);
	list_init(&psim->events,list_free_item);
}
/*----------------------------------------------------------------------------*/
void timesim_free(my1timesim_t *psim) {
	psim->time_unit = 0;
	psim->time_complete = 0;
	psim->data = 0x0;
	psim->pcode_check = 0x0;
	psim->pcode_mtime = 0x0;
	psim->pcode_ntime = 0x0;
	/** clean simulation object/event lists */
	list_free(&psim->things);
	list_free(&psim->events);
}
/*----------------------------------------------------------------------------*/
void timesim_reset(my1timesim_t *psim) {
	psim->time_unit = 0;
	psim->time_complete = 0;
	/* reset all things */
	psim->things.curr = 0x0;
	while (list_scan_item(&psim->things)) {
		my1simthing_t *pthing = (my1simthing_t*)psim->things.curr->data;
		simthing_doreset(pthing,psim->time_unit);
	}
}
/*----------------------------------------------------------------------------*/
void* timesim_addthing(my1timesim_t *psim, my1simthing_t *pthing) {
	return (void*)list_push_item_data(&psim->things,(void*)pthing);
}
/*----------------------------------------------------------------------------*/
void* timesim_addevent(my1timesim_t *psim, my1simevent_t *pevent) {
	if (!psim->events.init)
		return (void*)list_push_item_data(&psim->events,(void*)pevent);
	/* events is time-sorted list */
	psim->events.curr = 0x0;
	while (list_scan_item(&psim->events)) {
		my1simevent_t *pcheck = (my1simevent_t*)psim->events.curr->data;
		if (pevent->time_pick<pcheck->time_pick)
			break;
	}
	return (void*)list_next_item_data(&psim->events,
		psim->events.curr,(void*)pevent);
}
/*----------------------------------------------------------------------------*/
void timesim_run(my1timesim_t *psim, word32_t time_unit) {
	int do_event = 0, do_count = 0;
	while (!do_event) /* stop when there is an event! */ {
		/* pre-sim check */
		if (psim->pcode_check)
			(*psim->pcode_check)((void*)psim,psim->time_unit,0);
		/* process all due events */
		while (1) {
			my1simevent_t *pevent = (my1simevent_t*) psim->events.init->data;
			if (!pevent) break;
			if (pevent->time_pick>psim->time_unit)
				break;
			/* get the thing related to this event */
			simthing_doevent(pevent->pthing,psim->time_unit,pevent->type);
			/* remove from list */
			list_pull_item(&psim->events);
			/* reinsert? */
			if (pevent->type==SIMEVENT_REPETETIVE&&pevent->time_next!=0) {
				word32_t swap = pevent->time_swap;
				pevent->time_pick += pevent->time_next;
				if (swap) {
					pevent->time_swap = pevent->time_next;
					pevent->time_next = swap;
				}
				/* need to properly insert! sorted list! */
				timesim_addevent(psim,pevent);
			}
			else free((void*)pevent);
			/* flag an event */
			do_event = 1;
		}
		/* pre-TU update - after event, before thing! */
		if (psim->pcode_mtime)
			(*psim->pcode_mtime)((void*)psim,psim->time_unit,0);
		/* process all things */
		psim->things.curr = 0x0;
		while(list_scan_item(&psim->things)) {
			my1simthing_t *pthing = (my1simthing_t*)psim->things.curr->data;
			simthing_docheck(pthing,psim->time_unit);
		}
		/* update time-unit */
		psim->time_unit++;
		/* post-TU update */
		if (psim->pcode_ntime)
			(*psim->pcode_ntime)((void*)psim,psim->time_unit,0);
		/* local time count for time requests */
		do_count++;
		if (time_unit>0&&time_unit==do_count)
			break;
	}
}
/*----------------------------------------------------------------------------*/
#if defined(TEST_MODULE) && defined(TEST_SIM)
/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
/*----------------------------------------------------------------------------*/
/**
 * simple logic simulator?
**/
/*----------------------------------------------------------------------------*/
#define LOGIC_NAMESIZE 64
#define TOTAL_SIM_TIME 100
/*----------------------------------------------------------------------------*/
typedef enum _my1logic_t {
	logic_1='1', logic_0='0',
	strong_1='H', strong_0='L',
	weak_1='h', weak_0='l',
	high_z='Z', unknown='X'
} my1logic_t;
/*----------------------------------------------------------------------------*/
#define LOGIC_STRONG 3
#define LOGIC_NORMAL 2
#define LOGIC_ISWEAK 1
/*----------------------------------------------------------------------------*/
int logic_strength(my1logic_t level) {
	int test = 0;
	switch (level) {
		case strong_1: test++;
		case logic_1: test++;
		case weak_1: test++; break;
		case strong_0: test--;
		case logic_0: test--;
		case weak_0: test--; break;
		case high_z:
		case unknown: break;
	}
	return test;
}
/*----------------------------------------------------------------------------*/
my1logic_t logic_getlevel(int strength) {
	if (!strength)
		return unknown;
	if (strength>0) {
		if (strength<LOGIC_NORMAL) return weak_1;
		if (strength<LOGIC_STRONG) return logic_1;
		return strong_1;
	}
	else /* if (strength<0) */ {
		strength = -strength;
		if (strength<LOGIC_NORMAL) return weak_0;
		if (strength<LOGIC_STRONG) return logic_0;
		return strong_0;
	}
}
/*----------------------------------------------------------------------------*/
my1logic_t logic_invert(my1logic_t src1) {
	int rate = logic_strength(src1);
	return logic_getlevel(-rate);
}
/*----------------------------------------------------------------------------*/
my1logic_t logic_or(my1logic_t src1, my1logic_t src2) {
	int chk1 = logic_strength(src1);
	int chk2 = logic_strength(src2);
	my1logic_t test = logic_0;
	if (!chk1||!chk2)
		return unknown;
	if (chk1>0||chk2>0)
		test = logic_1;
	return test;
}
/*----------------------------------------------------------------------------*/
my1logic_t logic_and(my1logic_t src1, my1logic_t src2) {
	int chk1 = logic_strength(src1);
	int chk2 = logic_strength(src2);
	my1logic_t test = logic_1;
	if (!chk1||!chk2)
		return unknown;
	if (chk1<0||chk2<0)
		test = logic_0;
	return test;
}
/*----------------------------------------------------------------------------*/
my1logic_t logic_xor(my1logic_t src1, my1logic_t src2) {
	int chk1 = logic_strength(src1);
	int chk2 = logic_strength(src2);
	my1logic_t test = logic_1;
	if (!chk1||!chk2)
		return unknown;
	if ((chk1<0&&chk2<0)||(chk1>0&&chk2>0))
		test = logic_0;
	return test;
}
/*----------------------------------------------------------------------------*/
typedef enum _my1logic_op_t {
	none=' ', inv='~', or='|', and='&', xor='^'
} my1logic_op_t;
/*----------------------------------------------------------------------------*/
#define SIMTHING_FLAG_NODE 0
#define SIMTHING_FLAG_GATE 1
/*----------------------------------------------------------------------------*/
#define NODE_RESET  0x00
#define NODE_LOGIC  0x01
#define NODE_SOURCE 0x02
/*----------------------------------------------------------------------------*/
#define NODE_MASK SIMEVENT_MASK
#define NODE_SET1 SIMEVENT_ACTIVATE
#define NODE_SET0 SIMEVENT_DEACTIVATE
#define NODE_TOG1 SIMEVENT_ONE_SHOT
#define NODE_TOG2 SIMEVENT_REPETETIVE
/*----------------------------------------------------------------------------*/
typedef struct _my1node_t {
	my1list_t driver; /* incoming logic => nodes! */
	my1logic_t target; /* logic level of this node */
	my1logic_op_t gateop; /* node is logic gate when gateop!=none */
	int xzflag; /* execute flag */
	char name[LOGIC_NAMESIZE];
} my1node_t;
/*----------------------------------------------------------------------------*/
void node_init(my1node_t* node) {
	list_init(&node->driver,0x0);
	node->target = unknown;
	node->gateop = none;
	node->xzflag = NODE_RESET;
	node->name[0] = 0x0;
}
/*----------------------------------------------------------------------------*/
void node_free(my1node_t* node) {
	list_free(&node->driver);
}
/*----------------------------------------------------------------------------*/
void node_driver(my1node_t* node,my1node_t* buff) {
	list_push_item_data(&node->driver,(void*)buff);
}
/*----------------------------------------------------------------------------*/
my1node_t* node_driver_find(my1node_t* node,char* name) {
	my1node_t* find = 0x0;
	node->driver.curr = 0x0;
	while (list_scan_item(&node->driver)) {
		my1node_t* temp = (my1node_t*) node->driver.curr->data;
		if (!strncmp(temp->name,name,LOGIC_NAMESIZE)) {
			find = temp;
			break;
		}
	}
	return find;
}
/*----------------------------------------------------------------------------*/
void node_reset(my1node_t* node) {
	node->xzflag &= ~NODE_LOGIC;
}
/*----------------------------------------------------------------------------*/
void node_source(my1node_t* node) {
	node->xzflag |= NODE_SOURCE;
}
/*----------------------------------------------------------------------------*/
my1logic_t node_logic(my1node_t* node) {
	if (node->xzflag&NODE_LOGIC||node->xzflag&NODE_SOURCE)
		return node->target;
	if (!node->driver.size) {
		/* nothing driving this node! */
		node->target = high_z;
	}
	else {
		/* check operation */
		switch (node->gateop) {
			case none: {
				int rate = 0;
				node->driver.curr = 0x0;
				while(list_scan_item(&node->driver)) {
					my1node_t* temp = (my1node_t*) node->driver.curr->data;
					rate += logic_strength(node_logic(temp));
				}
				node->target = logic_getlevel(rate);
				break;
			}
			case inv: {
				/* only use first - ignore the rest? */
				my1node_t* temp = (my1node_t*) node->driver.init->data;
				if (temp)
					node->target = logic_invert(node_logic(temp));
				else
					node->target = unknown;
				break;
			}
			case or: {
				int rate;
				my1logic_t test = logic_0;
				node->driver.curr = 0x0;
				while (list_scan_item(&node->driver)) {
					my1node_t* temp = (my1node_t*) node->driver.curr->data;
					rate = logic_strength(node_logic(temp));
					if (!rate) {
						test = unknown;
						break;
					}
					else if (rate>0) {
						test = logic_1;
						break;
					}
				}
				node->target = test;
				break;
			}
			case and: {
				int rate;
				my1logic_t test = logic_1;
				node->driver.curr = 0x0;
				while (list_scan_item(&node->driver)) {
					my1node_t* temp = (my1node_t*) node->driver.curr->data;
					rate = logic_strength(node_logic(temp));
					if (!rate) {
						test = unknown;
						break;
					}
					else if (rate<0) {
						test = logic_0;
						break;
					}
				}
				node->target = test;
				break;
			}
			case xor: {
				my1logic_t test = logic_0;
				node->driver.curr = 0x0;
				while (list_scan_item(&node->driver)) {
					my1node_t* temp = (my1node_t*) node->driver.curr->data;
					test = logic_xor(test,temp->target);
				}
				node->target = test;
				break;
			}
			default: node->target = unknown;
		}
	}
	node->xzflag |= NODE_LOGIC;
	return node->target;
}
/*----------------------------------------------------------------------------*/
void node_check_event(void* that,word32_t time,int flag) {
	my1node_t *node = (my1node_t*) that;
	switch (flag&NODE_MASK) {
		case NODE_SET1: node->target = logic_1; node_source(node); break;
		case NODE_SET0: node->target = logic_0; node_source(node); break;
		case NODE_TOG1:
		case NODE_TOG2: {
			int rate = logic_strength(node->target);
			/* it is a source - if no rate, generate! */
			if (!rate) rate = -LOGIC_NORMAL;
			node->target = logic_invert(logic_getlevel(rate));
			node_source(node);
			break;
		}
		default: node_logic(node);
	}
/*
	printf("[DEBUG] NodeCheck:{Node:%s,Logic:%c,Flag:%d}\n",
		node->name,(char)node->target,flag);
*/
}
/*----------------------------------------------------------------------------*/
typedef struct _my1gate_t {
	my1node_t node;
	char name[LOGIC_NAMESIZE];
} my1gate_t;
/*----------------------------------------------------------------------------*/
void gate_init(my1gate_t* gate, my1logic_op_t op) {
	node_init(&gate->node);
	gate->node.gateop = op;
	gate->name[0] = 0x0;
}
/*----------------------------------------------------------------------------*/
void gate_free(my1gate_t* gate) {
	node_free(&gate->node);
}
/*----------------------------------------------------------------------------*/
void gate_reset(my1gate_t* gate) {
	node_reset(&gate->node);
}
/*----------------------------------------------------------------------------*/
my1logic_t gate_logic(my1gate_t* gate) {
	return node_logic(&gate->node);
}
/*----------------------------------------------------------------------------*/
void gate_check_event(void* that,word32_t time,int flag) {
	my1gate_t *gate = (my1gate_t*) that;
	gate_logic(gate);
}
/*----------------------------------------------------------------------------*/
typedef struct _my1logic_circuit_t {
	int error;
	my1list_t nodes;
	my1list_t gates;
	my1timesim_t dosim;
} my1logic_circuit_t;
/*----------------------------------------------------------------------------*/
void circuit_init(my1logic_circuit_t* cir) {
	cir->error = 0;
	list_init(&cir->nodes,list_free_item);
	list_init(&cir->gates,list_free_item);
	timesim_init(&cir->dosim);
	cir->dosim.data = (void*) cir;
}
/*----------------------------------------------------------------------------*/
void circuit_free(my1logic_circuit_t* cir) {
	timesim_free(&cir->dosim);
	list_free(&cir->gates);
	list_free(&cir->nodes);
}
/*----------------------------------------------------------------------------*/
my1node_t* circuit_insert_node(my1logic_circuit_t* cir, char* name) {
	my1node_t* node = (my1node_t*) malloc(sizeof(my1node_t));
	node_init(node);
	strncpy(node->name,name,LOGIC_NAMESIZE);
	list_push_item_data(&cir->nodes,(void*)node);
	return node;
}
/*----------------------------------------------------------------------------*/
my1node_t* circuit_search_node(my1logic_circuit_t* cir, char* name) {
	my1node_t* node = 0x0;
	cir->nodes.curr = 0x0;
	while (list_scan_item(&cir->nodes)) {
		my1node_t* test = (my1node_t*) cir->nodes.curr->data;
		if (!strncmp(test->name,name,LOGIC_NAMESIZE)) {
			node = test;
			break;
		}
	}
	return node;
}
/*----------------------------------------------------------------------------*/
my1node_t* circuit_check_node(my1logic_circuit_t* cir, char* name) {
	/* search/create node */
	my1node_t* node = circuit_search_node(cir,name);
	if (!node) {
		node = circuit_insert_node(cir,name);
		if (!node) return node;
		/* 1 node => 1 simthing */
		my1simthing_t *pthing = (my1simthing_t*) malloc(sizeof(my1simthing_t));
		simthing_init(pthing,name,(void*)node);
		pthing->flag = SIMTHING_FLAG_NODE;
		/* usercode? */
		pthing->pcode = &node_check_event;
		/* add to sim! */
		timesim_addthing(&cir->dosim,pthing);
	}
	return node;
}
/*----------------------------------------------------------------------------*/
my1simthing_t* circuit_node_get_thing(my1logic_circuit_t* cir,
		my1node_t* node) {
	my1simthing_t *pcheck = 0x0;
	cir->dosim.things.curr = 0x0;
	while (list_scan_item(&cir->dosim.things)) {
		my1simthing_t *pthing = (my1simthing_t*)cir->dosim.things.curr->data;
		my1node_t *temp = (my1node_t*) pthing->data;
		if (!strncmp(node->name,temp->name,LOGIC_NAMESIZE)) {
			pcheck = pthing;
			break;
		}
	}
	return pcheck;
}
/*----------------------------------------------------------------------------*/
my1gate_t* circuit_insert_gate(my1logic_circuit_t* cir, char* name,
		my1logic_op_t lop) {
	my1gate_t* gate = (my1gate_t*) malloc(sizeof(my1gate_t));
	gate_init(gate,lop);
	strncpy(gate->name,name,LOGIC_NAMESIZE);
	list_push_item_data(&cir->gates,(void*)gate);
	return gate;
}
/*----------------------------------------------------------------------------*/
my1gate_t* circuit_search_gate(my1logic_circuit_t* cir, char* name) {
	my1gate_t* gate = 0x0;
	cir->gates.curr = 0x0;
	while (list_scan_item(&cir->gates)) {
		my1gate_t* test = (my1gate_t*) cir->gates.curr->data;
		if (!strncmp(test->name,name,LOGIC_NAMESIZE)) {
			gate = test;
			break;
		}
	}
	return gate;
}
/*----------------------------------------------------------------------------*/
my1gate_t* circuit_check_gate(my1logic_circuit_t* cir, char* name,
		my1logic_op_t lop) {
	/* check duplicate gate */
	my1gate_t* gate = circuit_search_gate(cir,name);
	if (gate) {
		cir->error++;
		return 0x0;
	}
	gate = circuit_insert_gate(cir,name,lop);
	if (!gate) return gate;
	/* 1 gate => 1 simthing */
	my1simthing_t *pthing = (my1simthing_t*) malloc(sizeof(my1simthing_t));
	simthing_init(pthing,name,(void*)gate);
	pthing->flag = SIMTHING_FLAG_GATE;
	/* add to sim! */
	timesim_addthing(&cir->dosim,pthing);
	return gate;
}
/*----------------------------------------------------------------------------*/
my1simthing_t* circuit_gate_get_thing(my1logic_circuit_t* cir,
		my1gate_t* gate) {
	my1simthing_t *pcheck = 0x0;
	cir->dosim.things.curr = 0x0;
	while (list_scan_item(&cir->dosim.things)) {
		my1simthing_t *pthing = (my1simthing_t*)cir->dosim.things.curr->data;
		my1gate_t *temp = (my1gate_t*) pthing->data;
		if (!strncmp(gate->name,temp->name,LOGIC_NAMESIZE)) {
			pcheck = pthing;
			break;
		}
	}
	return pcheck;
}
/*----------------------------------------------------------------------------*/
void circuit_flag_logic(void* that,word32_t time,int flag) {
	my1timesim_t* sim = (my1timesim_t*) that;
	/* flag all logic as 'old', to be reevaluate on next cycle */
	sim->things.curr = 0x0;
	while (list_scan_item(&sim->things)) {
		my1simthing_t* thing = (my1simthing_t*)sim->things.curr->data;
		if (thing->flag==SIMTHING_FLAG_NODE) {
			my1node_t* node = (my1node_t*) thing->data;
			node_reset(node);
		}
		else if (thing->flag==SIMTHING_FLAG_GATE) {
			my1gate_t* gate = (my1gate_t*) thing->data;
			gate_reset(gate);
		}
	}
}
/*----------------------------------------------------------------------------*/
void debug_print_event(my1simevent_t* event) {
	printf("Event(%p):{Type:%04x,Pick:%u,Next:%u,Swap:%u,Thing:%s}\n",event,
		event->type,event->time_pick,event->time_next,event->time_swap,
		event->pthing->name);
}
/*----------------------------------------------------------------------------*/
void circuit_show_logic(void* that,word32_t time,int flag) {
	my1timesim_t* sim = (my1timesim_t*) that;
	my1logic_circuit_t* cir = (my1logic_circuit_t*) sim->data;
	printf("\n|%3d|",sim->time_unit);
	cir->nodes.curr = 0x0;
	while (list_scan_item(&cir->nodes)) {
		my1node_t* node = (my1node_t*)cir->nodes.curr->data;
		printf(" %c |",(char)node->target);
	}
}
/*----------------------------------------------------------------------------*/
/**
 * circuit file format:
 * [gate/circuit definition]
 * <inv|or|and|xor> <gate-name> <op-node> <ip-node> [ip-node]
 * [simulation definition]
 * generate <node> <1|0|squarewave> <tu-delay> [tu-high] [tu-low]
 * simulate <tu-count>
 *
 * sample file:
xor g1 2 3 4
generate 4 squarewave 2 2 2
generate 3 squarewave 4 4 4
generate 4 0 0
generate 3 0 0
simulate 16
**/
/*----------------------------------------------------------------------------*/
#include "my1text.h"
#include "my1strtok.h"
/*----------------------------------------------------------------------------*/
#define COMMON_DELIM " \n\r\t"
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	my1text_t text;
	my1logic_circuit_t logic;
	printf("\n");
	printf("----------------------------------------\n");
	printf("Simple Logic Simulator (Based on my1sim)\n");
	printf("----------------------------------------\n");
	/* read in circuit file */
	if (argc<2) {
		printf("\nUsage: %s <circuit-file>\n\n",argv[0]);
		return -1;
	}
	text_init(&text);
	text_open(&text,argv[1]);
	if (!text.pfile)
		printf("\n** Cannot open file '%s'!\n",text.fname);
	else {
		my1strtok_t find;
		strtok_init(&find,COMMON_DELIM);
		/* initialize sim control */
		circuit_init(&logic);
		logic.dosim.pcode_check = &circuit_show_logic;
		logic.dosim.pcode_ntime = &circuit_flag_logic;
		printf("\nReading circuit from '%s'... ",text.fname);
		while (text_read(&text)>=CHAR_INIT) {
			strtok_prep(&find,&text.pbuff);
			/* empty line? */
			if (!strtok_next(&find)) continue;
			/* commented out? */
			if (find.token.buff[0]=='#') continue;
			/* check first level */
			if (!strncmp(find.token.buff,"simulate",8)) {
				do /* dummy loop - begin */ {
					/* try to get simulation time */
					if (!strtok_next(&find)) {
						printf("\n[ERROR:%d] simulate without time!\n",
							text.iline);
						logic.error++;
						break;
					}
					logic.dosim.time_complete =
						(word32_t)strtoul(find.token.buff,0x0,10);
					if (!logic.dosim.time_complete) {
						printf("\n[ERROR:%d] simulate time zero?!\n",
							text.iline);
						logic.error++;
						break;
					}
					/* any extra param? */
					if (strtok_next(&find)) {
						printf("\n[ERROR:%d] simulate command extra!\n",
							text.iline);
						logic.error++;
					}
				} while(0); /* dummy loop - ends */
			}
			else if (!strncmp(find.token.buff,"generate",8)) {
				do /* dummy loop - begin */ {
					my1node_t *node;
					my1simthing_t *pthing;
					my1simevent_t *pevent = 0x0;
					/* try to get node */
					if (!strtok_next(&find)) {
						printf("\n[ERROR:%d] generate without node!\n",
							text.iline);
						logic.error++;
						break;
					}
					node = circuit_check_node(&logic,find.token.buff);
					if (!node) {
						printf("\n[ERROR] Error creating node '%s'!\n",
							find.token.buff);
						logic.error++;
						break;
					}
					/* not driven is high-imp! */
					node->target = high_z;
					pthing = circuit_node_get_thing(&logic,node);
					if (!pthing) {
						printf("\n[ERROR:%d] generate no thing?!\n",
							text.iline);
						logic.error++;
						break;
					}
					/* try to get generate value */
					if (!strtok_next(&find)) {
						printf("\n[ERROR:%d] generate without value!\n",
							text.iline);
						logic.error++;
						break;
					}
					/* process value */
					if (!strncmp(find.token.buff,"squarewave",8)) {
						pevent = (my1simevent_t*) malloc(sizeof(my1simevent_t));
						simevent_init(pevent,NODE_TOG2,0);
					}
					else if (find.token.fill==1) {
						/* 1 or zero! */
						if (find.token.buff[0]=='1') {
							pevent = (my1simevent_t*)
								malloc(sizeof(my1simevent_t));
							simevent_init(pevent,NODE_SET1,0);
						}
						else if (find.token.buff[0]=='0') {
							pevent = (my1simevent_t*)
								malloc(sizeof(my1simevent_t));
							simevent_init(pevent,NODE_SET0,0);
						}
					}
					/* an error in no event created? */
					if (!pevent) {
						printf("\n[ERROR:%d] generate no event!\n",text.iline);
						logic.error++;
						break;
					}
					/* try to get delay value */
					if (!strtok_next(&find)) {
						printf("\n[ERROR:%d] generate command no delay!\n",
							text.iline);
						logic.error++;
						break;
					}
					pevent->time_pick =
						(word32_t)strtoul(find.token.buff,0x0,10);
					/* repetetive event need a couple more */
					if (pevent->type==SIMEVENT_REPETETIVE) {
						/* try to get tu-high value */
						if (!strtok_next(&find)) {
							printf("\n[ERROR:%d] generate without tu-high!\n",
								text.iline);
							logic.error++;
							break;
						}
						pevent->time_next =
							(word32_t)strtoul(find.token.buff,0x0,10);
						/* try to get tu-low value */
						if (!strtok_next(&find)) {
							printf("\n[ERROR:%d] generate without tu-low!\n",
								text.iline);
							logic.error++;
							break;
						}
						pevent->time_swap =
							(word32_t)strtoul(find.token.buff,0x0,10);
					}
					/* link a thing to this event */
					pevent->pthing = pthing;
					/* add to sim */
					timesim_addevent(&logic.dosim,pevent);
					/* any extra param? */
					if (strtok_next(&find)) {
						printf("\n[ERROR:%d] generate command extra!\n",
							text.iline);
						logic.error++;
					}
				} while(0); /* dummy loop - ends */
			}
			/* logic gates */
			else if (!strncmp(find.token.buff,"inv",3)) {
				do /* dummy loop - begin */ {
					my1gate_t *gate;
					my1node_t *node;
					my1simthing_t *pthing;
					/* try to get gate */
					if (!strtok_next(&find)) {
						printf("\n[ERROR:%d] inv gate name missing!\n",
							text.iline);
						logic.error++;
						break;
					}
					gate = circuit_check_gate(&logic,find.token.buff,inv);
					if (!gate) {
						printf("\n[ERROR] Error creating inv gate!\n");
						logic.error++;
						break;
					}
					pthing = circuit_gate_get_thing(&logic,gate);
					if (!pthing) {
						printf("\n[ERROR:%d] inv no thing?!\n",
							text.iline);
						logic.error++;
						break;
					}
					/* try to get op node */
					if (!strtok_next(&find)) {
						printf("\n[ERROR:%d] inv without op node!\n",
							text.iline);
						logic.error++;
						break;
					}
					node = circuit_check_node(&logic,find.token.buff);
					if (!node) {
						printf("\n[ERROR] inv node issue '%s'!\n",
							find.token.buff);
						logic.error++;
						break;
					}
					/* assign gate output to drive that node */
					node_driver(node,&gate->node);
					/* try to get ip node */
					if (!strtok_next(&find)) {
						printf("\n[ERROR:%d] inv without ip node!\n",
							text.iline);
						logic.error++;
						break;
					}
					node = circuit_check_node(&logic,find.token.buff);
					if (!node) {
						printf("\n[ERROR] inv node issue '%s'!\n",
							find.token.buff);
						logic.error++;
						break;
					}
					/* assign gate input */
					node_driver(&gate->node,node);
				} while(0); /* dummy loop - ends */
			}
			else if (!strncmp(find.token.buff,"or",2)) {
				do /* dummy loop - begin */ {
					my1gate_t *gate;
					my1node_t *node;
					my1simthing_t *pthing;
					/* try to get gate */
					if (!strtok_next(&find)) {
						printf("\n[ERROR:%d] or gate name missing!\n",
							text.iline);
						logic.error++;
						break;
					}
					gate = circuit_check_gate(&logic,find.token.buff,or);
					if (!gate) {
						printf("\n[ERROR] Error creating or gate!\n");
						logic.error++;
						break;
					}
					pthing = circuit_gate_get_thing(&logic,gate);
					if (!pthing) {
						printf("\n[ERROR:%d] or no thing?!\n",
							text.iline);
						logic.error++;
						break;
					}
					/* try to get op node */
					if (!strtok_next(&find)) {
						printf("\n[ERROR:%d] or without op node!\n",
							text.iline);
						logic.error++;
						break;
					}
					node = circuit_check_node(&logic,find.token.buff);
					if (!node) {
						printf("\n[ERROR] or node issue '%s'!\n",
							find.token.buff);
						logic.error++;
						break;
					}
					/* assign gate output to drive that node */
					node_driver(node,&gate->node);
					/* try to get ip node */
					if (!strtok_next(&find)) {
						printf("\n[ERROR:%d] or without ip node!\n",
							text.iline);
						logic.error++;
						break;
					}
					node = circuit_check_node(&logic,find.token.buff);
					if (!node) {
						printf("\n[ERROR] or node issue '%s'!\n",
							find.token.buff);
						logic.error++;
						break;
					}
					/* assign gate input */
					node_driver(&gate->node,node);
					/* try to get ip node */
					if (!strtok_next(&find)) {
						printf("\n[ERROR:%d] or without ip2 node!\n",
							text.iline);
						logic.error++;
						break;
					}
					node = circuit_check_node(&logic,find.token.buff);
					if (!node) {
						printf("\n[ERROR] or node issue '%s'!\n",
							find.token.buff);
						logic.error++;
						break;
					}
					/* assign gate input */
					node_driver(&gate->node,node);
				} while(0); /* dummy loop - ends */
			}
			else if (!strncmp(find.token.buff,"and",3)) {
				do /* dummy loop - begin */ {
					my1gate_t *gate;
					my1node_t *node;
					my1simthing_t *pthing;
					/* try to get gate */
					if (!strtok_next(&find)) {
						printf("\n[ERROR:%d] and gate name missing!\n",
							text.iline);
						logic.error++;
						break;
					}
					gate = circuit_check_gate(&logic,find.token.buff,and);
					if (!gate) {
						printf("\n[ERROR] Error creating and gate!\n");
						logic.error++;
						break;
					}
					pthing = circuit_gate_get_thing(&logic,gate);
					if (!pthing) {
						printf("\n[ERROR:%d] and no thing?!\n",
							text.iline);
						logic.error++;
						break;
					}
					/* try to get op node */
					if (!strtok_next(&find)) {
						printf("\n[ERROR:%d] and without op node!\n",
							text.iline);
						logic.error++;
						break;
					}
					node = circuit_check_node(&logic,find.token.buff);
					if (!node) {
						printf("\n[ERROR] and node issue '%s'!\n",
							find.token.buff);
						logic.error++;
						break;
					}
					/* assign gate output to drive that node */
					node_driver(node,&gate->node);
					/* try to get ip node */
					if (!strtok_next(&find)) {
						printf("\n[ERROR:%d] and without ip node!\n",
							text.iline);
						logic.error++;
						break;
					}
					node = circuit_check_node(&logic,find.token.buff);
					if (!node) {
						printf("\n[ERROR] and node issue '%s'!\n",
							find.token.buff);
						logic.error++;
						break;
					}
					/* assign gate input */
					node_driver(&gate->node,node);
					/* try to get ip node */
					if (!strtok_next(&find)) {
						printf("\n[ERROR:%d] and without ip2 node!\n",
							text.iline);
						logic.error++;
						break;
					}
					node = circuit_check_node(&logic,find.token.buff);
					if (!node) {
						printf("\n[ERROR] and node issue '%s'!\n",
							find.token.buff);
						logic.error++;
						break;
					}
					/* assign gate input */
					node_driver(&gate->node,node);
				} while(0); /* dummy loop - ends */
			}
			else if (!strncmp(find.token.buff,"xor",3)) {
				do /* dummy loop - begin */ {
					my1gate_t *gate;
					my1node_t *node;
					my1simthing_t *pthing;
					/* try to get gate */
					if (!strtok_next(&find)) {
						printf("\n[ERROR:%d] xor gate name missing!\n",
							text.iline);
						logic.error++;
						break;
					}
					gate = circuit_check_gate(&logic,find.token.buff,xor);
					if (!gate) {
						printf("\n[ERROR] Error creating xor gate!\n");
						logic.error++;
						break;
					}
					pthing = circuit_gate_get_thing(&logic,gate);
					if (!pthing) {
						printf("\n[ERROR:%d] xor no thing?!\n",
							text.iline);
						logic.error++;
						break;
					}
					/* try to get op node */
					if (!strtok_next(&find)) {
						printf("\n[ERROR:%d] xor without op node!\n",
							text.iline);
						logic.error++;
						break;
					}
					node = circuit_check_node(&logic,find.token.buff);
					if (!node) {
						printf("\n[ERROR] xor node issue '%s'!\n",
							find.token.buff);
						logic.error++;
						break;
					}
					/* assign gate output to drive that node */
					node_driver(node,&gate->node);
					/* try to get ip node */
					if (!strtok_next(&find)) {
						printf("\n[ERROR:%d] xor without ip node!\n",
							text.iline);
						logic.error++;
						break;
					}
					node = circuit_check_node(&logic,find.token.buff);
					if (!node) {
						printf("\n[ERROR] xor node issue '%s'!\n",
							find.token.buff);
						logic.error++;
						break;
					}
					/* assign gate input */
					node_driver(&gate->node,node);
					/* try to get ip node */
					if (!strtok_next(&find)) {
						printf("\n[ERROR:%d] xor without ip2 node!\n",
							text.iline);
						logic.error++;
						break;
					}
					node = circuit_check_node(&logic,find.token.buff);
					if (!node) {
						printf("\n[ERROR] xor node issue '%s'!\n",
							find.token.buff);
						logic.error++;
						break;
					}
					/* assign gate input */
					node_driver(&gate->node,node);
				} while(0); /* dummy loop - ends */
			}
			else {
				do /* dummy loop - begin */ {}
				while(0); /* dummy loop - ends */
				printf("\n[ERROR:%d] Invalid line! {%s}\n",
					text.iline,text.pbuff.buff);
			}
		}
		printf("done! (Lines=%d)\n",text.iline);
		strtok_free(&find);
		text_done(&text);
		/* do your thing! */
		printf("\n----------------\n");
		printf("Circuit Analysis\n");
		printf("----------------\n\n");
		printf("Node count: %d\n",logic.nodes.size);
		printf("Gate count: %d\n",logic.gates.size);
		printf("Things: %d\n",logic.dosim.things.size);
		printf("Events: %d\n",logic.dosim.events.size);
		logic.dosim.events.curr = 0x0;
		while (list_scan_item(&logic.dosim.events)) {
			my1simevent_t* ev = (my1simevent_t*)logic.dosim.events.curr->data;
			debug_print_event(ev);
		}
		/* run the simulation! */
		if (!logic.error) {
			printf("Simulation time: %d\n",logic.dosim.time_complete);
			printf("\n|###|");
			logic.nodes.curr = 0x0;
			while (list_scan_item(&logic.nodes)) {
				my1node_t* node = (my1node_t*)logic.nodes.curr->data;
				printf("%3s|",node->name);
			}
			do {
				timesim_run(&logic.dosim,1);
			} while (logic.dosim.time_unit<logic.dosim.time_complete);
			circuit_show_logic((void*)&logic.dosim,logic.dosim.time_unit,0);
			printf("\n\nSimulation Finished at Time: %u/%u\n",
				logic.dosim.time_unit,logic.dosim.time_complete);
			printf("Remaining Events: %d\n",logic.dosim.events.size);
			logic.dosim.events.curr = 0x0;
			while (list_scan_item(&logic.dosim.events)) {
				my1simevent_t* ev = (my1simevent_t*)
					logic.dosim.events.curr->data;
				debug_print_event(ev);
			}
		}
		printf("\n");
		/* release sim control */
		circuit_free(&logic);
	}
	text_free(&text);
	return 0;
}
/*----------------------------------------------------------------------------*/
#endif
/*----------------------------------------------------------------------------*/
