/*----------------------------------------------------------------------------*/
#ifndef __MY1ROLL_H__
#define __MY1ROLL_H__
/*----------------------------------------------------------------------------*/
#define SEED_STATE_SIZE 32
/*----------------------------------------------------------------------------*/
#define OFFS_RAND 0
#define OFFS_DICE 1
/*----------------------------------------------------------------------------*/
typedef struct _my1roll_t
{
	int roll, rmin;
	int offs; /* OFFS_RAND or OFFS_DICE */
	unsigned int seed; /* keep a copy of init seed */
	char sstate[SEED_STATE_SIZE], *pstate;
}
my1roll_t;
/*----------------------------------------------------------------------------*/
void roll_init(my1roll_t* roll, unsigned int seed);
int roll_take(my1roll_t* roll, int max); /* between 0 and (max-1) inclusive */
/*----------------------------------------------------------------------------*/
#endif /** __MY1ROLL_H__ */
/*----------------------------------------------------------------------------*/
