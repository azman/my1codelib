/*----------------------------------------------------------------------------*/
#ifndef __MY1SIM_H__
#define __MY1SIM_H__
/*----------------------------------------------------------------------------*/
/**
 *  my1sim time simulation library
 *  - written by azman@my1matrix.org
 *  - creates a framework for time simulation
 *  - requires my1list
 */
/*----------------------------------------------------------------------------*/
#include "my1type.h"
#include "my1list.h"
/*----------------------------------------------------------------------------*/
#define SIM_NAMESIZE 64
/*----------------------------------------------------------------------------*/
#define SIMTHING_CMD_MASK_ 0xFF
#define SIMTHING_CMD_EVENT 0x00
#define SIMTHING_CMD_RESET 0x01
#define SIMTHING_CMD_CHECK 0x02
/*----------------------------------------------------------------------------*/
typedef void (*pusercode)(void*,word32_t,int);
/*----------------------------------------------------------------------------*/
typedef struct _my1simthing_t {
	char name[SIM_NAMESIZE];
	int flag; /* for multiple thing types? :p */
	void* data;
	pusercode pcode;
} my1simthing_t;
/*----------------------------------------------------------------------------*/
#define SIMEVENT_MASK       0xFF00
#define SIMEVENT_BREAKPOINT 0x8000
#define SIMEVENT_ACTIVATE   0x0800
#define SIMEVENT_DEACTIVATE 0x0400
#define SIMEVENT_ONE_SHOT   0x0200
#define SIMEVENT_REPETETIVE 0x0100
/*----------------------------------------------------------------------------*/
typedef struct _my1simevent_t {
	word32_t type, time_pick, time_next, time_swap;
	my1simthing_t* pthing;
} my1simevent_t;
/*----------------------------------------------------------------------------*/
typedef struct _my1timesim_t {
	word32_t time_unit;
	word32_t time_complete;
	void* data;
	pusercode pcode_check, pcode_mtime, pcode_ntime;
	my1list_t things;
	my1list_t events;
} my1timesim_t;
/*----------------------------------------------------------------------------*/
/** simthing_free NOT needed because nothing to cleanup! */
void simthing_init(my1simthing_t *pthing, char *name, void *data);
int simthing_chkname(my1simthing_t *pthing, char *name); /* strncmp */
void simthing_doevent(my1simthing_t *pthing, word32_t time, int flag);
void simthing_doreset(my1simthing_t *pthing, word32_t time);
void simthing_docheck(my1simthing_t *pthing, word32_t time);
/*----------------------------------------------------------------------------*/
/** simevent_free NOT needed because nothing to cleanup! */
void simevent_init(my1simevent_t *pevent, word32_t type, word32_t time);
/*----------------------------------------------------------------------------*/
void timesim_init(my1timesim_t *psim);
void timesim_free(my1timesim_t *psim);
void timesim_reset(my1timesim_t *psim);
void* timesim_addthing(my1timesim_t *psim, my1simthing_t *pthing);
void* timesim_addevent(my1timesim_t *psim, my1simevent_t *pevent);
void timesim_run(my1timesim_t *psim, word32_t time_unit);
/*----------------------------------------------------------------------------*/
#endif /** __MY1SIM_H__ */
/*----------------------------------------------------------------------------*/
