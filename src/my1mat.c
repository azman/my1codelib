/*----------------------------------------------------------------------------*/
#include "my1mat.h"
/*----------------------------------------------------------------------------*/
#include <stdlib.h> /* for malloc and free? */
/*----------------------------------------------------------------------------*/
#define is_spacing(ch) ((ch==' ')||(ch=='\t'))
#define is_number(ch) (ch>=0x30&&ch<=0x39)
/*----------------------------------------------------------------------------*/
void mat_init(my1mat_t* mat) {
	mat->data = 0x0;
	mat->size = 0;
	mat->stat = MAT_OK;
	mat->cols = 0; /* width */
	mat->rows = 0; /* height */
	mat->lays = 0; /* depth */
	mat_resize(mat,1,1,1); /* default is single item matrix */
}
/*----------------------------------------------------------------------------*/
void mat_free(my1mat_t* mat) {
	if (mat->data) {
		free((void*)mat->data);
		mat->data = 0x0;
		mat->size = 0;
	}
}
/*----------------------------------------------------------------------------*/
#include <stdio.h>
/*----------------------------------------------------------------------------*/
void mat_str2(my1mat_t* mat,char* str) {
	my1num_t* num;
	int curr, irow, icol, mcol, init;
	curr = 0; irow = -1; icol = 0; init = 0; mcol = 0;
	while (str[curr]) {
		if (is_spacing(str[curr])) { curr++; continue; }
		printf("@@ Check:{%c,%d,%d,%d}\n",str[curr],curr,icol,irow);
		if (str[curr]=='[') {
			if (init) {
				mat->stat |= MAT_ERROR_STR2;
				break;
			}
			init++;
			irow++;
			if (mcol<icol) mcol = icol;
			icol = 0;
			curr++;
		}
		else if (str[curr]==',') {
			if (init) icol++;
			curr++;
		}
		else if (str[curr]==']') {
			if (!init) {
				mat->stat |= MAT_ERROR_STR2;
				break;
			}
			init--;
			icol++;
			curr++;
			while (is_spacing(str[curr])) curr++;
			/* must be a comma! */
			if (str[curr]!=',') {
				mat->stat |= MAT_ERROR_STR2;
				break;
			}
			while (is_spacing(str[curr])) curr++;
			/* must be a '['! */
			if (str[curr]!='[') {
				mat->stat |= MAT_ERROR_STR2;
				break;
			}
			curr++;
		}
		else {
			
		}
	}
	if (mat->stat&MAT_ERROR_STR2) return;
	else printf("@@ Debug:{%d,%d,%d}\n",icol,irow,1);
	/* create a single entity matrix if no item */
	num = mat_resize(mat,icol,irow,1);
	num_str2(num,str);
}
/*----------------------------------------------------------------------------*/
my1mat_t* mat_clone(my1mat_t* mat) {
	my1mat_t* tmp = (my1mat_t*) malloc(sizeof(my1mat_t));
	if (tmp) {
		mat_init(tmp);
		if (mat) {
			if (mat_resize(tmp,mat->cols,mat->rows,mat->lays))
				mat_copy(tmp,mat);
		}
	}
	return tmp;
}
/*----------------------------------------------------------------------------*/
my1num_t* mat_resize(my1mat_t* mat, int width, int height, int depth) {
	int irow, icol, idep;
	int icnt = height*width*depth;
	my1num_t *temp = (my1num_t*) realloc(mat->data,icnt*sizeof(my1num_t));
	if (temp) {
		/* copy data to new locations if possible */
		if (mat->size>0&&mat->size<icnt) {
			int fcount = mat->cols*mat->rows;
			int gcount = width*height;
			for (idep=depth-1;idep>0;idep--) {
				for (irow=height-1;irow>0;irow--) {
					for (icol=width-1;icol>0;icol--) {
						int old = idep*fcount+irow*mat->cols+icol;
						if (old<mat->size) {
							int new = idep*gcount+irow*width+icol;
							num_copy(&mat->data[new],&mat->data[old]);
						}
					}
				}
			}
		}
		else {
			my1num_t* num = temp;
			/* initialize as double precision floating point numbers */
			for (icol=0;icol<icnt;icol++)
				num_init(num++,NUMBER_FLOATING);
		}
		mat->cols = width;
		mat->rows = height;
		mat->lays = depth;
		mat->size = icnt;
		mat->data = temp;
	}
	return temp;
}
/*----------------------------------------------------------------------------*/
my1num_t* mat_getrow(my1mat_t* mat, int row)
{
	return (my1num_t*) &(mat->data[row*mat->cols]);
}
/*----------------------------------------------------------------------------*/
void mat_copy(my1mat_t* dst, my1mat_t* src)
{
	int icol;
	if (dst->size!=src->size)
		mat_resize(dst,src->cols,src->rows,src->lays);
	for (icol=0;icol<dst->size;icol++)
		num_copy(&dst->data[icol],&src->data[icol]);
}
/*----------------------------------------------------------------------------*/
void mat_move(my1mat_t* dst, my1mat_t* src)
{
	int icol;
	if (dst->size!=src->size)
		mat_resize(dst,src->cols,src->rows,src->lays);
	for (icol=0;icol<dst->size;icol++)
		num_move(&dst->data[icol],&src->data[icol]);
}
/*----------------------------------------------------------------------------*/
void mat_zero(my1mat_t* mat)
{
	int icol; my1num_t* num = mat->data;
	for (icol=0;icol<mat->size;icol++)
		num_zero(num++);
}
/*----------------------------------------------------------------------------*/
void mat_identity(my1mat_t* mat)
{
	int icol, next; my1num_t* num;
	/* lower width/height dimension */
	if (mat->cols!=mat->rows)
	{
		next = mat->cols;
		if (next>mat->rows)
			next = mat->rows;
		mat_resize(mat,next,next,1);
	}
	/* clear everything first! */
	mat_zero(mat);
	/* create ones on main diagonal */
	num = mat->data;
	next = mat->rows+1;
	for (icol=0;icol<mat->rows;icol++)
	{
		num_set_f(num,1.0,0.0);
		num += next;
	}
}
/*----------------------------------------------------------------------------*/
void mat_transpose(my1mat_t* mat)
{
	int irow, icol;
	int rows = mat->cols, cols = mat->rows;
	my1num_t *temp = (my1num_t*) malloc(mat->size*sizeof(my1num_t));
	my1num_t *dptr = temp;
	for (irow=0;irow<rows;irow++)
	{
		for (icol=0;icol<cols;icol++)
		{
			my1num_t *sptr = &mat->data[icol*rows+irow];
			num_copy(dptr++,sptr);
		}
	}
	free((void*)mat->data);
	mat->data = temp;
	mat->cols = cols;
	mat->rows = rows;
}
/*----------------------------------------------------------------------------*/
void mat_add_number(my1mat_t* dst, my1mat_t* mat1, my1num_t* num1)
{
	int icol;
	if (dst->size!=mat1->size)
		mat_resize(dst,mat1->cols,mat1->rows,mat1->lays);
	for (icol=0;icol<dst->size;icol++)
		num_add(&dst->data[icol],&mat1->data[icol],num1);
}
/*----------------------------------------------------------------------------*/
void mat_add(my1mat_t* dst, my1mat_t* mat1, my1mat_t* mat2)
{
	int icol;
	if (mat2->size==1)
	{
		mat_add_number(dst,mat1,&mat2->data[0]);
		return;
	}
	if (mat1->size==1)
	{
		mat_add_number(dst,mat2,&mat1->data[0]);
		return;
	}
	if (mat1->size!=mat2->size)
	{
		dst->stat = MAT_ERROR_SIZE;
		return;
	}
	if (dst->size!=mat1->size)
		mat_resize(dst,mat1->cols,mat1->rows,mat1->lays);
	for (icol=0;icol<dst->size;icol++)
		num_add(&dst->data[icol],&mat1->data[icol],&mat2->data[icol]);
}
/*----------------------------------------------------------------------------*/
void mat_subtract_number(my1mat_t* dst, my1mat_t* mat1, my1num_t* num1)
{
	int icol;
	if (dst->size!=mat1->size)
		mat_resize(dst,mat1->cols,mat1->rows,mat1->lays);
	for (icol=0;icol<dst->size;icol++)
		num_subtract(&dst->data[icol],&mat1->data[icol],num1);
}
/*----------------------------------------------------------------------------*/
void mat_subtract(my1mat_t* dst, my1mat_t* mat1, my1mat_t* mat2)
{
	int icol;
	if (mat2->size==1)
	{
		mat_subtract_number(dst,mat1,&mat2->data[0]);
		return;
	}
	if (mat1->size==1)
	{
		mat_subtract_number(dst,mat2,&mat1->data[0]);
		for (icol=0;icol<dst->size;icol++)
			num_negate(&dst->data[icol]);
		return;
	}
	if (mat1->size!=mat2->size)
	{
		dst->stat = MAT_ERROR_SIZE;
		return;
	}
	if (dst->size!=mat1->size)
		mat_resize(dst,mat1->cols,mat1->rows,mat1->lays);
	for (icol=0;icol<dst->size;icol++)
		num_subtract(&dst->data[icol],&mat1->data[icol],&mat2->data[icol]);
}
/*----------------------------------------------------------------------------*/
void mat_multinum_number(my1mat_t* dst, my1mat_t* mat1, my1num_t* num1)
{
	int icol;
	if (dst->size!=mat1->size)
		mat_resize(dst,mat1->cols,mat1->rows,mat1->lays);
	for (icol=0;icol<dst->size;icol++)
		num_multiply(&dst->data[icol],&mat1->data[icol],num1);
}
/*----------------------------------------------------------------------------*/
void mat_multiply(my1mat_t* dst, my1mat_t* mat1, my1mat_t* mat2)
{
	int irow, icol, loop;
	int rows = mat1->rows, cols = mat2->cols, cold = mat1->cols;
	my1num_t *pvr,*pv1,*pv2;
	my1num_t tmp, sum;
	if (mat2->size==1)
	{
		mat_multinum_number(dst,mat1,&mat2->data[0]);
		return;
	}
	if (mat1->size==1)
	{
		mat_multinum_number(dst,mat2,&mat1->data[0]);
		return;
	}
	if (mat1->cols!=mat2->rows)
	{
		dst->stat = MAT_ERROR_SIZE;
		return;
	}
	if (dst->rows!=rows&&dst->cols!=cols)
		mat_resize(dst,cols,rows,1);
	pvr = dst->data;
	num_init(&tmp,NUMBER_FLOATING);
	num_init(&sum,NUMBER_FLOATING);
	for (irow=0;irow<rows;irow++)
	{
		for (icol=0;icol<cols;icol++)
		{
			pv1 = mat_getrow(mat1,irow);
			pv2 = &mat2->data[icol];
			num_zero(pvr);
			for (loop=0;loop<cold;loop++)
			{
				num_multiply(&tmp,pv1++,pv2);
				pv2 += cols;
				num_copy(&sum,pvr);
				num_add(pvr,&sum,&tmp);
			}
			pvr++;
		}
	}
}
/*----------------------------------------------------------------------------*/
void mat_multinum(my1mat_t* dst, my1mat_t* mat1, my1mat_t* mat2)
{
	int icol;
	if (mat2->size==1)
	{
		mat_multinum_number(dst,mat1,&mat2->data[0]);
		return;
	}
	if (mat1->size==1)
	{
		mat_multinum_number(dst,mat2,&mat1->data[0]);
		return;
	}
	if (mat1->size!=mat2->size)
	{
		dst->stat = MAT_ERROR_SIZE;
		return;
	}
	if (dst->size!=mat1->size)
		mat_resize(dst,mat1->cols,mat1->rows,mat1->lays);
	for (icol=0;icol<dst->size;icol++)
		num_multiply(&dst->data[icol],&mat1->data[icol],&mat2->data[icol]);
}
/*----------------------------------------------------------------------------*/
void mat_divide(my1mat_t* dst, my1mat_t* mat1, my1mat_t* mat2)
{
	int icol;
	if (dst->size!=mat1->size)
		mat_resize(dst,mat1->cols,mat1->rows,mat1->lays);
	for (icol=0;icol<dst->size;icol++)
		num_divide(&dst->data[icol],&mat1->data[icol],&mat2->data[icol]);
}
/*----------------------------------------------------------------------------*/
void mat_divrem(my1mat_t* dst, my1mat_t* mat1, my1mat_t* mat2)
{
	int icol;
	if (dst->size!=mat1->size)
		mat_resize(dst,mat1->cols,mat1->rows,mat1->lays);
	for (icol=0;icol<dst->size;icol++)
		num_divrem(&dst->data[icol],&mat1->data[icol],&mat2->data[icol]);
}
/*----------------------------------------------------------------------------*/
void mat_power(my1mat_t* dst, my1mat_t* mat1, my1mat_t* mat2)
{
	int icol;
	if (dst->size!=mat1->size)
		mat_resize(dst,mat1->cols,mat1->rows,mat1->lays);
	for (icol=0;icol<dst->size;icol++)
		num_power(&dst->data[icol],&mat1->data[icol],&mat2->data[icol]);
}
/*----------------------------------------------------------------------------*/
void mat_log(my1mat_t* dst, my1mat_t* src)
{
	int icol;
	if (dst->size!=src->size)
		mat_resize(dst,src->cols,src->rows,src->lays);
	for (icol=0;icol<dst->size;icol++)
		num_log(&dst->data[icol],&src->data[icol]);
}
/*----------------------------------------------------------------------------*/
void mat_log10(my1mat_t* dst, my1mat_t* src)
{
	int icol;
	if (dst->size!=src->size)
		mat_resize(dst,src->cols,src->rows,src->lays);
	for (icol=0;icol<dst->size;icol++)
		num_log10(&dst->data[icol],&src->data[icol]);
}
/*----------------------------------------------------------------------------*/
void mat_sin(my1mat_t* dst, my1mat_t* src)
{
	int icol;
	if (dst->size!=src->size)
		mat_resize(dst,src->cols,src->rows,src->lays);
	for (icol=0;icol<dst->size;icol++)
		num_sin(&dst->data[icol],&src->data[icol]);
}
/*----------------------------------------------------------------------------*/
void mat_cos(my1mat_t* dst, my1mat_t* src)
{
	int icol;
	if (dst->size!=src->size)
		mat_resize(dst,src->cols,src->rows,src->lays);
	for (icol=0;icol<dst->size;icol++)
		num_cos(&dst->data[icol],&src->data[icol]);
}
/*----------------------------------------------------------------------------*/
void mat_tan(my1mat_t* dst, my1mat_t* src)
{
	int icol;
	if (dst->size!=src->size)
		mat_resize(dst,src->cols,src->rows,src->lays);
	for (icol=0;icol<dst->size;icol++)
		num_tan(&dst->data[icol],&src->data[icol]);
}
/*----------------------------------------------------------------------------*/
void mat_rad(my1mat_t* dst, my1mat_t* src)
{
	int icol;
	if (dst->size!=src->size)
		mat_resize(dst,src->cols,src->rows,src->lays);
	for (icol=0;icol<dst->size;icol++)
		num_rad(&dst->data[icol],&src->data[icol]);
}
/*----------------------------------------------------------------------------*/
void mat_deg(my1mat_t* dst, my1mat_t* src)
{
	int icol;
	if (dst->size!=src->size)
		mat_resize(dst,src->cols,src->rows,src->lays);
	for (icol=0;icol<dst->size;icol++)
		num_deg(&dst->data[icol],&src->data[icol]);
}
/*----------------------------------------------------------------------------*/
#if defined(TEST_MODULE) && defined(TEST_MAT)
/*----------------------------------------------------------------------------*/
#define RESULT_NAME "result"
/*----------------------------------------------------------------------------*/
#include "my1cstr_line.h"
#include "my1cstr_util.h"
#include "my1num.c"
#include "my1expr.c"
#include "my1chtest.c"
/*----------------------------------------------------------------------------*/
#include "my1expr.h"
#include "my1vars.h"
#include "my1shell.h"
/*----------------------------------------------------------------------------*/
void print_expression(my1expr_t *expr);
/*----------------------------------------------------------------------------*/
int mat_error(my1mat_t* mat) {
	return mat->stat;
}
/*----------------------------------------------------------------------------*/
void mat_print(my1mat_t* mat) {
	int irow, jloop;
	for (irow=0;irow<mat->rows;irow++)
	{
		my1num_t* num = mat_getrow(mat,irow);
		putchar('|');
		for (jloop=0;jloop<mat->cols;jloop++)
		{
			putchar(' ');
			num_print(num++);
		}
		printf(" |\n");
	}
}
/*----------------------------------------------------------------------------*/
void var_free_matrix(my1mat_t* pmat) {
	mat_free(pmat);
	free((void*)pmat);
}
/*----------------------------------------------------------------------------*/
my1var_t* make_shell_data(my1shell_t* that, char* name, char* dstr) {
	my1var_t* pvar = 0x0;
	my1mat_t *pmat = (my1mat_t*) malloc(sizeof(my1mat_t));
	if (pmat) {
		mat_init(pmat);
		if (dstr) mat_str2(pmat,dstr);
		pvar = shell_data(that,name,(void*)pmat,var_free_matrix);
		if (!pvar) free((void*)pmat);
	}
	return pvar;
}
/*----------------------------------------------------------------------------*/
void show_data(my1var_t* pvar) {
	printf("%s = ",pvar->name.buff);
	mat_print((my1mat_t*)pvar->data);
	printf("\n");
}
/*----------------------------------------------------------------------------*/
int exec_expr(my1expr_t* expr, void* data, void* more) {
	return 0;
}
/*----------------------------------------------------------------------------*/
void show_intro(void) {
	printf("\nMY1 Matrix Test Program\n\n");
	printf("- based on my1expr module in my1codelib\n");
	printf("- arithmetic expressions () / %% * + -\n");
	printf("  - unary operator not supported (e.g. 0-2 instead of -2)\n");
	printf("- trigonometry functions (sin,cos,tan)\n");
	printf("  - rad()/deg() function to convert degree to/from radian\n");
	printf("- logarithmic functions (log,ln)\n");
	printf("- complex number supported\n");
	printf("  - complex char '%c' specified AFTER number (e.g. 5%c)\n",
		CHAR_COMPLEX,CHAR_COMPLEX);
	printf("- variables are automatically created on assignment\n");
	printf("  - type 'vars' to list variables\n");
	printf("- matrix item can be access using zero-based index\n");
	printf("  - e.g. a[1] to access second item in vector a\n");
	printf("  - e.g. b[1:0] to access first item of second row in matrix b\n");
	printf("- literal matrix is not currently supported\n");
	printf("- type 'intro' to show this introductory message\n");
	printf("- type 'exit' or 'quit' to quit this program\n");
}
/*----------------------------------------------------------------------------*/
typedef my1mat_t my1math_t;
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	int test;
	my1cstr_t buff;
	my1expr_t expr;
	my1math_t math;
	cstr_init(&buff);
	expr_init(&expr);
	mat_init(&math);
	while (1) {
		printf("\nmymat> ");
		test = cstr_read_line(&buff,stdin);
		if (!test) break;
		cstr_trimws(&buff,1);
		if (!buff.fill) break;
		printf("-- Input: {%s} ",buff.buff);
		fflush(stdout);
		mat_str2(&math,buff.buff);
/*
		flag = expr_read(&expr,buff.buff);
		if (flag&FLAG_ERROR)
			printf("-- Error: (%08x) Input: '%s'\n",flag,buff.buff);
		else if (!expr.read)
			printf("-- Expr?: (%08x) Input: '%s'\n",flag,buff.buff);
		else {
			printf("-- Found: { ");
			if (expr.tval.fill)
				printf("%s = ",expr.tval.buff);
			print_expression(expr.read);
			printf("}\n");
		}
*/
	}
	mat_free(&math);
	expr_free(&expr);
	cstr_free(&buff);
	printf("\n");
	return 0;
}
/*----------------------------------------------------------------------------*/
#endif
/*----------------------------------------------------------------------------*/
