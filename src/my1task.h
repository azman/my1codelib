/*----------------------------------------------------------------------------*/
#ifndef __MY1TASK_H__
#define __MY1TASK_H__
/*----------------------------------------------------------------------------*/
#include "my1type.h"
/*----------------------------------------------------------------------------*/
typedef struct _my1task_t {
	word32_t type; /* task id? */
	int flag; /* return value of task execution */
	ptask_t task;
/**
 * sample prototype for task:
 *
 * int execute_task(my1task_t* task, pdata_t that, pdata_t xtra);
 * task = task object being executed
 * that = usually main data struct
 * xtra = anything...
**/
	pdata_t data;
	pdata_t temp;
} my1task_t;
/*----------------------------------------------------------------------------*/
void task_init(my1task_t* task, word32_t type);
void task_make(my1task_t* task, ptask_t func, pdata_t data);
int task_call(my1task_t* task, pdata_t that, pdata_t xtra);
/*----------------------------------------------------------------------------*/
#endif /** __MY1TASK_H__ */
/*----------------------------------------------------------------------------*/
