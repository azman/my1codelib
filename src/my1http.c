/*----------------------------------------------------------------------------*/
#include "my1http.h"
/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/*----------------------------------------------------------------------------*/
void http_init(my1http_t* http, int txsize, int rxsize) {
	http->txbuff = (char*) malloc(sizeof(char)*txsize);
	if (http->txbuff) http->txsize = txsize;
	else http->txsize = 0;
	http->rxbuff = (char*) malloc(sizeof(char)*rxsize);
	if (http->rxbuff) http->rxsize = rxsize;
	else http->rxsize = 0;
	http->host = 0x0;
	http->repl = 0x0; /* marker for http body in reply */
	http->type = 0;
	http->port = 80; /* default http port */
	http->txfill = 0;
	http->rxfill = 0;
	http->flag = 0;
	http->stat = 0;
	sockcli_init(&http->send);
}
/*----------------------------------------------------------------------------*/
void http_free(my1http_t* http) {
	sockcli_free(&http->send);
	free((void*)http->rxbuff);
	free((void*)http->txbuff);
}
/*----------------------------------------------------------------------------*/
void http_prep(my1http_t* http, char* host, int port) {
	if (!port) port = http->port;
	if (!sockcli_host(&http->send,host,port)) {
		http->stat |= HTTP_STAT_ERROR;
		return;
	}
	if (sockcli_open(&http->send))
		http->stat |= HTTP_STAT_ERROR;
}
/*----------------------------------------------------------------------------*/
int http_send(my1http_t* http) {
	int temp;
	temp = sockcli_send(&http->send,(void*)http->txbuff,http->txfill);
	if (temp<0) http->stat |= HTTP_STAT_SEND_ERROR;
	else if (temp<http->txfill) http->stat |= HTTP_STAT_SEND_LESS;
	return temp;
}
/*----------------------------------------------------------------------------*/
int http_wait(my1http_t* http) {
	int temp;
	temp = sockcli_read(&http->send,(void*)http->rxbuff,http->rxsize);
	if (temp<0) http->stat |= HTTP_STAT_WAIT_ERROR;
	else {
		http->rxfill = temp;
		http->rxbuff[http->rxfill] = 0x0;
	}
	return temp;
}
/*----------------------------------------------------------------------------*/
void http_query_init(my1http_t* http, char* page, int type) {
	int size;
	/** prepare line 1 */
	snprintf(http->tmpbuf,HTTP_TEMPSIZE,
		HTTP_TPLT_HEAD,type==HTTP_POST?"POST":"GET",page);
	size = strlen(http->tmpbuf);
	http->txfill = size;
	if (http->txfill>=http->txsize) {
		http->stat |= HTTP_STAT_ERROR;
		return;
	}
	strncpy(http->txbuff,http->tmpbuf,size);
	http->txbuff[http->txfill] = 0x0;
	/** prepare header host */
	snprintf(http->tmpbuf,HTTP_TEMPSIZE,HTTP_TPLT_HOST,http->send.host.name);
	size = strlen(http->tmpbuf);
	http->txfill += size;
	if (http->txfill>=http->txsize) {
		http->stat |= HTTP_STAT_ERROR;
		return;
	}
	strncat(http->txbuff,http->tmpbuf,size);
	/** prepare header agent */
	snprintf(http->tmpbuf,HTTP_TEMPSIZE,HTTP_TPLT_UAGT,HTTP_USRAGENT);
	size = strlen(http->tmpbuf);
	http->txfill += size;
	if (http->txfill>=http->txsize) {
		http->stat |= HTTP_STAT_ERROR;
		return;
	}
	strncat(http->txbuff,http->tmpbuf,size);
}
/*----------------------------------------------------------------------------*/
void http_query_done(my1http_t* http, char* mesg, int type) {
	int size, temp = 0;
	if (mesg) {
		/** prepare header content type */
		if (type==HTTP_BODY_JSON)
			snprintf(http->tmpbuf,HTTP_TEMPSIZE,HTTP_TYPE_JSON);
		else if (type==HTTP_BODY_HTML)
			snprintf(http->tmpbuf,HTTP_TEMPSIZE,HTTP_TYPE_HTML);
		else
			http->tmpbuf[0] = 0x0;
		size = strlen(http->tmpbuf);
		if (size) {
			http->txfill += size;
			if (http->txfill>=http->txsize) {
				http->stat |= HTTP_STAT_ERROR;
				return;
			}
			strncat(http->txbuff,http->tmpbuf,size);
		}
		/** prepare header content length */
		temp = strlen(mesg);
		snprintf(http->tmpbuf,HTTP_TEMPSIZE,HTTP_TPLT_CLEN,temp);
		size = strlen(http->tmpbuf);
		http->txfill += size;
		if (http->txfill>=http->txsize) {
			http->stat |= HTTP_STAT_ERROR;
			return;
		}
		strncat(http->txbuff,http->tmpbuf,size);
	}
	/** end header field */
	size = 4; /**strlen(HTTP_HEAD_ENDS);**/
	http->txfill += size;
	if (http->txfill>=http->txsize) {
		http->stat |= HTTP_STAT_ERROR;
		return;
	}
	strncat(http->txbuff,HTTP_HEAD_ENDS,size);
	if (mesg) {
		/** append message */
		size = temp;
		http->txfill += size;
		if (http->txfill>=http->txsize) {
			http->stat |= HTTP_STAT_ERROR;
			return;
		}
		strncat(http->txbuff,mesg,size);
	}
}
/*----------------------------------------------------------------------------*/
void http_reply_mark(my1http_t* http) {
	char *temp = http->rxbuff;
	int loop, size;
	/* should be run AFTER http_wait */
	http->repl = 0x0;
	if (strncmp(http->rxbuff,HTTP_HEAD_OK,HTTP_HEAD_OK_SIZE)) {
		http->stat |= HTTP_STAT_REPLY_ERROR;
		return;
	}
	loop = 0;
	size = http->rxfill;
	while (loop<size) {
		if (temp[loop]=='\r'&&temp[loop+1]=='\n') {
			if (temp[loop+2]=='\r'&&temp[loop+3]=='\n') {
				http->repl = &temp[loop+4];
				break;
			}
			loop++;
		}
		loop++;
	}
}
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#if defined(TEST_MODULE) && defined(TEST_HTTP)
/*----------------------------------------------------------------------------*/
#define ERROR_ARGS_OPT (HTTP_STAT_ERROR|0x0001)
#define ERROR_ARGS_PAR (HTTP_STAT_ERROR|0x0002)
#define ERROR_ARGS_PORT (HTTP_STAT_ERROR|0x0004)
#define ERROR_ARGS_HOST (HTTP_STAT_ERROR|0x0008)
#define ERROR_ARGS_PATH (HTTP_STAT_ERROR|0x0010)
#define ERROR_ARGS_POST (HTTP_STAT_ERROR|0x0020)
#define ERROR_ARGS_BODY (HTTP_STAT_ERROR|0x0040)
#define ERROR_MESG_OPEN (HTTP_STAT_ERROR|0x0100)
#define ERROR_HOST_MISSING (HTTP_STAT_ERROR|0x0200)
#define ERROR_HOST_CONNECT (HTTP_STAT_ERROR|0x0400)
/*----------------------------------------------------------------------------*/
#include "my1cstr.c"
#include "my1sockbase.c"
#include "my1sockcli.c"
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	my1http_t http;
	my1cstr_t mesg;
	FILE *plog;
	char *host, *path, *body, *pchk;
	unsigned int stat;
	int loop, type, temp;
	http_init(&http,HTTP_BUFFSIZE,HTTP_BUFFSIZE);
	cstr_init(&mesg);
	cstr_resize(&mesg,HTTP_BUFFSIZE);
	do {
		plog = 0x0; host = 0x0; path = 0x0; body = 0x0;
		stat = 0;
		for (loop=1;loop<argc;loop++) {
			if (argv[loop][0]=='-'&&argv[loop][1]=='-') {
				pchk = &argv[loop][2];
				if (!strcmp(pchk,"host")) {
					if (++loop<argc) host = argv[loop];
					else {
						printf("** No value for %s given!\n",argv[loop-1]);
						stat |= ERROR_ARGS_HOST;
					}
				}
				else if (!strcmp(pchk,"path")) {
					if (++loop<argc) path = argv[loop];
					else {
						printf("** No value for %s given!\n",argv[loop-1]);
						stat |= ERROR_ARGS_PATH;
					}
				}
				else if (!strcmp(pchk,"port")) {
					if (++loop<argc) http.port = atoi(argv[loop]);
					else {
						printf("** No value for %s given!\n",argv[loop-1]);
						stat |= ERROR_ARGS_PORT;
					}
				}
				else if (!strcmp(pchk,"post")) {
					if (++loop<argc) {
						body = argv[loop];
						http.flag |= HTTP_FLAG_DOPOST;
					}
					else {
						printf("** No value for %s given!\n",argv[loop-1]);
						stat |= ERROR_ARGS_POST;
					}
				}
				else if (!strcmp(pchk,"body")) {
					if (++loop<argc) body = argv[loop];
					else {
						printf("** No value for %s given!\n",argv[loop-1]);
						stat |= ERROR_ARGS_BODY;
					}
				}
				else if (!strcmp(pchk,"logs")) plog = stderr;
				else {
					printf("** Unknown option '%s'!\n",argv[loop]);
					stat |= ERROR_ARGS_OPT;
				}
			}
			else {
				printf("** Unknown parameter '%s'!\n",argv[loop]);
				stat |= ERROR_ARGS_PAR;
			}
		}
		if (stat&HTTP_STAT_ERROR) break;
		if (!host) {
			printf("** No host provided!\n");
			stat |= ERROR_HOST_MISSING;
			break;
		}
		if (body) {
			FILE* file = fopen(body,"rt");
			if (file) {
				//cstr_null(&mesg);
				while ((temp=fgetc(file))>=0)
					cstr_single(&mesg,(char)temp);
				fclose(file);
				printf("## Message file '%s' loaded! (%d/%d)[%x]\n",
					body,mesg.fill,mesg.size,mesg.buff[mesg.fill-1]);
			}
			else {
				printf("** Cannot open file '%s'!\n",body);
				stat |= ERROR_MESG_OPEN;
				break;
			}
		}
		http_prep(&http,host,0);
		if (http.stat&HTTP_STAT_ERROR) {
			printf("** Cannot connect to host: %s@%s(%d)\n",
				http.send.host.addr,http.send.host.name,http.send.host.port);
			stat |= ERROR_HOST_CONNECT;
			break;
		}
		/* prepare query */
		type = (http.flag&HTTP_FLAG_DOPOST) ? HTTP_POST : HTTP_GET;
		temp = HTTP_BODY_JSON;
		http_query_init(&http,path?path:"/",type);
		http_query_done(&http,body?mesg.buff:0x0,temp);
		printf("## Contacting %s@%s(%d)\n",
				http.send.host.addr,http.send.host.name,http.send.host.port);
		temp = http_send(&http);
		printf("## Send:[%d/%d]\n",temp,http.txfill);
		temp = http_wait(&http);
		printf("## Wait:[%d/%d]\n",temp,http.rxsize);
		http_reply_mark(&http);
		if (http.repl) {
			printf("## Reply Body: [INIT]\n");
			printf("%s",http.repl);
			printf("## Reply Body: [ENDS]\n");
		}
		if (plog) {
			fprintf(plog,"<QUERY>%s",http.txbuff);
			fprintf(plog,"</QUERY>[%d/%d]\n",http.txfill,http.txsize);
			fprintf(plog,"<REPLY>%s",http.rxbuff);
			fprintf(plog,"</REPLY>[%d/%d]\n",http.rxfill,http.rxsize);
		}
	} while (0);
	cstr_free(&mesg);
	http_free(&http);
	return 0;
}
/*----------------------------------------------------------------------------*/
/** Basic HTML template */
#define HTML_FILE_INIT "<html><head><meta charset=\"utf-8\"/></head><body>"
#define HTML_FILE_ENDS "</body></html>"
/*----------------------------------------------------------------------------*/
/** kept for sentimental reasons :p */
#define HTTP_HEAD_TMPL "%s %s HTTP/1.1\r\nHost: %s\r\nUser-Agent: %s\r\n\r\n"
/*----------------------------------------------------------------------------*/
#endif
/*----------------------------------------------------------------------------*/
