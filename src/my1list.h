/*----------------------------------------------------------------------------*/
#ifndef __MY1LIST_H__
#define __MY1LIST_H__
/*----------------------------------------------------------------------------*/
/**
 *  my1list_t is a (double) linked-list data structure (using my1item_t)
 *  - written by azman@my1matrix.org
 *  - basically a queue(fifo) - only list_pop_item for stack(lifo) support
 *  - dynamic item needs to be freed by ffree_item (list_free simply remove)
 *  - this allows for more control on item management (e.g. swap container)
**/
/*----------------------------------------------------------------------------*/
#include "my1item.h"
/*----------------------------------------------------------------------------*/
typedef void (*ffree_item)(void*); /* param: my1item_t pointer */
/*----------------------------------------------------------------------------*/
typedef struct _my1list_t {
	my1item_t *init, *last, *curr;
	ffree_item done;
	int size, step; /* step is scan index */
} my1list_t;
/*----------------------------------------------------------------------------*/
#define LIST(pl) ((my1list_t*)pl)
#define list_scan_prep(pl) LIST(pl)->curr = 0x0
/*----------------------------------------------------------------------------*/
void list_init(my1list_t*,ffree_item);
void list_free(my1list_t*);
void list_free_item(void*);
void list_free_item_data(void*);
void list_redo(my1list_t*);
my1item_t* list_push_item(my1list_t*,my1item_t*);
my1item_t* list_pull_item(my1list_t*);
my1item_t* list_scan_item(my1list_t*);
my1item_t* list_find_item(my1list_t*,void*);
my1item_t* list_hack_item(my1list_t*,my1item_t*);
my1item_t* list_next_item(my1list_t* list,my1item_t* here,my1item_t* item);
my1item_t* list_push_item_data(my1list_t*,void*);
my1item_t* list_next_item_data(my1list_t* list,my1item_t* here,void* data);
my1item_t* list_pop_item(my1list_t*); /* for stack support */
/*----------------------------------------------------------------------------*/
#endif /** __MY1LIST_H__ */
/*----------------------------------------------------------------------------*/
