/*----------------------------------------------------------------------------*/
#ifndef __MY1PATH_H__
#define __MY1PATH_H__
/*----------------------------------------------------------------------------*/
/**
 *  my1path file/folder path stat info
 *  - written by azman@my1matrix.org
 *  - basic access only, listing function taken out as extension
 */
/*----------------------------------------------------------------------------*/
#define PATH_MODE_USR_ 0x01C0
#define PATH_MODE_USRR 0x0100
#define PATH_MODE_USRW 0x0080
#define PATH_MODE_USRX 0x0040
#define PATH_MODE_GRP_ 0x0038
#define PATH_MODE_GRPR 0x0020
#define PATH_MODE_GRPw 0x0010
#define PATH_MODE_GRPX 0x0008
#define PATH_MODE_OTH_ 0x0007
#define PATH_MODE_OTHR 0x0004
#define PATH_MODE_OTHW 0x0002
#define PATH_MODE_OTHX 0x0001
/*----------------------------------------------------------------------------*/
#define PATH_TRAP_ERROR (~(~0U>>1))
#define PATH_TRAP_ERROR_GCWD (PATH_TRAP_ERROR|0x01)
#define PATH_TRAP_ERROR_STAT (PATH_TRAP_ERROR|0x02)
#define PATH_TRAP_ERROR_CHGD (PATH_TRAP_ERROR|0x04)
/*----------------------------------------------------------------------------*/
#define PATH_FLAG_PATH 0x0001
#define PATH_FLAG_FILE 0x0002
#define PATH_FLAG_LINK 0x0004
/*----------------------------------------------------------------------------*/
#ifndef PATH_SEPARATOR_CHAR
#define PATH_SEPARATOR_CHAR '/'
#endif
/*----------------------------------------------------------------------------*/
/* only need 10 + null */
#define PATH_PERM_CHARSIZE 16
/*----------------------------------------------------------------------------*/
typedef struct _my1path_t {
	unsigned int trap, padd; /* trap:error code, padd: alignment padding */
	unsigned int flag, mode, iown, igrp;
	char *name, *path, *full, *real, perm[PATH_PERM_CHARSIZE];
} my1path_t;
/*----------------------------------------------------------------------------*/
#define PATH(pt) ((my1path_t*)pt)
#define path_retrap(pt) PATH(pt)->trap = 0
/*----------------------------------------------------------------------------*/
void path_init(my1path_t* path);
void path_free(my1path_t* path);
void path_dostat(my1path_t* path, char* full);
void path_getcwd(my1path_t* path);
void path_chdir(my1path_t* path, char* full);
void path_access(my1path_t* path, char* name);
void path_parent(my1path_t* path, my1path_t* parent);
/*----------------------------------------------------------------------------*/
/* useful helper functions */
char* path_name_merge(char *path, char *file);
char* path_name_getcwd(void);
/*----------------------------------------------------------------------------*/
#endif /** __MY1PATH_H__ */
/*----------------------------------------------------------------------------*/
