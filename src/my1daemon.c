/*----------------------------------------------------------------------------*/
#include "my1daemon.h"
/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <time.h>
#include <sys/time.h>
#include <sys/stat.h>
/*----------------------------------------------------------------------------*/
/** ISO8601 date time format => YYYY-MM-DDThh:mm:ss.sTZD */
#define DATETIME_FORMAT "%04d-%02d-%02dT%02d:%02d:%02d"
#define DATETIME_FORMAT_LEN (19+1)
/*----------------------------------------------------------------------------*/
void daemon_log(my1daemon_t* dstuff, char* message)
{
	char timestring[DATETIME_FORMAT_LEN];
	time_t currtime = time(0x0);
	struct tm timestamp = *localtime(&currtime);
	FILE* pfile = fopen(dstuff->filelogs,"a");
	if (!pfile) return;
	sprintf(timestring,DATETIME_FORMAT,
		(1900+timestamp.tm_year),(timestamp.tm_mon+1),timestamp.tm_mday,
		timestamp.tm_hour,timestamp.tm_min,timestamp.tm_sec);
	fprintf(pfile,"[%s][%s] %s\n",timestring,dstuff->name,message);
	fclose(pfile);
}
/*----------------------------------------------------------------------------*/
void daemon_error(my1daemon_t* dstuff, char* message)
{
	daemon_log(dstuff,message);
	exit(1);
}
/*----------------------------------------------------------------------------*/
void exec_daemon(my1daemon_t* dstuff)
{
	/* create lock */
	dstuff->lock = open(dstuff->filelock,O_RDWR|O_CREAT|O_EXCL,0666);
	if (dstuff->lock<0)
		daemon_error(dstuff,"Cannot create lock file!\n");
	/* fork to create new session? */
	dstuff->pid = fork();
	if (dstuff->pid < 0) daemon_error(dstuff,"Cannot fork!\n");
	if (dstuff->pid > 0) exit(0); /* parent process gracefully terminates */
	dstuff->pid = getpid();
	/* create new session - must be an orphaned child? :p */
	dstuff->sid = setsid();
	if (dstuff->sid < 0)
		daemon_error(dstuff,"Cannot create session ID!\n");
	/* close standard file descriptors */
	close(STDIN_FILENO);
	close(STDOUT_FILENO);
	close(STDERR_FILENO);
	/* no mask at all to allow access? */
	umask(0);
	/* write pid to lock file */
	sprintf(dstuff->buff,"%d\n",dstuff->pid);
	write(dstuff->lock,dstuff->buff,strlen(dstuff->buff));
	/* current working directory */
	if (chdir("/tmp") < 0)
		daemon_error(dstuff,"Cannot change CWD!\n");
}
/*----------------------------------------------------------------------------*/
void stop_daemon(my1daemon_t* dstuff)
{
	/* just remove lock */
	if (close(dstuff->lock))
		daemon_error(dstuff,"Cannot close lock file!\n");
	remove(dstuff->filelock);
	dstuff->lock = 0;
}
/*----------------------------------------------------------------------------*/
#if defined(TEST_MODULE) && defined(TEST_DAEMON)
/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include <signal.h>
/*----------------------------------------------------------------------------*/
#define DAEMON_NAME "my1daemon"
#define LOGS_NAME DAEMON_NAME".log"
#define LOCK_NAME DAEMON_NAME".lock"
#define TEMP_PATH "/tmp"
#define LOGS_FILE TEMP_PATH"/"LOGS_NAME
#define LOCK_FILE TEMP_PATH"/"LOCK_NAME
/*----------------------------------------------------------------------------*/
#include "my1daemon.h"
/*----------------------------------------------------------------------------*/
int flag_SIGUSR1 = 0;
int flag_SIGUSR2 = 0;
/*----------------------------------------------------------------------------*/
void signal_handler(int signum, siginfo_t* siginfo, void* context)
{
	switch(signum)
	{
	case SIGUSR1:
		flag_SIGUSR1 = 1;
		break;
	case SIGUSR2:
		flag_SIGUSR2 = 1;
		break;
	/** other signals are ignored? */
	}
}
/*----------------------------------------------------------------------------*/
#define TIMER_SECOND_S 1
#define SYSTEM_CLOCK_S 20
/*----------------------------------------------------------------------------*/
static int timer_system;
/*----------------------------------------------------------------------------*/
void timer_handler(int signum)
{
	(void) signum;
	if(timer_system>0) timer_system--;
}
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[])
{
	/* system handler */
	struct sigaction sysact;
	/* timer stuff */
	struct sigaction action;
	struct itimerval timer;
	/* setup daemon properties */
	my1daemon_t daemon = {0,0,0,0,"",DAEMON_NAME,LOGS_FILE,LOCK_FILE};
	/* turn into a daemon */
	exec_daemon(&daemon);
	/* signal_handler for system task */
	memset(&sysact, 0, sizeof (sysact));
	//sysact.sa_handler = &signal_handler;
	sysact.sa_sigaction = &signal_handler;
	sysact.sa_flags = SA_SIGINFO;
	if (sigaction(SIGUSR1, &sysact, NULL)<0)
		daemon_error(&daemon,"Cannot set handler for SIGUSR!");
	/* timer_handler for timing task */
	memset(&action, 0, sizeof (action));
	action.sa_handler = &timer_handler;
	/**sigaction(SIGVTALRM, &action, NULL);*/
	if (sigaction(SIGALRM, &action, NULL)<0)
		daemon_error(&daemon,"Cannot set handler for SIGALRM!");
	/*  initial timer timeout */
	timer.it_value.tv_sec = TIMER_SECOND_S;
	timer.it_value.tv_usec = 0;
	/* auto-reload interval? */
	timer.it_interval.tv_sec = TIMER_SECOND_S;
	timer.it_interval.tv_usec = 0;
	/* start timer - count down? */
	timer_system = SYSTEM_CLOCK_S;
	/**if(setitimer(ITIMER_VIRTUAL,&timer,NULL)<0)*/
	if(setitimer(ITIMER_REAL,&timer,NULL)<0)
		fprintf(stderr,"[WARNING] Cannot set timer!\n");
	/* publish our existence */
	daemon_log(&daemon,"Initiated.");
	/* main loop */
	while (1)
	{
		if (!timer_system)
		{
			daemon_log(&daemon,"I am Alive!");
			timer_system = SYSTEM_CLOCK_S;
		}
		if (flag_SIGUSR1) break;
	}
	daemon_log(&daemon,"Terminated.");
	/* remove daemon lock */
	stop_daemon(&daemon);
	return 0;
}
/*----------------------------------------------------------------------------*/
#endif
/*----------------------------------------------------------------------------*/
