/*----------------------------------------------------------------------------*/
#include "my1date.h"
/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
/*----------------------------------------------------------------------------*/
static int _month_days[] = { 31,28,31,30,31,30,31,31,30,31,30,31 };
/*----------------------------------------------------------------------------*/
static char dttmpbuf[DATETIME_FORMAT_LEN];
static char* dtformat = DATETIME_FORMAT;
static char* t_format = TIME_ISOFORMAT;
static char* d_format = DATE_ISOFORMAT;
/*----------------------------------------------------------------------------*/
int year_is_leap(int year) {
	if ((year%4==0&&year%100!=0)||year%400==0)
		return 1;
	return 0;
}
/*----------------------------------------------------------------------------*/
int month_days(int month, int leap) {
	int temp = _month_days[month-1];
	if (leap&&month==2) temp++;
	return temp;
}
/*----------------------------------------------------------------------------*/
int date_is_valid(int year, int month, int day) {
	int temp;
	/* must have positive year - AD! */
	if (year<0) return DATE_INVALID;
	/* 12 months! */
	if (month<1||month>12) return DATE_INVALID;
	/* get day range in given month/year! */
	temp = month_days(month,year_is_leap(year));
	/* check day! */
	if (day<1||day>temp) return DATE_INVALID;
	/* return valid date in format */
	return MY1DATE_MERGE(year,month,day);
}
/*----------------------------------------------------------------------------*/
void date_init(my1date_t* date) {
	date->date = DATE_INVALID;
}
/*----------------------------------------------------------------------------*/
void date_direct(my1date_t* date, int date_int) {
	date->date = date_is_valid(MY1DATE_SPLIT(date_int));
}
/*----------------------------------------------------------------------------*/
void date_assign(my1date_t* date, int year, int month, int day) {
	date->date = date_is_valid(year,month,day);
}
/*----------------------------------------------------------------------------*/
void date_my1str(my1date_t* date, char* dstr) {
	int temp = 0;
	date->date = DATE_INVALID;
	/* make sure all numeric */
	while (dstr[temp]) {
		if (dstr[temp]<0x30||dstr[temp]>0x39)
			return;
		temp++;
	}
	/* make sure full YYYYMMDD format */
	if (temp!=8) return;
	temp = atoi(dstr);
	date_direct(date,temp);
}
/*----------------------------------------------------------------------------*/
void date_isostr(my1date_t* date, char* dstr) {
	int temp = 0;
	date->date = DATE_INVALID;
	/* make sure all numeric */
	while (dstr[temp]) {
		if (temp==4||temp==7) {
			if (dstr[temp]!='-') return;
		}
		else {
			if (dstr[temp]<0x30||dstr[temp]>0x39) return;
		}
		temp++;
	}
	/* make sure full YYYY-MM-DD format */
	if (temp!=10) return;
	/* shift MM & DD into place */
	for (temp=4;temp<9;temp++) {
		if (temp<6) dstr[temp] = dstr[temp+1];
		else if (temp<8) dstr[temp] = dstr[temp+2];
		else dstr[temp] = 0x0;
	}
	temp = atoi(dstr);
	date_direct(date,temp);
}
/*----------------------------------------------------------------------------*/
#define DATE_SIZE 10
/*----------------------------------------------------------------------------*/
void date_chkstr(my1date_t* date, char* dstr, char dsep) {
	char str_day[3], str_month[3], str_year[5], buff;
	int cnt_day = 0, cnt_month = 0, cnt_year = 0, cnt_sep = 0;
	int error = 0, loop, size = 0;
	/* by default set to error value */
	date->date = DATE_INVALID;
	/* get string size and validate */
	size = strlen(dstr);
	if (size<DATE_SIZE) return;
	/* format is dd{sep}mm{sep}yyyy, {sep} configurable */
	for (loop=0;loop<DATE_SIZE;loop++) {
		buff = dstr[loop];
		if (buff==dsep) {
			cnt_sep++;
			if (cnt_sep>2) {
				error = DATE_STR_ERROR_FORMAT;
				break;
			}
			continue;
		}
		else if (buff<'0'||buff>'9') {
			error = DATE_STR_ERROR_NOT_NUM;
			break;
		}
		if (!cnt_sep) {
			if (cnt_day==2) {
				error = DATE_STR_ERROR_FORMAT;
				break;
			}
			str_day[cnt_day++] = buff;
		}
		else if (cnt_sep==1) {
			if (cnt_month==2) {
				error = DATE_STR_ERROR_FORMAT;
				break;
			}
			str_month[cnt_month++] = buff;
		}
		else if (cnt_sep==2) {
			if (cnt_year==4) {
				error = DATE_STR_ERROR_FORMAT;
				break;
			}
			str_year[cnt_year++] = buff;
		}
	}
	if (!error) {
		str_day[cnt_day] = 0x0;
		str_month[cnt_month] = 0x0;
		str_year[cnt_year] = 0x0;
		cnt_day = atoi(str_day);
		cnt_month = atoi(str_month);
		cnt_year = atoi(str_year);
		date->date = date_is_valid(cnt_year,cnt_month,cnt_day);
	}
	return;
}
/*----------------------------------------------------------------------------*/
int date_year_day_count(my1date_t* date) {
	int loop, leap = year_is_leap(MY1DATE_YEAR(date->date));
	int month = MY1DATE_MONTH(date->date);
	int count = 0;
	for (loop=1;loop<month;loop++)
		count += month_days(loop,leap);
	count += MY1DATE_DAY(date->date);
	return count;
}
/*----------------------------------------------------------------------------*/
int date_week_day(my1date_t* date) {
	int temp = MY1DATE_YEAR(date->date);
	int init = temp + ((temp-1)/4) - ((temp-1)/100) + ((temp-1)/400);
	init = init % 7;
	temp = date_year_day_count(date)-1;
	temp += init;
	temp = temp % 7;
	return temp;
}
/*----------------------------------------------------------------------------*/
int date_month_remaining_days(my1date_t* date) {
	int curr, temp;
	curr = MY1DATE_MONTH(date->date);
	temp = month_days(curr,year_is_leap(MY1DATE_YEAR(date->date)));
	return temp-MY1DATE_DAY(date->date);
}
/*----------------------------------------------------------------------------*/
int date_year_remaining_days(my1date_t* date) {
	int curr, temp, leap, loop;
	curr = MY1DATE_MONTH(date->date);
	leap = year_is_leap(MY1DATE_YEAR(date->date));
	temp = month_days(curr,leap);
	temp -= MY1DATE_DAY(date->date);
	for (loop=curr;loop<12;loop++)
		temp += month_days(loop,leap);
	return temp;
}
/*----------------------------------------------------------------------------*/
int date_subtract(my1date_t* date, my1date_t* subs) {
	int days = 0, temp, buff, swap = 0;
	my1date_t chk;
	my1date_t *chk1 = date;
	my1date_t *chk2 = subs; /* assume subs is an earlier date */
	if (chk2->date>chk1->date) {
		/* nope... subs is later! */
		chk1 = subs;
		chk2 = date;
		swap = 1;
	}
	chk = *chk2;
	chk2 = &chk;
	/* check year - make equal */
	buff = MY1DATE_YEAR(chk1->date);
	while (MY1DATE_YEAR(chk2->date)<buff) {
		temp = date_year_remaining_days(chk2)+1;
		date_add_days(chk2,temp);
		days += temp;
	}
	/* check month - make equal */
	buff = MY1DATE_MONTH(chk1->date);
	while (MY1DATE_MONTH(chk2->date)<buff) {
		temp = date_month_remaining_days(chk2)+1;
		date_add_days(chk2,temp);
		days += temp;
	}
	/* check day - make equal */
	days += MY1DATE_DAY(chk1->date)-MY1DATE_DAY(chk2->date);
	/* check negative - subs is later than date */
	if (swap) days = -days;
	return days;
}
/*----------------------------------------------------------------------------*/
void date_add_days(my1date_t* date, int days) {
	int day = MY1DATE_DAY(date->date);
	int month = MY1DATE_MONTH(date->date);
	int year = MY1DATE_YEAR(date->date);
	int temp = date_month_remaining_days(date);
	while (days>temp) {
		days -= temp;
		month++;
		if (month>12) {
			month = 1;
			year++;
		}
		day = 0;
		temp = month_days(month,year_is_leap(year));
	}
	day += days;
	date->date = MY1DATE_MERGE(year,month,day);
}
/*----------------------------------------------------------------------------*/
void date_sub_days(my1date_t* date, int days) {
	int day = MY1DATE_DAY(date->date);
	int month = MY1DATE_MONTH(date->date);
	int year = MY1DATE_YEAR(date->date);
	int temp = day, full = 0;
	while (days>temp) {
		days -= temp;
		if (full) {
			month--;
			if (month==0) {
				month = 12;
				year--;
			}
		}
		else {
			day = 0;
			full = 1;
		}
		temp = month_days(month,year_is_leap(year));
	}
	day = temp - days;
	date->date = MY1DATE_MERGE(year,month,day);
}
/*----------------------------------------------------------------------------*/
void date_add_months(my1date_t* date, int months) {
	int day = MY1DATE_DAY(date->date);
	int month = MY1DATE_MONTH(date->date);
	int year = MY1DATE_YEAR(date->date);
	int temp = 12 - month;
	while (months>temp) {
		months -= temp;
		year++;
		month = 0;
		temp = 12;
	}
	month += months;
	temp = month_days(month,year_is_leap(year));
	if (day>temp) day = temp;
	date->date = MY1DATE_MERGE(year,month,day);
}
/*----------------------------------------------------------------------------*/
void date_add_years(my1date_t* date, int years) {
	date->date += years*10000;
}
/*----------------------------------------------------------------------------*/
char* date_2str(my1date_t* date, char* pstr) {
	if (!pstr) pstr = dttmpbuf;
	sprintf(pstr,d_format,MY1DATE_YEAR(date->date),
		MY1DATE_MONTH(date->date),MY1DATE_DAY(date->date));
	return pstr;
}
/*----------------------------------------------------------------------------*/
char* date_2my1(my1date_t* date, char* pstr) {
	if (!pstr) pstr = dttmpbuf;
	sprintf(pstr,DATE_MY1FORMAT,MY1DATE_YEAR(date->date),
		MY1DATE_MONTH(date->date),MY1DATE_DAY(date->date));
	return pstr;
}
/*----------------------------------------------------------------------------*/
int time_is_valid(int hour, int minute, int second) {
	if (hour<0||hour>23) return TIME_INVALID;
	if (minute<0||minute>59) return TIME_INVALID;
	if (second<0||second>59) return TIME_INVALID;
	return MY1TIME_MERGE(hour,minute,second);
}
/*----------------------------------------------------------------------------*/
int time_check(my1time_t* time) {
	if (time->date==DATE_INVALID) return time->date;
	if (time->time==TIME_INVALID) return time->time;
	return 0;
}
/*----------------------------------------------------------------------------*/
void time_read(my1time_t* this, int zone) {
	time_t curr = time(0x0);
	struct tm that;
	if (zone) localtime_r(&curr,&that);
	else gmtime_r(&curr,&that);
	date_assign((my1date_t*)this,(1900+that.tm_year),
		(that.tm_mon+1),that.tm_mday);
	time_assign(this,that.tm_hour,that.tm_min,that.tm_sec);
}
/*----------------------------------------------------------------------------*/
void time_init(my1time_t* time) {
	time_read(time,1); /* by default, get localtime */
}
/*----------------------------------------------------------------------------*/
void time_direct(my1time_t* time, int date_int, int time_int) {
	time->date = date_is_valid(MY1DATE_SPLIT(date_int));
	time->time = time_is_valid(MY1TIME_SPLIT(time_int));
}
/*----------------------------------------------------------------------------*/
void time_assign(my1time_t* time, int hour, int minute, int second) {
	time->time = time_is_valid(hour,minute,second);
}
/*----------------------------------------------------------------------------*/
void time_my1str(my1time_t* time, char* tstr) {
	int temp = 0;
	time->time = TIME_INVALID;
	/* make sure all numeric */
	while (tstr[temp]) {
		if (tstr[temp]<0x30||tstr[temp]>0x39)
			return;
		temp++;
	}
	/* make sure full HHMMSS format */
	if (temp!=6) return;
	temp = atoi(tstr);
	time_assign(time,MY1TIME_SPLIT(temp));
}
/*----------------------------------------------------------------------------*/
void time_isostr(my1time_t* time, char* tstr) {
	int temp = 0;
	time->time = TIME_INVALID;
	/* make sure all numeric */
	while (tstr[temp]) {
		if (temp==2||temp==5) {
			if (tstr[temp]!=':') return;
		}
		else {
			if (tstr[temp]<0x30||tstr[temp]>0x39) return;
		}
		temp++;
	}
	/* make sure full HH:MM:SS format */
	if (temp!=8) return;
	/* shift MM & SS into place */
	for (temp=2;temp<7;temp++) {
		if (temp<4) tstr[temp] = tstr[temp+1];
		else if (temp<6) tstr[temp] = tstr[temp+2];
		else tstr[temp] = 0x0;
	}
	temp = atoi(tstr);
	time_assign(time,MY1TIME_SPLIT(temp));
}
/*----------------------------------------------------------------------------*/
void time_next_hour(my1time_t* time, int *ph) {
	(*ph)++;
	if ((*ph)==24) {
		(*ph) = 0;
		date_add_days((my1date_t*)time,1);
	}
}
/*----------------------------------------------------------------------------*/
void time_next_minute(my1time_t* time, int *ph, int *pm) {
	(*pm)++;
	if ((*pm)==60) {
		(*pm) = 0;
		time_next_hour(time,ph);
	}
}
/*----------------------------------------------------------------------------*/
void time_add_seconds(my1time_t* time, int seconds) {
	int second = MY1TIME_SECOND(time->time);
	int minute = MY1TIME_MINUTE(time->time);
	int hour = MY1TIME_HOUR(time->time);
	/* break it down */
	int delh = seconds/3600;
	int delt = seconds%3600;
	int delm = delt/60;
	int dels = delt%60;
	/* start process */
	second += dels;
	if (second>=60) {
		second -= 60;
		time_next_minute(time,&hour,&minute);
	}
	minute += delm;
	if (minute>=60) {
		minute -= 60;
		time_next_hour(time,&hour);
	}
	hour += delh;
	if (hour>=24) {
		delt = hour/24;
		hour = hour%24;
		date_add_days((my1date_t*)time,delt);
	}
	time->time = MY1TIME_MERGE(hour,minute,second);
}
/*----------------------------------------------------------------------------*/
void time_add_direct(my1time_t* time, int timediff) {
	int second = MY1TIME_SECOND(time->time);
	int minute = MY1TIME_MINUTE(time->time);
	int hour = MY1TIME_HOUR(time->time);
	/* break it down */
	int delh = MY1TIME_HOUR(timediff);
	int delm = MY1TIME_MINUTE(timediff);
	int dels = MY1TIME_SECOND(timediff), delt;
	/* start process */
	second += dels;
	if (second>=60) {
		second -= 60;
		time_next_minute(time,&hour,&minute);
	}
	minute += delm;
	if (minute>=60) {
		minute -= 60;
		time_next_hour(time,&hour);
	}
	hour += delh;
	if (hour>=24) {
		delt = hour/24;
		hour = hour%24;
		date_add_days((my1date_t*)time,delt);
	}
	time->time = MY1TIME_MERGE(hour,minute,second);
}
/*----------------------------------------------------------------------------*/
void time_add_minutes(my1time_t* time, int minutes) {
	int second = MY1TIME_SECOND(time->time);
	int minute = MY1TIME_MINUTE(time->time);
	int hour = MY1TIME_HOUR(time->time);
	/* break it down */
	int delh = minutes/60;
	int delm = minutes%60, delt;
	/* start process */
	minute += delm;
	if (minute>=60) {
		minute -= 60;
		time_next_hour(time,&hour);
	}
	hour += delh;
	if (hour>=24) {
		delt = hour/24;
		hour = hour%24;
		date_add_days((my1date_t*)time,delt);
	}
	time->time = MY1TIME_MERGE(hour,minute,second);
}
/*----------------------------------------------------------------------------*/
void time_add_hours(my1time_t* time, int hours) {
	int second = MY1TIME_SECOND(time->time);
	int minute = MY1TIME_MINUTE(time->time);
	int hour = MY1TIME_HOUR(time->time)+hours, delt;
	if (hour>=24) {
		delt = hour/24;
		hour = hour%24;
		date_add_days((my1date_t*)time,delt);
	}
	time->time = MY1TIME_MERGE(hour,minute,second);
}
/*----------------------------------------------------------------------------*/
void time_prev_hour(my1time_t* time, int *ph) {
	(*ph)--;
	if ((*ph)<0) {
		(*ph) = 23;
		date_sub_days((my1date_t*)time,1);
	}
}
/*----------------------------------------------------------------------------*/
void time_prev_minute(my1time_t* time, int *ph, int *pm) {
	(*pm)--;
	if ((*pm)<0) {
		(*pm) = 59;
		time_prev_hour(time,ph);
	}
}
/*----------------------------------------------------------------------------*/
void time_sub_seconds(my1time_t* time, int seconds) {
	int second = MY1TIME_SECOND(time->time);
	int minute = MY1TIME_MINUTE(time->time);
	int hour = MY1TIME_HOUR(time->time);
	/* break it down */
	int delh = seconds/3600;
	int delt = seconds%3600;
	int delm = delt/60;
	int dels = delt%60;
	/* start process */
	if (second<dels) {
		time_prev_minute(time,&hour,&minute);
		second += 60; /* gather 1 minute's worth */
	}
	second -= dels;
	if (minute<delm) {
		time_prev_hour(time,&hour);
		minute += 60; /* gather 1 hour's worth */
	}
	minute -= delm;
	if (hour<delh) {
		delt = delh/24;
		delh = delh%24;
		if (hour<delh) delt++;
		date_sub_days((my1date_t*)time,delt);
		hour += 24; /* gather 1 day's worth */
	}
	hour -= delh;
	time->time = MY1TIME_MERGE(hour,minute,second);
}
/*----------------------------------------------------------------------------*/
void time_sub_direct(my1time_t* time, int timediff) {
	int second = MY1TIME_SECOND(time->time);
	int minute = MY1TIME_MINUTE(time->time);
	int hour = MY1TIME_HOUR(time->time);
	/* break it down */
	int delh = MY1TIME_HOUR(timediff);
	int delm = MY1TIME_MINUTE(timediff);
	int dels = MY1TIME_SECOND(timediff), delt;
	/* start process */
	if (second<dels) {
		time_prev_minute(time,&hour,&minute);
		second += 60; /* gather 1 minute's worth */
	}
	second -= dels;
	if (minute<delm) {
		time_prev_hour(time,&hour);
		minute += 60; /* gather 1 hour's worth */
	}
	minute -= delm;
	if (hour<delh) {
		delt = delh/24;
		delh = delh%24;
		if (hour<delh) delt++;
		date_sub_days((my1date_t*)time,delt);
		hour += 24; /* gather 1 day's worth */
	}
	hour -= delh;
	time->time = MY1TIME_MERGE(hour,minute,second);
}
/*----------------------------------------------------------------------------*/
char* time_2str(my1time_t* time, char* pstr) {
	if (!pstr) pstr = dttmpbuf;
	sprintf(pstr,t_format,MY1TIME_HOUR(time->time),MY1TIME_MINUTE(time->time),
		MY1TIME_SECOND(time->time));
	return pstr;
}
/*----------------------------------------------------------------------------*/
char* time_2my1(my1time_t* time, char* pstr) {
	if (!pstr) pstr = dttmpbuf;
	sprintf(pstr,TIME_MY1FORMAT,MY1TIME_HOUR(time->time),
		MY1TIME_MINUTE(time->time),MY1TIME_SECOND(time->time));
	return pstr;
}
/*----------------------------------------------------------------------------*/
char* time_2my1_full(my1time_t* time, char* pstr) {
	if (!pstr) pstr = dttmpbuf;
	sprintf(pstr,DATETIME_MY1FORMAT,MY1DATE_YEAR(time->date),
		MY1DATE_MONTH(time->date),MY1DATE_DAY(time->date),
		MY1TIME_HOUR(time->time),MY1TIME_MINUTE(time->time),
		MY1TIME_SECOND(time->time));
	return pstr;
}
/*----------------------------------------------------------------------------*/
int time_diff(my1time_t* time, my1time_t* subs, int* days) {
	my1time_t chk, *chk1, *chk2;
	int buff, temp, hour, mmin, ssec;
	int secs = 0, swap = 0;
	int dayz = date_subtract((my1date_t*)time,(my1date_t*)subs);
	/* check if subs is actually later */
	chk1 = time; chk2 = subs;
	if (dayz<0||(!dayz&&(chk2->time>chk1->time))) {
		dayz = -dayz; /* make dayz >= 0 */
		chk1 = subs;
		chk2 = time;
		swap = 1;
	}
	chk = *chk2; /* processing second operand as chk */
	chk2 = &chk;
	/* if different date, get seconds needed to next day */
	if (dayz) {
		temp = MY1TIME_SECOND(chk2->time);
		if (temp) {
			temp = 60-temp; /* next minute in seconds */
			time_add_seconds(chk2,temp);
			secs += temp;
		}
		temp = MY1TIME_MINUTE(chk2->time);
		if (temp) {
			temp = (60-temp)*60; /* next hour in seconds */
			time_add_seconds(chk2,temp);
			secs += temp;
		}
		temp = MY1TIME_HOUR(chk2->time);
		if (temp) {
			temp = (24-temp)*60*60; /* next day in seconds */
			time_add_seconds(chk2,temp);
			secs += temp;
		}
		if (secs>0) dayz--; /* got to next day! */
	}
	/* check hour - make equal */
	buff = MY1TIME_HOUR(chk1->time);
	while (MY1TIME_HOUR(chk2->time)<buff) {
		temp = 60-MY1TIME_SECOND(chk2->time); /* next minute in seconds */
		time_add_seconds((my1time_t*)chk2,temp);
		secs += temp;
		temp = (60-MY1TIME_MINUTE(chk2->time))*60; /* next hour in seconds */
		time_add_seconds((my1time_t*)chk2,temp);
		secs += temp;
	}
	/* check minute - make equal */
	buff = MY1TIME_MINUTE(chk1->time);
	while (MY1TIME_MINUTE(chk2->time)<buff) {
		temp = 60-MY1TIME_SECOND(chk2->time); /* next minute in seconds */
		time_add_seconds((my1time_t*)chk2,temp);
		secs += temp;
	}
	/* check second - make equal */
	temp = MY1TIME_SECOND(chk1->time)-MY1TIME_SECOND(chk2->time);
	if (temp) {
		time_add_seconds((my1time_t*)chk2,temp);
		secs += temp;
	}
	/* get in proper time for diff */
	hour = secs/3600;
	secs = secs%3600;
	mmin = secs/60;
	ssec = secs%60;
	secs = MY1TIME_MERGE(hour,mmin,ssec);
	if (swap) secs |= DATETIME_DIFF_SWAP;
	if (days) *days = dayz;
	return secs;
}
/*----------------------------------------------------------------------------*/
int date_time_diff(my1date_time_t* dt, int pick) {
	my1time_t *zutc, *zone;
	my1time_t chk1, chk2;
	zutc = &chk1; zone = &chk2;
	if (dt) {
		if (pick) zone = (my1time_t*)dt;
		else zutc = (my1time_t*)dt;
	}
	time_read(zutc,0); /* utc */
	time_read(zone,1); /* localtime */
	return time_diff(zone,zutc,0x0);
}
/*----------------------------------------------------------------------------*/
void date_time_init(my1date_time_t* dt) {
	dt->diff = date_time_diff(dt,0); /* picking utc */
}
/*----------------------------------------------------------------------------*/
char* date_time_zutc(my1date_time_t* dt, char* pstr) {
	if (!pstr) pstr = dttmpbuf;
	sprintf(pstr,dtformat,MY1DATE_YEAR(dt->date),
		MY1DATE_MONTH(dt->date),MY1DATE_DAY(dt->date),
		MY1TIME_HOUR(dt->time),MY1TIME_MINUTE(dt->time),
		MY1TIME_SECOND(dt->time));
	return pstr;
}
/*----------------------------------------------------------------------------*/
char* date_time_zone(my1date_time_t* dt, char* pstr) {
	my1date_time_t buff;
	date_time_zone_check(dt,&buff);
	if (!pstr) pstr = dttmpbuf;
	sprintf(pstr,dtformat,MY1DATE_YEAR(buff.date),
		MY1DATE_MONTH(buff.date),MY1DATE_DAY(buff.date),
		MY1TIME_HOUR(buff.time),MY1TIME_MINUTE(buff.time),
		MY1TIME_SECOND(buff.time));
	return pstr;
}
/*----------------------------------------------------------------------------*/
void date_time_zone_check(my1date_time_t* dt, my1date_time_t* zone) {
	int temp = (dt->diff&DATETIME_DIFF_MASK);
	int flag = (dt->diff&DATETIME_DIFF_SWAP);
/**
	int secs = (MY1TIME_HOUR(temp)*3600)+
		(MY1TIME_MINUTE(temp)*60)+MY1TIME_SECOND(temp);
*/
	*zone = *dt;
/**
	if (flag) time_sub_seconds(&buff,secs);
	else time_add_seconds(&buff,secs);
*/
	if (flag) time_sub_direct((my1time_t*)zone,temp);
	else time_add_direct((my1time_t*)zone,temp);
}
/*----------------------------------------------------------------------------*/
void date_time_pick_format(int pick) {
	switch (pick) {
		case DATETIME_PICK_MY1FORMAT:
			dtformat = DATETIME_MY1FORMAT;
			t_format = TIME_MY1FORMAT;
			d_format = DATE_MY1FORMAT;
			break;
		default:
		case DATETIME_PICK_ISOFORMAT:
			dtformat = DATETIME_ISOFORMAT;
			t_format = TIME_ISOFORMAT;
			d_format = DATE_ISOFORMAT;
			break;
	}
}
/*----------------------------------------------------------------------------*/
void date_time_my1str(my1date_time_t* dt, char* pstr) {
	char buff[16];
	int chk1, chk2;
	dt->date = DATE_INVALID;
	dt->time = TIME_INVALID;
	/* make sure all numeric */
	chk1 = 0;
	while (pstr[chk1]) {
		if (pstr[chk1]<0x30||pstr[chk1]>0x39)
			return;
		chk1++;
	}
	/* make sure full YYYYMMDDHHMMSS format */
	if (chk1!=14) return;
	strncpy(buff,pstr,8); /* null NOT placed! */
	buff[8] = 0x0;
	chk1 = atoi(buff);
	chk2 = atoi(&pstr[8]);
	date_time_direct(dt,chk1,chk2);
}
/*----------------------------------------------------------------------------*/
#if defined(TEST_MODULE) && defined(TEST_DATE)
/*----------------------------------------------------------------------------*/
void print_date_time(my1date_time_t* dt, int show_utc, int show_day) {
	char* sel;
	if (show_utc) sel = date_time_zutc(dt,0x0);
	else sel = date_time_zone(dt,0x0);
	printf("%s",sel);
	if (show_day) {
		my1date_time_t buff = *dt;
		char *day_name[] = { "Sunday", "Monday", "Tuesday", "Wednesday",
			"Thursday", "Friday", "Saturday", "Unknown" };
		time_add_direct((my1time_t*)&buff,buff.diff);
		int day = date_week_day((my1date_t*)&buff);
		switch (day) {
			case DAY_SUN: case DAY_MON: case DAY_TUE: case DAY_WED:
			case DAY_THU: case DAY_FRI: case DAY_SAT:
				break;
			default:
				day = DAY_HUH;
		}
		printf(" (@%s)",day_name[day]);
	}
}
/*----------------------------------------------------------------------------*/
void print_calendar(my1date_t* date) {
	int step, days, loop, init;
	int curr, year;
	char *day_name[] = { "Sun", "Mon", "Tue", "Wed",
		"Thu", "Fri", "Sat", "Unknown" };
	char *mon_name[] = { "Jan", "Feb", "Mac", "Apr",
		"May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
	year = MY1DATE_YEAR(date->date);
	curr = MY1DATE_MONTH(date->date);
	init = date_week_day(date);
	days = month_days(curr,year_is_leap(year));
	printf("\n %s %d (%d/%d)\n\n",mon_name[curr-1],year,init,days);
	for (loop=0;loop<7;loop++)
		printf(" %s",day_name[loop]);
	step = 0;
	while (step<=days) {
		for (loop=0;loop<7;loop++) {
			if (!loop) printf("\n");
			if (!step) {
				if (loop==init) step = 1;
				else printf("    ");
			}
			if (step) {
				printf(" %3d",step++);
				if (step>days) break;
			}
		}
	}
	printf("\n\n");
}
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	int test;
	my1date_time_t temp, save;
	char dstr[16]; /* using DATETIME_MY1FORMAT */
	if (argc>1) {
		/* need to check early to detect calendar request */
		test = strlen(argv[1]);
		if (test==6) {
			/* print calendar and exit */
			test = atoi(argv[1]);
			test *= 100;
			test++; /* first day of month */
			date_direct((my1date_t*)&temp,test);
			if (temp.date==DATE_INVALID) {
				fprintf(stderr,"** Invalid date ('%s')!\n",argv[1]);
				return -1;
			}
			print_calendar((my1date_t*)&temp);
			return 0;
		}
		if (test==8) {
			if (argc<3) {
				fprintf(stderr,"** Second date not given!\n");
				return -1;
			}
			if (strlen(argv[2])!=8) {
				fprintf(stderr,"** Invalid length ('%s')!\n",argv[2]);
				return -1;
			}
			/* testing date_subtract */
			printf("-- Reading date1 from command-line... ");
			fflush(stdout);
			date_my1str((my1date_t*)&temp,argv[1]);
			if (temp.date==DATE_INVALID) {
				fprintf(stderr,"\n** Invalid date1 ('%s')!\n",argv[1]);
				return -1;
			}
			printf("done.\n");
			printf("-- Reading date2 from command-line... ");
			fflush(stdout);
			date_my1str((my1date_t*)&save,argv[2]);
			if (save.date==DATE_INVALID) {
				fprintf(stderr,"\n** Invalid date2 ('%s')!\n",argv[2]);
				return -1;
			}
			printf("done.\n");
			printf("[1st] Date: %s\n",date_2str((my1date_t*)&temp,dstr));
			printf("[2nd] Date: %s\n",date_2str((my1date_t*)&save,dstr));
			printf("[Sub] Days: %d\n",
				date_subtract((my1date_t*)&temp,(my1date_t*)&save));
			return 0;
		}
		if (test==14) {
			printf("-- Reading time from command-line... ");
			fflush(stdout);
			date_time_my1str(&temp,argv[1]);
			if (date_time_check(&temp)) {
				fprintf(stderr,"\n** Invalid date/time ('%s'=>%d,%d)!\n",
					argv[1],temp.date,temp.time);
				return -1;
			}
			/* make sure get time zone */
			temp.diff = date_time_diff(0x0,0);
		}
		else {
			fprintf(stderr,"** Invalid input ('%s')!\n",argv[1]);
			return -1;
		}
	}
	else {
		printf("-- Getting system time... ");
		fflush(stdout);
		date_time_init(&temp);
	}
	printf("done!");
	printf("\n[Raw] Data: %08d,%06d,%c,%06d",temp.date,temp.time,
		temp.diff&DATETIME_DIFF_SWAP?'-':'+',temp.diff&DATETIME_DIFF_MASK);
	printf("\n[Chk] Date: %s (UTC)",date_2str((my1date_t*)&temp,dstr));
	fflush(stdout);
	printf("\n[UTC] Time: ");
	print_date_time(&temp,1,0);
	printf("\n[MY1] Time: ");
	date_time_pick_format(DATETIME_PICK_MY1FORMAT);
	print_date_time(&temp,0,1);
	date_time_pick_format(DATETIME_PICK_DEFAULT);
	printf("\nLocal Time: ");
	print_date_time(&temp,0,1);
	save = temp;
	printf("\nSaved Time: ");
	print_date_time(&save,0,1);
	printf("\n+1 Day    : ");
	date_add_days((my1date_t*)&temp,1);
	print_date_time(&temp,0,1);
	printf("\n+1 Hour   : ");
	time_add_hours((my1time_t*)&temp,1);
	print_date_time(&temp,0,1);
	printf("\n-1 Minute : ");
	time_add_minutes((my1time_t*)&temp,-1);
	print_date_time(&temp,0,1);
	printf("\n-1 Second : ");
	time_sub_seconds((my1time_t*)&temp,1);
	print_date_time(&temp,0,1);
	/* restore saved */
	printf("\n-- Restoring saved time... ");
	temp = save;
	/* inverse time difference */
	temp.diff ^= DATETIME_DIFF_SWAP;
	printf("\n(inv) Zone: %08d,%06d,%c,%06d",temp.date,temp.time,
		temp.diff&DATETIME_DIFF_SWAP?'-':'+',temp.diff&DATETIME_DIFF_MASK);
	printf("\n[Chk] Time: ");
	print_date_time(&temp,0,1);
	printf("\n");
	return 0;
}
/*----------------------------------------------------------------------------*/
#endif
/*----------------------------------------------------------------------------*/
