/*----------------------------------------------------------------------------*/
#include "my1expr.h"
#include "my1list_data.h"
#include "my1chtest.h"
/*----------------------------------------------------------------------------*/
#include <stdlib.h>
#include <string.h>
/*----------------------------------------------------------------------------*/
/**
#define MY1DEBUG_EXPR_READ
**/
/*----------------------------------------------------------------------------*/
void expr_init(my1expr_t* expr) {
	expr->type = EXPR_TYPE_CHK;
	expr->temp = 0;
	cstr_init(&expr->tval);
	expr->exec = 0x0;
	expr->data = 0x0;
	expr->opr1 = 0x0;
	expr->opr2 = 0x0;
	expr->read = 0x0;
}
/*----------------------------------------------------------------------------*/
void expr_free(my1expr_t* expr) {
	if (expr->opr1) {
		expr_free(expr->opr1);
		free((void*)expr->opr1); /* ALWAYS USE HEAP? */
		expr->opr1 = 0x0;
	}
	if (expr->opr2) {
		expr_free(expr->opr2);
		free((void*)expr->opr2);
		expr->opr2 = 0x0;
	}
	if (expr->read) {
		expr_free(expr->read);
		free((void*)expr->read);
		expr->read = 0x0;
	}
	if (expr->data) {
		free(expr->data);
		expr->data = 0x0;
	}
	cstr_free(&expr->tval);
}
#include <stdio.h>
/*----------------------------------------------------------------------------*/
void expr_exec(my1expr_t* expr, pdata_t chk1, pdata_t chk2) {
	if (expr->exec) {
		if (expr->read) {
			/* top-level expression that reads! exec this! */
			expr->read->exec = expr->exec;
			expr_exec(expr->read,chk1,chk2);
			expr->temp |= expr->read->temp;
			return;
		}
		if (expr->opr1) {
			expr->opr1->exec = expr->exec;
			expr_exec(expr->opr1,chk1,chk2);
		}
		if (expr->opr2) {
			expr->opr2->exec = expr->exec;
			expr_exec(expr->opr2,chk1,chk2);
		}
		expr->temp |= expr->exec(expr,chk1,chk2);
	}
}
/*----------------------------------------------------------------------------*/
#ifdef MY1DEBUG_EXPR_READ
/*----------------------------------------------------------------------------*/
#include <stdio.h>
/*----------------------------------------------------------------------------*/
void print_stack(my1list_t* pstack) {
	printf("[STACK](%d): ",pstack->size);
	my1item_t* pitem = pstack->init;
	while (pitem) {
		my1expr_t* ptoken = (my1expr_t*) pitem->data;
		printf("%s ",ptoken->tval.buff);
		pitem = pitem->next;
	}
	printf("[END]\n");
}
/*----------------------------------------------------------------------------*/
void print_queue(my1list_t* pqueue) {
	printf("[QUEUE](%d): ",pqueue->size);
	my1item_t* pitem = pqueue->init;
	while (pitem) {
		my1expr_t* ptoken = (my1expr_t*) pitem->data;
		printf("%s ",ptoken->tval.buff);
		pitem = pitem->next;
	}
	printf("[END]\n");
}
/*----------------------------------------------------------------------------*/
#endif
/*----------------------------------------------------------------------------*/
int opr_precedence(char ch) {
	int order = 0;
	switch (ch) {
		case OPERATOR_AND: case OPERATOR_ORR: case OPERATOR_XOR:
			order = 2; break;
		case OPERATOR_ADD: case OPERATOR_SUB:
			order = 3; break;
		case OPERATOR_MUL: case OPERATOR_DIV: case OPERATOR_MOD:
			order = 4; break;
	}
	return order;
}
/*----------------------------------------------------------------------------*/
int expr_read(my1expr_t* that, char* psrc) {
	/* info */
	int flag, step;
	my1list_t queue, stack;
	int iequ, curr, test;
	char *pbuf, *name;
	my1expr_t *pnew, *pchk, *ptmp;
	/* init */
	that->type = 0;
	flag = FLAG_OK;
	if (that->read) {
		expr_free(that->read);
		free((void*)that->read);
		that->read = 0x0;
	}
	cstr_null(&that->tval);
	list_init(&queue,LIST_OF_DATA);
	list_init(&stack,LIST_OF_DATA);
	name = 0x0;
	/* shunting yard algorithm => create Reverse Polish Notation! */
	step = 0; curr = 0; iequ = -1;
	/* separate terms (queue) from operators (stack) */
	while (psrc[curr]) {
		if (is_spacing(psrc[curr])) { curr++; continue; }
#ifdef MY1DEBUG_EXPR_READ
			printf("@@ Check:{%c|%d}\n",psrc[curr],curr);
#endif
		if ((test=chtest_number(&psrc[curr]))>0) {
			pbuf = chtest_tokenize(&psrc[curr],test);
			if (!pbuf) {
				flag |= FLAG_MAKEERROR;
				break;
			}
			test += curr;
			while (is_spacing(psrc[test])) test++;
#ifdef MY1DEBUG_EXPR_READ
			printf("@@ Token:{%s}[%d|%c]\n",pbuf,curr,psrc[test]);
#endif
			curr = test;
			pnew = (my1expr_t*) malloc(sizeof(my1expr_t));
			if (pnew) {
				expr_init(pnew);
				cstr_assign(&pnew->tval,pbuf);
				pnew->type = EXPR_TYPE_VAL;
				list_push_data(&queue,(void*)pnew);
#ifdef MY1DEBUG_EXPR_READ
				printf("   >VAL\n");
#endif
			}
			else flag |= FLAG_MAKEERROR;
			free((void*)pbuf);
			if (flag&FLAG_MAKEERROR) break;
			step++;
		}
		else if ((test=chtest_cname(&psrc[curr]))>0) {
			pbuf = chtest_tokenize(&psrc[curr],test);
			if (!pbuf) {
				flag |= FLAG_MAKEERROR;
				break;
			}
			/* was delim ws? */
			test += curr;
			while (is_spacing(psrc[test])) test++;
#ifdef MY1DEBUG_EXPR_READ
			printf("@@ Token:{%s}[%d|%c]\n",pbuf,curr,psrc[test]);
#endif
			curr = test;
			pnew = (my1expr_t*) malloc(sizeof(my1expr_t));
			if (pnew) {
				expr_init(pnew);
				cstr_assign(&pnew->tval,pbuf);
				/* check possible function */
				if (psrc[curr]=='(') {
					pnew->type = EXPR_TYPE_FUN;
					list_push_data(&stack,(void*)pnew);
#ifdef MY1DEBUG_EXPR_READ
					printf("   >FUN\n");
#endif
				}
				else {
					pnew->type = EXPR_TYPE_VAR;
					list_push_data(&queue,(void*)pnew);
#ifdef MY1DEBUG_EXPR_READ
					printf("   >VAR");
#endif
					if (psrc[curr]=='[') {
						/* an index? */
						while (psrc[curr]&&(psrc[curr]!=']')) {
							/* trimming whitespace */
							if (!is_spacing(psrc[curr]))
								cstr_single(&pnew->tval,psrc[curr]);
							curr++;
						}
						if (!psrc[curr]) break;
						/* should be ']' */
						cstr_single(&pnew->tval,psrc[curr]);
						curr++;
#ifdef MY1DEBUG_EXPR_READ
						printf("[]");
#endif
					}
#ifdef MY1DEBUG_EXPR_READ
					printf("\n");
#endif
				}
			}
			else flag |= FLAG_MAKEERROR;
			free((void*)pbuf);
			if (flag&FLAG_MAKEERROR) break;
			/* a possible LHS? */
			if (!step) name = pnew->tval.buff;
			step++;
		}
		else if (psrc[curr]=='(') {
			pnew = (my1expr_t*) malloc(sizeof(my1expr_t));
			if (pnew) {
				expr_init(pnew);
				pnew->type = EXPR_TYPE_BRK;
				cstr_single(&pnew->tval,psrc[curr]);
				list_push_data(&stack,(void*)pnew);
			}
			else {
				flag |= FLAG_MAKEERROR;
				break;
			}
			curr++;
			step++;
		}
		else if (psrc[curr]==')') {
			while (1) {
				pchk = (my1expr_t*) list_pop_data(&stack);
				if (!pchk) {
					flag |= FLAG_MISSING_B;
					break;
				}
				/* done when '(' found */
				if (pchk->type&EXPR_TYPE_BRK) {
					expr_free(pchk);
					free((void*)pchk);
					break;
				}
				/* push to queue */
				list_push_data(&queue,(void*)pchk);
			}
			if (flag&FLAG_MISSING_B) break;
			curr++;
		}
		else if (psrc[curr]=='=') {
			if (iequ>=0) {
				flag |= FLAG_DOUBLE_EQ;
				break;
			}
			if (!name) {
				flag |= FLAG_EQ_NOLEFT;
				break;
			}
			if (queue.size!=1) {
				flag |= FLAG_SYM_ERROR;
				break;
			}
			test = curr;
			while (is_spacing(psrc[curr])) curr++;
			pchk = (my1expr_t*) list_pull_data(&queue);
			if (name!=pchk->tval.buff) flag |= FLAG_UNKNOWN_1;
			else {
				cstr_copy(&that->tval,&pchk->tval);
				name = that->tval.buff;
#ifdef MY1DEBUG_EXPR_READ
				printf("@@ LHS:{%s}[%d|%c]\n",name,test,psrc[curr]);
#endif
			}
			expr_free(pchk);
			free((void*)pchk);
			if (flag&FLAG_UNKNOWN_1) break;
			iequ = curr;
			curr++;
		}
		else if (chtest_delim(psrc[curr],CHTEST_OPRS)) {
			while (stack.last) {
				pchk = (my1expr_t*) stack.last->data;
				pbuf = pchk->tval.buff;
				/* skip if found '(' */
				if (pchk->type==EXPR_TYPE_BRK) break;
				/* skip if NOT functions AND lower precedence */
				if (pchk->type!=EXPR_TYPE_FUN) {
					/**
					 * a = stack, b = curr
					 * pull from stack if prec(a) > prec(b) OR
					 *   prec(a) = prec(b) when b is left assoc
					 * assume left assoc only: skip when prec(a) < prec(b)
					**/
					if (opr_precedence(pbuf[0])<opr_precedence(psrc[curr]))
						break;
				}
				/* if temp is left-assoc. or not higher prec. */
				list_push_data(&queue,(void*)pchk);
				list_pop_data(&stack);
#ifdef MY1DEBUG_EXPR_READ
				printf("@@ Stack->Queue:{%s}\n",pchk->tval.buff);
#endif
			}
#ifdef MY1DEBUG_EXPR_READ
			printf("@@ Op:{%c}[%d]\n",psrc[curr],curr);
#endif
			pnew = (my1expr_t*) malloc(sizeof(my1expr_t));
			if (pnew) {
				expr_init(pnew);
				pnew->type = EXPR_TYPE_OPR;
				cstr_single(&pnew->tval,psrc[curr]);
				list_push_data(&stack,(void*)pnew);
			}
			else {
				flag |= FLAG_MAKEERROR;
				break;
			}
			curr++;
		}
		else {
			flag |= FLAG_UNKNOWN_1;
#ifdef MY1DEBUG_EXPR_READ
			printf("@@ Stray:{%c|%d}\n",psrc[curr],curr);
#endif
			break;
		}
	}
#ifdef MY1DEBUG_EXPR_READ
	printf("@@ Loop Done! (%08x)\n",flag);
	print_stack(&stack);
	print_queue(&queue);
#endif
	/* check remaining item in stack - create rpn in queue */
	if (!(flag&FLAG_ERROR)) {
		while ((pchk=(my1expr_t*)list_pop_data(&stack))) {
			if (pchk->type==EXPR_TYPE_BRK) /* if '(' found, extra! */
				flag |= FLAG_EXTRA_BRK;
			else /* push all operators to queue */
				list_push_data(&queue,(void*)pchk);
		}
		/* stack is empty! */
	}
#ifdef MY1DEBUG_EXPR_READ
	printf("@@ RPN Created! (%08x)\n",flag);
	print_stack(&stack);
	print_queue(&queue);
#endif
	/* process queue to finalize expr structure */
	if (!(flag&FLAG_ERROR)) {
		/* process output queue - stack should be empty! */
		while ((pchk=(my1expr_t*)list_pull_data(&queue))) {
			if (pchk->type==EXPR_TYPE_VAL||pchk->type==EXPR_TYPE_VAR)
				list_push_data(&stack,(void*)pchk);
			else if(pchk->type==EXPR_TYPE_FUN) {
				pnew = (my1expr_t*)list_pop_data(&stack);
				if (!pnew) {
					flag |= FLAG_MISSING_P;
					break;
				}
				/* pnew should NOT be an operator? */
				if ((pnew->type&EXPR_TYPE_OPR)) {
					flag |= FLAG_OPR_PARAM;
					break;
				}
				pchk->opr1 = pnew;
				list_push_data(&stack,(void*)pchk);
			}
			else if(pchk->type==EXPR_TYPE_OPR) {
				/* all binary operators! */
				ptmp = (my1expr_t*) list_pop_data(&stack);
				pnew = (my1expr_t*) list_pop_data(&stack);
				if (!pnew||!ptmp) {
					flag |= FLAG_MISSING_O;
					break;
				}
				pchk->opr1 = pnew;
				pchk->opr2 = ptmp;
				list_push_data(&stack,(void*)pchk);
			}
			else {
				flag |= FLAG_UNKNOWN_2;
				break;
			}
		}
		if (!(flag&FLAG_ERROR)) {
			if (stack.size!=1) flag |= FLAG_EVAL_STEP;
			else that->read = (my1expr_t*) list_pop_data(&stack);
		}
	}
#ifdef MY1DEBUG_EXPR_READ
	printf("[DEBUG] EXPR READ (%08x)\n",flag);
	print_stack(&stack);
	print_queue(&queue);
#endif
	/* clean up! need this because data list! no free task! */
	if (queue.size) flag |= FLAG_LEFTOVER1;
	test = queue.size; step = 0;
	while (queue.init) {
		pchk = (my1expr_t*)list_pull_data(&queue);
		expr_free(pchk);
		free((void*)pchk);
		step++;
	}
	if (step!=test) flag |= FLAG_LEFTOVER2;
	if (stack.size) flag |= FLAG_LEFTOVER1;
	test = stack.size; step = 0;
	while (stack.init) {
		pchk = (my1expr_t*)list_pop_data(&stack);
		expr_free(pchk);
		free((void*)pchk);
		step++;
	}
	if (step!=test) flag |= FLAG_LEFTOVER2;
	list_free(&stack);
	list_free(&queue);
	/* finalize */
	if (flag&FLAG_ERROR) {
		if (that->read) {
			expr_free(that->read);
			free((void*)that->read);
			that->read = 0x0;
		}
	}
	return flag;
}
/*----------------------------------------------------------------------------*/
#if defined(TEST_MODULE)
/*----------------------------------------------------------------------------*/
#include <stdio.h>
/*----------------------------------------------------------------------------*/
void print_expression(my1expr_t *expr) {
	if (expr->type==EXPR_TYPE_VAL||expr->type==EXPR_TYPE_VAR)
		printf("%s ",expr->tval.buff);
	else if (expr->type==EXPR_TYPE_FUN) {
		printf("%s ( ",expr->tval.buff);
		if (!expr->opr1) printf("??? ");
		else print_expression(expr->opr1);
		printf(" ) ");
	}
	else if (expr->type==EXPR_TYPE_OPR) {
		printf("( ");
		if (!expr->opr1) printf("??? ");
		else print_expression(expr->opr1);
		printf("%s ",expr->tval.buff);
		if (!expr->opr2) printf("??? ");
		else print_expression(expr->opr2);
		printf(") ");
	}
	else printf("<?%x:%s?> ",expr->type,expr->tval.buff);
}
/*----------------------------------------------------------------------------*/
#endif
/*----------------------------------------------------------------------------*/
#if defined(TEST_MODULE) && defined(TEST_EXPR)
/*----------------------------------------------------------------------------*/
#include "my1cstr.c"
#include "my1cstr_util.c"
#include "my1cstr_line.c"
#include "my1list.c"
#include "my1list_data.c"
#include "my1chtest.c"
/*----------------------------------------------------------------------------*/
/**
 * i: 3 + 4 * 2 / ( 1 - 5 ) ^ 2 ^ 3
 * o: 3 4 2 * 1 5 - 2 3 ^ ^ / +
 *
 * i: sin ( max ( 2 + 3 ) / 3 * pi )
 * o: 2 3 max 3 / pi * sin
**/
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	int test, flag;
	my1cstr_t buff;
	my1expr_t expr;
	cstr_init(&buff);
	expr_init(&expr);
	while (1) {
		printf("\nmy1expr> ");
		test = cstr_read_line(&buff,stdin);
		if (!test) break;
		cstr_trimws(&buff,1);
		if (!buff.fill) break;
		flag = expr_read(&expr,buff.buff);
		if (flag&FLAG_ERROR)
			printf("-- Error: (%08x) Input: '%s'\n",flag,buff.buff);
		else if (!expr.read)
			printf("-- Expr?: (%08x) Input: '%s'\n",flag,buff.buff);
		else {
			printf("-- Found: { ");
			if (expr.tval.fill)
				printf("%s = ",expr.tval.buff);
			print_expression(expr.read);
			printf("}\n");
		}
	}
	expr_free(&expr);
	cstr_free(&buff);
	printf("\n");
	return 0;
}
/*----------------------------------------------------------------------------*/
#endif
/*----------------------------------------------------------------------------*/
