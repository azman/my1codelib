/*----------------------------------------------------------------------------*/
#ifndef __MY1SOCKSRV_H__
#define __MY1SOCKSRV_H__
/*----------------------------------------------------------------------------*/
/**
 *  my1socksrv server socket interface library
 *  - written by azman@my1matrix.org
 */
/*----------------------------------------------------------------------------*/
#include "my1sockbase.h"
/*----------------------------------------------------------------------------*/
typedef int (*pserve_t)(void*); /* should return non-zero to exit */
typedef pserve_t pabort_t;
typedef pserve_t pclean_t;
/*----------------------------------------------------------------------------*/
#define SERVER_READY SOCKET_OK
#define SERVER_ERROR SOCKET_ERROR
#define SERVER_ERROR_SOCK (SERVER_ERROR|0x01)
#define SERVER_ERROR_EXEC (SERVER_ERROR|0x02)
#define SERVER_ERROR_ACCEPT (SERVER_ERROR|0x04)
#define SERVER_ERROR_PEEK (SERVER_ERROR|0x08)
/*----------------------------------------------------------------------------*/
typedef struct _my1socksrv_t {
	my1sock_t sock;
	my1conn_t conn;
	pserve_t code;
	pabort_t done;
	pclean_t tidy;
	void* data;
	int port,ccnt;
	int flag,exit;
	unsigned int opt1, opt2; /* application level flags... 4u to use! */
} my1socksrv_t;
/*----------------------------------------------------------------------------*/
void socksrv_init(my1socksrv_t* psrv, int port);
void socksrv_free(my1socksrv_t* psrv);
int socksrv_prep(my1socksrv_t* psrv);
int socksrv_exec(my1socksrv_t* psrv);
/*----------------------------------------------------------------------------*/
#endif /** __MY1SOCKSRV_H__ */
/*----------------------------------------------------------------------------*/
