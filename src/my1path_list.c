/*----------------------------------------------------------------------------*/
#include "my1path_list.h"
#include "my1list_sort.h"
/*----------------------------------------------------------------------------*/
#include <stdlib.h>
#include <string.h>
/* for struct dirent */
#include <dirent.h>
/* for stat */
#include <sys/stat.h>
/*----------------------------------------------------------------------------*/
int path_compare_name(void* pitem, void* qitem) {
	char *pname = (char*) pitem;
	char *qname = (char*) qitem;
	return strncmp(pname,qname,PATH_MAX);
}
/*----------------------------------------------------------------------------*/
void path_dolist(my1path_t* path, my1list_t* list, int list_flag) {
	DIR *chk;
	struct dirent *dir;
	struct stat statbuff;
	my1list_t plist, flist, *pick;
	my1item_t* item;
	if (!path->full||!(path->flag&PATH_FLAG_PATH)) {
		path->trap |= PATH_TRAP_ERROR;
		return;
	}
	/* browse given path */
	chk = opendir(path->full);
	if (!chk) {
		path->trap |= PATH_TRAP_ERROR_ODIR;
		return;
	}
	/* prepare internal temp list */
	list_init(&plist,0x0);
	list_init(&flist,0x0);
	/* browse dir - use temp list */
	while ((dir=readdir(chk))!=NULL) {
		char *name = dir->d_name, *buff, *full;
		/* skip current, parent path */
		if (!strncmp(name,".",1)||!strncmp(name,"..",2))
			continue;
		/* create full pathname */
		buff = strdup(path->full);
		full = path_name_merge(buff,name);
		free((void*)buff);
		/* basic stat - no link info? */
		if (!stat(full,&statbuff)) {
			item = make_item();
			if (item) {
				item->data = (void*)full;
				pick = 0x0;
				switch (statbuff.st_mode&S_IFMT) {
					case S_IFREG: pick = &flist; break;
					case S_IFDIR: pick = &plist; break;
				}
				if (pick) {
					full = 0x0; /* if NOT picked, should be freed! */
					list_push_item(pick,item);
				}
				else path->trap |= PATH_TRAP_ERROR_LIST;
			}
			else path->trap |= PATH_TRAP_ERROR_LMEM;
		}
		else path->trap |= PATH_TRAP_ERROR_SLST;
		if (full) free((void*)full);
	}
	closedir(chk);
	if (!(list_flag&PATH_LIST_BOTH)) list_flag = PATH_LIST_BOTH;
	if (list_flag&PATH_LIST_PATH) {
		/* sort the lists */
		list_sort(&plist,&path_compare_name);
		/* copy to main list (should be prepared externally) */
		while ((plist.curr=list_pull_item(&plist)))
			list_push_item(list,plist.curr);
	}
	else plist.done = list_free_item_data; /* free item/data if not used */
	if (list_flag&PATH_LIST_FILE) {
		list_sort(&flist,&path_compare_name);
		while ((flist.curr=list_pull_item(&flist)))
			list_push_item(list,flist.curr);
	}
	else flist.done = list_free_item_data; /* free item/data if not used */
	/* cleanup list */
	list_free(&plist);
	list_free(&flist);
}
/*----------------------------------------------------------------------------*/
char* path_getenv(my1list_t* list) {
	char* path = getenv("PATH");
	char *next, *curr, *buff;
	int size;
	if (list) {
		curr = path;
		while (1) {
			next = curr; size = 0;
			while (*next&&*next!=':') {
				next++;
				size++;
			}
			buff = strndup(curr,size);
			list_push_item_data(list,(void*)buff);
			if (!*next) break;
			curr = next + 1;
		}
	}
	return path;
}
/*----------------------------------------------------------------------------*/
#if defined(TEST_MODULE) && defined(TEST_PATH_LIST)
/*----------------------------------------------------------------------------*/
#include <stdio.h>
/*----------------------------------------------------------------------------*/
#include "my1path.c"
#include "my1list.c"
#include "my1list_sort.c"
/*----------------------------------------------------------------------------*/
void path_print_list(my1path_t* path) {
	printf("  %s %s",path->perm,path->name);
	if (path->real)
		printf(" -> %s",path->real);
	if (path->flag&PATH_FLAG_PATH)
		putchar(PATH_SEPARATOR_CHAR);
	printf("\n");
}
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	my1path_t test, next;
	my1list_t list;
	char *pick = argc>1?argv[1] : ".";
	/* check name given */
	path_init(&test);
	path_access(&test,pick);
	if (test.trap)
		printf("** Error accessing '%s' (%d)\n",pick,test.trap);
	else {
		/* check parent */
		path_init(&next);
		path_parent(&test,&next);
		if (next.trap&PATH_TRAP_ERROR)
			printf("** Error getting parent for '%s' (%x)\n",
				test.name,next.trap);
		else
			printf("Parent: %s %s\n",next.perm,next.full);
		printf("Access: %s %s\n",test.perm,test.full);
		if (test.flag&PATH_FLAG_PATH) {
			/* check paths */
			printf("DoList:\n");
			list_init(&list,list_free_item_data);
			path_dolist(&test,&list,PATH_LIST_BOTH);
			list.curr = 0x0;
			while (list_scan_item(&list)) {
				pick = (char*) list.curr->data;
				path_access(&next,pick);
				if (next.name)
					path_print_list(&next);
			}
			if (!list.size) printf("  Empty path!\n");
			list_free(&list);
		}
		path_free(&next);
	}
	/* cleanup */
	path_free(&test);
	/* show env PATH */
	list_init(&list,list_free_item_data);
	pick = path_getenv(&list);
	if (pick) {
		printf("PATH=%s\nList:\n",pick);
		list.curr = 0x0;
		while (list_scan_item(&list)) {
			pick = (char*) list.curr->data;
			printf("--> %s\n",pick);
		}
		printf("Done.\n");
	}
	list_free(&list);
	return 0;
}
/*----------------------------------------------------------------------------*/
#endif
/*----------------------------------------------------------------------------*/
