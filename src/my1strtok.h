/*----------------------------------------------------------------------------*/
#ifndef __MY1STRTOK_H__
#define __MY1STRTOK_H__
/*----------------------------------------------------------------------------*/
/**
 *  my1strtok_t : simple token-based reader (custom delimiter)
 *  - written by azman@my1matrix.org
 *  - requires my1cstr_util
 */
/*----------------------------------------------------------------------------*/
#define TSTOP_FLAG_DELIM 0x0100
#define TSTOP_FLAG_PFIND 0x0200
#define TSTOP_FLAG_MASK 0xFF
/*----------------------------------------------------------------------------*/
#include "my1cstr.h"
/*----------------------------------------------------------------------------*/
typedef struct _my1strtok_t {
	my1cstr_t* instr;
	my1cstr_t token;
	/* token detection */
	char *delim, *pfind, *pnext;
	int tnext, tstep, tsize, tstop;
} my1strtok_t;
/*----------------------------------------------------------------------------*/
typedef my1strtok_t my1cstr_token;
/*----------------------------------------------------------------------------*/
void strtok_init(my1strtok_t* tool, char* dlim);
void strtok_free(my1strtok_t* tool);
void strtok_prep(my1strtok_t* tool, my1cstr_t* psrc);
char* strtok_skip(my1strtok_t* tool, char* dlim);
char* strtok_next(my1strtok_t* tool);
char* strtok_next_delim(my1strtok_t* tool, char* dlim);
/*----------------------------------------------------------------------------*/
#endif /** __MY1STRTOK_H__ */
/*----------------------------------------------------------------------------*/
