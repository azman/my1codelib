/*----------------------------------------------------------------------------*/
#ifndef __MY1DATE_H__
#define __MY1DATE_H__
/*----------------------------------------------------------------------------*/
/**
 *  my1date access/manipulation to date/time info
 *  - written by azman@my1matrix.org
 */
/*----------------------------------------------------------------------------*/
#define MY1DATE_MERGE(yy,mm,dd) ((yy*10000)+(mm*100)+dd)
#define MY1DATE_YEAR(date) (date/10000)
#define MY1DATE_MONTH(date) ((date%10000)/100)
#define MY1DATE_DAY(date) (date%100)
#define MY1DATE_SPLIT(date) MY1DATE_YEAR(date), \
	MY1DATE_MONTH(date),MY1DATE_DAY(date)
/*----------------------------------------------------------------------------*/
#define DATE_INVALID -1
/*----------------------------------------------------------------------------*/
#define DATE_STR_ERROR_GENERAL -1
#define DATE_STR_ERROR_NOT_NUM -2
#define DATE_STR_ERROR_FORMAT  -3
/*----------------------------------------------------------------------------*/
/* date_week_day output constants */
#define DAY_SUN 0
#define DAY_MON 1
#define DAY_TUE 2
#define DAY_WED 3
#define DAY_THU 4
#define DAY_FRI 5
#define DAY_SAT 6
#define DAY_HUH 7
/*----------------------------------------------------------------------------*/
#define DATE_ISOFORMAT "%04d-%02d-%02d"
#define DATE_MY1FORMAT "%04d%02d%02d"
/*----------------------------------------------------------------------------*/
typedef struct _my1date_t {
	int date; /* YYYYMMDD in a single int */
} my1date_t;
/*----------------------------------------------------------------------------*/
int year_is_leap(int year);
int month_days(int month, int leap);
int date_is_valid(int year, int month, int day);
/*----------------------------------------------------------------------------*/
void date_init(my1date_t* date);
void date_direct(my1date_t* date, int date_int);
void date_assign(my1date_t* date, int year, int month, int day);
void date_my1str(my1date_t* date, char* dstr); /* YYYYMMDD */
void date_isostr(my1date_t* date, char* dstr); /* YYYY-MM-DD */
void date_chkstr(my1date_t* date, char* dstr, char dsep); /* d-m-y */
int date_year_day_count(my1date_t* date);
int date_week_day(my1date_t* date); /* 0 - Sunday, 6 - Saturday */
int date_month_remaining_days(my1date_t* date);
int date_year_remaining_days(my1date_t* date);
int date_subtract(my1date_t* date, my1date_t* subs);
void date_add_days(my1date_t* date, int days);
void date_sub_days(my1date_t* date, int days);
void date_add_months(my1date_t* date, int months);
void date_add_years(my1date_t* date, int years);
/*----------------------------------------------------------------------------*/
char* date_2str(my1date_t* date, char* pstr);
char* date_2my1(my1date_t* date, char* pstr);
/*----------------------------------------------------------------------------*/
#define MY1TIME_MERGE(hh,mm,ss) ((hh*10000)+(mm*100)+ss)
#define MY1TIME_HOUR(time) (time/10000)
#define MY1TIME_MINUTE(time) ((time%10000)/100)
#define MY1TIME_SECOND(time) (time%100)
#define MY1TIME_SPLIT(time) MY1TIME_HOUR(time), \
	MY1TIME_MINUTE(time),MY1TIME_SECOND(time)
/*----------------------------------------------------------------------------*/
#define TIME_INVALID -2
/*----------------------------------------------------------------------------*/
#define TIME_ISOFORMAT "%02d:%02d:%02d"
#define TIME_MY1FORMAT "%02d%02d%02d"
/*----------------------------------------------------------------------------*/
typedef struct _my1time_t {
	int date; /* YYYYMMDD in a single int */
	int time; /* HHMMSS in single int (24h format) */
} my1time_t;
/*----------------------------------------------------------------------------*/
int time_is_valid(int hour, int minute, int second);
int time_check(my1time_t* time);
/*----------------------------------------------------------------------------*/
void time_read(my1time_t* this, int zone);
void time_init(my1time_t* time);
void time_direct(my1time_t* time, int date_int, int time_int);
void time_assign(my1time_t* time, int hour, int minute, int second);
void time_my1str(my1time_t* time, char* tstr);
void time_isostr(my1time_t* time, char* tstr);
void time_add_seconds(my1time_t* time, int seconds);
void time_sub_seconds(my1time_t* time, int seconds);
void time_add_minutes(my1time_t* time, int minutes);
void time_add_hours(my1time_t* time, int hours);
void time_add_direct(my1time_t* time, int timediff);
void time_sub_direct(my1time_t* time, int timediff);
/*----------------------------------------------------------------------------*/
char* time_2str(my1time_t* time, char* pstr);
char* time_2my1(my1time_t* time, char* pstr);
char* time_2my1_full(my1time_t* time, char* pstr);
/*----------------------------------------------------------------------------*/
/* math for time difference */
int time_diff(my1time_t* time, my1time_t* subs, int* days);
/*----------------------------------------------------------------------------*/
/** ISO8601 date time format => YYYY-MM-DDThh:mm:ss.sTZD */
#define DATETIME_ISOFORMAT DATE_ISOFORMAT"T"TIME_ISOFORMAT
#define DATETIME_ISOFORMAT_LEN (19+1+3)
#define DATETIME_MY1FORMAT DATE_MY1FORMAT""TIME_MY1FORMAT
#define DATETIME_MY1FORMAT_LEN (14+1+1)
/* by default use iso format */
#define DATETIME_FORMAT DATETIME_ISOFORMAT
#define DATETIME_FORMAT_LEN DATETIME_ISOFORMAT_LEN
/*----------------------------------------------------------------------------*/
#define DATETIME_PICK_ISOFORMAT 0
#define DATETIME_PICK_MY1FORMAT 1
#define DATETIME_PICK_DEFAULT DATETIME_PICK_ISOFORMAT
/*----------------------------------------------------------------------------*/
#define DATETIME_DIFF_MASK 0x00FFFFFF
#define DATETIME_DIFF_SWAP 0x80000000
/*----------------------------------------------------------------------------*/
typedef struct _my1date_time_t {
	int date, time; /* my1time_t */
	int diff; /* diff - local/utc flag */
	int tpad; /* alignment padding (temp storage) */
} my1date_time_t;
/*----------------------------------------------------------------------------*/
/* get timezone diff and assign dt based on pick (utc=0/local=!0) */
int date_time_diff(my1date_time_t* dt, int pick);
void date_time_init(my1date_time_t* dt); /* utc time with diff */
char* date_time_zutc(my1date_time_t* dt, char* pstr);
char* date_time_zone(my1date_time_t* dt, char* pstr);
void date_time_zone_check(my1date_time_t* dt, my1date_time_t* zone);
void date_time_pick_format(int pick);
void date_time_my1str(my1date_time_t* dt, char* pstr);
/*----------------------------------------------------------------------------*/
#define date_time_direct(dt,date,time) time_direct((my1time_t*)dt,date,time)
#define date_time_check(dt) time_check((my1time_t*)dt)
/*----------------------------------------------------------------------------*/
#endif /** __MY1DATE_H__ */
/*----------------------------------------------------------------------------*/
