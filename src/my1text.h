/*----------------------------------------------------------------------------*/
#ifndef __MY1TEXT_H__
#define __MY1TEXT_H__
/*----------------------------------------------------------------------------*/
/**
 *  my1text_t : read text file per line
 *  - written by azman@my1matrix.org
 *  - requires my1cstr_line
 */
/*----------------------------------------------------------------------------*/
#include "my1cstr_line.h"
/*----------------------------------------------------------------------------*/
#define TEXT_FILENAME_SIZE 256
/*----------------------------------------------------------------------------*/
#define LINE_INIT 0
#define LINE_NONE -1
#define LINE_DONE -2
#define CHAR_INIT 0
#define CHAR_NONE -1
#define CHAR_DONE -2
/*----------------------------------------------------------------------------*/
typedef struct _my1text_t {
	char fname[TEXT_FILENAME_SIZE];
	int iline, ichar;
	FILE *pfile;
	my1cstr_t pbuff;
} my1text_t;
/*----------------------------------------------------------------------------*/
void text_init(my1text_t* ptext);
void text_free(my1text_t* ptext);
void text_open(my1text_t* ptext, char* pname);
void text_done(my1text_t* ptext);
int text_read(my1text_t* ptext); /* read 1 line */
/*----------------------------------------------------------------------------*/
#endif /** __MY1TEXT_H__ */
/*----------------------------------------------------------------------------*/
