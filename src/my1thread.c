/*----------------------------------------------------------------------------*/
#include "my1thread.h"
/*----------------------------------------------------------------------------*/
#ifdef DO_MINGW
#include <process.h>
#endif
/*----------------------------------------------------------------------------*/
void thread_init(my1thread_t* thread, thread_code_t pcode) {
	thread->id = 0;
	thread->created = 0;
	thread->running = 0;
	thread->stopped = 0;
	thread->runcode = pcode;
}
/*----------------------------------------------------------------------------*/
void thread_free(my1thread_t* thread) {
#ifdef DO_MINGW
	if (thread->created) {
		CloseHandle(thread->created);
		thread->created = 0;
	}
#endif
	thread->id = 0;
	thread->running = 0;
	thread->stopped = 0;
	thread->created = 0;
	thread->runcode = 0x0;
}
/*----------------------------------------------------------------------------*/
int thread_exec(my1thread_t* thread,void* arg) {
#ifdef DO_MINGW
	/** default attr, 4K stack, run immediately */
	thread->created = (HANDLE) _beginthreadex(0x0,4096,thread->runcode,
		arg,0,&thread->id);
#else
	/** creating thread using default attribute? */
	thread->created = pthread_create(&thread->id,0x0,thread->runcode,arg);
#endif
	return thread->created ? 0 : 1;
}
/*----------------------------------------------------------------------------*/
int thread_wait(my1thread_t* thread) {
#ifdef DO_MINGW
	WaitForSingleObject(thread->created,INFINITE);
#else
	pthread_join(thread->id,0x0);
#endif
	return 0;
}
/*----------------------------------------------------------------------------*/
int thread_done(my1thread_t* thread) {
#ifdef DO_MINGW
	_endthreadex(0x0);
#else
	pthread_exit(0x0);
#endif
	return 0;
}
/*----------------------------------------------------------------------------*/
int thread_created(my1thread_t* thread) {
	return (int)thread->created;
}
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#if defined(TEST_MODULE) && defined(TEST_THREAD)
/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include <unistd.h>
/*----------------------------------------------------------------------------*/
/* declare a global variable */
static int check = 0;
/*----------------------------------------------------------------------------*/
/* function to be executed by created thread */
#ifdef DO_MINGW
unsigned __stdcall go_smith(void* arg)
#else
void* go_smith(void* arg)
#endif
{
	my1thread_t *psmith = (my1thread_t*) arg;
	psmith->running = 1;
	while (!psmith->stopped) {
		check++;
		printf("-- I am Agent Smith 0x%x @%p (%d)\n",
			(unsigned)psmith->id,&check,check);
		sleep(3);
	}
	psmith->running = 0;
	thread_done(psmith);
	return 0x0;
}
/*----------------------------------------------------------------------------*/
int main(int argc, char *argv[]) {
	my1thread_t smith;
	check++;
	thread_init(&smith,&go_smith);
	printf("Executing Agent Smith and waiting 10 seconds [%d]\n",check);
	thread_exec(&smith,&smith);
	sleep(10);
	smith.stopped = 1;
	thread_wait(&smith);
	thread_free(&smith);
	printf("I am Agent Smith 0 @%p (%d)\n",&check,check);
	return 0;
}
/*----------------------------------------------------------------------------*/
#endif
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
