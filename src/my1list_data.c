/*----------------------------------------------------------------------------*/
#include "my1list_data.h"
/*----------------------------------------------------------------------------*/
#include <stdlib.h>
/*----------------------------------------------------------------------------*/
void* list_push_data(my1list_t* list, void* data) {
	my1item_t* item;
	item = make_item();
	if (item) {
		item->data = data;
		list_push_item(list,item);
	}
	return (void*)item;
}
/*----------------------------------------------------------------------------*/
void* list_pull_data(my1list_t* list) {
	my1item_t* item;
	void *data;
	data = 0x0;
	item = list->init;
	if (item) {
		list->init = item->next;
		if (!list->init) list->last = 0x0;
		else list->init->prev = 0x0;
		data = item->data;
		free((void*)item);
		list->size--;
	}
	return data;
}
/*----------------------------------------------------------------------------*/
void* list_pop_data(my1list_t* list) {
	my1item_t* item;
	void *data;
	data = 0x0;
	item = list->last;
	if (item) {
		list->last = item->prev;
		if (!list->last) list->init = 0x0;
		else list->last->next = 0x0;
		data = item->data;
		free((void*)item);
		list->size--;
	}
	return data;
}
/*----------------------------------------------------------------------------*/
void* list_scan_data(my1list_t* list) {
	my1item_t* item;
	item = list_scan_item(list);
	return item?item->data:0x0;
}
/*----------------------------------------------------------------------------*/
#if defined(TEST_MODULE) && defined(TEST_LIST_DATA)
/*----------------------------------------------------------------------------*/
/* will create a proper test later :p */
#define TEST_LIST
#include "my1list.c"
/*----------------------------------------------------------------------------*/
#endif
/*----------------------------------------------------------------------------*/
