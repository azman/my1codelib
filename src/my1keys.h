/*----------------------------------------------------------------------------*/
#ifndef __MY1KEYS_H__
#define __MY1KEYS_H__
/*----------------------------------------------------------------------------*/
/**
 *  my1keys - module for keyboard access
 *  - written by azman@my1matrix.org
 *  - imported from my1termu project (my1cons)
**/
/*----------------------------------------------------------------------------*/
/* common 'basic' keys */
#define KEY_NONE   0x00000000
#define KEY_ESCAPE 0x0000001B
#define KEY_SPACE  0x00000020
#define KEY_BSPACE 0x0000007F
#define KEY_TAB    0x00000009
/** control alpha-keys - on linux, also work for uppercase! */
#define KEY_CTRL_a 0x00000001
#define KEY_CTRL_b 0x00000002
#define KEY_CTRL_d 0x00000004
#define KEY_CTRL_f 0x00000006
#define KEY_CTRL_p 0x00000010
#define KEY_CTRL_r 0x00000012
#define KEY_CTRL_t 0x00000014
#define KEY_CTRL_z 0x0000001A
/** alternate keys - on linux, it is simply normal ascii codes */
/* platform dependent */
/*----------------------------------------------------------------------------*/
#ifdef DO_MINGW
/*----------------------------------------------------------------------------*/
#define KEY_ENTER  0x0000000D
/* escaped keys - null on win32 (subs with 0x1b) */
#define KEY_F1  0x00001B3B
#define KEY_F2  0x00001B3C
#define KEY_F3  0x00001B3D
#define KEY_F4  0x00001B3E
#define KEY_F5  0x00001B3F
#define KEY_F6  0x00001B40
#define KEY_F7  0x00001B41
#define KEY_F8  0x00001B42
#define KEY_F9  0x00001B43
#define KEY_F10 0x00001B44
#define KEY_ALTF1 0x00001B68
#define KEY_ALTF2 0x00001B69
#define KEY_ALTF3 0x00001B6A
#define KEY_ALTF4 0x00001B6B
#define KEY_ALTF5 0x00001B6C
#define KEY_ALTF6 0x00001B6D
#define KEY_ALTF7 0x00001B6E
#define KEY_ALTF8 0x00001B6F
#define KEY_ALTF9  0x00001B70
#define KEY_ALTF10 0x00001B71
/* these have 0xE0 in key */
#define KEY_F11 0x00001B85
#define KEY_F12 0x00001B86
#define KEY_ALTF11 0x00001B8B
#define KEY_ALTF12 0x00001B8C
#define KEY_UP    0x00001B48
#define KEY_DOWN  0x00001B50
#define KEY_RIGHT 0x00001B4B
#define KEY_LEFT  0x00001B4D
#define KEY_HOME  0x00001B47
#define KEY_END   0x00001B4F
#define KEY_PAGEUP   0x00001B49
#define KEY_PAGEDOWN 0x00001B51
#define KEY_INSERT   0x00001B52
#define KEY_DELETE   0x00001B53
/*----------------------------------------------------------------------------*/
#include <conio.h>
/*----------------------------------------------------------------------------*/
#else
/*----------------------------------------------------------------------------*/
#define KEY_ENTER  0x0000000A
#define KEY_F1 0x00004F50
#define KEY_F2 0x00004F51
#define KEY_F3 0x00004F52
#define KEY_F4 0x00004F53
#define KEY_F5 0x5B31357E
#define KEY_F6 0x5B31377E
#define KEY_F7 0x5B31387E
#define KEY_F8 0x5B31397E
#define KEY_F9  0x5B32307E
#define KEY_F10 0x5B32317E
#define KEY_F11 0x5B32337E
#define KEY_F12 0x5B32347E
#define KEY_UP    0x00005B41
#define KEY_DOWN  0x00005B42
#define KEY_RIGHT 0x00005B43
#define KEY_LEFT  0x00005B44
#define KEY_HOME  0x00005B48
#define KEY_END   0x00005B46
#define KEY_PAGEUP   0x005B357E
#define KEY_PAGEDOWN 0x005B367E
#define KEY_INSERT   0x005B327E
#define KEY_DELETE   0x005B337E
#define KEY_ALTF1 0x313B3350
#define KEY_ALTF2 0x313B3351
#define KEY_ALTF3 0x313B3352
#define KEY_ALTF4 0x313B3353
#define KEY_ALTF5 0x353B337E
#define KEY_ALTF6 0x373B337E
#define KEY_ALTF7 0x383B337E
#define KEY_ALTF8 0x393B337E
#define KEY_ALTF9  0x303B337E
#define KEY_ALTF10 0x313B337E
#define KEY_ALTF11 0x333B337E
#define KEY_ALTF12 0x343B337E
/*----------------------------------------------------------------------------*/
/** conio on linux! */
int getch(void);
int kbhit(void);
/*----------------------------------------------------------------------------*/
/* useful to have? */
void makeraw_iterminal(void);
void restore_iterminal(void);
/*----------------------------------------------------------------------------*/
#endif
/*----------------------------------------------------------------------------*/
typedef unsigned int my1key_t; /* must be at least 32-bit! */
/*----------------------------------------------------------------------------*/
my1key_t get_keypress(my1key_t *extended);
my1key_t get_key(void);
my1key_t get_keyhit(void); /* non-blocking wrapper for get_key */
my1key_t get_keyhit_extended(my1key_t *extended);
int get_param_int(int argc, char* argv[], int *index, int *value);
char* get_param_str(int argc, char* argv[], int *index);
/*----------------------------------------------------------------------------*/
#endif /** __MY1KEYS_H__ */
/*----------------------------------------------------------------------------*/
