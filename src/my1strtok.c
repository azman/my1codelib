/*----------------------------------------------------------------------------*/
#include "my1strtok.h"
#include "my1cstr_util.h"
/*----------------------------------------------------------------------------*/
#include <stdlib.h>
#include <string.h>
/*----------------------------------------------------------------------------*/
void strtok_init(my1strtok_t* tool, char* dlim) {
	tool->instr = 0x0;
	tool->delim = dlim;
	tool->pfind = 0x0; /* additional delim just to stop (NOT trimmed!) */
	/** strtok_prep will initialize these...
	tool->pnext = 0x0;
	tool->tnext = 0;
	tool->tstep = 0;
	tool->tsize = 0;
	tool->tstop = 0;*/
	cstr_init(&tool->token);
}
/*----------------------------------------------------------------------------*/
void strtok_free(my1strtok_t* tool) {
	cstr_free(&tool->token);
}
/*----------------------------------------------------------------------------*/
void strtok_prep(my1strtok_t* tool, my1cstr_t* psrc) {
	if (!psrc) return;
	tool->instr = psrc;
	tool->pnext = 0x0;
	tool->tnext = 0;
	tool->tstep = 0;
	tool->tsize = 0;
	tool->tstop = 0;
	cstr_null(&tool->token);
	cstr_resize(&tool->token,psrc->fill);
}
/*----------------------------------------------------------------------------*/
char* strtok_skip(my1strtok_t* tool, char* dlim) {
	char *skip = 0x0, *test = tool->instr->buff;
	int loop, size;
	loop = tool->tnext;
	size = tool->instr->fill;
	if (!dlim) dlim = tool->delim;
	/* skip leading delimiters */
	while (loop<size&&cstr_delim(dlim,test[loop])) loop++;
	if (loop<size)
		skip = &test[loop];
	tool->tnext = loop;
	return skip; /* the next non-delimiter character */
}
/*----------------------------------------------------------------------------*/
char strtok_stop(my1strtok_t* tool, char test) {
	if (cstr_delim(tool->delim,test)) {
		tool->tstop = TSTOP_FLAG_DELIM|((int)test&TSTOP_FLAG_MASK);
		return test;
	}
	if (tool->pfind&&cstr_delim(tool->pfind,test)) {
		tool->tstop = TSTOP_FLAG_PFIND|((int)test&TSTOP_FLAG_MASK);
		return test;
	}
	return (char)0;
}
/*----------------------------------------------------------------------------*/
char* strtok_next_exec(my1strtok_t* tool) {
	int loop;
	char *test = tool->instr->buff;
	/* prepare find */
	tool->pnext = 0x0;
	tool->tsize = 0;
	tool->tstop = 0;
	/* trim left */
	if (!strtok_skip(tool,0x0))
		return 0x0;
	loop = tool->tnext;
	while (test[loop]) {
		if (strtok_stop(tool,test[loop])) {
			tool->pnext = &test[tool->tnext];
			tool->tnext = loop+1;
			break;
		}
		loop++;
		tool->tsize++;
	}
	/* null without a delimiter? */
	if (!tool->pnext&&tool->tsize>0) {
		tool->pnext = &test[tool->tnext];
		tool->tnext = -1; /* last token! */
	}
	return tool->pnext;
}
/*----------------------------------------------------------------------------*/
char* strtok_next(my1strtok_t* tool) {
	char buff;
	if (!tool->instr||!tool->instr->buff) return 0x0;
	if (strtok_next_exec(tool)) {
		tool->tstep++;
		/* save delimiter (overwrite null!) */
		buff = tool->pnext[tool->tsize];
		tool->pnext[tool->tsize] = 0x0;
		/* copy the token */
		cstr_assign(&tool->token,tool->pnext);
		/* restore delimiter */
		tool->pnext[tool->tsize] = buff;
	}
	return tool->pnext;
}
/*----------------------------------------------------------------------------*/
char* strtok_next_delim(my1strtok_t* tool, char* dlim) {
	char *save, *sav2, *next;
	save = tool->delim;
	tool->delim = dlim; /* override default delim */
	sav2 = tool->pfind;
	tool->pfind = 0x0; /* suppress pfind */
	next = strtok_next(tool);
	tool->delim = save; /* restore default delim */
	tool->pfind = sav2;
	return next;
}
/*----------------------------------------------------------------------------*/
#if defined(TEST_MODULE) && defined(TEST_STRTOK)
/*----------------------------------------------------------------------------*/
#include "my1cstr.c"
#include "my1cstr_line.c"
#include "my1cstr_util.c"
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	int test;
	my1cstr_t buff;
	my1strtok_t find;
	cstr_init(&buff);
	strtok_init(&find,COMMON_DELIM);
	while (1) {
		printf("\nEnter text (empty string to exit): ");
		test = cstr_read_line(&buff,stdin);
		if (!test) break;
		cstr_trimws(&buff,1);
		if (!buff.fill) break; /* break on empty string */
		printf("\nInput: '%s' (%d:%d/%d)",buff.buff,test,buff.fill,buff.size);
		strtok_prep(&find,&buff);
		printf("\nToken: ");
		while (strtok_next(&find))
			printf("[%d:%s]",find.tstep,find.token.buff);
		printf("\n");
	}
	strtok_free(&find);
	cstr_free(&buff);
	printf("\n");
	return 0;
}
/*----------------------------------------------------------------------------*/
#endif
/*----------------------------------------------------------------------------*/
