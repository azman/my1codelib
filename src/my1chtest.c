/*----------------------------------------------------------------------------*/
#include "my1chtest.h"
/*----------------------------------------------------------------------------*/
#include <stdlib.h>
#include <string.h>
/*----------------------------------------------------------------------------*/
int chtest_delim(char achar, char* filter) {
	char find = '\0';
	while (filter[0]) {
		if (achar==filter[0]) {
			find = achar;
			break;
		}
		filter++;
	}
	return (int)find;
}
/*----------------------------------------------------------------------------*/
int chtest_cname(char *pstr) {
	int step = 0;
	if (is_name_init(pstr[step])) {
		step++;
		while (pstr[step]) {
			if (!is_name(pstr[step]))
				break;
			step++;
		}
	}
	return step;
}
/*----------------------------------------------------------------------------*/
int chtest_number(char *pstr) {
	int step, idot;
	step = 0;
	if (is_number(pstr[step])) {
		/* consider e? */
		idot = -1;
		step++;
		while (pstr[step]) {
			if (!is_number(pstr[step])) {
				if (pstr[step]==CHAR_COMPLEX) {
					step++; break;
				}
				if (pstr[step]!='.') break;
				if (idot>=0) break;
				idot = step;
			}
			step++;
		}
	}
	return step;
}
/*----------------------------------------------------------------------------*/
int chtest_cstring(char *pstr) {
	int step = 0;
	if (pstr[step]=='"') {
		step++;
		while (pstr[step]) {
			if (pstr[step]=='"')
				break;
			if (pstr[step]=='\\') {
				/* escape char */
				step++;
				if (!pstr[step]) break;
			}
			step++;
		}
	}
	return step;
}
/*----------------------------------------------------------------------------*/
char* chtest_tokenize(char *pstr, int last) {
	/* last is output of chtest_cname @ chtest_number */
	char* pbuf;
	char temp;
	temp = pstr[last];
	pstr[last] = 0x0;
	pbuf = strdup(pstr);
	pstr[last] = temp;
	return pbuf;
}
/*----------------------------------------------------------------------------*/
#if defined(TEST_MODULE) && defined(TEST_CHTEST)
/*----------------------------------------------------------------------------*/
#include "my1cstr.c"
#include "my1cstr_line.c"
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	int test, curr;
	char *pstr, *pbuf;
	my1cstr_t buff;
	cstr_init(&buff);
	int chk1, chk2, chk3;
	while (1) {
		printf("\nmy1chtest> ");
		test = cstr_read_line(&buff,stdin);
		if (!test) break;
		chk1 = chk2 = chk3 = 0; /* () {} [] */
		pstr = buff.buff; curr = 0;
		while (pstr[curr]) {
			if (is_spacing(pstr[curr])) {
				curr++;
				continue;
			}
			if ((test=chtest_cname(&pstr[curr]))>0) {
				pbuf = chtest_tokenize(&pstr[curr],test);
				if (!pbuf) {
					printf("** malloc error? (%c|%d)\n",pstr[curr],test);
					break;
				}
				printf("## cName:{%s}\n",pbuf);
				free((void*)pbuf);
				curr += test;
			}
			else if ((test=chtest_number(&pstr[curr]))>0) {
				pbuf = chtest_tokenize(&pstr[curr],test);
				if (!pbuf) {
					printf("** malloc error? (%c|%d)\n",pstr[curr],test);
					break;
				}
				printf("## Number:{%s}\n",pbuf);
				free((void*)pbuf);
				curr += test;
			}
			else if ((test=chtest_cstring(&pstr[curr]))>0) {
				pbuf = chtest_tokenize(&pstr[curr],test);
				if (!pbuf) {
					printf("** malloc error? (%c|%d)\n",pstr[curr],test);
					break;
				}
				printf("## cString:{%s}\n",pbuf);
				free((void*)pbuf);
				curr += test;
			}
			else {
				if (pstr[curr]=='(')
					printf("@@ '(' Level:%d\n",++chk1);
				else if (pstr[curr]==')')
					printf("@@ '(' Level:%d\n",--chk1);
				else if (pstr[curr]=='{')
					printf("@@ '{' Level:%d\n",++chk2);
				else if (pstr[curr]=='}')
					printf("@@ '{' Level:%d\n",--chk2);
				else if (pstr[curr]=='[')
					printf("@@ '[' Level:%d\n",++chk3);
				else if (pstr[curr]==']')
					printf("@@ '[' Level:%d\n",--chk3);
				else if (chtest_delim(pstr[curr],CHTEST_OPRS))
					printf("@@ Operation:{%c}\n",pstr[curr]);
				else if (pstr[curr]=='=')
					printf("@@ Assignment:{%c}\n",pstr[curr]);
				else 
					printf("** Stray:{%c}\n",pstr[curr]);
				curr++;
			}
		}
		if (chk1) printf("** Imbalanced [] (%d)\n",chk1);
		if (chk2) printf("** Imbalanced {} (%d)\n",chk2);
		if (chk3) printf("** Imbalanced [] (%d)\n",chk3);
	}
	cstr_free(&buff);
	printf("\n");
	return 0;
}
/*----------------------------------------------------------------------------*/
#endif
/*----------------------------------------------------------------------------*/
