/*----------------------------------------------------------------------------*/
#ifndef __MY1SOCKBASE_H__
#define __MY1SOCKBASE_H__
/*----------------------------------------------------------------------------*/
/**
 *  my1sock socket interface library
 *  - written by azman@my1matrix.org
 *  - requires linking to ws2_32 on win32
 */
/*----------------------------------------------------------------------------*/
#define SOCKET_OK 0
#define SOCKET_ERROR (~(~0U>>1))
/*----------------------------------------------------------------------------*/
#define ESOCK_GENERAL (SOCKET_ERROR|0x0001)
#define ESOCK_RESERVE (SOCKET_ERROR|0x0002)
#define ESOCK_CREATE  (SOCKET_ERROR|0x0004)
#define ESOCK_CLOSE   (SOCKET_ERROR|0x0008)
#define ESOCK_BIND    (SOCKET_ERROR|0x0010)
#define ESOCK_CONNECT (SOCKET_ERROR|0x0020)
#define ESOCK_LISTEN  (SOCKET_ERROR|0x0040)
#define ESOCK_ACCEPT  (SOCKET_ERROR|0x0080)
#ifdef DO_MINGW
#define ESOCK_WSASTART (SOCKET_ERROR|0x1000)
#define ESOCK_WSASOCK2 (SOCKET_ERROR|0x2000)
#endif
/*----------------------------------------------------------------------------*/
#ifdef DO_MINGW
#include <windows.h>
#ifdef _WIN32_WINNT
#undef _WIN32_WINNT
#define _WIN32_WINNT 0x0501
#endif
#include <winsock2.h>
#else
/*struct sockaddr_in*/
#include <netinet/in.h>
#define INVALID_SOCKET -1
#endif
/*----------------------------------------------------------------------------*/
typedef struct _my1sock_t {
	int sock, temp, stat;
	unsigned int flag;
	void* data;
	struct sockaddr_in addr;
} my1sock_t;
/*----------------------------------------------------------------------------*/
int sock_init(my1sock_t*);
int sock_free(my1sock_t*);
/*----------------------------------------------------------------------------*/
int sock_create(my1sock_t*); /* create socket object */
int sock_server(my1sock_t*,int); /* start listening socket */
int sock_client(my1sock_t*); /* start connection socket */
int sock_finish(my1sock_t*,int); /* done with socket object */
int sock_done(int sock,int shut);
int sock_send(int sock,int size,void* buff);
int sock_read(int sock,int size,void* buff);
int sock_peek(int sock,int size,void* buff);
/*----------------------------------------------------------------------------*/
#define HOST_CHARSIZE 64
#define IPV4_CHARSIZE 16
#define PORT_CHARSIZE 8
#define BUFF_CHARSIZE 1024
/*----------------------------------------------------------------------------*/
typedef struct _my1conn_t {
	int sock, alen;
	struct sockaddr_in icli;
	struct in_addr iadd;
	char addr[IPV4_CHARSIZE];
	void* data; /* connection data */
} my1conn_t;
/*----------------------------------------------------------------------------*/
int sock_accept(my1sock_t*,my1conn_t*); /* accept/serve incoming */
/*----------------------------------------------------------------------------*/
#define HOST_FLAG_NAME_MASK 0xFF
#define HOST_FLAG_NAME_VALID 0x08
#define HOST_FLAG_NAME_ERROR 0x80
#define HOST_FLAG_NAME_ERR01 (HOST_FLAG_NAME_ERROR|0x10)
#define HOST_FLAG_NAME_ERR02 (HOST_FLAG_NAME_ERROR|0x20)
#define HOST_FLAG_NAME_ERR04 (HOST_FLAG_NAME_ERROR|0x40)
/*----------------------------------------------------------------------------*/
#define HOST_DEFAULT_PORT 1337
/*----------------------------------------------------------------------------*/
typedef struct _my1host_t {
	unsigned int flag;
	int port;
	char *pstr, *ipv4;
	struct in_addr iadd;
	char name[HOST_CHARSIZE];
	char addr[IPV4_CHARSIZE];
} my1host_t;
/*----------------------------------------------------------------------------*/
#define HOST(ph) ((my1host_t*)ph)
#define host_port(ph,pt) HOST(ph)->port = pt
#define host_null(ph) HOST(ph)->addr[0] = 0x0; HOST(ph)->name[0] = 0x0
#define host_prep(ph) host_null(ph); HOST(ph)->flag &= ~HOST_FLAG_NAME_MASK
#define host_ipv4(ph,vv) HOST(ph)->pstr = 0x0; HOST(ph)->ipv4 = vv
#define host_pstr(ph,ss) HOST(ph)->ipv4 = 0x0; HOST(ph)->pstr = ss
#define host_init(ph) host_null(ph); HOST(ph)->flag = 0
/*----------------------------------------------------------------------------*/
void host_from_name(my1host_t* host, char *name, int port);
void host_from_ip(my1host_t* host, char *ip, int port);
/*----------------------------------------------------------------------------*/
#endif /** __MY1SOCKBASE_H__ */
/*----------------------------------------------------------------------------*/
