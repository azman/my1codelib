/*----------------------------------------------------------------------------*/
#include "my1shell.h"
#include "my1cstr_util.h"
#include "my1cstr_line.h"
/*----------------------------------------------------------------------------*/
#include <stdlib.h>
#include <string.h>
/*----------------------------------------------------------------------------*/
#define ARGS_BUFFSIZE 80
/*----------------------------------------------------------------------------*/
void shell_init(my1shell_t* that, char* name) {
	cmds_init(&that->cmds);
	vars_init(&that->vars);
	cstr_init(&that->buff);
	cstr_init(&that->name);
	cstr_init(&that->arg0);
	cstr_init(&that->args);
	if (name) cstr_assign(&that->name,name);
	cstr_resize(&that->arg0,ARGS_BUFFSIZE);
	cstr_resize(&that->args,ARGS_BUFFSIZE);
	that->pvar = 0x0;
	that->pdef = 0x0;
	that->pcmd = 0x0;
	that->done = 0x0;
	that->show = 0x0;
	that->post = 0x0;
	that->pbuf = 0x0;
	that->flag = 0; /* multi-purpose flag :p */
	/* always have an exit command */
	shell_task(that,"exit","Exit Shell",CMDFLAG_EXIT,0x0);
	shell_task(that,"quit",0x0,CMDFLAG_EXIT,0x0);
}
/*----------------------------------------------------------------------------*/
void shell_free(my1shell_t* that) {
	cstr_free(&that->args);
	cstr_free(&that->arg0);
	cstr_free(&that->name);
	cstr_free(&that->buff);
	vars_free(&that->vars);
	cmds_free(&that->cmds);
}
/*----------------------------------------------------------------------------*/
void shell_wait(my1shell_t* that) {
	that->flag &= ~SHELL_FLAG_SKIP;
	that->flag &= ~SHELL_FLAG_WORD;
	printf("\n%s> ",that->name.buff);
	if (!cstr_read_line(&that->buff,stdin))
		that->flag |= SHELL_FLAG_SKIP;
	else {
		cstr_trimws(&that->buff,0); /* MY1CSTR_TRIM_NOREPEAT? */
		if (!that->buff.fill||!that->buff.buff)
			that->flag |= SHELL_FLAG_SKIP;
		else
			that->pbuf = that->buff.buff;
	}
}
/*----------------------------------------------------------------------------*/
void shell_exec(my1shell_t* that) {
	int test = cstr_findwd(&that->buff," ");
	if (test<0) {
		that->flag |= SHELL_FLAG_WORD;
		cstr_null(&that->args);
	}
	else {
		cstr_substr(&that->args,&that->buff,test+1,0);
		cstr_trimws(&that->args,0);
	}
	cstr_substr(&that->arg0,&that->buff,0,test);
	/* start processing */
	that->pcmd = cmds_find(&that->cmds,that->arg0.buff);
	if (!that->pcmd) return;
	else if (that->pcmd->flag&CMDFLAG_DONE)
		that->flag |= SHELL_FLAG_DONE;
	else if (that->pcmd->flag&CMDFLAG_TASK) {
		if (that->pcmd->exec)
			that->temp = that->pcmd->exec(that);
		that->flag |= SHELL_FLAG_SKIP;
	}
}
/*----------------------------------------------------------------------------*/
void shell_post(my1shell_t* that) {
	/* check if show_data requested */
	if (that->show&&(that->flag&SHELL_FLAG_WORD)) {
		that->pvar = shell_find_data(that,that->pbuf);
		if (that->pvar) {
			that->show(that->pvar);
			return;
		}
	}
	if (that->post) that->post(that);
}
/*----------------------------------------------------------------------------*/
my1cmd_t* shell_task(my1shell_t* that, char* name, char* info,
		word32_t flag, ptask_t task) {
	if (!var_name_valid8(name)) return 0x0;
	if (cmds_find(&that->cmds,name)) return 0x0;
	return cmds_make(&that->cmds,name,info,flag,task);
}
/*----------------------------------------------------------------------------*/
my1var_t* shell_data(my1shell_t* that, char* name, pdata_t data, pfunc_t done) {
	if (!var_name_valid8(name)) return 0x0;
	if (vars_find(&that->vars,name)) return 0x0;
	return vars_make(&that->vars,name,data,done);
}
/*----------------------------------------------------------------------------*/
my1cmd_t* shell_find_task(my1shell_t* that, char* name) {
	if (!var_name_valid8(name)) return 0x0;
	return cmds_find(&that->cmds,name);
}
/*----------------------------------------------------------------------------*/
my1var_t* shell_find_data(my1shell_t* that, char* name) {
	if (!var_name_valid8(name)) return 0x0;
	return vars_find(&that->vars,name);
}
/*----------------------------------------------------------------------------*/
my1var_t* shell_remove_data(my1shell_t* that, char* name) {
	if (!var_name_valid8(name)) return 0x0;
	return vars_hack(&that->vars,name);
}
/*----------------------------------------------------------------------------*/
int shell_list_vars(my1shell_t* that) {
	my1list_t *list = &that->vars.list;
	printf("\n-- Variables(s):\n\n");
	list_scan_prep(list);
	while (list_scan_item(list)) {
		my1var_t* pvar = (my1var_t*) list->curr->data;
		printf("{%s} ", pvar->name.buff);
	}
	printf("\n\nEnd of list.\n");
	return 0;
}
/*----------------------------------------------------------------------------*/
int shell_list_cmds(my1shell_t* that) {
	my1cmd_t* pcmd;
	pcmd = that->cmds.list;
	that->cmds.loop = 0;
	printf("\n-- Command(s):\n\n");
	while (that->cmds.loop<that->cmds.fill) {
		printf("   %s - %s\n",pcmd->name,pcmd->info);
		that->cmds.loop++;
		pcmd++;
	}
	printf("\nEnd of list.\n");
	return 0;
}
/*----------------------------------------------------------------------------*/
#if defined(TEST_MODULE) && defined(TEST_SHELL)
/*----------------------------------------------------------------------------*/
#include "my1cmds.c"
#include "my1vars.c"
#include "my1list.c"
#include "my1cstr.c"
#include "my1cstr_line.c"
#include "my1cstr_util.c"
/*----------------------------------------------------------------------------*/
my1cstr_t* test_make_data(char* what) {
	my1cstr_t *pstr = (my1cstr_t*) malloc(sizeof(my1cstr_t));
	if (pstr) {
		cstr_init(pstr);
		if (what&&strlen(what)>0)
			cstr_setstr(pstr,what);
		else {
			cstr_resize(pstr,2); /* minimum to create storage */
			pstr->buff[0] = 0x0;
		}
	}
	return pstr;
}
/*----------------------------------------------------------------------------*/
void test_free_data(my1cstr_t* pstr) {
	cstr_free(pstr);
	free((void*)pstr);
}
/*----------------------------------------------------------------------------*/
void show_data(my1var_t* pvar) {
	my1cstr_t *pstr = (my1cstr_t*) pvar->data;
	printf("## {%s} = %s\n",pvar->name.buff,pstr->buff);
}
/*----------------------------------------------------------------------------*/
int shell_make_data(my1shell_t* that) {
	my1var_t *pvar;
	char *pstr;
	my1cstr_t *pdat, arg1;
	cstr_init(&arg1);
	do {
		/* that->arg0 should be 'make' */
		if (!that->args.fill) {
			printf("** No name for making var!\n");
			break;
		}
		cstr_shword(&that->args,&arg1,STRING_DELIM);
		pstr = arg1.buff;
		if (!var_name_valid8(pstr)) {
			printf("** Invalid var name '%s'!\n",pstr);
			break;
		}
		if (shell_find_data(that,pstr)) {
			printf("** Var {%s} exists!\n",pstr);
			break;
		}
		pdat = test_make_data(that->args.buff);
		if (!pdat) {
			printf("** Failed to make data for '%s'!\n",pstr);
			break;
		}
		pvar = shell_data(that,pstr,pdat,test_free_data);
		if (!pvar) {
			printf("** Failed to make var '%s'!\n",pstr);
			break;
		}
		printf("-- Made {%s}",pvar->name.buff);
		if (pdat->fill>0)
			printf(" <= %s",pdat->buff);
		printf("\n");
	} while (0);
	cstr_free(&arg1);
	return 0;
}
/*----------------------------------------------------------------------------*/
int shell_copy_data(my1shell_t* that) {
	my1var_t *pvar, *pvr2;
	char *pstr;
	my1cstr_t arg1;
	cstr_init(&arg1);
	do {
		if (!that->args.fill) {
			printf("** No copy destination!\n");
			break;
		}
		cstr_shword(&that->args,&arg1,STRING_DELIM);
		pstr = arg1.buff;
		if (!var_name_valid8(pstr)) {
			printf("** Invalid var name '%s'!\n",pstr);
			break;
		}
		if (!(pvar=shell_find_data(that,pstr))) {
			printf("** Var {%s} not found!\n",pstr);
			break;
		}
		if (!cstr_shword(&that->args,&arg1,STRING_DELIM)) {
			printf("** No copy source for '%s'!\n",pvar->name.buff);
			break;
		}
		pstr = arg1.buff;
		if (!var_name_valid8(pstr)) {
			printf("** Invalid var2 name '%s'!\n",pstr);
			break;
		}
		if (!(pvr2=shell_find_data(that,pstr))) {
			printf("** Var {%s} not found!\n",pstr);
			break;
		}
		cstr_copy(pvar->data,pvr2->data);
		printf("-- {%s} <= {%s}\n",pvar->name.buff,pvr2->name.buff);
	} while (0);
	cstr_free(&arg1);
	return 0;
}
/*----------------------------------------------------------------------------*/
int shell_sets_data(my1shell_t* that) {
	my1var_t *pvar;
	char *pstr;
	my1cstr_t arg1;
	cstr_init(&arg1);
	do {
		if (!that->args.fill) {
			printf("** No var to set!\n");
			break;
		}
		cstr_shword(&that->args,&arg1,STRING_DELIM);
		pstr = arg1.buff;
		if (!var_name_valid8(pstr)) {
			printf("** Invalid var name '%s'!\n",pstr);
			break;
		}
		if (!(pvar=shell_find_data(that,pstr))) {
			printf("** Var {%s} not found!\n",pstr);
			break;
		}
		if (!that->args.fill) {
			printf("** No value to set to {%s}!\n",pstr);
			break;
		}
		cstr_setstr(pvar->data,that->args.buff);
		printf("-- {%s} <= %s\n",pvar->name.buff,that->args.buff);
	} while (0);
	cstr_free(&arg1);
	return 0;
}
/*----------------------------------------------------------------------------*/
void post_exec(my1shell_t* that) {
	printf("** Unknown command '%s' (%s)\n",that->arg0.buff,that->buff.buff);
}
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	my1shell_t that;
	shell_init(&that,"my1shell");
	shell_task(&that,"list","List Variables",CMDFLAG_TASK,shell_list_vars);
	shell_task(&that,"help","List Commands",CMDFLAG_TASK,shell_list_cmds);
	shell_task(&that,"make","Make Variable (e.g. make <name> [text])",
		CMDFLAG_TASK,shell_make_data);
	shell_task(&that,"copy","Copy Variable (e.g. copy <vdst> <vsrc>)",
		CMDFLAG_TASK,shell_copy_data);
	shell_task(&that,"sets","Sets Variable (e.g. sets <var> <text>)",
		CMDFLAG_TASK,shell_sets_data);
	that.show = show_data;
	that.post = post_exec;
	while (1) {
		shell_wait(&that);
		if (that.flag&SHELL_FLAG_SKIP) continue;
		shell_exec(&that);
		if (that.flag&SHELL_FLAG_DONE) break;
		if (that.flag&SHELL_FLAG_SKIP) continue;
		shell_post(&that);
	}
	shell_free(&that);
	printf("\nBye!\n\n");
	return 0;
}
/*----------------------------------------------------------------------------*/
#endif
/*----------------------------------------------------------------------------*/
