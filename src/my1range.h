/*----------------------------------------------------------------------------*/
#ifndef __MY1RANGE_H__
#define __MY1RANGE_H__
/*----------------------------------------------------------------------------*/
/**
 *  my1range is an array of real values (double)
 *  - written by azman@my1matrix.org
 */
/*----------------------------------------------------------------------------*/
typedef struct  _my1range_t {
	double* data;
	int size, fill;
} my1range_t;
/*----------------------------------------------------------------------------*/
void range_init(my1range_t* that);
void range_free(my1range_t* that);
void range_make(my1range_t* that, int size);
void range_fill(my1range_t* that, double fill);
void range_slip(my1range_t* that, double data); /* adds 1 data to array */
void range_more(my1range_t* that, double* psrc, int size); /* adds >1 byte */
void range_copy(my1range_t* that, my1range_t* from);
/*----------------------------------------------------------------------------*/
void range_offset(my1range_t* that, double offs); /* add to all */
void range_scaled(my1range_t* that, double sval); /* mul to all */
/*----------------------------------------------------------------------------*/
/* basic math - 'from' must be same size (fill) as 'that' */
void range_add(my1range_t* that, my1range_t* from);
void range_mul(my1range_t* that, my1range_t* from);
/*----------------------------------------------------------------------------*/
#endif
/*----------------------------------------------------------------------------*/
