/*----------------------------------------------------------------------------*/
#ifndef __MY1CSTR_H__
#define __MY1CSTR_H__
/*----------------------------------------------------------------------------*/
/**
 *  my1cstr_t : basic string implementation for c
 *  - written by azman@my1matrix.org
 *  - only provide basic functions for c string management
**/
/*----------------------------------------------------------------------------*/
typedef struct _my1cstr_t {
	char *buff;
	int size; /* buff size - NOT string length! */
	int fill; /* this is strlen! */
} my1cstr_t;
/*----------------------------------------------------------------------------*/
void cstr_init(my1cstr_t*);
void cstr_free(my1cstr_t*);
void cstr_null(my1cstr_t*);
char* cstr_substr(my1cstr_t*,my1cstr_t* from,int offs,int size);
char* cstr_resize(my1cstr_t*,int);
char* cstr_assign(my1cstr_t*,char*); /* exact size */
char* cstr_setstr(my1cstr_t*,char*); /* expand only */
char* cstr_append(my1cstr_t*,char*);
char* cstr_prefix(my1cstr_t*,char*);
char* cstr_single(my1cstr_t*,char); /* single char append */
char cstr_rmtail(my1cstr_t*); /* remove last char */
/*----------------------------------------------------------------------------*/
/** useful macros */
#define CSTR(ps) ((my1cstr_t*)ps)
#define cstr_copy(pd,ps) cstr_substr(CSTR(pd),CSTR(ps),0,0)
/*----------------------------------------------------------------------------*/
#endif /** __MY1CSTR_H__ */
/*----------------------------------------------------------------------------*/
