/*----------------------------------------------------------------------------*/
#ifndef __MY1CONF_H__
#define __MY1CONF_H__
/*----------------------------------------------------------------------------*/
/**
 *  my1conf - simple config file interface
 *  - written by azman@my1matrix.org
 *  - requires my1text/my1cstr_util
 */
/*----------------------------------------------------------------------------*/
#include "my1cstr.h"
/*----------------------------------------------------------------------------*/
#define MY1CONF_VALID 0
#define MY1CONF_NOFILE -1
#define MY1CONF_INVALID -2
#define MY1CONF_NOTFOUND -3
/*----------------------------------------------------------------------------*/
int conf_validate(char* name);
int conf_find_param(char* name, char* atag, char* akey, my1cstr_t* find);
/*----------------------------------------------------------------------------*/
#endif /** __MY1CONF_H__ */
/*----------------------------------------------------------------------------*/
