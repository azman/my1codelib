/*----------------------------------------------------------------------------*/
#ifndef __MY1COMPLEX_H__
#define __MY1COMPLEX_H__
/*----------------------------------------------------------------------------*/
/**
 *  my1complex_t is a structure for complex number
 *  - as an alternative to complex.h
**/
/*----------------------------------------------------------------------------*/
#define DO_PRECISION_01N 0.0000000010
#define DO_PRECISION_p1N 0.0000000001
/* allow user defined precision */
#ifndef DO_PRECISION
#define DO_PRECISION DO_PRECISION_01N
#endif
/*----------------------------------------------------------------------------*/
#define zero_threshold(data,prec) if(fabs(data)<prec)data=0.0
/*----------------------------------------------------------------------------*/
typedef struct _my1complex_t {
	double real, imag;
} my1complex_t;
/*----------------------------------------------------------------------------*/
void complex_zero(my1complex_t*);
void complex_real(my1complex_t*,double);
void complex_imag(my1complex_t*,double);
void complex_full(my1complex_t*,double,double);
void complex_zero_threshold(my1complex_t*,double);
void complex_zero_check(my1complex_t*);
void complex_conjugate(my1complex_t*);
double complex_magnitude(my1complex_t*);
double complex_phase(my1complex_t*);
void complex_from_exponent(my1complex_t*,double);
void complex_make_polar(my1complex_t*);
void complex_make_cartesian(my1complex_t*);
void complex_math_add(my1complex_t*,my1complex_t*,my1complex_t*);
void complex_math_sub(my1complex_t*,my1complex_t*,my1complex_t*);
void complex_math_mul(my1complex_t*,my1complex_t*,my1complex_t*);
void complex_math_div(my1complex_t*,my1complex_t*,my1complex_t*);
/*----------------------------------------------------------------------------*/
#endif /** __MY1COMPLEX_H__ */
/*----------------------------------------------------------------------------*/
