/*----------------------------------------------------------------------------*/
#include "my1text.h"
/*----------------------------------------------------------------------------*/
#include <stdlib.h>
#include <string.h>
/*----------------------------------------------------------------------------*/
void text_init(my1text_t* ptext) {
	ptext->fname[0] = 0x0;
	ptext->iline = LINE_NONE;
	ptext->ichar = CHAR_NONE;
	ptext->pfile = 0x0;
	cstr_init(&ptext->pbuff);
}
/*----------------------------------------------------------------------------*/
void text_free(my1text_t* ptext) {
	if (ptext->pfile) {
		fclose(ptext->pfile);
		ptext->pfile = 0x0;
	}
	cstr_free(&ptext->pbuff);
}
/*----------------------------------------------------------------------------*/
void text_open(my1text_t* ptext, char* pname) {
	if (!ptext->pfile) {
		strncpy(ptext->fname,pname,TEXT_FILENAME_SIZE);
		ptext->pfile = fopen((const char*)&ptext->fname,"rt");
		if (ptext->pfile)
			ptext->iline = LINE_INIT;
	}
}
/*----------------------------------------------------------------------------*/
void text_done(my1text_t* ptext) {
	if (ptext->pfile) {
		fclose(ptext->pfile);
		ptext->pfile = 0x0;
		ptext->iline = LINE_DONE;
	}
}
/*----------------------------------------------------------------------------*/
int text_read(my1text_t* ptext) {
	int count = cstr_read_line(&ptext->pbuff,ptext->pfile);
	if (count<0) ptext->ichar = CHAR_DONE;
	else {
		ptext->ichar = count; /** CHAR_INIT */
		ptext->iline++;
	}
	return ptext->ichar;
}
/*----------------------------------------------------------------------------*/
#if defined(TEST_MODULE) && defined(TEST_TEXT)
/*----------------------------------------------------------------------------*/
#include "my1cstr.c"
#include "my1cstr_line.c"
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	my1text_t text;
	text_init(&text);
	text_open(&text,argc==2?argv[1]:"test.txt");
	if (text.pfile) {
		while (text_read(&text)>=CHAR_INIT) {
			printf("[Line%03d] %s\n",text.iline,
				text.pbuff.buff?text.pbuff.buff:"");
		}
		text_done(&text);
	}
	else printf("** Cannot open file '%s'!\n",text.fname);
	text_free(&text);
	return 0;
}
/*----------------------------------------------------------------------------*/
#endif
/*----------------------------------------------------------------------------*/
