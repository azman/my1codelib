/*----------------------------------------------------------------------------*/
#include "my1vect0r_ft.h"
/*----------------------------------------------------------------------------*/
#include <math.h>
/*----------------------------------------------------------------------------*/
void vector_freq_shift(my1vector_t* vec) {
	complex_t swap;
	int loop, offs, size;
	offs = size = vec->fill;
	if (offs%2) offs++;
	offs >>= 1; size >>= 1;
	for (loop=0;loop<size;loop++) {
		swap = vec->data[loop];
		vec->data[loop] = vec->data[loop+offs];
		vec->data[loop+offs] = swap;
	}
}
/*----------------------------------------------------------------------------*/
void vector_freq_window(my1vector_t* vec, int size) {
	int loop, step, save;
	save = vec->fill; /* should we get nyquist at save/2? */
	vec->fill = size; /* size>0 && size<save/2 */
	for (loop=0,step=save-size;loop<size;loop++,step++)
		vec->data[vec->fill++] = vec->data[step];
}
/*----------------------------------------------------------------------------*/
void vector_freq_insert(my1vector_t* vec, int size) {
	int loop, step, iter, full;
	full = vec->fill + size; /* size>0 && size%2==0 */
	if (full<vec->size)
		vector_make(vec,full);
	/* shift -ve freq to make way */
	size >>= 1; iter = vec->fill-1; step = full-1;
	for (loop=0;loop<size;loop++,iter--,step--)
		vec->data[step] = vec->data[iter];
	/* zero out inserted spaces */
	size <<= 1;
	for (loop=0;loop<size;loop++)
		complex_zero(&vec->data[iter++]);
	vec->fill = full;
}
/*----------------------------------------------------------------------------*/
void vector_dft_base(my1vector_t* vec, my1vector_t* out, int opts) {
	int loop, next, icnt, ocnt, ochk;
	double step, that, term;
#ifndef USE_TRIGFUNCTION
	complex_t temp, dmul;
#else
	double temp;
#endif
	/* check params: output vector must be set to desired size */
	icnt = vec->fill;
	ocnt = out->size;
	/*  */
	/* phase step in that */
	step = 2*CONSTANT_PI_/icnt;
	that = 0.0;
	if (opts&DFT_EXP_NEGATIVE) step = -step;
	ochk = (opts&DFT_CALC_FRQBAND)?(ocnt>>1):-1;
	/* for each output term */
	for (loop=0;loop<ocnt;loop++) {
		if (loop==ochk) {
			/* opts&DFT_CALC_FRQBAND */
			/* halfway: start to calculate -ve freq */
			that = (double)(icnt-loop)*step; /* UNDER REVIEW! */
		}
		out->data[loop].real = 0.0;
		out->data[loop].imag = 0.0;
		term = 0.0;
		for (next=0;next<icnt;next++) {
			/* accumulate input terms */
#ifndef USE_TRIGFUNCTION
			/*complex_from_exponent(&temp,-2*CONSTANT_PI_*loop*next/icnt);*/
			complex_from_exponent(&temp,term);
			complex_math_mul(&dmul,&vec->data[next],&temp);
			complex_math_add(&out->data[loop],&out->data[loop],&dmul);
#else
			/* cosine term */
			temp = cos(term);
			out->data[loop].real += vec->data[next].real*temp;
			out->data[loop].imag += vec->data[next].imag*temp;
			/* sine term */
			temp = sin(term);
			out->data[loop].real -= vec->data[next].imag*temp;
			out->data[loop].imag += vec->data[next].real*temp;
			/* next phase */
#endif
			term += that;
		}
		if (opts&DFT_EXEC_AVERAGE) {
			out->data[loop].real /= icnt;
			out->data[loop].imag /= icnt;
		}
		zero_threshold(out->data[loop].real,DFT_PRECISION);
		zero_threshold(out->data[loop].imag,DFT_PRECISION);
		that += step;
	}
	out->fill = loop;
}
/*----------------------------------------------------------------------------*/
int bit_reverse(int that, int step) {
	int test = 0, mask = step;
	while (mask>0) {
		test >>= 1;
		if (that&mask)
			test |= step;
		mask >>= 1;
	}
	return test;
}
/*----------------------------------------------------------------------------*/
void vector_fft(my1vector_t* vec, my1vector_t* out) {
	complex_t term, that;
	int loop, temp, wide, next, iter, itop, ibot;
	int test = 0, step = 1, size = vec->fill, half = size >> 1;
	/* find step size - size MUST BE 2^n! */
	while (step<size) { step <<= 1; test++; }
	step >>= 1; /* needed for rearrangement */
	/* prepare output - force same size! */
	vector_make(out,size);
	out->fill = size; /* will fill to size! */
	/* rearrange for butterfly - decimation in time! */
	for (loop=0;loop<size;loop++) {
		temp = bit_reverse(loop,step);
		out->data[temp] = vec->data[loop];
	}
	/* loop through instead of recursive */
	for (loop=0,step=1,temp=size;loop<test;loop++) {
		wide = step; /* butterfly pair width */
		step <<= 1; /* butterfly step */
		temp >>= 1; /* number of butterly pair(s) */
		/* do the butterfly */
		for (next=0,iter=0;next<wide;next++,iter+=temp) {
			complex_from_exponent(&term,-CONSTANT_PI_*iter/half);
			for (itop=next;itop<size;itop+=step) {
				ibot = itop + wide;
				complex_math_mul(&that,&out->data[ibot],&term);
				complex_math_sub(&out->data[ibot],&out->data[itop],&that);
				complex_math_add(&out->data[itop],&out->data[itop],&that);
			}
		}
	}
}
/*----------------------------------------------------------------------------*/
void vector_fft_inv(my1vector_t* vec, my1vector_t* out) {
	complex_t term, that;
	int loop, temp, wide, next, iter, itop, ibot;
	int test = 0, step = 1, size = vec->fill, half = size >> 1;
	while (step<size) { step <<= 1; test++; }
	step >>= 1;
	vector_make(out,size);
	out->fill = size;
	for (loop=0;loop<size;loop++) {
		temp = bit_reverse(loop,step);
		out->data[temp] = vec->data[loop];
	}
	for (loop=0,step=1,temp=size;loop<test;loop++) {
		wide = step;
		step <<= 1;
		temp >>= 1;
		for (next=0,iter=0;next<wide;next++,iter+=temp) {
			/* exactly like fft... but, step 1: make this +ve */
			complex_from_exponent(&term,CONSTANT_PI_*iter/half);
			for (itop=next;itop<size;itop+=step) {
				ibot = itop + wide;
				complex_math_mul(&that,&out->data[ibot],&term);
				complex_math_sub(&out->data[ibot],&out->data[itop],&that);
				complex_math_add(&out->data[itop],&out->data[itop],&that);
			}
		}
	}
	/* exactly like fft... but, step 2: divide by N */
	for (loop=0;loop<size;loop++) {
		out->data[loop].real /= size;
		out->data[loop].imag /= size;
		zero_threshold(out->data[loop].real,FFT_PRECISION);
		zero_threshold(out->data[loop].imag,FFT_PRECISION);
	}
}
/*----------------------------------------------------------------------------*/
#if defined(TEST_MODULE) && defined(TEST_VECT0R_FT)
/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include <sys/time.h>
/*----------------------------------------------------------------------------*/
#include "my1complex1.c"
#include "my1vect0r.c"
/*----------------------------------------------------------------------------*/
void print_complex(complex_t* num) {
	if (!num->imag) printf("%g",num->real);
	else printf("(%g,%gi)[%g]",num->real,num->imag,
		complex_magnitude(num));
}
/*----------------------------------------------------------------------------*/
void vector_show(my1vector_t* vec, char* str) {
	int loop, size = vec->fill;
	printf("%s{%p}[%d/%d]: ",str,vec,size,vec->size);
	for (loop=0;loop<size;loop++) {
		if (loop) printf(", ");
		print_complex(&vec->data[loop]);
	}
	printf("\n");
}
/*----------------------------------------------------------------------------*/
void vector_text(my1vector_t* vec, char* str) {
	int loop, size = vec->fill;
	printf("%s{%p}[%d/%d]:\n",str,vec,size,vec->size);
	for (loop=0;loop<size;loop++) {
		printf("REAL[%d]=%10lf\t",loop,vec->data[loop].real);
		printf("IMAG[%d]=%10lf\t",loop,vec->data[loop].imag);
		printf("ABS[%d]=%10lf\t",loop,complex_magnitude(&vec->data[loop]));
		printf("ARG[%d]=%10lf\n",loop,complex_phase(&vec->data[loop]));
	}
}
/*----------------------------------------------------------------------------*/
#define DATASIZE 8
/*----------------------------------------------------------------------------*/
void vector_real_sequence(my1vector_t* vec, int fill, int offs, int step) {
	int loop, next, stop;
	vec->fill = 0; stop = fill*step;
	for (loop=0,next=offs;loop<vec->size;loop++,next+=step) {
		if (next>stop) next -= stop;
		vector_append(vec,loop<fill?(double)next:(double)0,(double)0);
	}
}
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	int loop;
	my1vector_t test, buff, next;
	struct timeval init, stop, diff;
	/* print tool info */
	printf("\nTest program for my1vector_ft extension module\n");
	printf("  => by azman@my1matrix.org\n\n");
	/* initialize */
	vector_init(&test);
	vector_init(&buff);
	vector_init(&next);
	vector_make(&test,DATASIZE);
	/* prepare input test values */
	vector_real_sequence(&test,4,1,1);
	/* show it! */
	vector_show(&test,"DAT");
	printf("\n");
	/* run dft */
	vector_dft(&test,&buff);
	vector_show(&buff,"DFT");
	/* run fft */
	vector_fft(&test,&buff);
	vector_show(&buff,"FFT");
	printf("\n");
	/* run dft_inv */
	vector_dft_inv(&buff,&next);
	vector_show(&next,"INV");
	/* run fft_inv */
	vector_fft_inv(&buff,&next);
	vector_show(&next,"INV");
	printf("\n");
	/* tests */
	vector_fill(&test,0.0,0.0);
	test.data[0].real = (double)1;
	vector_fft(&test,&buff);
	vector_text(&test,"TEST1");
	vector_text(&buff,"BUFF1");
	test.data[0].real = (double)0;
	test.data[1].real = (double)1;
	vector_fft(&test,&buff);
	vector_text(&test,"TEST2");
	vector_text(&buff,"BUFF1");
	printf("\n");
	/* special test */
	for (loop=0;loop<8;loop++)
	{
		vector_real_sequence(&test,8,(loop+1),2);
		vector_show(&test,"DAT");
		vector_dft(&test,&buff);
		vector_norm(&buff,1);
		vector_show(&buff,"DFT-NORM");
	}
	printf("\n");
	vector_freq_window(&buff,2);
	vector_show(&buff,"WIN");
	printf("\n");
	/* check exec time */
	gettimeofday(&init,0x0);
	for (loop=0;loop<1024;loop++)
		vector_dft(&test,&buff);
	gettimeofday(&stop,0x0);
	timersub(&stop,&init,&diff);
	printf("-- Diff(DFT):<%ld.%06ld>\n",
		(long int)(diff.tv_sec),(long int)(diff.tv_usec));
	gettimeofday(&init,0x0);
	for (loop=0;loop<1024;loop++)
		vector_fft(&test,&buff);
	gettimeofday(&stop,0x0);
	timersub(&stop,&init,&diff);
	printf("-- Diff(FFT):<%ld.%06ld>\n",
		(long int)(diff.tv_sec),(long int)(diff.tv_usec));
	/* release resource */
	vector_free(&next);
	vector_free(&test);
	vector_free(&buff);
	/* done! */
	printf("\n");
	return 0;
}
/*----------------------------------------------------------------------------*/
#endif
/*----------------------------------------------------------------------------*/
