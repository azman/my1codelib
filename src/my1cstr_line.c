/*----------------------------------------------------------------------------*/
#include "my1cstr_line.h"
/*----------------------------------------------------------------------------*/
int cstr_read_line(my1cstr_t* line, FILE *file) {
	char buff[MAX_LINES_CHAR];
	int done, test, size;
	done = 0; test = 0;
	/* clear buffer if reusing */
	cstr_null(line);
	/* in case a long line! */
	while (!done) {
		/* try to read a line, limited buffer */
		if (!fgets((char*)&buff, MAX_LINES_CHAR, file)) {
			test = -1;
			break;
		}
		/* no problem, got the string - count it! */
		size = 0;
		while (buff[size]) {
			/* strip newline! */
			if (buff[size]=='\n'||buff[size]=='\r')  {
				buff[size] = 0x0;
				done = 1;
				break;
			}
			size++;
		}
		/* did we get anything? */
		if (size>0) {
			cstr_append(line,buff);
			test += size; /* return value is char count only! */
			//if (feof(file)) done = 1; /* done if EOF */
		}
	}
	return test;
}
/*----------------------------------------------------------------------------*/
#if defined(TEST_MODULE) && defined(TEST_CSTR_LINE)
/*----------------------------------------------------------------------------*/
#include "my1cstr.c"
#include <stdio.h>
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	int stat, test, step;
	my1cstr_t buff;
	FILE* fptr;
	cstr_init(&buff);
	do {
		stat = 0; step = 0;
		if (argc<2) {
			printf("** No input file given!\n");
			stat = -1;
			break;
		}
		fptr = fopen(argv[1],"rt");
		if (!fptr) {
			printf("** Cannot open file '%s'!\n",argv[1]);
			stat = -1;
			break;
		}
		printf("@@ Reading '%s'...\n",argv[1]);
		while ((test=cstr_read_line(&buff,fptr))>=0) {
			step++;
			printf("[Line:%3d] '%s' (%d:%d/%d)\n",step,
				buff.buff,test,buff.fill,buff.size);
		}
		fclose(fptr);
		printf("@@ Done.\n");
	} while(0);
	cstr_free(&buff);
	return stat;
}
/*----------------------------------------------------------------------------*/
#endif
/*----------------------------------------------------------------------------*/
