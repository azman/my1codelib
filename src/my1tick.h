/*----------------------------------------------------------------------------*/
#ifndef __MY1TICK_H__
#define __MY1TICK_H__
/*----------------------------------------------------------------------------*/
/**
 *  my1tick - module to allow timer-based code execution
 *  - written by azman@my1matrix.org
 */
/*----------------------------------------------------------------------------*/
#include <signal.h>
#include <time.h>
/*----------------------------------------------------------------------------*/
typedef void (*ttask_t)(int,siginfo_t*,void*);
/*----------------------------------------------------------------------------*/
typedef struct _my1tick_t {
	void* udata; /* user data */
	ttask_t ttask; /* tick action */
	timer_t timer;
} my1tick_t;
/*----------------------------------------------------------------------------*/
int tick_init(my1tick_t* tick);
int tick_free(my1tick_t* tick);
int tick_exec(my1tick_t* tick, int seconds, ttask_t handler);
int tick_once(my1tick_t* tick, int seconds, ttask_t handler);
int tick_stop(my1tick_t* tick);
/*----------------------------------------------------------------------------*/
#endif /** __MY1TICK_H__ */
/*----------------------------------------------------------------------------*/
