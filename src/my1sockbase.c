/*----------------------------------------------------------------------------*/
#include "my1sockbase.h"
/*----------------------------------------------------------------------------*/
#if defined(DO_MINGW) || defined(DO_WINSOCK)
#define EWOULDBLOCK WSAEWOULDBLOCK
int inet_aton(const char *cp, struct in_addr *addr)
{
	addr->s_addr = inet_addr(cp);
	return (addr->s_addr == INADDR_NONE) ? 0 : 1;
}
#else
#include <unistd.h>
#endif
/* free/malloc */
#include <stdlib.h>
/* memcpy */
#include <string.h>
/* fcntl */
#include <fcntl.h>
/* errno */
#include <errno.h>
/* snprintf */
#include <stdio.h>
/*----------------------------------------------------------------------------*/
#ifdef DO_MINGW
#include <ws2tcpip.h>
#else
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#endif
/*----------------------------------------------------------------------------*/
int sock_init(my1sock_t* sock) {
	sock->sock = INVALID_SOCKET;
	sock->flag = SOCKET_OK;
	return sock->flag;
}
/*----------------------------------------------------------------------------*/
int sock_free(my1sock_t* sock) {
#ifdef DO_MINGW
	WSACleanup();
#endif
	return sock->flag;
}
/*----------------------------------------------------------------------------*/
int sock_create(my1sock_t* sock) {
#ifdef DO_MINGW
#define WINSOCK_VERSION 0x0002
	WSADATA cData; /* requesting WinSock2.0 */
	if (WSAStartup(WINSOCK_VERSION, &cData)!=0) {
		sock->flag |= SOCKERR_WSASTART;
		return sock->flag;
	}
	/** display string cData.szDescription? */
	if (cData.wVersion!=WINSOCK_VERSION) {
		/** version: cData.wVersion&0xff . ((cData.wVersion>>8)&0xff */
		WSACleanup();
		sock->flag |= SOCKERR_WSASOCK2;
		return sock->flag;
	}
#endif
	sock->sock = socket(AF_INET, SOCK_STREAM, 0);
	if (sock->sock==INVALID_SOCKET) {
#ifdef DO_MINGW
		WSACleanup();
#endif
		sock->flag |= ESOCK_CREATE;
	}
	return sock->flag;
}
/*----------------------------------------------------------------------------*/
int sock_server(my1sock_t* sock, int lcnt) {
	/* make non-blocking socket */
#ifndef DO_MINGW
	fcntl(sock->sock, F_SETFL,fcntl(sock->sock,F_GETFL,0)|O_NONBLOCK);
#endif
	/* bind socket */
	sock->stat = bind(sock->sock,(struct sockaddr *)&sock->addr,
		sizeof(struct sockaddr_in));
	if (sock->stat<0) {
		sock->flag |= ESOCK_BIND;
		return sock->flag;
	}
	/* start listening socket, lcnt connection queue! */
	if (listen(sock->sock,lcnt)<0)
		sock->flag |= ESOCK_LISTEN;
	return sock->flag;
}
/*----------------------------------------------------------------------------*/
int sock_client(my1sock_t* sock) {
	sock->stat = connect(sock->sock,
		(struct sockaddr *)&sock->addr,sizeof(struct sockaddr_in));
	if (sock->stat<0)
		sock->flag |= ESOCK_CONNECT;
	return sock->flag;
}
/*----------------------------------------------------------------------------*/
int sock_finish(my1sock_t* sock, int shut) {
	sock_done(sock->sock,shut);
	sock->sock = INVALID_SOCKET;
	return sock->flag;
}
/*----------------------------------------------------------------------------*/
int sock_done(int sock,int shut) {
	if (shut) shutdown(sock,2); /* is this ok on win32? */
#ifdef DO_MINGW
	closesocket(sock);
#else
	close(sock);
#endif
	return 0;
}
/*----------------------------------------------------------------------------*/
int sock_send(int sock,int size,void* buff) {
	return send(sock,(const void*)buff,(size_t)size,0);
}
/*----------------------------------------------------------------------------*/
int sock_read(int sock,int size,void* buff) {
	return recv(sock,buff,(size_t)size,0);
}
/*----------------------------------------------------------------------------*/
int sock_peek(int sock,int size,void* buff) {
	int test;
	test = recv(sock,buff,(size_t)size,MSG_DONTWAIT);
	if (test<0) {
		if (errno==EAGAIN||errno==EWOULDBLOCK)
			test = 0;
	}
	return test;
}
/*----------------------------------------------------------------------------*/
int sock_accept(my1sock_t* sock, my1conn_t* conn) {
	/* accept should be non-blocking! */
	conn->alen = sizeof(struct sockaddr);
	conn->sock = accept(sock->sock,
		(struct sockaddr*)&conn->icli,(socklen_t*)&conn->alen);
	if (conn->sock<0) {
		/* continue only if errno is EAGAIN or EWOULDBLOCK? */
		if (errno==EAGAIN||errno==EWOULDBLOCK) return 0;
		else {
			/* socket NOT created - safe to return? */
			sock->flag |= ESOCK_ACCEPT;
			return -1;
		}
	}
	/* get client ipv4 */
	memcpy(&conn->iadd,&conn->icli.sin_addr.s_addr,4);
	strcpy(conn->addr,inet_ntoa(conn->iadd));
	/* caller should close conn->sock */
	return 1;
}
/*----------------------------------------------------------------------------*/
void host_from_name(my1host_t* host, char *name, int port) {
	char buff[PORT_CHARSIZE];
	struct addrinfo *addr, hints;
	addr = 0x0;
	host_pstr(host,name);
	host_port(host,port);
	host_prep(host);
	/* get ip address from hostname */
	snprintf(buff,PORT_CHARSIZE,"%d",port);
	memset(&hints,0,sizeof(struct addrinfo));
	hints.ai_family = AF_INET; /* IPV4 only! */
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE;
	if (!getaddrinfo(host->pstr,buff,&hints,&addr)&&addr) {
		struct sockaddr_in *tadd = (struct sockaddr_in*)addr->ai_addr;
		host->iadd = tadd->sin_addr;
		/* copy valid ipv4 and hostname */
		strncpy(host->addr,inet_ntoa(host->iadd),IPV4_CHARSIZE-1);
		strncpy(host->name,name,HOST_CHARSIZE-1);
		/* just in case */
		host->addr[IPV4_CHARSIZE-1] = 0x0;
		host->name[HOST_CHARSIZE-1] = 0x0;
		host->flag |= HOST_FLAG_NAME_VALID;
	}
	else host->flag |= HOST_FLAG_NAME_ERR01;
	if (addr) freeaddrinfo(addr);
}
/*----------------------------------------------------------------------------*/
void host_from_ip(my1host_t* host, char *ip, int port) {
	struct sockaddr_in addr;
	char hbuf[NI_MAXHOST];
	host_ipv4(host,ip);
	host_port(host,port);
	host_prep(host);
	/* check ip address */
	if (inet_aton(host->ipv4,&host->iadd)) {
		addr.sin_addr = host->iadd;
		strncpy(host->addr,inet_ntoa(host->iadd),IPV4_CHARSIZE-1);
		if (!getnameinfo((struct sockaddr*)&addr,
				sizeof(struct sockaddr_in),hbuf,sizeof(hbuf),
				0,0,NI_NAMEREQD))
			strncpy(host->name,hbuf,HOST_CHARSIZE-1);
		else
			strncpy(host->name,"UnknownHost",HOST_CHARSIZE-1);
		/* just in case */
		host->addr[IPV4_CHARSIZE-1] = 0x0;
		host->name[HOST_CHARSIZE-1] = 0x0;
		host->flag |= HOST_FLAG_NAME_VALID;
	}
	else host->flag |= HOST_FLAG_NAME_ERR02;
}
/*----------------------------------------------------------------------------*/
#if defined(TEST_MODULE) && defined(TEST_SOCKBASE)
/*----------------------------------------------------------------------------*/
#include <time.h>
/*----------------------------------------------------------------------------*/
#include "my1keys.c"
/*----------------------------------------------------------------------------*/
#define BUFFSIZE 1024
#define DEFAULT_PORT 1337
/*----------------------------------------------------------------------------*/
#define FLAG_SOCKDUMP 0x0000
#define FLAG_SOCKREAD 0x0001
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	int loop, port=DEFAULT_PORT;
	int size, test, wait;
	unsigned int flag;
	char buff[BUFFSIZE];
	char hdef[] = "localhost";
	char *pstr = hdef, *ipv4 = 0x0;
	my1sock_t sock;
	my1conn_t ccon;
	my1host_t host;
	my1key_t ikey;
	flag = FLAG_SOCKDUMP; /* server socket by default */
	/* process parameters */
	for (loop=1;loop<argc;loop++) {
		if (argv[loop][0]=='-'&&argv[loop][1]=='-') {
			if (!strcmp(&argv[loop][2],"read")||
					!strcmp(&argv[loop][2],"client"))
				flag |= FLAG_SOCKREAD;
			else if (!strcmp(&argv[loop][2],"dump")||
					!strcmp(&argv[loop][2],"server"))
				flag &= ~FLAG_SOCKREAD;
			else if (!strcmp(&argv[loop][2],"host")) {
				loop++;
				pstr = argv[loop];
				flag |= FLAG_SOCKREAD;
			}
			else if (!strcmp(&argv[loop][2],"ip")) {
				loop++;
				ipv4 = argv[loop];
				flag |= FLAG_SOCKREAD;
			}
			else if (!strcmp(&argv[loop][2],"port")) {
				loop++;
				port = atoi(argv[loop]);
			}
			else {
				printf("** Unknown option '%s'! Aborting!\n",argv[loop]);
				exit(1);
			}
		}
		else {
			printf("** Unknown parameter '%s'! Aborting!\n",argv[loop]);
			exit(1);
		}
	}
	if (flag&FLAG_SOCKREAD) {
		/* check host */
		if (ipv4) host_from_ip(&host,ipv4,port);
		else host_from_name(&host,pstr,port);
		if (host.flag&HOST_FLAG_NAME_VALID)
			printf("@@ Host: %s@%s(%d)\n",host.addr,host.name,host.port);
		else {
			printf("** [%04X] Invalid host (%s|%s){%s|%s}!\n",
				host.flag,pstr,ipv4,host.name,host.addr);
			exit(1);
		}
	}
	else {
		/* prepare random */
		srandom((unsigned int)time(0x0));
	}
	/** dummy loop */
	sock_init(&sock);
	do {
		if (flag&FLAG_SOCKREAD) {
			printf("-- Creating socket\n");
			sock_create(&sock);
			if (sock.sock==INVALID_SOCKET) {
				printf("** Socket create error! (%d)\n",sock.flag);
				break;
			}
			/* set remote host info */
			sock.addr.sin_family = AF_INET;
			sock.addr.sin_port = htons(host.port);
			sock.addr.sin_addr = host.iadd;
			printf("-- Prepare client socket\n");
			sock_client(&sock);
			if (sock.flag&SOCKET_ERROR) {
				printf("** Socket client error (%08x)!\n",sock.flag);
				break;
			}
			printf("-- Reading...\n");
			printf("## Press <ESC> to exit.\n");
			size = BUFFSIZE;
			while (1) {
				ikey = get_keyhit();
				if (ikey==KEY_ESCAPE||ikey=='q'||ikey=='Q') {
					printf("\nExit requested (0x%08X)\n",ikey);
					shutdown(sock.sock,2);
					break;
				}
				test = sock_peek(sock.sock,size,(void*)buff);
				if (test<0) {
					printf("\nSocket Error? (%d)\n",test);
					break;
				}
				else if (test>0) {
					loop = 0;
					while (buff[loop]!='$'&&loop<size-1) loop++;
					if (buff[loop]=='$') buff[loop+1] = 0x0;
					printf("Buff:%s\n",buff);
				}
			}
		}
		else {
			printf("-- Creating socket (Port:%d)\n",port);
			sock_create(&sock);
			if (sock.sock==INVALID_SOCKET) {
				printf("** Socket create error! (%d)\n",sock.flag);
				break;
			}
			/* set self host info */
			sock.addr.sin_family = AF_INET;
			sock.addr.sin_port = htons(port);
			sock.addr.sin_addr.s_addr = htonl(INADDR_ANY);
			printf("-- Prepare server socket\n");
			sock_server(&sock,1);
			if (sock.flag&SOCKET_ERROR) {
				printf("** Socket server error (%08x)!\n",sock.flag);
				break;
			}
			/* prepare client connection socket */
			wait = 1;
			ccon.alen = sizeof(struct sockaddr);
			/* start listening for clients */
			printf("-- Listening at port %d!\n",port);
			printf("## Press <ESC> to exit.\n");
			while (1) {
				ikey = get_keyhit();
				if (ikey==KEY_ESCAPE||ikey=='q'||ikey=='Q') {
					printf("\n@@ Exit requested (0x%08X)\n",ikey);
					break;
				}
				if (wait) {
					test = sock_accept(&sock,&ccon);
					if (!test) continue;
					else if (test<0) {
						printf("** Socket accept error!\n");
						break;
					}
					/* should create new thread for this? */
					printf("@@ Connection from {%s}!\n",ccon.addr);
					wait = 0;
				}
				sprintf(buff,"#%d$",(int)(random()%100)+1);
				size = strlen(buff);
				test = sock_send(ccon.sock,size,(void*)buff);
				if (test<0) {
					printf("** Socket error (closed?)! (%d)!\n",test);
					close(ccon.sock);
					wait = 1;
					continue;
				}
				else if (test!=size) {
					/* error? abort! */
					printf("** Cannot complete data send! (%d/%d)!\n",
						test,size);
					break;
				}
				printf("@@ Sent {%s}[%d/%d]\n",buff,test,size);
				fflush(stdout);
				sleep(3);
			}
			if (!wait) {
				/* clean up connection */
				printf("@@ Closing connection from {%s}!\n",ccon.addr);
				sock_done(ccon.sock,1);
			}
		}
		printf("-- Closing socket\n");
		sock_finish(&sock,1);
	} while (0); /* end of dummy loop */
	sock_free(&sock);
	return 0;
}
/*----------------------------------------------------------------------------*/
#endif
/*----------------------------------------------------------------------------*/
