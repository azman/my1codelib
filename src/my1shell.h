/*----------------------------------------------------------------------------*/
#ifndef __MY1SHELL_H__
#define __MY1SHELL_H__
/*----------------------------------------------------------------------------*/
/**
 *  my1shell - console interface
 *  - written by azman@my1matrix.org
 *  - using my1vars and my1cmds
 */
/*----------------------------------------------------------------------------*/
#include "my1cmds.h"
#include "my1vars.h"
#include "my1list.h"
#include "my1cstr.h"
/*----------------------------------------------------------------------------*/
#define SHELL_FLAG_DONE 0x01
#define SHELL_FLAG_SKIP 0x02
#define SHELL_FLAG_WORD 0x04
/*----------------------------------------------------------------------------*/
typedef struct _my1shell_t {
	my1cmds_t cmds;
	my1vars_t vars;
	my1cstr_t buff, name, arg0, args;
	my1var_t *pvar, *pdef; /* general purpose var pointer */
	my1cmd_t *pcmd;
	/* pfunc_t => void <func> () : 0 or more args */
	pfunc_t done; /* function to free data resources */
	pfunc_t show; /* function to display a var */
	pfunc_t post; /* function to be executed AFTER cmds */
	char *pbuf;
	int flag, temp;
} my1shell_t;
/*----------------------------------------------------------------------------*/
void shell_init(my1shell_t* that, char* name);
void shell_free(my1shell_t* that);
void shell_wait(my1shell_t* that);
/* executes command in cmds */
void shell_exec(my1shell_t* that);
/* application specific: post */
void shell_post(my1shell_t* that);
/*----------------------------------------------------------------------------*/
my1cmd_t* shell_task(my1shell_t* that, char* name, char* info,
	word32_t flag, ptask_t task);
my1var_t* shell_data(my1shell_t* that, char* name, pdata_t data, pfunc_t done);
/*----------------------------------------------------------------------------*/
my1cmd_t* shell_find_task(my1shell_t* that, char* name);
my1var_t* shell_find_data(my1shell_t* that, char* name);
/*----------------------------------------------------------------------------*/
my1var_t* shell_remove_data(my1shell_t* that, char* name);
/*----------------------------------------------------------------------------*/
int shell_list_vars(my1shell_t* that);
int shell_list_cmds(my1shell_t* that);
/*----------------------------------------------------------------------------*/
#endif /** __MY1SHELL_H__ */
/*----------------------------------------------------------------------------*/
