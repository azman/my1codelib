/*----------------------------------------------------------------------------*/
#include "my1tick.h"
/*----------------------------------------------------------------------------*/
#include <string.h>
/*----------------------------------------------------------------------------*/
int tick_init(my1tick_t* tick) {
	struct sigevent event;
	memset((void*)&event,0,sizeof(struct sigevent));
	/* prepare sigevent */
	event.sigev_notify = SIGEV_SIGNAL; /* default */
	event.sigev_signo = SIGALRM; /* default */
	/** event.sigev_value.sival_int is by default the timerID */
	/** event.sigev_value is a union */
	event.sigev_value.sival_ptr = (void*) tick;
	/* user stuffs */
	tick->udata = 0x0;
	tick->ttask = 0x0;
	/* need -lrt when linking */
	return timer_create(CLOCK_REALTIME,&event,&tick->timer);
}
/*----------------------------------------------------------------------------*/
int tick_free(my1tick_t* tick) {
	/* need -lrt when linking */
	return timer_delete(tick->timer);
}
/*----------------------------------------------------------------------------*/
int tick_exec(my1tick_t* tick, int seconds, ttask_t handler) {
	struct sigaction action;
	struct itimerspec timeout;
	/* setup signal handler */
	if (!handler) handler = tick->ttask;
	memset((void*)&action,0,sizeof(struct sigaction));
	action.sa_sigaction = handler;
	action.sa_flags = SA_SIGINFO;
	sigaction(SIGALRM,&action,0x0);
	/* initial expiration - if zero, timer is disarmed! */
	timeout.it_value.tv_sec = seconds; /** time_t */
	timeout.it_value.tv_nsec = 0; /** long */
	/* auto-reload interval */
	timeout.it_interval.tv_sec = seconds;
	timeout.it_interval.tv_nsec = 0;
	/* set that (with no flags and ignore old) */
	return timer_settime(tick->timer,0,&timeout,0x0);
}
/*----------------------------------------------------------------------------*/
int tick_once(my1tick_t* tick, int seconds, ttask_t handler) {
	struct sigaction action;
	struct itimerspec timeout;
	/* setup signal handler */
	if (!handler) handler = tick->ttask;
	memset((void*)&action,0,sizeof(struct sigaction));
	action.sa_sigaction = handler;
	action.sa_flags = SA_SIGINFO;
	sigaction(SIGALRM,&action,0x0);
	/* initial expiration - if zero, timer is disarmed! */
	timeout.it_value.tv_sec = seconds; /** time_t */
	timeout.it_value.tv_nsec = 0; /** long */
	/* auto-reload interval */
	timeout.it_interval.tv_sec = 0;
	timeout.it_interval.tv_nsec = 0;
	/* set that (with no flags and ignore old) */
	return timer_settime(tick->timer,0,&timeout,0x0);
}
/*----------------------------------------------------------------------------*/
int tick_stop(my1tick_t* tick) {
	struct sigaction action;
	struct itimerspec timeout;
	/* reset handler to default */
	memset((void*)&action,0,sizeof(struct sigaction));
	action.sa_handler = SIG_DFL;
	/* set signal handler back to default - assumed ok! */
	sigaction(SIGALRM,&action,0x0);
	/* if initial expiration zero, timer is disarmed! */
	memset((void*)&timeout,0,sizeof(struct itimerspec));
	/* set that (with no flags and ignore old) */
	return timer_settime(tick->timer,0,&timeout,0x0);
}
/*----------------------------------------------------------------------------*/
#if defined(TEST_MODULE) && defined(TEST_TICK)
/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include <unistd.h>
/*----------------------------------------------------------------------------*/
#include "my1keys.c"
/*----------------------------------------------------------------------------*/
void delayed_print(int signum, siginfo_t* sigchk, void* dummy) {
	(void) dummy;
	printf("\n  Delayed print - %d (%d:%p)... ",signum,
		sigchk->si_int,sigchk->si_ptr);
	fflush(stdout);
}
/*----------------------------------------------------------------------------*/
void counter_1s(int signum, siginfo_t* sigchk, void* dummy) {
	my1tick_t* tick = (my1tick_t*)sigchk->si_ptr;
	int *pstep = (int*)tick->udata;
	(void) dummy;
	printf("\nCounter:{%d}",(*pstep)++);
	fflush(stdout);
}
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	int loop, step = 23;
	my1key_t cKey, cEsc;
	my1tick_t tick;
	tick_init(&tick);
	tick.udata = (void*)&step;
	printf("\n[TICK] (%p)... ",&tick);
	fflush(stdout);
	tick_once(&tick,5,delayed_print);
	for (loop=10;loop>=0;loop--) {
		printf("\n  waiting (%d)... ",loop);
		fflush(stdout);
		sleep(1);
	}
	printf("\n\nStart main loop... ");
	fflush(stdout);
	tick_exec(&tick,1,counter_1s);
	while (1) {
		cKey = get_keypress(&cEsc);
		if (cEsc==KEY_NONE&&cKey==KEY_ESCAPE) {
			printf("\nUser Abort! Exiting!\n\n");
			break;
		}
	}
	tick_stop(&tick);
	tick_free(&tick);
	return 0;
}
/*----------------------------------------------------------------------------*/
#endif
/*----------------------------------------------------------------------------*/
