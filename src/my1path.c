/*----------------------------------------------------------------------------*/
#include "my1path.h"
/*----------------------------------------------------------------------------*/
/* for snprintf */
#include <stdio.h>
/* for malloc/free */
#include <stdlib.h>
/* for strlen/strdup */
#include <string.h>
/* for struct dirent */
#include <dirent.h>
/* for stat */
#include <sys/stat.h>
/* for getcwd and chdir */
#include <unistd.h>
/* for basename and dirname */
#include <libgen.h>
/*----------------------------------------------------------------------------*/
/**
  S_IFMT     0170000   bit mask for the file type bit field
  S_IFSOCK   0140000   socket
  S_IFLNK    0120000   symbolic link
  S_IFREG    0100000   regular file
  S_IFBLK    0060000   block device
  S_IFDIR    0040000   directory
  S_IFCHR    0020000   character device
  S_IFIFO    0010000   FIFO
*/
/*----------------------------------------------------------------------------*/
/**
  S_ISUID     04000   set-user-ID bit
  S_ISGID     02000   set-group-ID bit (see below)
  S_ISVTX     01000   sticky bit (see below)
  S_IRWXU     00700   owner has read, write and execute permission
  S_IRUSR     00400   owner has read permission
  S_IWUSR     00200   owner has write permission
  S_IXUSR     00100   owner has execute permission
  S_IRWXG     00070   group has read, write and execute permission
  S_IRGRP     00040   group has read permission
  S_IWGRP     00020   group has write permission
  S_IXGRP     00010   group has execute permission
  S_IRWXO     00007   others have read, write and execute permission
  S_IROTH     00004   others have read permission
  S_IWOTH     00002   others have write permission
  S_IXOTH     00001   others have execute permission
*/
/*----------------------------------------------------------------------------*/
void path_init(my1path_t* path) {
	path->trap = 0;
	/**path->padd = 0;*/
	path->flag = 0;
	path->mode = 0;
	path->iown = 0;
	path->igrp = 0;
	path->name = 0x0;
	path->path = 0x0;
	path->full = 0x0;
	path->real = 0x0;
	path->perm[0] = 0x0;
}
/*----------------------------------------------------------------------------*/
void path_free(my1path_t* path) {
	if (path->name) free((void*)path->name);
	if (path->path) free((void*)path->path);
	if (path->full) free((void*)path->full);
	if (path->real) free((void*)path->real);
}
/*----------------------------------------------------------------------------*/
char* path_name_merge(char *path, char *file) {
	char *full;
	int chk1 = strlen(file);
	int chk2 = strlen(path);
	int size = chk1+chk2+1;
	/* create space for full - add separator and NULL */
	if (path[chk2-1]==PATH_SEPARATOR_CHAR) {
		if (file[0]==PATH_SEPARATOR_CHAR) {
			size--;
			full = (void*) malloc(size);
			snprintf(full,size,"%s%s",path,&file[1]);
		}
		else {
			full = (void*) malloc(size);
			snprintf(full,size,"%s%s",path,file);
		}
	}
	else {
		size++;
		full = (void*) malloc(size);
		snprintf(full,size,"%s%c%s",path,PATH_SEPARATOR_CHAR,file);
	}
	return full;
}
/*----------------------------------------------------------------------------*/
char* path_name_check(char *gcwd, char *name) {
	char *full, *buff, *temp;
	/* check input for absolute/relative name */
	if (name[0]==PATH_SEPARATOR_CHAR) {
		/* absolute name */
		full = strdup(name);
	}
	else {
		/* is . or .. specified? */
		if (name[0]=='.') {
			if (!name[1]) {
				/* relative name for current dir! */
				full = strdup(gcwd);
			}
			else if (name[1]==PATH_SEPARATOR_CHAR) {
				/* current path */
				full = path_name_merge(gcwd,&name[2]);
			}
			else if (name[1]=='.') {
				/* parent path */
				buff = strdup(gcwd);
				temp = dirname(buff);
				/*  */
				if (!name[2]) {
					full = strdup(temp);
				}
				else if (name[2]==PATH_SEPARATOR_CHAR) {
					/* create full pathname */
					full = path_name_merge(temp,&name[3]);
				}
				else {
					/* an error? simply merge and see what happens */
					full = path_name_merge(gcwd,name);
				}
				/* cleanup */
				free((void*)buff);
				buff = 0x0;
			}
			else {
				/* current path - hidden file/path? */
				full = path_name_merge(gcwd,name);
			}
		}
		else {
			/* current path */
			full = path_name_merge(gcwd,name);
		}
	}
	/* path is in full */
	return full;
}
/*----------------------------------------------------------------------------*/
void path_dostat(my1path_t* path, char* full) {
	struct stat statbuff, statlink;
	char test[3] = "rwx";
	char temp[PATH_MAX]; /* is this ok? using PATH_MAX?  */
	int size, perm = S_IRUSR, what = 0, loop = 1, mask = PATH_MODE_USRR;
	char *buff, *name;
	/* run stat - trying lstat */
	if (lstat(full,&statbuff)) {
		path->trap |= PATH_TRAP_ERROR_STAT;
		return;
	}
	/* cleanup if path is assigned */
	if (path->name) {
		path_free(path);
		path->real = 0x0; /* the rest will get new values, real is for links */
	}
	/* assign names */
	name = strdup(full);
	path->name = strdup(basename(name));
	free((void*)name);
	buff = strdup(full);
	path->path = strdup(dirname(buff));
	free((void*)buff);
	path->full = strdup(full);
	/* check type */
	switch (statbuff.st_mode&S_IFMT) {
		case S_IFREG:
			path->perm[0] = '-';
			path->flag |= PATH_FLAG_FILE;
			break;
		case S_IFDIR:
			path->perm[0] = 'd';
			path->flag |= PATH_FLAG_PATH;
			break;
		case S_IFLNK:
			path->perm[0] = 'l';
			path->flag |= PATH_FLAG_LINK;
			size = readlink(full,temp,PATH_MAX-1);
			if (size>0) {
				temp[size] = 0x0;
				buff = path_name_check(path->path,temp);
				path->real = strdup(buff);
				free((void*)buff);
				if (!stat(path->real,&statlink)) {
					/* check type */
					switch (statlink.st_mode&S_IFMT) {
						case S_IFREG: path->flag |= PATH_FLAG_FILE; break;
						case S_IFDIR: path->flag |= PATH_FLAG_PATH; break;
					}
				}
			}
			break;
		default: path->perm[0] = '?'; break;
	}
	/* check permission */
	path->mode = 0;
	while (perm) {
		if (statbuff.st_mode&perm) {
			path->perm[loop] = test[what];
			path->mode |= mask;
		}
		else path->perm[loop] = '-';
		loop++;
		perm >>= 1;
		mask >>= 1;
		what++;
		if (what==3) what = 0;
	}
	path->perm[loop] = 0x0;
	/* look for owner & group */
	path->iown = statbuff.st_uid;
	path->igrp = statbuff.st_gid;
	/* done */
	return;
}
/*----------------------------------------------------------------------------*/
char* path_name_getcwd(void) {
	char cwd[PATH_MAX];
	if (!getcwd(cwd,sizeof(cwd)))
		return 0x0;
	return strdup(cwd);
}
/*----------------------------------------------------------------------------*/
void path_getcwd(my1path_t* path) {
	char *gcwd = path_name_getcwd();
	if (!gcwd) {
		/* should not happen? */
		path->trap |= PATH_TRAP_ERROR_GCWD;
		return;
	}
	/* do stat */
	path_dostat(path,gcwd);
	/* cleanup */
	free((void*)gcwd);
}
/*----------------------------------------------------------------------------*/
void path_chdir(my1path_t* path, char* full) {
	if (chdir(full)<0) {
		path->trap |= PATH_TRAP_ERROR_CHGD;
		return;
	}
	path_dostat(path,full);
	/* gets real path! */
	/**path_getcwd(path);*/
}
/*----------------------------------------------------------------------------*/
void path_access(my1path_t* path, char* name) {
	char *full, *gcwd;
	if (!(gcwd=path_name_getcwd())) {
		/* should not happen? */
		path->trap |= PATH_TRAP_ERROR_GCWD;
		return;
	}
	/* resolve name */
	full = path_name_check(gcwd,name);
	/* do stat */
	path_dostat(path,full);
	/* cleanup */
	free((void*)full);
	free((void*)gcwd);
}
/*----------------------------------------------------------------------------*/
void path_parent(my1path_t* path, my1path_t* parent) {
	if (!path->path) {
		parent->trap |= PATH_TRAP_ERROR;
		return;
	}
	/* assume path is a valid path! */
	path_access(parent,path->path);
}
/*----------------------------------------------------------------------------*/
#if defined(TEST_MODULE) && defined(TEST_PATH)
/*----------------------------------------------------------------------------*/
void path_print_info(my1path_t* path, int full) {
	if (!path->path) return;
	printf("  %s ",path->perm);
	if (full) {
		printf("%s\n",path->full);
		printf(">> path:{%s}\n",path->path);
		printf(">> name:{%s}\n",path->name);
	}
	else {
		printf("%s",path->name);
		if (path->real)
			printf(" -> %s",path->real);
		if (path->flag&PATH_FLAG_PATH)
			putchar(PATH_SEPARATOR_CHAR);
		printf("\n");
	}
}
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	my1path_t test, next;
	char *pick = argc>1?argv[1] : ".";
	/* check running progam */
	path_init(&test);
	path_access(&test,argv[0]);
	if (!test.trap) {
		printf("DoExec: ");
		path_print_info(&test,1);
	}
	else path_retrap(&test);
	/* check name given */
	path_access(&test,pick);
	if (test.trap)
		printf("Error accessing '%s' (%d)\n",pick,test.trap);
	else {
		/* check parent */
		path_init(&next);
		path_parent(&test,&next);
		if (next.trap&PATH_TRAP_ERROR)
			printf("Error getting parent for '%s' (%x)\n",test.name,next.trap);
		else {
			printf("Parent: ");
			path_print_info(&next,1);
		}
		printf("Access: ");
		path_print_info(&test,1);
		/* free parent */
		path_free(&next);
	}
	/* cleanup */
	path_free(&test);
	return 0;
}
/*----------------------------------------------------------------------------*/
#endif
/*----------------------------------------------------------------------------*/
