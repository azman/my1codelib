/*----------------------------------------------------------------------------*/
#ifndef __MY1JSON_FILE_H__
#define __MY1JSON_FILE_H__
/*----------------------------------------------------------------------------*/
/**
 *  my1json extension to read/write from/to file
 *  - written by azman@my1matrix.org
 */
/*----------------------------------------------------------------------------*/
#include "my1json.h"
/*----------------------------------------------------------------------------*/
#include <stdio.h>
/*----------------------------------------------------------------------------*/
int json_read(my1json_t* json, FILE* file);
int json_view(my1json_t* json, FILE* file, int eol);
int json_view_once(my1json_t* json, FILE* file);
int json_load_string(my1json_t* json,char* string, int size);
int json_load(my1json_t* json, char* filename);
int json_save(my1json_t* json, char* filename, int eol);
/*----------------------------------------------------------------------------*/
#endif  /** __MY1JSON_FILE_H__ */
/*----------------------------------------------------------------------------*/
