/*----------------------------------------------------------------------------*/
#include "my1roll.h"
/*----------------------------------------------------------------------------*/
#include <stdlib.h>
/*----------------------------------------------------------------------------*/
static unsigned int seed_prev = 0;
/*----------------------------------------------------------------------------*/
void roll_init(my1roll_t* roll, unsigned int seed)
{
	/* seed is usually time-based */
	if (seed<=seed_prev) seed = seed_prev+1;
	/* assume ok? */
	roll->pstate = initstate(seed,roll->sstate,SEED_STATE_SIZE);
	seed_prev = seed;
	/* restore previous if found */
	if (roll->pstate)
		setstate(roll->pstate);
	roll->roll = 0;
	roll->rmin = 0; /* cheat? :p make sure NOT greater than max! */
	roll->offs = OFFS_RAND; /* by default, random number 0 to max-1 */
	roll->seed = seed;
}
/*----------------------------------------------------------------------------*/
int roll_take(my1roll_t* roll, int max)
{
	roll->pstate = setstate(roll->sstate);
	/* cannot roll?! */
	if (!roll->pstate) return 0;
	while (1)
	{
		roll->roll = (random()%max)+roll->offs;
		if (roll->roll>=roll->rmin)
			break;
	}
	setstate(roll->pstate);
	return roll->roll;
}
/*----------------------------------------------------------------------------*/
#if defined(TEST_MODULE) && defined(TEST_ROLL)
/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[])
{
	my1roll_t roll1, roll2;
	int loop, test, take, mult, curr, roll;
	if (argc<2) return 0;
	loop = 1;
	if (!strncmp(argv[loop],"--demo",7))
	{
		test = 0;
		if (++loop<argc)
		{
			roll = atoi(argv[loop]);
			if (roll<=0)
			{
				fprintf(stderr,"** Invalid roll request '%s'?\n",argv[loop]);
				return -1;
			}
		} else roll = 6;
		while (++loop<argc)
		{
			fprintf(stderr,"## Extra argument '%s'(%d)!\n",argv[loop],argc);
			test++;
		}
		printf("@@PREP D1 ...");
		fflush(stdout);
		roll_init(&roll1,(unsigned int)time(0x0));
		printf("done.\n");
		printf("@@PREP D2 ...");
		fflush(stdout);
		roll_init(&roll2,(unsigned int)time(0x0));
		printf("done.\n");
		printf("## Rolling 1d%d\n",roll);
		printf("##       D1 | D2\n");
		fflush(stdout);
		for (loop=0;loop<roll;loop++)
		{
			roll_take(&roll1,roll);
			roll_take(&roll2,roll);
			printf("## Roll: %-2d | %-2d\n",roll1.roll+1,roll2.roll+1);
			fflush(stdout);
		}
		return test;
	}
	/* roll request? */
	take = atoi(argv[loop]);
	if (take<=0)
	{
		fprintf(stderr,"** Invalid roll request '%s'?\n",argv[loop]);
		return -1;
	}
	if (++loop<argc)
	{
		mult = atoi(argv[loop]);
		if (!mult)
		{
			fprintf(stderr,"** Invalid roll count '%s'?\n",argv[loop]);
			return -1;
		}
	} else mult = 1;
	if (++loop<argc)
	{
		if (!strncmp(argv[loop],"--debug",8))
		{
			fprintf(stderr,"[DEBUG] Rolling %dd%d => ",mult,take);
			fflush(stderr);
		}
		else
			fprintf(stderr,"## Extra argument '%s'(%d)!\n",argv[loop],argc);
	}
	while (++loop<argc)
		fprintf(stderr,"## Extra argument '%s'(%d)!\n",argv[loop],argc);
	/* init */
	test = 0;
	roll_init(&roll1,(unsigned int)time(0x0));
	roll1.offs = OFFS_DICE; /* 1 to max (die roll) */
	for (loop=0;loop<mult;loop++)
	{
		curr = roll_take(&roll1,take);
		if (loop) putchar(',');
		printf("%d",curr);
		test += curr;
	}
	if (mult>1) printf(" => %d",test);
	putchar('\n');
	return 0;
}
/*----------------------------------------------------------------------------*/
#endif
/*----------------------------------------------------------------------------*/
