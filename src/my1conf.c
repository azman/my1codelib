/*----------------------------------------------------------------------------*/
#include "my1conf.h"
#include "my1text.h"
#include "my1cstr_util.h"
/*----------------------------------------------------------------------------*/
#include <string.h>
/*----------------------------------------------------------------------------*/
char* cstr_extract_tag(my1cstr_t* pstr, my1cstr_t* atag) {
	my1cstr_t tmp;
	char *psrc, *pdst, buff, prev;
	int loop, trim, init;
	int size = pstr->fill+1; /* strlen + 1! */
	char *ptag = 0x0;
	cstr_init(&tmp);
	cstr_resize(&tmp,size);
	if (tmp.size>=size) {
		trim = 1; init = 0; size--; /* discount null space */
		psrc = pstr->buff;
		pdst = tmp.buff;
		for (loop=0;loop<size;loop++) {
			buff = *psrc;
			if (trim) {
				if (buff!=' ') {
					trim = 0;
					if (!init) {
						if (buff=='[') init = 1;
						else break;
					}
					else {
						if (buff==']') {
							if(prev==' ') pdst--;
							loop = size; /* end this loop */
						}
					}
					*pdst = buff;
					pdst++;
				}
			}
			else {
				if (buff==' ') {
					trim = 1;
					if (init) {
						*pdst = buff;
						pdst++;
					}
				}
				else {
					*pdst = buff;
					pdst++;
					if (buff==']') loop = size; /* end loop */
				}
			}
			prev = buff;
			psrc++;
		}
		if (prev==']') {
			*pdst = 0x0;
			size = strlen(tmp.buff) + 1;
			cstr_null(atag);
			cstr_resize(atag,size);
			if (atag->size>=size) {
				cstr_append(atag,tmp.buff);
				ptag = atag->buff;
			}
		}
	}
	cstr_free(&tmp);
	return ptag;
}
/*----------------------------------------------------------------------------*/
char* cstr_read_tag(my1cstr_t* pstr, my1cstr_t* atag) {
	char *ptag = 0x0;
	int that = pstr->fill-1;
	char buff = pstr->buff[that];
	if (pstr->buff[0]=='['&&buff==']') {
		pstr->buff[that] = 0x0;
		cstr_null(atag);
		cstr_append(atag,&pstr->buff[1]);
		pstr->buff[that] = buff;
		ptag = atag->buff;
	}
	return ptag;
}
/*----------------------------------------------------------------------------*/
char* cstr_extract_hash(my1cstr_t* pstr, my1cstr_t* hash) {
	my1cstr_t tmp;
	char *psrc, *pdst, buff;
	int loop, step, trim, echk;
	int size = pstr->fill+1; /* strlen + 1! */
	int epos = -1;
	char *pdat = 0x0;
	cstr_init(&tmp);
	cstr_resize(&tmp,size);
	if (tmp.size>=size) {
		trim = 1; echk = 0; step = 0;
		size--; /* discount null space */
		psrc = pstr->buff;
		pdst = tmp.buff;
		for (loop=0;loop<size;loop++) {
			buff = *psrc;
			if (trim) {
				if (buff!=' ') {
					trim = 0;
					/* at least 1 char before checking for '=' */
					if (echk) {
						if (buff=='=') {
							epos = step;
							trim = 1;
							echk = 0;
						}
						else break;
					}
					else if (epos<0) echk = 1;
					*pdst = buff;
					pdst++;
					step++;
				}
			}
			else {
				if (epos<0&&buff=='=') {
					epos = step;
					trim = 1;
					echk = 0;
				}
				*pdst = buff;
				pdst++;
				step++;
			}
			psrc++;
		}
		if (epos>0) {
			*pdst = 0x0;
			size = strlen(tmp.buff) + 1;
			cstr_null(hash);
			cstr_resize(hash,size);
			if (hash->size>=size) {
				cstr_append(hash,tmp.buff);
				pdat = hash->buff;
			}
		}
	}
	cstr_free(&tmp);
	return pdat;
}
/*----------------------------------------------------------------------------*/
char* cstr_read_key(my1cstr_t* pstr, my1cstr_t* akey) {
	char buff, *pkey = 0x0, *pchk = pstr->buff, *ptmp;
	/* trim... just in case */
	while ((*pchk)==' ') pchk++;
	/* start key */
	pkey = pchk;
	while ((*pchk)!=' '&&(*pchk)!='=') pchk++;
	/* save that */
	buff = *pchk;
	ptmp = pchk;
	/* find that '=' */
	while ((*ptmp)!='=') ptmp++;
	if ((*ptmp)=='=') {
		*pchk = 0x0;
		cstr_null(akey);
		cstr_append(akey,pkey);
		*pchk = buff;
		pkey = akey->buff;
	}
	else pkey = 0x0;
	return pkey;
}
/*----------------------------------------------------------------------------*/
char* cstr_read_val(my1cstr_t* pstr, my1cstr_t* aval) {
	char *pval = 0x0, *pchk = pstr->buff;
	/* find that '=' */
	while ((*pchk)!='=') pchk++;
	pchk++; pval = pchk;
	cstr_null(aval);
	cstr_append(aval,pval);
	return aval->buff;
}
/*----------------------------------------------------------------------------*/
char* cstr_extract_comment(my1cstr_t* pstr, my1cstr_t* comment) {
	int loop = 0;
	int size = pstr->fill;
	char *pcom = 0x0;
	char *psrc = pstr->buff;
	while (loop<size&&(*psrc)==' ') {
		psrc++;
		loop++;
	}
	/* first non-space char => must be a # */
	if (loop<size&&(*psrc)=='#') {
		if (!comment) return pstr->buff; /* no copy requested */
		cstr_null(comment);
		cstr_append(comment,psrc);
		pcom = comment->buff;
	}
	return pcom;
}
/*----------------------------------------------------------------------------*/
int conf_validate(char* name) {
	my1cstr_t find;
	my1text_t text;
	int flag = MY1CONF_VALID;
	text_init(&text);
	text_open(&text,name);
	if (text.pfile) {
		cstr_init(&find);
		while (text_read(&text)>=CHAR_INIT) {
			if (cstr_extract_comment(&text.pbuff,&find)) continue;
			if (cstr_extract_tag(&text.pbuff,&find)) continue;
			if (cstr_extract_hash(&text.pbuff,&find)) continue;
			if (!cstr_trimws(&text.pbuff,0)) continue;
			flag = MY1CONF_INVALID; break;
		}
		text_done(&text);
		cstr_free(&find);
	}
	else flag = MY1CONF_NOFILE;
	text_free(&text);
	return flag;
}
/*----------------------------------------------------------------------------*/
int conf_find_param(char* name, char* atag, char *akey, my1cstr_t* pprm) {
	my1cstr_t find, temp, *pchk = 0x0;
	my1text_t text;
	int ctag = 0, flag = MY1CONF_VALID;
	text_init(&text);
	text_open(&text,name);
	if (text.pfile) {
		cstr_init(&find);
		cstr_init(&temp);
		while (text_read(&text)>=CHAR_INIT) {
			if (cstr_extract_comment(&text.pbuff,&find)) continue;
			if (cstr_extract_tag(&text.pbuff,&find)) {
				if (!ctag) {
					cstr_read_tag(&find,&temp);
					if (!strncmp(temp.buff,atag,temp.fill))
						ctag = 1; /** tag found! */
					continue;
				}
				else break; /* found another tag! end search! */
			}
			if (cstr_extract_hash(&text.pbuff,&find)) {
				if (ctag) {
					/* only check if tag found! */
					cstr_read_key(&find,&temp);
					if (!strncmp(temp.buff,akey,temp.fill)) {
						/* key found! */
						pchk = &find;
						break;
					}
				}
				continue;
			}
			if (!cstr_trimws(&text.pbuff,0)) continue;
			flag = MY1CONF_INVALID; break;
		}
		text_done(&text);
		if (flag==MY1CONF_VALID) {
			if (pchk) cstr_read_val(pchk,pprm);
			else flag = MY1CONF_NOTFOUND;
		}
		cstr_free(&temp);
		cstr_free(&find);
	}
	else flag = MY1CONF_NOFILE;
	text_free(&text);
	return flag;
}
/*----------------------------------------------------------------------------*/
#if defined(TEST_MODULE) && defined(TEST_CONF)
/*----------------------------------------------------------------------------*/
#include "my1cstr.c"
#include "my1cstr_line.c"
#include "my1cstr_util.c"
#include "my1text.c"
/*----------------------------------------------------------------------------*/
#define FLAG_ERROR 0x01
#define FLAG_DEBUG 0x80
/*----------------------------------------------------------------------------*/
void conf_validate_debug(char* name) {
	my1cstr_t find;
	my1text_t text;
	text_init(&text);
	text_open(&text,name);
	if (text.pfile) {
		cstr_init(&find);
		while (text_read(&text)>=CHAR_INIT) {
			if (cstr_extract_comment(&text.pbuff,&find))
				printf("-- Comment:{%s}\n",text.pbuff.buff);
			else if (cstr_extract_tag(&text.pbuff,&find))
				printf("-- Tag:{%s}\n",text.pbuff.buff);
			else if (cstr_extract_hash(&text.pbuff,&find))
				printf("-- Hash:{%s}\n",text.pbuff.buff);
			else if (!cstr_trimws(&text.pbuff,0))
				printf("## Empty:{%s}\n",text.pbuff.buff);
			else
				printf("** UNKNOWN:{%s}\n",text.pbuff.buff);
		}
		text_done(&text);
		cstr_free(&find);
	}
	text_free(&text);
}
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	my1cstr_t find;
	int loop, flag = 0;
	char *name = 0x0, *atag = 0x0, *akey = 0x0;
	/* process parameter */
	for (loop=1;loop<argc;loop++) {
		if (!strncmp(argv[loop],"--tag",6)) {
			loop++; if (loop<argc) atag = argv[loop];
		}
		else if (!strncmp(argv[loop],"--key",6)) {
			loop++; if (loop<argc) akey = argv[loop];
		}
		else if (!strncmp(argv[loop],"--debug",8))
			flag |= FLAG_DEBUG;
		else if (argv[loop][0]!='-') {
			if (name)
				printf("** Warning! Multiple filename? (%s)\n",name);
			name = argv[loop];
		}
		else {
			printf("** Unknown option '%s'\n",argv[loop]);
			flag |= FLAG_ERROR;
		}
	}
	if (flag&FLAG_ERROR) return -1;
	if (!name) {
		printf("** No config file specified!\n");
		return -1;
	}
	/* validate */
	switch (conf_validate(name)) {
		case MY1CONF_INVALID:
			printf("** Invalid config file '%s'\n",name);
			flag |= FLAG_ERROR;
			conf_validate_debug(name);
			break;
		case MY1CONF_NOFILE:
			printf("** Cannot open file '%s'\n",name); 
			flag |= FLAG_ERROR;
			break;
		case MY1CONF_VALID:
			printf("-- Valid config file '%s'\n",name);
			break;
		default:
			printf("** Unknown error reading '%s'\n",name); 
			flag |= FLAG_ERROR;
			break;
	}
	if (flag&FLAG_ERROR) return -1;
	if (atag&&akey) {
		cstr_init(&find);
		switch (conf_find_param(name,atag,akey,&find)) {
			case MY1CONF_VALID:
				printf("Tag:'%s', Key:'%s' => ",atag,akey);
				printf("Val:'%s'\n",find.buff);
				break;
			case MY1CONF_NOTFOUND:
				printf("Cannot find {tag:%s}{key:%s} in %s\n",atag,akey,name);
				break;
			default:
				printf("Unknown error finding {tag:%s}{key:%s}\n",atag,akey);
				break;
		}
		cstr_free(&find);
	}
	else if (atag) printf("Specify key using --key switch (tag:%s)\n",atag);
	else if (akey) printf("Specify tag using --tag switch (key:%s)\n",akey);
	else if (flag&FLAG_DEBUG) conf_validate_debug(name);
	return 0;
}
/*----------------------------------------------------------------------------*/
#endif
/*----------------------------------------------------------------------------*/
