/*----------------------------------------------------------------------------*/
#include "my1cmds.h"
/*----------------------------------------------------------------------------*/
#include <stdlib.h>
#include <string.h>
/*----------------------------------------------------------------------------*/
void cmds_init(my1cmds_t* that) {
	that->list = 0x0;
	that->size = 0;
	that->fill = 0;
	that->data = 0x0;
	that->prom = 0x0;
}
/*----------------------------------------------------------------------------*/
void cmds_free(my1cmds_t* that) {
	if (that->list)
		free((void*)that->list);
	if (that->prom)
		free((void*)that->prom);
}
/*----------------------------------------------------------------------------*/
void cmds_prompt(my1cmds_t* that, char* prompt) {
	if (that->prom)
		free((void*)that->prom);
	that->prom = strdup(prompt);
}
/*----------------------------------------------------------------------------*/
my1cmd_t* cmds_size(my1cmds_t* that, int size) {
	my1cmd_t *temp;
	if (size>0&&size!=that->size) {
		temp = (my1cmd_t*)realloc((void*)that->list,size*sizeof(my1cmd_t));
		if (temp) {
			that->list = temp;
			that->size = size;
			if (that->fill>size)
				that->fill = size;
		}
	}
	temp = (that->size>=size) ? that->list : 0x0;
	return temp;
}
/*----------------------------------------------------------------------------*/
my1cmd_t* cmds_find(my1cmds_t* that, char* pstr) {
	my1cmd_t *look;
	look = that->list;
	that->pick = 0x0;
	for (that->loop=0;that->loop<that->fill;that->loop++,look++) {
		if (!strncmp(pstr,look->name,look->scmp)) {
			that->pick = look;
			break;
		}
	}
	return that->pick;
}
/*----------------------------------------------------------------------------*/
my1cmd_t* cmds_make(my1cmds_t* that, char* name, char* info,
		word32_t flag, ptask_t exec) {
	my1cmd_t* pcmd;
	if (that->fill==that->size) {
		if (!cmds_size(that,that->fill+2))
			return 0x0;
	}
	/* space ensured? */
	pcmd = &that->list[that->fill];
	that->fill++;
	pcmd->flag = flag;
	strncpy(pcmd->name,name,MY1CMD_NAME_SIZE);
	pcmd->name[MY1CMD_NAME_SIZE-1] = 0x0; /* just in case */
	pcmd->scmp = strlen(pcmd->name)+1;
	if (info) {
		strncpy(pcmd->info,info,MY1CMD_INFO_SIZE);
		pcmd->info[MY1CMD_INFO_SIZE-1] = 0x0;
	}
	else pcmd->info[0] = 0x0;
	pcmd->exec = exec;
	that->pick = pcmd;
	return pcmd;
}
/*----------------------------------------------------------------------------*/
my1cmd_t* cmds_exec(my1cmds_t* that) {
	if (that->pick) {
		if (that->pick->exec)
			that->pick->exec(that);
	}
	return that->pick;
}
/*----------------------------------------------------------------------------*/
#if defined(TEST_MODULE) && defined(TEST_CMDS)
/*----------------------------------------------------------------------------*/
#include <stdio.h>
/*----------------------------------------------------------------------------*/
#include "my1cstr.c"
#include "my1cstr_line.c"
#include "my1cstr_util.c"
/*----------------------------------------------------------------------------*/
#define SHPROMPT "my1cmds"
/*----------------------------------------------------------------------------*/
my1cmd_t* cmds_wait(my1cmds_t* that) {
	my1cstr_t buff;
	cstr_init(&buff);
	that->pick = 0x0;
	printf("\n%s> ",that->prom?that->prom:"");
	if (cstr_read_line(&buff,stdin)) {
		cstr_trimws(&buff,1);
		if (buff.fill) {
			if (!cmds_find(that,buff.buff))
				fprintf(stderr,"\n** Unknown cmd '%s'!\n",buff.buff);
		}
	}
	cstr_free(&buff);
	return that->pick;
}
/*----------------------------------------------------------------------------*/
int show_help(my1cmds_t* that) {
	my1cmd_t* pcmd;
	pcmd = that->list;
	that->loop = 0;
	printf("\n-- Command(s):\n\n");
	while (that->loop<that->fill) {
		printf("   %s - %s\n",pcmd->name,pcmd->info);
		that->loop++;
		pcmd++;
	}
	return 0;
}
/*----------------------------------------------------------------------------*/
void just_laugh(my1cmds_t* that) {
	printf("\nHAHAHAHA!\n");
}
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	my1cmds_t cmds;
	cmds_init(&cmds);
	cmds_make(&cmds,"help","Show this help",CMD_TASK,show_help);
	cmds_make(&cmds,"haha","Just laugh",CMD_TASK,(ptask_t)just_laugh);
	cmds_make(&cmds,"rest","Do Nothing",CMD_TASK,0x0);
	cmds_make(&cmds,"exit","Exit Program",CMD_EXIT,0x0);
	cmds_make(&cmds,"quit","",CMD_EXIT,0x0);
	cmds_prompt(&cmds,SHPROMPT);
	while (1) {
		cmds_wait(&cmds);
		if (cmds_done(&cmds))
			break;
		cmds_exec(&cmds);
	}
	printf("\n");
	cmds_free(&cmds);
	return 0;
}
/*----------------------------------------------------------------------------*/
#endif
/*----------------------------------------------------------------------------*/
