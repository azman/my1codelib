/*----------------------------------------------------------------------------*/
#ifndef __MY1UART_H__
#define __MY1UART_H__
/*----------------------------------------------------------------------------*/
#define MAX_COM_PORT 32
#define MAX_COM_CHAR 16
/*----------------------------------------------------------------------------*/
#ifdef DO_MINGW
#include <windows.h> /* win api for com terminal */
#define COM_PORT(X) (X)
#else
#include <termios.h> /* terminal control structure */
#define COM_PORT(X) (X-1)
#endif
/*----------------------------------------------------------------------------*/
#define UART_TIMEOUT -1
#define INVALID_PORT_INDEX -1
/*----------------------------------------------------------------------------*/
#define MY1BAUD9600 0
#define MY1BAUD19200 1
#define MY1BAUD38400 2
/** THESE ARE NON-POSIX? */
#define MY1BAUD57600 3
#define MY1BAUD115200 4
/** SOME OLD STUFF MAY NEED THESE? */
#define MY1BAUD1200 5
#define MY1BAUD2400 6
#define MY1BAUD4800 7
/*----------------------------------------------------------------------------*/
#define MY1PARITY_NONE 0
#define MY1PARITY_ODD 1
#define MY1PARITY_EVEN 2
#define MY1PARITY_MARK 3
#define MY1PARITY_SPACE 4
/*----------------------------------------------------------------------------*/
#define MY1PORT_STATUS_OK 0
#define MY1PORT_STATUS_EIO 5
/*----------------------------------------------------------------------------*/
#define MY1PIN_RTS 0x01
#define MY1PIN_CTS 0x02
#define MY1PIN_DTR 0x04
#define MY1PIN_DSR 0x08
/*----------------------------------------------------------------------------*/
#include "my1type.h"
/*----------------------------------------------------------------------------*/
typedef struct _my1uart_conf_t {
	byte08_t baud; /* 0-9600(default),1-19200,2-38400,3-57600,4-115200 */
	byte08_t size; /* number of bits/byte, 4-8 (default=8, linux:min=5) */
	byte08_t bpar; /* 0-4=no,odd,even,mark,space (default=0) */
	byte08_t stop; /* 0,1,2 = 1, 1.5, 2 (default=0, linux: 1 not impl.) */
} my1uart_conf_t;
/*----------------------------------------------------------------------------*/
typedef struct _my1uart_t {
#ifdef DO_MINGW
	DCB curr, save;
	HANDLE hand;
#else
	struct termios curr, save;
	long hand;
#endif
	int term, stat;
	char name[MAX_COM_CHAR];
	char temp[MAX_COM_CHAR];
} my1uart_t;
/*----------------------------------------------------------------------------*/
void uart_init(my1uart_t*);
int uart_find(my1uart_t*,int*); /* return 1st available (int* count) */
int uart_prep(my1uart_t*,int); /* [1,MAX_COM_PORT] */
int uart_open(my1uart_t*);
int uart_done(my1uart_t*);
int uart_isopen(my1uart_t*);
int uart_okstat(my1uart_t*);
void uart_name(my1uart_t*,char*); /* change device name */
void uart_purge(my1uart_t*); /** purge input */
void uart_flush(my1uart_t*); /** flush output*/
void uart_get_config(my1uart_t*,my1uart_conf_t*);
void uart_set_config(my1uart_t*,my1uart_conf_t*);
int uart_open_args(my1uart_t*,int,byte08_t,byte08_t,byte08_t,byte08_t);
void uart_send_byte(my1uart_t*,byte08_t);
byte08_t uart_read_byte(my1uart_t*);
void uart_outgoing(my1uart_t*);
int uart_incoming(my1uart_t*);
int uart_read_byte_timed(my1uart_t*,int); /** timeout_us */
int uart_send_data(my1uart_t*,byte08_t*,int);
int uart_read_data(my1uart_t*,byte08_t*,int);
/** Functions to control/check DTR/DSR/RTS/CTS: */
/** e.g. assert_pin(&cPort,MY1PIN_DTR,1); */
/** Note: assert non-zero is a +12V at pin! 0 => -12V */
int uart_pin_assert(my1uart_t*,int,int);
int uart_pin_detect(my1uart_t*,int,int*);
/*----------------------------------------------------------------------------*/
/* helper functions to check/assign baudrate */
int uart_actual_baudrate(int); /* actual <- encoded */
int uart_encoded_baudrate(int); /* encoded <- actual */
int uart_get_baudrate(my1uart_t*);
int uart_set_baudrate(my1uart_t*,int);
int uart_set_parity(my1uart_t*,int);
/*----------------------------------------------------------------------------*/
#endif /** __MY1UART_H__ */
/*----------------------------------------------------------------------------*/
