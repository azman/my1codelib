/*----------------------------------------------------------------------------*/
#ifndef __MY1CSTR_CONV_H__
#define __MY1CSTR_CONV_H__
/*----------------------------------------------------------------------------*/
/**
 * - extension for my1cstr_t to provide conversion functions
 * - written by azman@my1matrix.org
 * - converts values to/from string (my1cstr_t object)
**/
/*----------------------------------------------------------------------------*/
#include "my1cstr.h"
/*----------------------------------------------------------------------------*/
void cstr_append_uint(my1cstr_t* pstr, unsigned int that);
void cstr_append_int(my1cstr_t* pstr, int that);
void cstr_append_real(my1cstr_t* pstr, float that, int dcnt);
void cstr_make_uint(my1cstr_t* pstr, unsigned int* pval, int offs);
void cstr_make_int(my1cstr_t* pstr, int* pval, int offs);
void cstr_make_real(my1cstr_t* pstr, float* pval, int offs);
/*----------------------------------------------------------------------------*/
#endif /** __MY1CSTR_CONV_H__ */
/*----------------------------------------------------------------------------*/
