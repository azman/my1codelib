/*----------------------------------------------------------------------------*/
#include "my1json.h"
/*----------------------------------------------------------------------------*/
#include <stdlib.h>
#include <string.h>
/*----------------------------------------------------------------------------*/
void json_init(my1json_t* json) {
	json->type = JSON_VALUE;
	json->pkey = 0x0;
	json->pval = 0x0;
	json->root = 0x0;
	list_init(&json->data,LIST_OF_DATA);
}
/*----------------------------------------------------------------------------*/
void json_free(my1json_t* json) {
	if (json->pkey) {
		free((void*)json->pkey);
		json->pkey = 0x0;
	}
	if (json->pval) {
		free((void*)json->pval);
		json->pval = 0x0;
	}
	while (json->data.size>0) {
		my1json_t* pjson = (my1json_t*) list_pull_data(&json->data);
		json_free(pjson);
		free((void*)pjson);
	}
	list_free(&json->data);
}
/*----------------------------------------------------------------------------*/
void json_set_key(my1json_t* json, char* pkey) {
	char *buff;
	int size = strlen(pkey);
	if (pkey[0]=='"'&&pkey[size-1]=='"') {
		pkey[size-1] = 0x0; size--;
		pkey++; size--;
	}
	buff = (char*) realloc((void*)json->pkey,++size);
	if (buff) {
		strncpy(buff,pkey,size);
		json->pkey = buff;
	}
}
/*----------------------------------------------------------------------------*/
char is_number(char cval) {
	if (cval<0x30||cval>0x39) return 0x0;
	else return cval;
}
/*----------------------------------------------------------------------------*/
int type_check(char* ptoken) {
	int checkt = JSON_VALUE_STRING, ccount = 0, negate = 0;
	int indexf = -1, indexe = -1, indexs = -1;
	/* check negative number */
	if (ptoken[0]=='-'&&is_number(ptoken[1])) {
		negate = 1;
		ccount++;
	}
	/* if possibly a number, run through */
	if (negate||is_number(ptoken[ccount])) {
		checkt = JSON_VALUE_NUMBER;
		while (ptoken[++ccount]) {
			/* check if not a number */
			if (!is_number(ptoken[ccount])) {
				switch (ptoken[ccount]) {
					case '.':
						if (indexf<0) indexf = ccount;
						else checkt = JSON_VALUE_STRING;
						break;
					case 'e':
					case 'E':
						if (indexf>0&&indexe<0) indexe = ccount;
						else checkt = JSON_VALUE_STRING;
						break;
					case '-':
					case '+':
						if (indexe==ccount-1&&indexs<0) indexs = ccount;
						else checkt = JSON_VALUE_STRING;
						break;
				}
				if (checkt==JSON_VALUE_STRING)
					break;
			}
		}
	}
	/* check for boolean if a string */
	if (checkt==JSON_VALUE_STRING) {
		if (!strcasecmp(ptoken,"true")||!strcasecmp(ptoken,"false")||
				!strcasecmp(ptoken,"null"))
			checkt = JSON_VALUE_BOOLEAN;
	}
	return checkt;
}
/*----------------------------------------------------------------------------*/
void json_set_val(my1json_t* json, char* pval) {
	char *buff;
	int size = strlen(pval), type = JSON_VALUE_UNKNOWN;
	if (pval[0]=='"'&&pval[size-1]=='"') {
		pval[size-1] = 0x0; size--;
		pval++; size--;
		type = JSON_VALUE_STRING;
	}
	buff = (char*) realloc((void*)json->pval,++size);
	if (buff) {
		strncpy(buff,pval,size);
		json->pval = buff;
		if (type == JSON_VALUE_UNKNOWN)
			json->type = type_check(pval);
		else json->type = type;
	}
}
/*----------------------------------------------------------------------------*/
void json_set_keyval(my1json_t* json, char* pkey, char* pval) {
	if (pkey) json_set_key(json,pkey);
	if (pval) json_set_val(json,pval);
}
/*----------------------------------------------------------------------------*/
void json_add_object(my1json_t* json, my1json_t* data) {
	/** data should be a malloc'ed my1json_t object */
	data->root = json;
	list_push_data(&json->data,(void*)data);
}
/*----------------------------------------------------------------------------*/
my1json_t* json_add_keyval(my1json_t* json, char* pkey, char* pval) {
	my1json_t *temp = (my1json_t*) malloc(sizeof(my1json_t));
	if (temp) {
		json_init(temp);
		json_set_keyval(temp,pkey,pval);
		json_add_object(json,temp);
	}
	return temp;
}
/*----------------------------------------------------------------------------*/
my1json_t* json_add_array(my1json_t* json, int type) {
	my1json_t *temp = (my1json_t*) malloc(sizeof(my1json_t));
	if (temp) {
		json_init(temp);
		if (type!=JSON_ARRAY&&type!=JSON_ASSOC)
			type = JSON_ASSOC; /* default type */
		temp->type = type;
		json_add_object(json,temp);
	}
	return temp;
}
/*----------------------------------------------------------------------------*/
int json_is_valid(my1json_t* json) {
	my1json_t* that;
	if (json->type&JSON_ARRAY_MASK) {
		/* invalid if value assigned */
		if (json->type&JSON_VALUE_MASK) return 0;
		if (json->pval) return 0;
		/* validate items */
		if (json->data.size) {
			list_scan_prep(&json->data);
			while ((that=(my1json_t*)list_scan_data(&json->data))) {
				/* root property must be set */
				if (that->root!=json) return 0;
				/* JSON_ASSOC items MUST have key assigned */
				if ((json->type&JSON_ASSOC)&&!that->pkey) return 0;
				/* JSON_ARRAY items MUST NOT have key assigned */
				if ((json->type&JSON_ARRAY)&&that->pkey) return 0;
				/* validate that item */
				if (!json_is_valid(that)) return 0;
			}
		}
	}
	else {
		if (json->type&JSON_VALUE_MASK) {
			/* must have value assigned */
			if (!json->pval) return 0;
		}
		else /** json->type==JSON_VALUE_UNKNOWN */ {
			/* must NOT have value assigned */
			if (json->pval) return 0;
		}
		/* should NOT have items */
		if (json->data.size) return 0;
	}
	return 1; /* this json object IS VALID! */
}
/*----------------------------------------------------------------------------*/
int json_find_root(my1json_t* json, my1json_t* root) {
	int test = 0, find = 0;
	while (json->root) {
		test++;
		if (root&&root==json->root) {
			find = test;
			break;
		}
		json = json->root;
	}
	if (root) test = find;
	return test;
}
/*----------------------------------------------------------------------------*/
void json_item_init(my1json_t* json) {
	list_scan_prep(&json->data);
	while (list_scan_data(&json->data))
		json_item_init((my1json_t*)json->data.curr->data);
	/* initialize to first item */
	json->data.curr = json->data.init;
}
/*----------------------------------------------------------------------------*/
my1json_t* json_item_next(my1json_t* json) {
	my1json_t *item = 0x0, *root = json->root;
#if defined(MY1DEBUG) || defined(MY1DEBUG_ITEM)
	fprintf(stdout,"[JSON] Trying (%p=>",json);
	if (json->pkey) fprintf(stdout,"%s:",json->pkey);
	if (json->pval) fprintf(stdout,"%s",json->pval);
	if (json->type&JSON_ARRAY_MASK) {
		fprintf(stdout,"%c%d%c",json->type==JSON_ASSOC?'{':'[',
			json->data.size,json->type==JSON_ASSOC?'}':']');
		fprintf(stdout,"{%p}",json->data.curr?json->data.curr->data:0x0);
	}
	fprintf(stdout,")\n");
#endif
	if (json->type&JSON_ARRAY_MASK) {
		if (json->data.curr) {
			item = (my1json_t*) json->data.curr->data;
			list_scan_item(&json->data);
		}
	}
	while (!item&&root) {
		item = json_item_next(root);
		root = root->root;
	}
	return item;
}
/*----------------------------------------------------------------------------*/
my1json_t* json_item_item(my1json_t* json, my1json_t* prev) {
	my1json_t *item = 0x0;
	if (!prev) json->data.curr = 0x0;
	if (list_scan_item(&json->data))
		item = (my1json_t*) json->data.curr->data;
	return item;
}
/*----------------------------------------------------------------------------*/
my1json_t* json_find_item(my1json_t* json, char* pkey) {
	my1json_t *item, *find = 0x0;
	my1item_t *save = json->data.curr;
	list_scan_prep(&json->data);
	while (list_scan_item(&json->data)) {
		item = (my1json_t*) json->data.curr->data;
		if (item->pkey&&!strncmp(item->pkey,pkey,strlen(item->pkey))) {
			find = item;
			break;
		}
	}
	json->data.curr = save;
	return find;
}
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
/**
 * reader component
 * - expose or separate these?
**/
/*----------------------------------------------------------------------------*/
void json_read_data_init(json_read_data_t* data, my1json_t* json) {
	data->task = TASK_FIND_INIT;
	data->line = 1;
	data->skip = 1;
	data->size = 0;
	data->text = 0;
	/**
	data->test = 0;
	data->flag = 0;
	**/
	data->stat = 0; /* error? */
	data->curr = json;
	data->temp = 0x0;
	data->pbuf = 0x0;
}
/*----------------------------------------------------------------------------*/
my1json_t* json_read_block_init(int type, my1json_t *curr, my1json_t *temp) {
	if (!temp) {
		temp = (my1json_t*) malloc(sizeof(my1json_t));
		json_init(temp);
	}
	if (type==(int)'{') temp->type = JSON_ASSOC;
	else temp->type = JSON_ARRAY;
	json_add_object(curr,temp);
#if defined(MY1DEBUG) || defined(MY1DEBUG_READ)
	fprintf(stderr,"[JSON][%p] Found value (%p=>",curr,temp);
	if (temp->pkey) fprintf(stderr,"%s:",temp->pkey);
	fprintf(stderr,"%s)\n",temp->type==JSON_ASSOC?"{}":"[]");
#endif
	return temp;
}
/*----------------------------------------------------------------------------*/
char json_char_ws(char achar, int* line) {
	switch (achar) {
		case '\n': if (line) (*line)++;
		case '\r': case '\t': case ' ':
			break;
		default: achar = 0x0;
	}
	return achar;
}
/*----------------------------------------------------------------------------*/
int json_read_data_char(json_read_data_t* data, char what) {
	data->buff[data->size] = what;
	/* json_char_ws must run before skip to update line number! */
	if (json_char_ws(what,&data->line)&&data->skip)
		return 1;
	/** select current task to run! */
	if (data->task==TASK_FIND_INIT) {
		if (what=='{') {
			data->curr->type = JSON_ASSOC;
			data->task = TASK_FIND_KEY;
		}
		else if (what=='[') {
			data->curr->type = JSON_ARRAY;
			data->task = TASK_FIND_VALUE;
		}
		else {
			data->stat = data->line*ERROR_LINE_OFFSET + ERROR_JSON_INIT;
			return 0;
		}
#if defined(MY1DEBUG) || defined(MY1DEBUG_READ)
fprintf(stderr,"[INIT][%p:%d] Top level is {%s}\n",
	data->curr,data->task,
	data->curr->type==JSON_ASSOC?"JSON_ASSOC":"JSON_ARAY");
#endif
	}
	else if (data->task==TASK_FIND_KEY) {
		if (!data->size) /* look for first char */ {
			if (what=='}') /* allow empty object */ {
				data->curr = data->curr->root; /* should we check temp? */
				if (data->curr) data->task = TASK_FIND_COMMA;
				else data->task = TASK_FIND_DONE;
			}
			else if (what!='"') /* must be quoted */ {
				data->stat = data->line*ERROR_LINE_OFFSET + ERROR_JSON_FIND_KEY;
				return 0;
			}
			/** parse settings - accumulate, do not skip whitespace! */
			data->size++; data->skip = 0;
			return 1;
		}
		/* look for ending '"' */
		if (what=='"') {
			data->buff[++data->size] = 0x0;
			if (data->temp) {
				data->stat = data->line*ERROR_LINE_OFFSET + ERROR_JSON_TEMP;
				return 0;
			}
			/* create new item */
			data->temp = (my1json_t*) malloc(sizeof(my1json_t));
			json_init(data->temp);
			json_set_key(data->temp,data->buff);
#if defined(MY1DEBUG) || defined(MY1DEBUG_READ)
fprintf(stderr,"[JSON][%p] Found key {%p=>%s}\n",
	data->curr,data->temp,data->temp->pkey);
#endif
			/** parse settings - new buffer, skip whitespace! */
			data->size = 0; data->skip = 1;
			data->task = TASK_FIND_COLON;
		}
		else {
			data->size++;
			if (data->size==BUFF_SIZE-1) {
				data->buff[BUFF_SIZE-1] = 0x0;
				data->stat = data->line*ERROR_LINE_OFFSET + ERROR_JSON_BUFF;
				return 0;
			}
		}
	}
	else if (data->task==TASK_FIND_COLON) {
		if (what!=':') /* first non-ws must be colon! */ {
			data->stat = data->line*ERROR_LINE_OFFSET + ERROR_JSON_FIND_COLON;
			return 0;
		}
#if defined(MY1DEBUG) || defined(MY1DEBUG_READ)
fprintf(stderr,"[JSON][%p] Found ':' {Key:%p=>%s}\n",data->curr,
	data->temp,data->temp->pkey);
#endif
		data->task = TASK_FIND_VALUE;
	}
	else if (data->task==TASK_FIND_VALUE) {
		if (!data->size) /* look for first char */ {
			if (what=='}'||what==']') {
				 /* allow empty object */
				if ((what=='}'&&data->curr->type!=JSON_ASSOC)||
						(what==']'&&data->curr->type!=JSON_ARRAY)) {
					data->stat =
						data->line*ERROR_LINE_OFFSET + ERROR_JSON_SYNTAX;
					return 0;
				}
				data->curr = data->curr->root;
				if (data->curr) data->task = TASK_FIND_COMMA;
				else data->task = TASK_FIND_DONE;
			}
			else if (what=='{'||what=='[') {
				data->curr = json_read_block_init((int)what,
					data->curr,data->temp);
				if (data->curr->type==JSON_ASSOC) data->task = TASK_FIND_KEY;
				else data->task = TASK_FIND_VALUE;
				data->temp = 0x0;
			}
			else {
				/* found potential value? - no more skipping ws! */
				data->size++;
				data->skip = 0;
			}
			return 1;
		}
		/* is it closing text quote? */
		if (what=='"') {
			if (data->buff[0]!='"') {
				data->stat = data->line*ERROR_LINE_OFFSET + ERROR_JSON_SYNTAX;
				return 0;
			}
			data->buff[++data->size] = 0x0;
			/** check if extended text! */
			if (data->text) {
				data->text = data->text + data->size - 1;
				data->pbuf = (char*) realloc(data->pbuf,data->text);
				strcat(data->pbuf,&data->buff[1]);
			}
			else data->pbuf = data->buff;
			if (!data->temp) {
				data->temp = (my1json_t*) malloc(sizeof(my1json_t));
				json_init(data->temp);
			}
			/* assign value */
			json_set_val(data->temp,data->pbuf);
			json_add_object(data->curr,data->temp);
#if defined(MY1DEBUG) || defined(MY1DEBUG_READ)
fprintf(stderr,"[JSON][%p:%d] (%p:%s) Found value {string:%s}\n",
	data->curr,data->task,data->temp,
	data->temp->pkey?data->temp->pkey:"<no-key>",data->temp->pval);
#endif
			/* just reset, find next item */
			data->temp = 0x0;
			/* remove buffer for extended text */
			if (data->text) {
				free((void*)data->pbuf);
				data->text = 0;
			}
			data->pbuf = 0x0;
			/* next? */
			data->size = 0; data->skip = 1;
			data->task = TASK_FIND_COMMA;
		}
		else {
			if (data->buff[0]!='"') {
				if (json_char_ws(what,0x0)) {
					data->buff[data->size] = 0x0;
					if (!data->temp) {
						data->temp = (my1json_t*) malloc(sizeof(my1json_t));
						json_init(data->temp);
					}
					/* assign value */
					json_set_val(data->temp,data->buff);
					json_add_object(data->curr,data->temp);
#if defined(MY1DEBUG) || defined(MY1DEBUG_READ)
fprintf(stderr,"[JSON][%p:%d] (%p:%s) Found value {spaced:%s}\n",
	data->curr,data->task,data->temp,
	data->temp->pkey?data->temp->pkey:"<no-key>",data->temp->pval);
#endif
					/* just reset, find next item */
					data->temp = 0x0;
					/* next? */
					data->size = 0; data->skip = 1;
					data->task = TASK_FIND_COMMA;
				}
				else if (what=='}'||what==']') {
					 /* check mismatched symbol */
					if ((what=='}'&&data->curr->type!=JSON_ASSOC)||
							(what==']'&&data->curr->type!=JSON_ARRAY)) {
						data->stat =
							data->line*ERROR_LINE_OFFSET + ERROR_JSON_SYNTAX;
						return 0;
					}
					data->buff[data->size] = 0x0;
					if (!data->temp) {
						data->temp = (my1json_t*) malloc(sizeof(my1json_t));
						json_init(data->temp);
					}
					/* assign value */
					json_set_val(data->temp,data->buff);
					json_add_object(data->curr,data->temp);
#if defined(MY1DEBUG) || defined(MY1DEBUG_READ)
fprintf(stderr,"[JSON][%p:%d] (%p:%s) Found value {ending:%s}\n",
	data->curr,data->task,data->temp,
	data->temp->pkey?data->temp->pkey:"<no-key>",data->temp->pval);
#endif
					data->temp = 0x0;
					/* next? */
					data->size = 0; data->skip = 1;
					data->curr = data->curr->root;
					if (data->curr) data->task = TASK_FIND_COMMA;
					else data->task = TASK_FIND_DONE;
				}
				else if (what==',') {
					data->buff[data->size] = 0x0;
					if (!data->temp) {
						data->temp = (my1json_t*) malloc(sizeof(my1json_t));
						json_init(data->temp);
					}
					/* assign value */
					json_set_val(data->temp,data->buff);
					json_add_object(data->curr,data->temp);
#if defined(MY1DEBUG) || defined(MY1DEBUG_READ)
fprintf(stderr,"[JSON][%p:%d] (%p:%s) Found value {comma:%s}\n",
	data->curr,data->task,data->temp,
	data->temp->pkey?data->temp->pkey:"<no-key>",data->temp->pval);
#endif
					data->temp = 0x0;
					/* next? */
					data->size = 0; data->skip = 1;
					if (data->curr->type==JSON_ASSOC)
						data->task = TASK_FIND_KEY;
					else data->task = TASK_FIND_VALUE;
				}
				else {
					data->size++;
					if (data->size==BUFF_SIZE-1) {
						data->buff[BUFF_SIZE-1] = 0x0;
						data->stat =
							data->line*ERROR_LINE_OFFSET + ERROR_JSON_BUFF;
						return 0;
					}
				}
			}
			else {
				data->size++;
				if (data->size==BUFF_SIZE-1) {
					data->buff[BUFF_SIZE-1] = 0x0;
					/** allow long quoted strings */
					if (data->text) {
						data->text = data->text + data->size - 1;
						data->pbuf = (char*) realloc(data->pbuf,data->text);
						strcat(data->pbuf,&data->buff[1]);
					}
					else {
						data->text = data->size + 1;
						data->pbuf = (char*) malloc(data->text);
						strcpy(data->pbuf,data->buff);
						data->pbuf[data->size] = 0x0;
					}
					data->size = 1;
				}
			}
		}
	}
	else if (data->task==TASK_FIND_COMMA) {
		if (what== '}'||what== ']') {
			if ((what=='}'&&data->curr->type!=JSON_ASSOC)||
					(what==']'&&data->curr->type!=JSON_ARRAY)) {
				data->stat = data->line*ERROR_LINE_OFFSET + ERROR_JSON_SYNTAX;
				return 0;
			}
			data->curr = data->curr->root;
			if (data->curr) data->task = TASK_FIND_COMMA;
			else data->task = TASK_FIND_DONE;
			return 1;
		}
		else if (what!=',') {
			data->stat = data->line*ERROR_LINE_OFFSET + ERROR_JSON_OCOM;
			return 0;
		}
		/* found comma! next object */
		data->size = 0; data->skip = 1;
		if (data->curr->type==JSON_ASSOC) data->task = TASK_FIND_KEY;
		else data->task = TASK_FIND_VALUE;
	}
	else if (data->task==TASK_FIND_DONE) {
		data->stat = data->line*ERROR_LINE_OFFSET + ERROR_JSON_SYNTAX;
		return 0;
	}
	else {
		data->stat = data->line*ERROR_LINE_OFFSET + ERROR_JSON_TASK;
		return 0;
	}
	return 1;
}
/*----------------------------------------------------------------------------*/
void fill_str_char(char** conv, int* size, int that) {
	int temp = *size;
	char *pstr = *conv;
	int make = temp+1;
	if (!temp) make++;
	pstr = (char*) realloc((void*)pstr,make);
	if (pstr) {
		if (temp) temp--;
		pstr[temp++] = (char)that;
		pstr[temp++] = 0x0;
		*conv = pstr;
		*size = make; /* size includes null */
	}
}
/*----------------------------------------------------------------------------*/
void fill_str_that(char** conv, int* size, char* that) {
	int temp = *size;
	char *pstr = *conv;
	int test = strlen(that);
	int make = temp+test;
	if (!temp) make++;
	pstr = (char*) realloc((void*)pstr,make);
	if (pstr) {
		strncat(pstr,that,test);
		*conv = pstr;
		*size = make;
	}
}
/*----------------------------------------------------------------------------*/
void json_make_str_this(my1json_t* json, char** conv, int* size) {
	if (json->pkey) {
		fill_str_char(conv,size,(int)'"');
		fill_str_that(conv,size,json->pkey);
		fill_str_that(conv,size,"\":");
	}
	if (json->pval) {
		if (json->type==JSON_VALUE_STRING)
			fill_str_char(conv,size,(int)'"');
		fill_str_that(conv,size,json->pval);
		if (json->type==JSON_VALUE_STRING)
			fill_str_char(conv,size,(int)'"');
	}
}
/*----------------------------------------------------------------------------*/
void json_make_str_that(my1json_t* json, char** conv, int* size) {
	json_make_str_this(json,conv,size);
	if (json->data.size) {
		void* temp = json->data.curr;
		if (json->type&JSON_ARRAY_MASK) {
			if (json->type==JSON_ASSOC) fill_str_char(conv,size,(int)'{');
			else fill_str_char(conv,size,(int)'[');
		}
		list_scan_prep(&json->data);
		while (list_scan_item(&json->data)) {
			my1json_t* that = (my1json_t*) json->data.curr->data;
			json_make_str_that(that,conv,size);
			if (json->data.curr->next)
				fill_str_char(conv,size,(int)',');
		}
		json->data.curr = temp;
		if (json->type&JSON_ARRAY_MASK) {
			if (json->type==JSON_ASSOC) fill_str_char(conv,size,(int)'}');
			else fill_str_char(conv,size,(int)']');
		}
	}
	else if (!json->pval) /* still indicate empty array */ {
		if (json->type==JSON_ASSOC) fill_str_that(conv,size,"{}");
		else fill_str_that(conv,size,"[]");
	}
}
/*----------------------------------------------------------------------------*/
char* json_make_str(my1json_t* json) {
	char* pstr = 0x0;
	int size = 0;
	json_make_str_that(json,&pstr,&size);
	return pstr;
}
/*----------------------------------------------------------------------------*/
char* json_from_str(my1json_t* json, char* conv) {
	json_read_data_t data;
	json_read_data_init(&data,json);
	while (*conv) {
#ifdef MY1DEBUG
fprintf(stderr,"[CONV] '%c' [%d]\n",*conv,data.size);
#endif
		if (!json_read_data_char(&data,*conv))
			break;
		conv++;
	}
	if (data.temp) {
		json_free(data.temp);
		free((void*)data.temp);
	}
	if (data.stat) {
		json_free(json);
		return 0x0;
	}
	return conv;
}
/*----------------------------------------------------------------------------*/
#if defined(TEST_MODULE) && defined(TEST_JSON)
/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include "my1list.c"
#include "my1list_data.c"
#include "my1cstr.c"
#include "my1cstr_util.c"
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	my1json_t data;
	my1cstr_t buff;
	char* pstr;
	int loop;
	cstr_init(&buff);
	json_init(&data);
	for (loop=1;loop<argc;loop++) {
		cstr_addcol(&buff,argv[loop],MY1CSTR_PREFIX_SPACING);
	}
	if (buff.fill>0) {
		printf("@@ Data:'%s'!\n",buff.buff);
		fflush(stdout);
		if (!json_from_str(&data,buff.buff))
			printf("** Cannot read JSON string:'%s'!\n",buff.buff);
		else {
			if (!json_is_valid(&data))
				printf("[WARN] Invalid json!\n");
			pstr = json_make_str(&data);
			if (pstr) {
				printf("[JSON]Begins...\n%s",pstr);
				printf("\n[JSON]Ends...\n");
				free((void*)pstr);
			}
		}
	}
	json_free(&data);
	cstr_free(&buff);
	return 0;
}
/*----------------------------------------------------------------------------*/
#endif
/*----------------------------------------------------------------------------*/
